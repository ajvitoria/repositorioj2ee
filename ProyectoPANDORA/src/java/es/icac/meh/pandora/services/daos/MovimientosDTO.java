/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.icac.meh.pandora.services.daos;

import es.icac.meh.pandora.modelo.Apunte;
import es.icac.meh.pandora.services.ConnectionService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MAJIVIAL
 */
public class MovimientosDTO {

    public RecaudacionesDAO getRecaudacionesDAO() {
        return recaudacionesDAO;
    }

    public void setRecaudacionesDAO(RecaudacionesDAO recaudacionesDAO) {
        this.recaudacionesDAO = recaudacionesDAO;
    }

    public TrabajosRealizadosDAO getTrabajosDAO() {
        return trabajosDAO;
    }

    public void setTrabajosDAO(TrabajosRealizadosDAO trabajosDAO) {
        this.trabajosDAO = trabajosDAO;
    }

    public GestorApuntesServiceDAO getApuntesDAO() {
        return apuntesDAO;
    }

    public void setApuntesDAO(GestorApuntesServiceDAO apuntesDAO) {
        this.apuntesDAO = apuntesDAO;
    }

    public ArrayList<Apunte> getListaApuntes() {
        return listaApuntes;
    }

    public void setListaApuntes(ArrayList<Apunte> listaApuntes) {
        this.listaApuntes = listaApuntes;
    }

    private RecaudacionesDAO recaudacionesDAO;
    private TrabajosRealizadosDAO trabajosDAO;
    private GestorApuntesServiceDAO apuntesDAO;
    
    private static final Logger LOG = Logger.getLogger(MovimientosDTO.class.getName());
  
   
    ArrayList<Apunte> listaApuntes;
    
    public MovimientosDTO(ConnectionService connection){

            this.recaudacionesDAO=new RecaudacionesDAO(connection);
            this.trabajosDAO=new TrabajosRealizadosDAO(connection);
            this.apuntesDAO= new GestorApuntesServiceDAO(connection);   
            this.listaApuntes=new ArrayList<>();
    }   
    
    public void reseteaApuntes(){
          this.listaApuntes=new ArrayList<>();
    }
    public boolean borrarApuntesEjercicio(String ejercicio) {
        try {
            return apuntesDAO.borrarApuntesByEjercicio(ejercicio);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Excepcion al BORRAR LOS APUNTES previos!", ex);
            return false;
        }
    

    }
    public int insertarTrabajos(String ejercicio) {
                Collection<? extends Apunte> listaTrabajos = null;
        try {
            listaTrabajos = trabajosDAO.obtenerTrabajosEjercicio(ejercicio);
            listaApuntes.addAll(listaTrabajos);            
        } catch (SQLException ex) {
               LOG.log(Level.SEVERE, "Excepcion al extraer las trabajos para convertir en apuntes", ex);
        }

      
            return apuntesDAO.insertarApuntes(listaApuntes, ejercicio);
                
    }

    public int insertarRecaudaciones(String ejercicio) {
        int retorno=0;
        Collection<? extends Apunte> listaRecaudaciones = null;
        try {
            listaRecaudaciones = recaudacionesDAO.obtenerRecaudacionesByEjercicio(ejercicio);
            listaApuntes.addAll(listaRecaudaciones);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Excepcion al extraer las recaudaciones para convertir en apuntes", ex);
        }

        try {
            retorno=apuntesDAO.insertarApuntes(listaApuntes, ejercicio);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Excepcion al insertar las recaudaciones en los apuntes", ex);
        }
        return retorno;
    }
}
