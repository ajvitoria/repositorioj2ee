package es.icac.meh.pandora.services.impl;

import app.ProbandoCruceRecaudacionsTrabajosDAO;
import es.icac.meh.pandora.exportacion.ExportarTrabajos;
import es.icac.meh.pandora.modelo.Apunte;

import es.icac.meh.pandora.services.daos.TrabajosRealizadosDAO;
import es.icac.meh.pandora.services.daos.RecaudacionesDAO;
import es.icac.meh.pandora.services.daos.TrabajosDTO;
import es.icac.meh.pandora.services.daos.GestorApuntesServiceDAO;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collection;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;



        
public class GeneracionCrucesServicioImpl {

    private static final Logger LOG = Logger.getLogger(GeneracionCrucesServicioImpl.class.getName());
    private TrabajosDTO dto;
    private TrabajosRealizadosDAO servicioTrabajosDAO;
    private RecaudacionesDAO servicioRecaudacionsDAO;
    private  ArrayList<Apunte> diario = new ArrayList();
    private Preprocesador preprocesados;
    private GestorApuntesServiceDAO servicioApuntesDAO;
    
    private GeneracionCrucesServicioImpl() {
    }
    
    public GeneracionCrucesServicioImpl(TrabajosRealizadosDAO servicioTrabajosDAO, RecaudacionesDAO servicioRecaudacionsDAO, TrabajosDTO dto, GestorApuntesServiceDAO servicioApuntesDAO) {
        this.dto = dto;
        this.servicioTrabajosDAO = servicioTrabajosDAO;
        this.servicioRecaudacionsDAO = servicioRecaudacionsDAO;
        this.servicioApuntesDAO = servicioApuntesDAO;
     //   dto.importarTrabajosAPartirEjerciciosConfiguracion();
    }

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("config/applicationContext.xml");
        System.out.println("=====LISTA DE BEANS CARGADOS=====");
        for (String tmp : applicationContext.getBeanDefinitionNames()) {
            System.out.println(" " + tmp);
        }
        GeneracionCrucesServicioImpl servicio = applicationContext.getBean("servicioCruces", GeneracionCrucesServicioImpl.class);
        servicio = applicationContext.getBean("servicioCruces", GeneracionCrucesServicioImpl.class);

        servicio.procesarCruces("2019", true);
        long finTime = System.currentTimeMillis();

        System.out.println("TIEMPO DEL PROCESAMIENTO SEGUNDOS:" + (finTime - startTime) / 1000);
    }
    
//revisar para ver si es esta usando
    public void procesarCruce(String anyo) {
        long startTime = System.currentTimeMillis();

        procesarCruces(anyo, true);
        long finTime = System.currentTimeMillis();

        System.out.println("TIEMPO DEL PROCESAMIENTO SEGUNDOS:"+(finTime-startTime)/1000);
    }

//este es el que se debe ejecutar
    public boolean procesarCruces(String pEjer, boolean regenerar) {

       
        
        Runnable extractorRecaudaciones = new Runnable() {
            public boolean realizadaExtraccion = false;
            @Override
            public void run() {
                try {
                    Collection<? extends Apunte> listaRecaudacions = null;
                    listaRecaudacions = servicioRecaudacionsDAO.obtenerRecaudacionesByEjercicio(pEjer);
                    diario.addAll(listaRecaudacions);
                    realizadaExtraccion = true;   
                    
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, "fallo al generar los cruces del periodo" + pEjer, ex);
                    realizadaExtraccion = false;
                }
            };
            public boolean haFinalizado(){
                return realizadaExtraccion;
            }
        };
    //  Thread hilo1=new Thread(extractorTrabajos);   
      Thread hilo2=new Thread(extractorRecaudaciones);
      
  //    hilo1.start();
      hilo2.start();
      
//      while(hilo1.isAlive() || hilo2.isAlive()){
//            try {
//                Thread.sleep(5000);
//                           
//                     LOG.log(Level.INFO, "Esperando a que termine {0}", new java.util.Date().toString());
//            } catch (InterruptedException ex) {
//                Logger.getLogger(GeneracionCrucesServicioImpl.class.getName()).log(Level.SEVERE, null, ex);
//            }
//          
//      }
        try {
            this.servicioApuntesDAO.insertarApuntes(diario, pEjer);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "error insertado las cRuces", ex);
        }
        return true;
    }

    public TrabajosDTO getDto() {
        return dto;
    }

    public void setDto(TrabajosDTO dto) {
        this.dto = dto;
    }

    public TrabajosRealizadosDAO getServicioTrabajosDAO() {
        return servicioTrabajosDAO;
    }

    public void setServicioTrabajosDAO(TrabajosRealizadosDAO servicioTrabajosDAO) {
        this.servicioTrabajosDAO = servicioTrabajosDAO;
    }

    public RecaudacionesDAO getServicioRecaudacionsDAO() {
        return servicioRecaudacionsDAO;
    }

    public void setServicioRecaudacionsDAO(RecaudacionesDAO servicioRecaudacionsDAO) {
        this.servicioRecaudacionsDAO = servicioRecaudacionsDAO;
    }
    
    //este no es es el que se llama desde el servlet
//    public void generarCruces(String paramEjercicio){
//       //    ArrayList<Apunte> diario=new ArrayList();
//
//       LOG.log(Level.INFO,"=====LISTA DE BEANS CARGADOS=====");
//
//
//   //  dto.importarTrabajosAPartirEjerciciosConfiguracion();
//
//
//        Collection<? extends Apunte> listaTrabajos=null;
//        try {
//            listaTrabajos = servicioTrabajosDAO.obtenerTrabajosEjercicio(paramEjercicio);
//        } catch (SQLException ex) {
//               LOG.log(Level.SEVERE, "ERROR EN EL DAO DE TRABAJOS EN EL GESTOR DE CRUCES", ex);
//        }
//
//       diario.addAll(listaTrabajos);
// Collection<? extends Apunte> listaRecaudacions=null;
//        try {
//            listaRecaudacions = servicioRecaudacionsDAO.obtenerRecaudacionesByEjercicio(paramEjercicio);
//        } catch (SQLException ex) {
//           LOG.log(Level.SEVERE, "ERROR EN EL DAO DE COBROS EN EL GESTOR DE CRUCES", ex);
//        }
//     diario.addAll(listaRecaudacions);
//       
//     /*
//ExportarTrabajos.generarExcel("c:\\javam\\listaApuntes.xlsx", diario);
//System.out.println("realizado el trabajo");
//
//       
//
//        
//
//
//
//       
//               
//        double saldo=0;
//        double saldoH=0;
//        double saldoD=0;
//        
//        for (Apunte tmp : diario) {
//            System.out.print("\n" + tmp.getFecha() + "\t" + tmp.getDenominacion() + "\t" + tmp.getRoac());
//
//            if (tmp.esDebe()) {
//                System.out.print("\t" + tmp.getImporteString());
//                saldoD +=tmp.getImporte();
//                saldo +=tmp.getImporte();
//            } else {
//                System.out.print("\t\t\t" + tmp.getImporteString());
//                saldoH += tmp.getImporte();
//                saldo -=tmp.getImporte();
//            }
//        }
//        System.out.println("\n*************TOTALES**************\t"+saldoD+"\t"+saldoH);
//        System.out.println("\n**************SALDO***************\t"+saldo); 
//
//*/
//    }
//        
//      /*       
//    public void generarTodos(){
//       //    ArrayList<Apunte> diario=new ArrayList();
//
//       LOG.log(Level.INFO,"=====LISTA DE BEANS CARGADOS=====");
//
//
//   //  dto.importarTrabajosAPartirEjerciciosConfiguracion();
//
//
//        Collection<? extends Apunte> listaTrabajos=null;
//        try {
//            listaTrabajos = servicioTrabajosDAO.obtenerTrabajos();
//        } catch (SQLException ex) {
//            Logger.getLogger(ProbandoCruceRecaudacionsTrabajosDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//       diario.addAll(listaTrabajos);
// Collection<? extends Apunte> listaRecaudacions=null;
//      
//            listaRecaudacions = servicioRecaudacionsDAO.obtenerTodosRecaudacions();
//       
//     diario.addAll(listaRecaudacions);
//       
//
//ExportarTrabajos.generarExcel("c:\\javam\\listaApuntes.xlsx", diario);
//System.out.println("realizado el trabajo");
//
//        
//        double saldo=0;
//        double saldoH=0;
//        double saldoD=0;
//        
//        for (Apunte tmp : diario) {
//            System.out.print("\n" + tmp.getFecha() + "\t" + tmp.getDenominacion() + "\t" + tmp.getRoac());
//
//            if (tmp.esDebe()) {
//                System.out.print("\t" + tmp.getImporteString());
//                saldoD +=tmp.getImporte();
//                saldo +=tmp.getImporte();
//            } else {
//                System.out.print("\t\t\t" + tmp.getImporteString());
//                saldoH += tmp.getImporte();
//                saldo -=tmp.getImporte();
//            }
//        }
//        System.out.println("\n*************TOTALES**************\t"+saldoD+"\t"+saldoH);
//        System.out.println("\n**************SALDO***************\t"+saldo); 
//
//
//    }
//*/
//    //CREO QUE NO SE ESTA USANDO
//    public void generarkk() {
//                   
//        diario = new ArrayList();
//
//        Collection<? extends Apunte> listaTrabajos = null;
//        try {
//            listaTrabajos = servicioTrabajosDAO.obtenerTrabajos();
//        } catch (SQLException ex) {
//            Logger.getLogger(GeneracionCrucesServicioImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        diario.addAll(listaTrabajos);
//        Collection<? extends Apunte> listaRecaudacions = null;
//        
//            listaRecaudacions = servicioRecaudacionsDAO.obtenerTodosRecaudacions();
//      
//
//        
//        diario.addAll(listaRecaudacions);
//
//        ExportarTrabajos.generarExcel("c:\\javam\\listaApuntes.xlsx", diario);
//        System.out.println("realizado el trabajo");
//
//        double saldo = 0;
//        double saldoH = 0;
//        double saldoD = 0;
//
//        for (Apunte tmp : diario) {
//            System.out.print("\n" + tmp.getFecha() + "\t" + tmp.getDenominacion() + "\t" + tmp.getRoac());
//
//            if (tmp.esDebe()) {
//                System.out.print("\t" + tmp.getImporteString());
//                saldoD += tmp.getImporte();
//                saldo += tmp.getImporte();
//            } else {
//                System.out.print("\t\t\t" + tmp.getImporteString());
//                saldoH += tmp.getImporte();
//                saldo -= tmp.getImporte();
//            }
//        }
//        System.out.println("\n*************TOTALES**************\t" + saldoD + "\t" + saldoH);
//        System.out.println("\n**************SALDO***************\t" + saldo);
//    }
//    
    
     public boolean generarExcelApuntes(){
         try{
            ExportarTrabajos.generarExcel("c:\\javam\\listaApuntes.xlsx", diario);
            System.out.println("realizado el trabajo");
         }catch(Exception ex){
             Logger.getLogger(GeneracionCrucesServicioImpl.class.getName()).log(Level.SEVERE, "fallo al generar el excel!!", ex);
             return false;
         }
         return true;
     }

    public ArrayList<Apunte> getDiario() {
        return diario;
    }

    public void setDiario(ArrayList<Apunte> diario) {
        this.diario = diario;
    }

    public Preprocesador getPreprocesados() {
        return preprocesados;
    }

    public void setPreprocesados(Preprocesador preprocesados) {
        this.preprocesados = preprocesados;
    }
}
