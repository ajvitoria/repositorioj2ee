
package es.icac.meh.pandora.services.daos;

import es.icac.meh.pandora.modelo.Apunte;
import es.icac.meh.pandora.modelo.ApunteImpl;
import es.icac.meh.pandora.services.ConnectionService;
import es.icac.meh.pandora.utils.JDBCUtilities;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

import java.util.logging.Level;
import java.util.logging.Logger;


public class GestorApuntesServiceDAO {

    private ConnectionService connection;
    private final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");
    private SimpleDateFormat SDF_SQL=new SimpleDateFormat("yyyy-MM-dd");
                             
    SimpleDateFormat SDF_LOC = new java.text.SimpleDateFormat("dd/MM/yyyy");
    
    private static final Logger LOG = Logger.getLogger(GestorApuntesServiceDAO.class.getName());


    public GestorApuntesServiceDAO(ConnectionService connection) {
        this.connection = connection;

    }

    public void cambiarApunteDeEjercicio(Apunte apunte, String nuevoEjercicioCruce) throws SQLException {
        String sqlPreparada = "UPDATE T_APUNTES "
                + "SET "
                + " roac=?,"
                + " fecha=?,"
                + " nifAuditor=?,"
                + " nombreAuditor=?,"
                + " ejercicio=?,"
                + " esdebe=?,"
                + " descripcion=?,"
                + " importe=?"
                + " WHERE roac=? AND fecha=? AND ejercicio=? AND esdebe=?";

        PreparedStatement pstmt = connection.getConnection().prepareStatement(sqlPreparada);

        String roac = apunte.getRoac();
        
        Date fecha = new Date(apunte.getFecha().getTime());
        
        String nifAuditor=apunte.getNifAuditor();
         String nombreAuditor=apunte.getNombreAuditor();
        int ejercicioApunte = apunte.getEjercicio();
        boolean esDebe = apunte.esDebe();

        //PARA ACTUALIZARLO
        String descripcion = apunte.getDenominacion();
        double importe = apunte.getImporte();
        pstmt.setString(1, roac);
        pstmt.setDate(2, fecha);
        pstmt.setString(3, nifAuditor);
        pstmt.setString(4, nombreAuditor);
        pstmt.setInt(3, ejercicioApunte);
        pstmt.setBoolean(4, esDebe);
        pstmt.setString(5, descripcion);
        pstmt.setDouble(6, importe);

        //campos del where
        pstmt.setString(7, roac);
        pstmt.setDate(8, fecha);
        pstmt.setInt(9, ejercicioApunte);
        pstmt.setBoolean(10, esDebe);

        boolean rs = pstmt.execute();

        if (rs) {
            Logger.getLogger(GestorApuntesServiceDAO.class.getName()).log(Level.SEVERE, "he actulizado un apunte que ya estaba");
        } else {
            throw new SQLException();
        }

    }
public ArrayList<ApunteImpl> obtenerApuntes(String ejercicio) throws SQLException {

        String queryVER = "select "
                + "      [roac]\n"
                + "      ,[fecha]\n"
                + "      ,[nifAuditor]\n"
                + "      ,[nombreAuditor]\n"
                + "      ,[ejercicioApunte]\n"
                + "      ,[trimestreApunte]\n"
                + "      ,[esDebe]\n"
                + "      ,[descripcion]\n"
                + "      ,[importe]\n"
                + "      ,[ejercicioCruce]\n"
                + "      ,[fechaPag] \n"
                + "from T_APUNTES "
                + "WHERE ejercicioCruce=" + ejercicio
                + " order by roac, fecha; ";

        System.out.println("kk_>SQL=\n" + queryVER);
        ArrayList<ApunteImpl> apuntes;
        
        ResultSet rs=null;
        try (Statement pstmt = connection.getConnection().createStatement()) {
            rs = pstmt.executeQuery(queryVER);
            apuntes = new ArrayList<>();
            if (rs != null) {
                while (rs.next()) {
                    String codRoac = rs.getString("roac");
                    Date fechaApunte = rs.getDate("fecha");//YYYY-mm-dd
                    String nifAuditor = rs.getString("nifAuditor");//YYYY-mm-dd
                    String nombreAuditor = rs.getString("nombreAuditor");//YYYY-mm-dd

                    int ejercicioApunte = rs.getInt("ejercicioApunte");
                    int trimestreApunte = rs.getInt("trimestreApunte");
                    boolean esDebe = rs.getBoolean("esDebe");

                    String descripcion = rs.getString("descripcion");
                    int ejercicioCruce = rs.getInt("ejercicioCruce");
                    Date fechaPag = rs.getDate("fechaPag");
                    
                    double importe = rs.getDouble("importe");

                   
                    ApunteImpl apunte = null;
                    try {
                        apunte = new ApunteImpl();
                        if (codRoac==null)
                            codRoac="desconocido";
                        
                        apunte.setRoac(codRoac);
                        apunte.setFecha(fechaApunte);
                        apunte.setNifAuditor(nifAuditor);
                        apunte.setNombreAuditor(nombreAuditor);

                        apunte.setTrimestre(trimestreApunte);
                        
                        apunte.setEjercicio(ejercicioApunte);
                        apunte.setAnyo(ejercicioCruce);
                        
                        apunte.setFechaPag(fechaPag);
                        apunte.setImporte(importe);
                        apunte.setDenominacion(descripcion);

                        apunte.setEsDebe(esDebe);
                        apuntes.add(apunte);

                    } catch (Exception creandoTrabajoException) {
                        LOG.log(Level.SEVERE, "==============================================");
                        LOG.log(Level.SEVERE, "ERROR CREANDO APUNTES " + codRoac, creandoTrabajoException);
                        LOG.log(Level.SEVERE, "==============================================");
                        LOG.log(Level.SEVERE, "APUNTES CONSTRUIDO HASTA EXCEPCION: ");
                        LOG.log(Level.SEVERE, apunte != null ? apunte.toString() : "NULL");
                        LOG.log(Level.SEVERE, "==============================================");
                    }
                }
            } else {
                LOG.log(Level.WARNING, "Intentando obtener TrabajosRealizados desde una ResultSet y este es NULL");
            }
        } finally {
            if (rs != null) {
                rs.close();                
            }         
            
        }
        return apuntes;
    }
    public int insertarApuntes(ArrayList<Apunte> apuntes, String ejercicioCruce) {
   Apunte posibleApunte=null;
   int numeroApuntes=0;
   
               String sqlInsert = "INSERT INTO T_APUNTES("
                    +"roac,fecha,nifAuditor, "
                    +"nombreAuditor,ejercicioApunte, trimestreApunte, "
                    +"esDebe,descripcion,importe, "
                    +"ejercicioCruce, fechaPag) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pstmtInsert = null;
            
            
        try {
            int iEjercicioCruce = Integer.parseInt(ejercicioCruce);

            pstmtInsert = connection.getConnection().prepareStatement(sqlInsert);
            String roac = null;
            java.sql.Date fecha,fechaPlazo = null;
            String nifAuditor= null;
            String nombreAuditor=null;
            int ejercicioApunte = 0;
            int trimestreApunte = 0;
            boolean esDebe = false;
            //PARA ACTUALIZARLO
            String descripcion = null;
            double importe = 0d;
            
         
            
            for (Apunte apunte : apuntes) {
                
                posibleApunte=apunte;
                roac = apunte.getRoac();
                fecha = new java.sql.Date(apunte.getFecha().getTime());
                fechaPlazo = new java.sql.Date(apunte.getFechaPago().getTime());
                nifAuditor = apunte.getNifAuditor();
                nombreAuditor = apunte.getNombreAuditor();
                
                ejercicioApunte = apunte.getEjercicio();
                trimestreApunte = apunte.getTrimestre();
                esDebe = apunte.esDebe();
                // fechaPag = apunte.g(apunte);
                //PARA ACTUALIZARLO
                descripcion = apunte.getDenominacion();
                importe = apunte.getImporte();
                
                pstmtInsert.setString(1, roac);
                pstmtInsert.setDate(2,fechaPlazo );
                pstmtInsert.setString(3, nifAuditor);
                pstmtInsert.setString(4, nombreAuditor);
                pstmtInsert.setInt(5, ejercicioApunte);
                pstmtInsert.setInt(6, trimestreApunte);
                pstmtInsert.setBoolean(7, esDebe);
                pstmtInsert.setString(8, descripcion);
                pstmtInsert.setDouble(9, importe);
                pstmtInsert.setInt(10, iEjercicioCruce);
                pstmtInsert.setDate(11, fecha);
                
                
                numeroApuntes+=pstmtInsert.executeUpdate(); //quitar cuando funcione
                //cuando funcione. pstmtInsert.addBatch();
                
            }   
            
           // int[] affectedRecords = pstmtInsert.executeBatch();
            //LOG.log(Level.FINE, "insertados : ", affectedRecords);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "ERROR INSERTANDO EL APUNTES: ", ex);
            LOG.log(Level.SEVERE, "APUNTE QUE SE HA TRATADO DE INSERTAR: "+posibleApunte);
       try {
           JDBCUtilities.printSqlStatement(pstmtInsert, sqlInsert);
       } catch (SQLException ex1) {
           LOG.log(Level.SEVERE, "ERROR IMPRIMIENDO QUERY DE LA PREPARED", ex1);
       }
        }
return numeroApuntes;
    }

    private long transformar(String fecha) {
        java.util.Date date = null;
        try {
            date = SDF.parse(fecha);
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return date.getTime();

    }

    /*
    
    CREATE TABLE T_APUNTES (
            id INT NOT NULL IDENTITY PRIMARY KEY,
            roac VARCHAR(45),
            fecha datetime,
            nifAuditor VARCHAR(35),
            nombreAuditor VARCHAR(145),
	    ejercicioApunte INT,
            trimestreApunte INT,
            esDebe BIT,
            descripcion VARCHAR(145),
            importe FLOAT,
            ejercicioCruce INT,
            fechaPag datetime,
	)
    */

    private Date obtenerFecha(Apunte apunte) {
        
        Date retorno=null;
      
        
        int ejercicio = apunte.getEjercicio();
                
        String fecha=null;
        String dia = "20/";
        int trimestre = apunte.getTrimestre() + 1;
        if (trimestre > 3) {
            ejercicio = ejercicio + 1;
        }
        switch (trimestre) {
            case 0:
                fecha = dia + "01/" + ejercicio;
                break;
            case 1:
                fecha = dia + "04/" + ejercicio;
                break;
            case 2:
                fecha = dia + "07/" + ejercicio;
                break;
            case 3:
                fecha = dia + "10/" + ejercicio;
                break;
            default:
                fecha = dia + "01/" + ejercicio;        
        }
            try {
                retorno = new java.sql.Date(SDF_LOC.parse(fecha).getTime());
            } catch (ParseException ex) {
                Logger.getLogger(GestorApuntesServiceDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        return retorno;
    }
    public boolean borrarApuntesByEjercicio(String ejercicio) throws SQLException {

        Statement pstmt = connection.getConnection().createStatement();

        String sql = "DELETE FROM T_APUNTES WHERE ejercicioCruce="+ejercicio;

        LOG.log(Level.WARNING, "Se han borrado apuntes del ejercicio {0} " + ejercicio);
        return pstmt.execute(sql);

    }
    void insertarApuntes(Collection<? extends Apunte> listaRecaudaciones, String ejercicio) throws SQLException {
           int iEjercicioCruce = Integer.parseInt(ejercicio);
        //borrar todos los apuntes pertenecientes al cruce que se está 
        // realizando (ejercicioCruce) para dar de alta los nuevos

     
        
        String sqlInsert = "INSERT INTO T_APUNTES("
                   +"roac,fecha,nifAuditor, "
                   +"nombreAuditor,ejercicioApunte, trimestreApunte, "
                   +"esDebe,descripcion,importe, "
                   +"ejercicioCruce, fechaPag) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement pstmtInsert = null;
      
        pstmtInsert = connection.getConnection().prepareStatement(sqlInsert);
        System.out.println("sql=\n" + sqlInsert);

        String roac = null;
        java.sql.Date fecha = null;
        String nifAuditor= null;
        String nombreAuditor=null;
        
        int ejercicioApunte = 0;
        int trimestreApunte = 0;
        boolean esDebe = false;

        //PARA ACTUALIZARLO
        String descripcion = null;
        double importe = 0d;
        java.sql.Date fechaPag = null;
        
        
        
        for (Apunte apunte : listaRecaudaciones) {
            roac = apunte.getRoac();
            fecha = new java.sql.Date(apunte.getFecha().getTime());
            
            nifAuditor = apunte.getNifAuditor();            
            nombreAuditor = apunte.getNombreAuditor();
            
            ejercicioApunte = apunte.getEjercicio();
            trimestreApunte = apunte.getTrimestre();
            esDebe = apunte.esDebe();
           // fechaPag = apunte.g(apunte);
            //PARA ACTUALIZARLO
            descripcion = apunte.getDenominacion();
            importe = apunte.getImporte();

            pstmtInsert.setString(1, roac);
            pstmtInsert.setDate(2, fecha);
            pstmtInsert.setString(3, nifAuditor);
            pstmtInsert.setString(4, nombreAuditor);
            pstmtInsert.setInt(5, ejercicioApunte);
            pstmtInsert.setInt(6, trimestreApunte);
            pstmtInsert.setBoolean(7, esDebe);
            pstmtInsert.setString(8, descripcion);
            pstmtInsert.setDouble(9, importe);
            pstmtInsert.setInt(10, iEjercicioCruce);
            pstmtInsert.setDate(11, fecha);
            pstmtInsert.addBatch();

        }

        int[] affectedRecords = pstmtInsert.executeBatch();
        pstmtInsert.close();

    }
}
