package es.icac.meh.pandora.services.daos;

import es.icac.meh.pandora.modelo.Recaudacion;
import es.icac.meh.pandora.modelo.Lote;
import es.icac.meh.pandora.services.ConnectionService;
import es.icac.meh.pandora.utils.JDBCUtilities;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MAJIVIAL
 */
public class LoteDAO {

    private static final Logger LOG = Logger.getLogger(LoteDAO.class.getName());

    private ConnectionService connection;
    
    private RecaudacionesDAO recaudacionesDAO;

    public LoteDAO() {
    }

    public LoteDAO(ConnectionService connection) {
        this.connection = connection;
        this.recaudacionesDAO = new RecaudacionesDAO(connection);
    }
    public boolean borrarLote(int idLote) {
        String sqlPreparada = "DELETE FROM T_LOTES_COBROS WHERE id=?";
        boolean retorno=true;
        
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.getConnection().prepareStatement(sqlPreparada);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Error en el borrado del lote " + idLote);
            retorno = false;
        }
        try {
            pstmt.setInt(1, idLote);
            if (pstmt.executeUpdate() > 0) {
                retorno = true;
            }
        } catch (SQLException | NullPointerException ex) {
            LOG.log(Level.SEVERE, "No se puedo borrar LOTE " + idLote +" "+ sqlPreparada);
            retorno = false;
        }
        return retorno;
    }
    
        public boolean modificarLote(Lote lote, int id) {
            
        String sqlPreparada = "UPDATE T_LOTES_COBROS"
                + " SET descripcion='" + lote.getDescripcion()+"',"
                + " fecha='" + lote.getFechaProcesadoString()+"'"
                + " WHERE id=?";
  
        //MODIFICAR LOS COBROS ASIGNADOS A ESE LOTE EN PARTICULAR

                
        boolean retorno=true;
        
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.getConnection().prepareStatement(sqlPreparada);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Error en la insercion del lote " + lote, ex);
            retorno = false;
        }
        try {
            if (pstmt.executeUpdate() > 0) {
                retorno = true;
            }
        } catch (SQLException | NullPointerException ex) {
            LOG.log(Level.SEVERE, "No se puedo insertar LOTE " + lote + sqlPreparada, ex);
            retorno = false;
        }
        
        if(retorno){
               LOG.log(Level.INFO, "se ha insertado LOTE " + lote );
        }else{
             LOG.log(Level.WARNING, "NO se ha insertado LOTE " + lote + "==="+sqlPreparada);
        }
        return retorno;
    }
        
        
    
    public Lote insertarLote(Lote lote) {
              
        String sqlPreparada = "INSERT INTO T_LOTES_COBROS(descripcion, fecha) VALUES(?,?);";
        Lote retorno=new Lote(lote.getDescripcion(),lote.getFechaProcesadoString());
        
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.getConnection().prepareStatement(sqlPreparada);
            pstmt.setString(1, lote.getDescripcion());
            
          pstmt.setDate(2, new java.sql.Date(lote.getFechaProcesado().getTime()));
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Error en la insercion del lote " + lote,ex);
          
        }
        try {
            if (pstmt.executeUpdate() > 0) {
                //insertar RUTAS ASOCIADAS A ESE LOTE
                //obtener ultimo LOTE INSERTADO PARA ASOCIAR A CADA RUTA
                int ultimo=obtenerUltimoId();
                retorno.setId(ultimo);
                if(lote.getListaRecaudacions()!=null && lote.getListaRecaudacions().size()>0){
                    for(Recaudacion cobro: lote.getListaRecaudacions()){
                        cobro.setEjercicio(lote.getEjercicio());
                        this.recaudacionesDAO.insertarRecaudacion(cobro, retorno);
                    }
                }else{
                    LOG.log(Level.WARNING, "Se ha insertado SIN COBROS el lote "+ retorno);
                }
                            
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "No se puedo insertar LOTE " , ex);
            try {
                JDBCUtilities.printSqlStatement(pstmt, sqlPreparada);
            } catch (SQLException ex1) {
                LOG.log(Level.SEVERE, "error mostrando query!!", ex1);
            }
        }catch (NullPointerException ex) {
            LOG.log(Level.SEVERE, "No se puedo insertar LOTE NULL POINTER");
        }
        return retorno;
    }

    public Lote obtenerLoteById(int id) throws SQLException {

        String sql = "select * from T_LOTES_COBROS WHERE id=?";
        PreparedStatement pstmt = connection.getConnection().prepareStatement(sql);
        pstmt.setInt(1, id);
        ResultSet rs = pstmt.executeQuery();
        Lote retorno = null;
        if (rs != null) {
            rs.next();
            retorno = new Lote(rs.getInt("id"), rs.getString("descripcion"), rs.getDate("fecha"));
        } else {
            LOG.log(Level.WARNING, "La obtenci\u00f3n de lotes con ID={0}sql={1} NO HA PRODUCIDO RESULTADOS", new Object[]{id, sql});
        }
        return retorno;
    }
    
    public int obtenerUltimoId() throws SQLException {

        String sql = "SELECT MAX(id) FROM T_LOTES_COBROS";

        Statement pstmt = connection.getConnection().createStatement();

        ResultSet rs = pstmt.executeQuery(sql);

        if (rs != null) {
            rs.next();
            return rs.getInt(1);
        } else {
            LOG.log(Level.WARNING, "La obtención del ultimo registro NO HA PRODUCIDO RESULTADOS (ESTA VACIA?)");
            return 0;
        }
    }


    public ArrayList<Lote> obtenerLotes() throws SQLException {

        String sql = "select * from T_LOTES_COBROS";
        PreparedStatement pstmt = connection.getConnection().prepareStatement(sql);
        ArrayList<Lote> retorno = new ArrayList();
        ResultSet rs = pstmt.executeQuery();
        if (rs != null) {
            rs.next();
            retorno.add(new Lote(rs.getInt("id"), rs.getString("descripcion"), rs.getDate("fecha")));
        } else {
            LOG.log(Level.WARNING, "La obtenci\u00f3n de LOTE {0} NO HA PRODUCIDO RESULTADOS", sql);
        }
        return retorno;
    }
    /*
    CREATE TABLE T_LOTES_COBROS (
            id INT NOT NULL IDENTITY PRIMARY KEY,
            descripcion VARCHAR(100) NOT NULL,
            fecha datetime
    );

     */
}
