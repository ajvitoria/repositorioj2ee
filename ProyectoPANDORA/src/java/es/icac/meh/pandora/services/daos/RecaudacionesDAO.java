package es.icac.meh.pandora.services.daos;

import es.icac.meh.pandora.modelo.Recaudacion;
import es.icac.meh.pandora.modelo.Lote;
import es.icac.meh.pandora.modelo.RegistroROAC;
import es.icac.meh.pandora.services.ConnectionService;
import es.icac.meh.pandora.utils.JDBCUtilities;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.InitializingBean;

/**
 *
 * @author MAJIVIAL
 */
public class RecaudacionesDAO implements InitializingBean{

    private static final Logger LOG = Logger.getLogger(RecaudacionesDAO.class.getName());

    private boolean DEBUG=false;

    private ConnectionService connection;
    private DocumentoRoacDAO docROAC;
    
    private ArrayList<RegistroROAC> listaROACS;
   
    Map<String,RegistroROAC> indexNif;

    public Map<String, RegistroROAC> getIndexNif() {
        if(indexNif==null){
            if(docROAC==null){
               docROAC=getDocROAC();
            }           
            
            return docROAC.obtenerMapaRoacsByNif();
        }else
            return indexNif;
        
     //   return docROAC.obtenerMapaRoacsByNif();
    }

    public void setIndexNif(Map<String, RegistroROAC> indexNif) {
        this.indexNif = indexNif;
    }

    public Map<String, RegistroROAC> getIndexRoac() {
        return indexRoac;
    }

    public void setIndexRoac(Map<String, RegistroROAC> indexRoac) {
        this.indexRoac = indexRoac;
    }
    Map<String,RegistroROAC> indexRoac;

    
    public RecaudacionesDAO(ConnectionService connection) {
        this.connection = connection;

    }

    
                    
                        
    public boolean borrarRecaudacionByArchivo(String nombreArchivo) {
        String sqlPreparada = "DELETE FROM T_RECAUDACIONES WHERE archivoFuente=?";
        boolean retorno=true;
        
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.getConnection().prepareStatement(sqlPreparada);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Error en el borrado del cobro del archivo:{0}", nombreArchivo);
            retorno = false;
        }
        try {
            pstmt.setString(1, nombreArchivo);
            if (pstmt.executeUpdate() > 0) {
                retorno = true;
            }
        } catch (SQLException | NullPointerException ex) {
            LOG.log(Level.SEVERE, "No se puedo borrar RECAUDACION DEL archivo: " + nombreArchivo +" "+ sqlPreparada);
            retorno = false;
        }
        return retorno;
    }
    public boolean borrarRecaudacion(int idRecaudacion) {
        String sqlPreparada = "DELETE FROM T_RECAUDACIONES WHERE id=?";
        boolean retorno=true;
        
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.getConnection().prepareStatement(sqlPreparada);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Error en el borrado del cobro {0}", idRecaudacion);
            retorno = false;
        }
        try {
            pstmt.setInt(1, idRecaudacion);
            if (pstmt.executeUpdate() > 0) {
                retorno = true;
            }
        } catch (SQLException | NullPointerException ex) {
            LOG.log(Level.SEVERE, "No se puedo borrar LOTE " + idRecaudacion +" "+ sqlPreparada);
            retorno = false;
        }
        return retorno;
    }
    
    
    public boolean moverRecaudacionAOtroLote(Recaudacion cobroAActualizar, Lote loteViejo,Lote loteNuevo){
    
         String sqlPreparada = "UPDATE T_RECAUDACIONES"
                + " SET FK_LOTE=" + loteNuevo.getId()                
                + " WHERE FK_LOTE=? AND ID=?";
  
                
        boolean retorno=false;
        
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.getConnection().prepareStatement(sqlPreparada);
             pstmt.setLong(2, cobroAActualizar.getId());
            pstmt.setInt(1, loteViejo.getId());
           
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Error en la actualizaci\u00f3n del cobro {0}", cobroAActualizar);
            retorno = false;
        }
        try {
            if (pstmt.executeUpdate() > 0) {
                retorno = true;
            }
        } catch (SQLException | NullPointerException ex) {
            retorno = false;
        }

        if (retorno) {
            LOG.log(Level.INFO, "Movido cobro " + cobroAActualizar.getId() + " DESDE el lote " + loteViejo.getId() + "\n HACIA EL lote " + loteNuevo.getId() + "\n en la sql=" + sqlPreparada);
        } else {
            LOG.log(Level.WARNING, "No se puedo mover el COBRO ID=" + cobroAActualizar.getId() + " DESDE lote " + loteViejo.getId() + "\n HACIA EL lote " + loteNuevo.getId() + "\n en la sql=" + sqlPreparada);
        }
        return retorno;

    }
            
            
            
        public boolean modificarRecaudacion(Recaudacion cobro) {

                                      
    
        String sqlPreparada = "UPDATE T_RECAUDACIONES"
                + " SET id=" + cobro.getId()
                /* NO PERMITO DESDE AQUI MODIFICAR EL LOTE DEL COBRO + " SET FK_LOTE=" + cobro.getLote().getId()*/
                
                /* SI QUIERES MODIFICAR EL LOTE DE UN COBRO USAR moverRecaudacionAOtroLote() */
                + " SET codigoTasa='" + cobro.getCodigoTasa()+"',"
                + " SET numeroJustificante='" + cobro.getNumeroJustificante()+"',"
                + " SET codigovalorSubcampo1='" + cobro.getCodigovalorSubcampo1()+"',"
                + " SET codigovalorSubcampo2='" + cobro.getCodigovalorSubcampo2()+"',"
                + " SET codigoAnyoEjercicio=" + cobro.getCodigoAnyoEjercicio()+","
                + " SET codigovalorTrimestre=" + cobro.getCodigovalorTrimestre()+","
                + " SET codigovalorSubcampo5='" + cobro.getCodigovalorSubcampo5()+"',"
                + " SET codigovalorSubcampo6='" + cobro.getCodigovalorSubcampo6()+"',"
                + " SET nif='" + cobro.getNif()+"',"
                + " SET apellidosNombre='" + cobro.getApellidosNombre()+"',"
                + " SET fechaIngreso='" + cobro.getFechaIngreso()+"',"
                + " SET fechaPlazo='" + cobro.getFechaPlazo()+"',"
                + " SET archivoFuente='" + cobro.getArchivoFuente()+"',"
                + " SET anyo='" + cobro.getAnyo()+"',"
                + " SET ejercicio='" + cobro.getEjercicio()+"',"
                + " SET importeEuros=" + cobro.getImporteEuros()+","
                + " SET entidad='" + cobro.getEntidad()+"',"
                + " SET sucursal='" + cobro.getSucursal()+"'"
                + " WHERE id=?";
  
     
                
        boolean retorno=true;
        
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.getConnection().prepareStatement(sqlPreparada);
            pstmt.setLong(1, cobro.getId());
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Error en la actualización del cobro " + cobro);
            retorno = false;
        }
        try {
            if (pstmt.executeUpdate() > 0) {
                retorno = true;
            }
        } catch (SQLException | NullPointerException ex) {
            LOG.log(Level.SEVERE, "No se puedo actualizar el COBRO ID=" + cobro.getId()+"\n con la los datos "+ cobro + "\n en la sql=" + sqlPreparada);
            retorno = false;
        }
        return retorno;
    }
        
public void printSqlStatement(PreparedStatement preparedStatement, String sql) throws SQLException{
        
    System.out.println("****************************************************************");
    
    System.out.println(preparedStatement.toString());
    
    
        String[] sqlArrya= new String[preparedStatement.getParameterMetaData().getParameterCount()];
        try {
               Pattern pattern = Pattern.compile("\\?");
               Matcher matcher = pattern.matcher(sql);
               StringBuffer sb = new StringBuffer();
               int indx = 1;  // Parameter begin with index 1
               while (matcher.find()) {
             matcher.appendReplacement(sb,String.valueOf(sqlArrya[indx]));
               }
               matcher.appendTail(sb);
              System.out.println("Executing Query [" + sb.toString() + "] with Database[" + "] ...");
               } catch (Exception ex) {
                   System.out.println("Executing Query [" + sql + "] with Database[" +  "] ...");
            }

    }        
    //actualizado 09/06/2020
    public boolean insertarRecaudacion(Recaudacion cobro, Lote lote) {
        String sqlPreparada = "INSERT INTO T_RECAUDACIONES("
                +" FK_LOTE, codigoTasa, numeroJustificante,"
                + "codigovalorSubcampo1, codigovalorSubcampo2, codigoAnyoEjercicio, "
                + "codigovalorTrimestre, codigovalorSubcampo5, codigovalorSubcampo6, "                
                + "nif, apellidosNombre, fechaIngreso, "
                + "fechaPlazo, archivoFuente, anyo, "
                + "ejercicio, importeEuros, entidad, "
                + "sucursal, roac)"
                + " VALUES("
                + " ?,?,?,"
                + " ?,?,?,"
                + " ?,?,?,"
                + " ?,?,?,"
                + " ?,?,?,"
                + " ?,?,?,"
                + " ?,?)";
        
        boolean retorno=true;
                PreparedStatement pstmt = null;
        try {
            pstmt = connection.getConnection().prepareStatement(sqlPreparada);
            
            pstmt.setInt(1, lote.getId());  //es el lote        
            pstmt.setString(2, cobro.getCodigoTasa());            
            pstmt.setString(3, cobro.getNumeroJustificante());
            
            pstmt.setString(4, cobro.getCodigovalorSubcampo1());
            pstmt.setString(5, cobro.getCodigovalorSubcampo2());                 
            pstmt.setInt(6,cobro.getCodigoAnyoEjercicio() );
            
            pstmt.setInt(7, cobro.getCodigovalorTrimestre());            
            pstmt.setString(8, cobro.getCodigovalorSubcampo5());
            pstmt.setString(9, cobro.getCodigovalorSubcampo6());
            
            pstmt.setString(10, cobro.getNif());
            String denominacion=cobro.getApellidosNombre().trim();
            try{
            if (denominacion.equals("")){
                denominacion=this.getIndexNif().get(cobro.getNif()).getDenominacion();
            }
            }catch(Exception e){
                denominacion="denominacion "+cobro.getNif();
            }
            pstmt.setString(11, denominacion);            
            pstmt.setDate(12, new java.sql.Date(cobro.getFechaIngreso().getTime()));
            
            pstmt.setDate(13, new java.sql.Date(cobro.getFechaPlazo().getTime()));              
            pstmt.setString(14, cobro.getArchivoFuente());  
            pstmt.setInt(15, cobro.getAnyo());
            
            pstmt.setInt(16, cobro.getEjercicio());               
            pstmt.setDouble(17, cobro.getImporteEuros());
            pstmt.setString(18, cobro.getEntidad());
            
            pstmt.setString(19, cobro.getSucursal()); 
            String codigoRoac=null;
            String coletilla=cobro.getNif()+ " "+cobro.getNumeroJustificante();
            try{
                codigoRoac=getIndexNif().get(cobro.getNif()).getCodRoac();
            }catch(Exception e){
                LOG.log(Level.SEVERE, "Se ha instentado insertar el cobro "+coletilla+ " con este roac "+codigoRoac);
                codigoRoac="roac desconocido "+coletilla;
            }finally{
                if (null==codigoRoac && null==cobro.getNif()){
                    if(null==cobro.getNif())
                        codigoRoac="Roac desconocido SIN NIF";
                    else
                        codigoRoac="roac desconocido ".concat(coletilla);
                }
            }
            
            
            pstmt.setString(20,codigoRoac);
            
         
     if(DEBUG)
         JDBCUtilities.printSqlStatement(pstmt, sqlPreparada);
     
     
     
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Error en la insercion del cobro \n}", ex);
         
        //    LOG.log(Level.SEVERE, "Error en la insercion del cobro \n{0}", cobro);
            retorno = false;
        }

        try {
            if (pstmt.executeUpdate() > 0) {
                //insertar RUTAS ASOCIADAS A ESE LOTE
                //obtener ultimo LOTE INSERTADO PARA ASOCIAR A CADA RUTA
                int ultimo=obtenerUltimoId();
                
                retorno = true;
            }
        } catch (SQLException | NullPointerException ex) {
            LOG.log(Level.SEVERE, "No se puedo insertar COBRO", ex);
            LOG.log(Level.SEVERE, cobro.toString());
            try {
                printSqlStatement(pstmt, sqlPreparada);
            } catch (SQLException ex1) {
                Logger.getLogger(RecaudacionesDAO.class.getName()).log(Level.SEVERE, "******excepcion******", ex1);
            }
            
            retorno = false;
        }
        return retorno;
    }

    //actualizado 09/06/20
    public Recaudacion obtenerRecaudacionById(int id) throws SQLException {

        String sql = "select * from T_RECAUDACIONES WHERE id=?";
        PreparedStatement pstmt = connection.getConnection().prepareStatement(sql);
        pstmt.setInt(1, id);
        ResultSet rs = pstmt.executeQuery();
        
        Recaudacion retorno = new Recaudacion();
        String nif=null;
        
        if (rs != null) {
            rs.next();
            retorno.setId(rs.getInt("id"));
           /* Realmente no se necesita recupear el lote pues ya lo sabes   */
            retorno.setCodigoTasa(rs.getString("codigoTasa"));
            String numeroJustificante=rs.getString("numeroJustificante" );
            retorno.setNumeroJustificante(numeroJustificante);
            retorno.setCodigovalorSubcampo1(rs.getString("codigovalorSubcampo1"));
            retorno.setCodigovalorSubcampo2(rs.getString("codigovalorSubcampo2"));
            retorno.setCodigoAnyoEjercicio(rs.getInt("codigoAnyoEjercicio" ));
            retorno.setCodigovalorTrimestre(rs.getInt("codigovalorTrimestre" ));
            retorno.setCodigovalorSubcampo5(rs.getString("codigovalorSubcampo5"));
            retorno.setCodigovalorSubcampo6(rs.getString("codigovalorSubcampo6"));
            nif=rs.getString("nif" );
            String roac=rs.getString("roac");
            if(roac==null){
                roac=docROAC.obtenerRoacByDocumento(nif,numeroJustificante).getCodRoac();
            }
            retorno.setRoac(roac);
            
            retorno.setNif(nif);
            retorno.setApellidosNombre(rs.getString("apellidosNombre"));
            retorno.setFechaIngreso(rs.getDate("fechaIngreso" ));
            retorno.setFechaPlazo(rs.getDate("fechaPlazo" ));
            retorno.setArchivoFuente(rs.getString("archivoFuente"));
            retorno.setAnyo(rs.getInt("anyo"));            
            retorno.setEjercicio(rs.getInt("ejercicio"));        
                    
            retorno.setImporteEuros(rs.getDouble("importeEuros" ));
            retorno.setEntidad(rs.getString("entidad" ));
            retorno.setSucursal(rs.getString("sucursal" ));
        } else {
            LOG.log(Level.WARNING, "La obtención de cobro con ID=" + id + "sql=" + sql + " NO HA PRODUCIDO RESULTADOS");
        }
        //el codigo del roac lo tengo que obtener de la tabla de pares
      
        return retorno;
    }
    
    public int obtenerUltimoId() throws SQLException {
        String sql = "SELECT MAX(id) FROM T_RECAUDACIONES";
        Statement pstmt = connection.getConnection().createStatement();
        ResultSet rs = pstmt.executeQuery(sql);
        if (rs != null) {
            rs.next();
            return rs.getInt(1);
        } else {
            LOG.log(Level.WARNING, "La obtención del ultimo registro NO HA PRODUCIDO RESULTADOS (ESTA VACIA?)");
            return 0;
        }
    }
    


    //actualizado 09/06/20
    
        public ArrayList<Recaudacion> obtenerRecaudacionesByEjercicio(String ejercicio) throws SQLException {
    this.listaROACS= docROAC.obtenerListaRoacs() ;
        String sql = "select * from T_RECAUDACIONES WHERE anyo="+ejercicio;
        LOG.log(Level.INFO, "sql=" + sql );
        ResultSet rs =null;
        ArrayList<Recaudacion> listaRetorno;        
        try (PreparedStatement pstmt = connection.getConnection().prepareStatement(sql)) {
            listaRetorno = new ArrayList();
             rs = pstmt.executeQuery();
            if (rs != null) {
                while(rs.next()){
                    Recaudacion retorno = new Recaudacion();                    
                    /* Realmente no se necesita recupear el lote pues ya lo sabes   */
                    retorno.setId(rs.getLong("id"));
                    retorno.setCodigoTasa(rs.getString("codigoTasa"));
                    String numeroJustificante=rs.getString("numeroJustificante" );
                    retorno.setNumeroJustificante(numeroJustificante);
                    retorno.setCodigovalorSubcampo1(rs.getString("codigovalorSubcampo1"));
                    retorno.setCodigovalorSubcampo2(rs.getString("codigovalorSubcampo2"));
                    retorno.setCodigoAnyoEjercicio(rs.getInt("codigoAnyoEjercicio" ));
                    retorno.setCodigovalorTrimestre(rs.getInt("codigovalorTrimestre" ));
                    retorno.setCodigovalorSubcampo5(rs.getString("codigovalorSubcampo5"));
                    retorno.setCodigovalorSubcampo6(rs.getString("codigovalorSubcampo6"));
           
                    retorno.setApellidosNombre(rs.getString("apellidosNombre"));
                    
                    Date fechaI=rs.getDate("fechaIngreso" );
                    retorno.setFechaIngreso(new Date(fechaI.getTime()));
                    Date fechaP=rs.getDate("fechaPlazo" );
                    retorno.setFechaPlazo(new Date(fechaP.getTime()));
                    retorno.setArchivoFuente(rs.getString("archivoFuente"));
                    retorno.setAnyo(rs.getInt("anyo"));                              
                    retorno.setEjercicio(rs.getInt("ejercicio"));  
                    
                    retorno.setImporteEuros(rs.getDouble("importeEuros" ));//el importe esta en centimos
                    retorno.setEntidad(rs.getString("entidad" ));
                    retorno.setSucursal(rs.getString("sucursal" ));
                    String nif=rs.getString("nif" );
                    String roac=rs.getString("roac");
                    if(roac==null){
                        roac=docROAC.obtenerRoacByDocumento(nif,numeroJustificante).getCodRoac();
                    }
                    retorno.setRoac(roac);
                    
                    retorno.setNif(nif);
        //      el codigo del roac lo tengo que obtener de la tabla de pares
                    
                    retorno.setRoac(rs.getString("roac" ));
      
                    listaRetorno.add(retorno);                }
            } else {
                LOG.log(Level.WARNING, "La obtenci\u00f3n de cobros del ejercicio {0} del Lote con Isql={1} NO HA PRODUCIDO RESULTADOS", new Object[]{ejercicio, sql});
            }
            
        }finally{
                if(rs!=null)
                    rs.close();
        
        }
        return listaRetorno;
    }
    
     //actualizado 09/06/20
        public ArrayList<Recaudacion> obtenerTodosRecaudaciones() {

  
           
        ArrayList<Recaudacion> listaRetorno =null;
        ResultSet rs=null;
        PreparedStatement pstmt=null;
        try {
            String sql = "select * from T_RECAUDACIONES";
            LOG.log(Level.INFO, "sql={0}", sql);
            
            pstmt = connection.getConnection().prepareStatement(sql);
            
            listaRetorno = new ArrayList();
             rs = pstmt.executeQuery();           
             Recaudacion retorno =null;
               String nif=null;
            if (rs != null) {
                while(rs.next()){
                    retorno = new Recaudacion();
                    
                    /* Realmente no se necesita recupear el lote pues ya lo sabes   */
                    retorno.setCodigoTasa(rs.getString("codigoTasa"));
                    String numeroJustificante=rs.getString("numeroJustificante" );
                    retorno.setNumeroJustificante(numeroJustificante);
                    retorno.setCodigovalorSubcampo1(rs.getString("codigovalorSubcampo1"));
                    retorno.setCodigovalorSubcampo2(rs.getString("codigovalorSubcampo2"));
                    retorno.setCodigoAnyoEjercicio(rs.getInt("codigoAnyoEjercicio" ));
                    retorno.setCodigovalorTrimestre(rs.getInt("codigovalorTrimestre" ));
                    retorno.setCodigovalorSubcampo5(rs.getString("codigovalorSubcampo5"));
                    retorno.setCodigovalorSubcampo6(rs.getString("codigovalorSubcampo6"));
                    
                    retorno.setApellidosNombre(rs.getString("apellidosNombre"));
                    retorno.setFechaIngreso(rs.getDate("fechaIngreso" ));
                    
                    retorno.setFechaPlazo(rs.getDate("fechaPlazo" ));
                    retorno.setArchivoFuente(rs.getString("archivoFuente"));
                    
                    retorno.setAnyo(rs.getInt("anyo"));
                    retorno.setEjercicio(rs.getInt("ejercicio"));  
                    
                    retorno.setImporteEuros(rs.getDouble("importeEuros" )/100);//el importe esta en centimos
                    retorno.setEntidad(rs.getString("entidad" ));
                    retorno.setSucursal(rs.getString("sucursal" ));
                    nif=rs.getString("nif" );
                    retorno.setNif(nif);
                
                    
                   
                    retorno.setRoac(rs.getString("roac" ));
                    
//,numeroJustificante).getCodRoac());
              //       docROAC.obtenerMapaRoacsByCodRoac().get(LOG)
                    listaRetorno.add(retorno);
                }
            } else {
                LOG.log(Level.WARNING, "La obtención de cobros del Lote con Isql=" + sql + " NO HA PRODUCIDO RESULTADOS");
            }
           
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, "error liberando recursos", ex);
            }
        }

        return listaRetorno;
    }

    private void crearTabla() {
        /* 
        
    drop table T_RECAUDACIONES;
	CREATE TABLE T_RECAUDACIONES (
            id INT NOT NULL IDENTITY PRIMARY KEY,
            FK_lote INT,    
            codigoTasa VARCHAR(8),
            numeroJustificante VARCHAR(20),
            codigovalorSubcampo1 VARCHAR(8),
            codigovalorSubcampo2 VARCHAR(8),
            codigoAnyoEjercicio INT,
            codigovalorTrimestre INT,
            codigovalorSubcampo5 VARCHAR(8),
            codigovalorSubcampo6 VARCHAR(8),
            nif VARCHAR(20),
            apellidosNombre VARCHAR(100),
            fechaIngreso datetime,
            fechaPlazo datetime,
            importeEuros FLOAT,
            entidad VARCHAR(6),
            sucursal VARCHAR(6),
            anyo INT,
            ejercicio INT,
            archivoFuente VARCHAR(125),
            roac VARCHAR(50)
	)
        
    )


PODRIA INTERESAR HACER ESTO!

ALTER TABLE T_COBROSd
ADD COLUMN  new_column <type>
AFTER       existing_column


         */
    }

    public DocumentoRoacDAO getDocROAC() {
        if (docROAC!=null)
            return docROAC;
        else
            return docROAC=new DocumentoRoacDAO(connection);
    }

    public void setDocROAC(DocumentoRoacDAO docROAC) {
        this.docROAC = docROAC;
    }

    public int getNumeroRecaudaciones(String ejercicio) {
        int resultado = 0;

        Statement pstmt = null;
        try {
            String sql = "select count(*) count from T_RECAUDACIONES WHERE anyo=" + ejercicio;
            LOG.log(Level.INFO, "sql={0}", sql);
            pstmt = connection.getConnection().createStatement();

            ResultSet rs = pstmt.executeQuery(sql);

            int rowCount = 0;
            while (rs.next()) {
                resultado = rs.getInt("count");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultado;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        try{
            if(docROAC==null){
                docROAC=new DocumentoRoacDAO(connection);
            }
            
        indexNif = docROAC.obtenerMapaRoacsByNif();
        indexRoac = docROAC.obtenerMapaRoacsByCodRoac();
        }catch(Exception ex){
        
            LOG.log(Level.SEVERE, "Error inicializando el bean de recaudaciones con los mapas del NIF/roac", ex);
        }
    }

}


