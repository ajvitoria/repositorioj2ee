
package es.icac.meh.pandora.services.daos;


import es.icac.meh.pandora.modelo.RegistroROAC;
import es.icac.meh.pandora.services.ConnectionService;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MAJIVIAL
 */
public class DocumentoRoacDAO {
    
   private static final Logger LOG = Logger.getLogger(DocumentoRoacDAO.class.getName());

    private ConnectionService connection;
    


    public DocumentoRoacDAO() {
    }

    public DocumentoRoacDAO(ConnectionService connection) {
        this.connection = connection;

    }
//    public RegistroROAC  obtenerRoacByDocumento(String nif, String numeroJustificante) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    public RegistroROAC obtenerRoacByDocumento(String documento, String numeroJustificante) throws SQLException {

        String sql2 = "SELECT Documento, COD_ROAC FROM T_Modelo_Datos WHERE Documento like ? ";
        
        
        String sql="select\n" +
            " isnull(d.COD_ROAC, '') AS COD_ROAC, \n" +
            " CASE \n" +
            " WHEN aud.Nombre is null or aud.Nombre='' THEN \n" +
            " aud.Razon_Social ELSE aud.Apellidos+', '+aud.Nombre \n" +
            " END AS DENOMINACION,  \n" +
            " isnull(d.Documento, '')	AS COD_CIF_NIF\n" +
            " FROM T_Modelo_Datos d\n" +
            " INNER JOIN [dbo].[T_Auditores_Sociedades] aud ON aud.COD_ROAC = d.COD_ROAC"+
            " WHERE COD_CIF_NIF like ? ";
        
        // SELECT COD_ROAC, DENOMINACION, COD_CIF
        
        PreparedStatement pstmt = connection.getConnection().prepareStatement(sql);
        pstmt.setString(1, documento);
        ResultSet rs = pstmt.executeQuery();
        RegistroROAC retorno = null;
        String roac="ROAC_DESCONOCIDO/nif="+documento, docu="NIF_DESCONOCIDO", desc="SIN_DESCRIPCION";
        if (rs != null) {
            
            while(rs.next()){
                try{
                    
                     roac=rs.getString("COD_ROAC");
                     docu=rs.getString("COD_CIF_NIF");
                     desc=rs.getString("DENOMINACION");
                     //se podria obtener nombre y apellidos
                    retorno = new RegistroROAC(roac,docu, desc );
                    break;
                }catch(SQLException ex){
                    if(roac==null) roac="ROAC ANONIMO";
                    if(docu==null) roac="DOCU ANONIMO";
                    if(desc==null) roac="SUB DESCRIPCION";
                     LOG.log(Level.WARNING,"excepcion obtener"+roac+"RoacByDocumento de "+documento+ " descripcion "+desc, ex);
                     
                  //   throw ex;
                }
            }
            if (retorno==null){
            
                   LOG.log(Level.WARNING, "NO ENCUENTRA ROAC para NIF {0} del {1}", new Object[]{documento, numeroJustificante});           
            }
             retorno = new RegistroROAC(roac, docu, desc);
        } 
        return retorno;
    }
        public RegistroROAC obtenerRoacDocumentoByRoac(String roac) throws SQLException {

   //     String sql = "SELECT Documento, COD_ROAC FROM T_Modelo_Datos WHERE COD_ROAC like ?";
        
        String sql="select\n" +
            " isnull(d.COD_ROAC, '') AS COD_ROAC, \n" +
            " CASE \n" +
            " WHEN aud.Nombre is null or aud.Nombre='' THEN \n" +
            " aud.Razon_Social ELSE aud.Apellidos+', '+aud.Nombre \n" +
            " END AS DENOMINACION,  \n" +
            " isnull(d.Documento, '')	AS COD_CIF_NIF\n" +
            " FROM T_Modelo_Datos d\n" +
            " INNER JOIN [dbo].[T_Auditores_Sociedades] aud ON aud.COD_ROAC = d.COD_ROAC"+
            " WHERE COD_ROAC like ? ";        
        
                
        RegistroROAC retorno;
       try (PreparedStatement pstmt = connection.getConnection().prepareStatement(sql)) {
           pstmt.setString(1, roac);
            try (ResultSet rs = pstmt.executeQuery()) {
                retorno = null;
                if (rs != null) {
                    rs.next();
                    retorno = new RegistroROAC(rs.getString("COD_ROAC"), rs.getString("COD_CIF_NIF"), rs.getString("DENOMINACION"));
                } else {
                    LOG.log(Level.WARNING, "La obtenci\u00f3n de ROAC(DOCUMENTO) con codigo roac={0} NO HA PRODUCIDO RESULTADOS", roac);
                }}
       }
        return retorno;
    }
    
   


    public ArrayList<RegistroROAC> obtenerListaRoacs() {
 ArrayList<RegistroROAC> retorno=null;
 Statement pstmt=null;
 ResultSet rs=null;
       try {
          // String sql = "SELECT Documento, COD_ROAC FROM T_Modelo_Datos ";
            
          String sql="select\n" +
            " isnull(d.COD_ROAC, '') AS COD_ROAC, \n" +
            " CASE \n" +
            " WHEN aud.Nombre is null or aud.Nombre='' THEN \n" +
            " aud.Razon_Social ELSE aud.Apellidos+', '+aud.Nombre \n" +
            " END AS DENOMINACION,  \n" +
            " isnull(d.Documento, '') AS COD_CIF_NIF\n" +
            " FROM T_Modelo_Datos d\n" +
            " INNER JOIN [dbo].[T_Auditores_Sociedades] aud ON aud.COD_ROAC=d.COD_ROAC";    
          
            pstmt = connection.getConnection().createStatement();
            
           retorno= new ArrayList();
            rs = pstmt.executeQuery(sql);
           if (rs != null) {
               while(rs.next()){
                   retorno.add(new RegistroROAC(rs.getString("COD_ROAC"), rs.getString("COD_CIF_NIF"),rs.getString("DENOMINACION")));
               }
               
           } else {
               LOG.log(Level.WARNING, "La obtenci\u00f3n de ListaRoacs {0} NO HA PRODUCIDO RESULTADOS", sql);
           }

       } catch (SQLException ex) {
                LOG.log(Level.WARNING, "La obtención de ListaRoacs  HA PRODUCIDO un error:", ex);
       }finally{

     try {
         if(pstmt!=null)
            pstmt.close();
         if(rs!=null)
            rs.close();
     } catch (SQLException ex) {
         Logger.getLogger(DocumentoRoacDAO.class.getName()).log(Level.SEVERE, null, ex);
     }
       }
         return retorno;
    }
    
    public Map<String, RegistroROAC> obtenerMapaRoacsByCodRoac()  {

        Map<String,RegistroROAC> retorno = new HashMap<>();

        String sql2 = "SELECT Documento, COD_ROAC FROM T_Modelo_Datos ORDER BY COD_ROAC";
        String sql="select\n" +
            " isnull(d.COD_ROAC, '') AS COD_ROAC, \n" +
            " CASE \n" +
            " WHEN aud.Nombre is null or aud.Nombre='' THEN \n" +
            " aud.Razon_Social ELSE aud.Apellidos+', '+aud.Nombre \n" +
            " END AS DENOMINACION,  \n" +
            " isnull(d.Documento, '')	AS COD_CIF_NIF\n" +
            " FROM T_Modelo_Datos d\n" +
            " INNER JOIN [dbo].[T_Auditores_Sociedades] aud ON aud.COD_ROAC = d.COD_ROAC\n" +
            " ORDER BY COD_ROAC";
        
       try (PreparedStatement pstmt = connection.getConnection().prepareStatement(sql); ResultSet rs = pstmt.executeQuery()) {
           if (rs != null) {
               while (rs.next()) {
                   RegistroROAC reg=new RegistroROAC(rs.getString("COD_ROAC"), rs.getString("COD_CIF_NIF"), rs.getString("DENOMINACION"));
                   retorno.put(rs.getString("COD_ROAC"), reg);
               }
           } else {
                
               LOG.log(Level.WARNING, "La obtenci\u00f3n de obtenerMapaRoacsByCodRoac {0} NO HA PRODUCIDO RESULTADOS", sql);
           }
       } catch (SQLException ex) {
           LOG.log(Level.SEVERE, "La obtenci\u00f3n de obtenerMapaRoacsByCodRoac {0} NO HA PRODUCIDO RESULTADOS", ex);

       }
        
        return retorno;
    }
    public Map<String, RegistroROAC> obtenerMapaRoacsByNif() {
Map<String,RegistroROAC> retorno=null;
 ResultSet rs =null;
 PreparedStatement pstmt =null;
       try {
           retorno = new HashMap<>();
           
           String sql2 = "SELECT Documento, COD_ROAC FROM T_Modelo_Datos ORDER BY Documento";
            String sql="select\n" +
            " isnull(d.COD_ROAC, '') AS COD_ROAC, \n" +
            " CASE \n" +
            " WHEN aud.Nombre is null or aud.Nombre='' THEN \n" +
            " aud.Razon_Social ELSE aud.Apellidos+', '+aud.Nombre \n" +
            " END AS DENOMINACION,  \n" +
            " isnull(d.Documento, '')	AS COD_CIF_NIF\n" +
            " FROM T_Modelo_Datos d\n" +
            " INNER JOIN [dbo].[T_Auditores_Sociedades] aud ON aud.COD_ROAC = d.COD_ROAC\n" +
            " ORDER BY COD_ROAC";           
           pstmt = connection.getConnection().prepareStatement(sql);
           
            rs = pstmt.executeQuery();
           if (rs != null) {
               while (rs.next()) {
                    RegistroROAC reg=new RegistroROAC(rs.getString("COD_ROAC"), rs.getString("COD_CIF_NIF"), rs.getString("DENOMINACION"));
                   retorno.put(rs.getString("COD_CIF_NIF"),reg );
               }
           } else {
               LOG.log(Level.WARNING, "La obtenci\u00f3n de obtenerMapaRoacsByNif {0} NO HA PRODUCIDO RESULTADOS", sql);
           }
          
       } catch (SQLException ex) {
          LOG.log(Level.WARNING, "La obtenci\u00f3n de obtenerMapaRoacsByNif {0} NO HA PRODUCIDO RESULTADOS", ex);
       }finally{
    try {
        if(rs!=null)
            rs.close();
        if(pstmt!=null)
            pstmt.close();
    } catch (SQLException ex) {
        LOG.log(Level.SEVERE, "ERROR AL CERRAR", ex);
    }
           
       }
        return retorno;
    }
    
    /*
    CREATE TABLE T_LOTES_COBROS (
            id INT NOT NULL IDENTITY PRIMARY KEY,
            descripcion VARCHAR(100) NOT NULL,
            fecha VARCHAR(10) NOT NULL
    );

     */


}
