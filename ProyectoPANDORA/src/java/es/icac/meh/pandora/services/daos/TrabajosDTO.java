/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.icac.meh.pandora.services.daos;

import es.icac.meh.pandora.modelo.LoteTrabajoRealizado;
import es.icac.meh.pandora.modelo.ResultadoExtraccion;
import es.icac.meh.pandora.modelo.TrabajoRealizado;
import es.icac.meh.pandora.modelo.TrabajoRealizadoAtlas;
import es.icac.meh.pandora.services.ConnectionService;
import es.icac.meh.pandora.services.impl.CalculoTarifaServiceImpl;
import es.icac.meh.pandora.services.impl.Preprocesador;
import java.util.logging.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
/**
 *
 * @author MAJIVIAL
 */
public class TrabajosDTO {
   
    private static final Logger LOG = Logger.getLogger(TrabajosDTO.class.getName());

    private ConnectionService connection;
    private TrabajosRealizadosAtlasDAO trabajosAtlasDAO;
    private TrabajosRealizadosDAO trabajosDAO;
    private Preprocesador preprocesado;


    
    ArrayList<TrabajoRealizado> lista;
    private final CalculoTarifaServiceImpl tarificador;
    
    public TrabajosDTO(ConnectionService connection){

            this.connection=connection;
            this.trabajosAtlasDAO=new TrabajosRealizadosAtlasDAO(connection);
            this.trabajosDAO=new TrabajosRealizadosDAO(connection);
            this.tarificador=new CalculoTarifaServiceImpl();
          //x
     //     si quieres que al arrancar el servidor haga la importación automaticamente
        //    importarTrabajosAPartirEjerciciosConfiguracion();
            
    }
   
    private boolean borrarTrabajosPrevios(String pEjercicioABorrar){
            
        LOG.log(Level.INFO, "Borrando TRABAJOSS DEL EJERCICIO {0}", pEjercicioABorrar);
        
        try {
           // ESTA BORRA TODO trabajosDAO.borrarDatosTabla();
             return trabajosDAO.borrarDatosTabla(pEjercicioABorrar);
             
        } catch (SQLException ex) {
            Logger.getLogger(TrabajosDTO.class.getName()).log(Level.SEVERE, "ERROR AL BORRAR LA TABLA DE TRABAJOS!", ex);
            return false;
        }
      
    }
    
    private int insertarTrabajos(TrabajoRealizadoAtlas[] trabajosAtlas, int anyoEjercicio){
        LOG.log(Level.INFO, "Obtenidos de ROAC-NET... AHORA INSERTO EN TRABAJOS");
        LoteTrabajoRealizado loteTrabajoPENDIENTE=new LoteTrabajoRealizado();
        loteTrabajoPENDIENTE.setId(222222);
        
        int d= trabajosDAO.insertarTrabajosRealizadoDesdeAtlas(trabajosAtlas,loteTrabajoPENDIENTE, anyoEjercicio );
        LOG.log(Level.INFO, "INSERTANDO EN TRABAJOS DE PANDORA, {0} trabajos IMPORTADOS DE ATLAS", d);
        return d;
    }
    
    
    
    
    
    //le llamo desde el servlet 01/06/2020
    public ResultadoExtraccion extraerTrabajos(String paramEJERC, boolean regenerar) {
        
        
        int anyoEjercicio=0;
        try{
            anyoEjercicio=Integer.parseInt(paramEJERC);
        }catch(Exception e){
             LOG.log(Level.SEVERE, "No se puede procesar el ejercicio "+paramEJERC);
        }finally{
            if(anyoEjercicio<2000 || anyoEjercicio>2100){
                LOG.log(Level.SEVERE, "No se puede procesar el ejercicio "+anyoEjercicio);
                System.exit(1);
            }
        }
        
        ResultadoExtraccion resultado=new ResultadoExtraccion();
        long tiempo=0;
        try {
            long tiempoInicial = new java.util.Date().getTime();

            LOG.log(Level.INFO, "Obteniendo trabajos de ATLAS");
//            ArrayList<TrabajoRealizadoAtlas> trabajosAtlas = new ArrayList<>();

            LOG.log(Level.INFO, "PREPROCESANDO PERIODO {0}", paramEJERC);
            TrabajoRealizadoAtlas[] tmp = trabajosAtlasDAO.obtenerTrabajosPeriodo(paramEJERC);
   
            resultado.setNumeroDatosExtraidos(tmp.length);
            
            LOG.log(Level.INFO, "VOY A CALCULAR TARIFAS");
            for (TrabajoRealizadoAtlas i: tmp){
             i.setTasa(tarificador.damePrecio(i));        
            
            }
            
            //borro de la tabla de t_TRABAJOS_REALIZADOS LO QUE HABIA EN EL EJERCICIO ANTERIOR
            if(regenerar){
                      LOG.log(Level.INFO, "BORRADOS T_TRABAJOS_REALIZADOS DEL EJERCICIO {0}", paramEJERC);
                      
                      borrarTrabajosPrevios(paramEJERC);
            }else{
                
                    LOG.log(Level.INFO, "NO SE HA BORRADO  T_TRABAJOS_REALIZADOS DEL EJERCICIO {0}", paramEJERC);
            }          
                      
            LOG.log(Level.INFO, "VOY A insertar los trabajos!");
            insertarTrabajos(tmp, anyoEjercicio);
            long tiempoFinal = new java.util.Date().getTime();

             tiempo = (tiempoFinal - tiempoInicial) / 1000;

            LOG.log(Level.INFO, "tiempo proceso  " + tiempo);
           

        } catch (SQLException ex) {
            Logger.getLogger(TrabajosDTO.class.getName()).log(Level.SEVERE, null, ex);
        }
        resultado.setTiempoProceso(tiempo);
        
         return resultado;
    }
        
        
//    public void importarTrabajosAPartirEjerciciosConfiguracion() {
//        try {
//            long tiempoInicial = new java.util.Date().getTime();
//
//            LOG.log(Level.INFO, "Obteniendo trabajos de ATLAS");
//            ArrayList<TrabajoRealizadoAtlas> trabajosAtlas = new ArrayList<>();
//            for (String paramEJERC : preprocesado.getEjercicios()) {
//                LOG.log(Level.INFO, "PREPROCESANDO PERIODO " + paramEJERC);
//                ArrayList<TrabajoRealizadoAtlas> tmp = null;//trabajosAtlasDAO.obtenerTrabajosPeriodo(paramEJERC);
//                int n = tmp.size();
//                trabajosAtlas.addAll(tmp);
//                int m = insertarTrabajos(tmp);
//
//                LOG.log(Level.INFO, "Numero de informes importados: " + n + " insertados= " + m);
//            }
//
//        //    LOG.log(Level.INFO, "BORRO CACHE DE TRABAJOS");
//    
//            insertarTrabajos(trabajosAtlas);
//            long tiempoFinal = new java.util.Date().getTime();
//
//            long tiempo = (tiempoFinal - tiempoInicial) / 1000;
//
//            LOG.log(Level.INFO, "tiempo proceso  " + tiempo);
//
//        } catch (SQLException ex) {
//            Logger.getLogger(TrabajosDTO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }

    public ConnectionService getConnection() {
        return connection;
    }

    public void setConnection(ConnectionService connection) {
        this.connection = connection;
    }

    public Preprocesador getPreprocesado() {
        return preprocesado;
    }

    public void setPreprocesado(Preprocesador preprocesado) {
        this.preprocesado = preprocesado;
    }          
           
     
}
