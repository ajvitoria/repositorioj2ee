package es.icac.meh.pandora.services.impl;

import es.icac.meh.pandora.modelo.ConnectionConfiguration;
import es.icac.meh.pandora.services.ConnectionService;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class ConnectionServiceImpl implements ConnectionService, DisposableBean, InitializingBean {

    private ConnectionConfiguration configuracion;
    private static Connection conn = null;
    private static final Logger LOG = Logger.getLogger(ConnectionServiceImpl.class.getName());
    
    String ipServer;
    String username;
    String password;
    String instance;
    String nombreBD;
    String driver;
    
    public ConnectionServiceImpl() {
    }

    public ConnectionServiceImpl(ConnectionConfiguration configuracion) {
        this.configuracion = configuracion;
    }



    public void inicia() {
        LOG.log(Level.INFO, "============Iniciando servicio de conexion. si se esta viendo esto... REVISAR!!!=====");
    
    }
    
    public void destruye() {
        LOG.log(Level.INFO, "=================Destruyendo servicio de conexion=====");
        ConnectionServiceImpl.cerrarConexion();
    }

    private  Connection createConnection() {
        Connection conn = null;



   LOG.log(Level.INFO, "******************Registrando Driver****************");
        /*
        private String url;
        private String username;
        private String password;
        private String instance;
        private String nombreBD;
         */
    

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
      
              LOG.log(Level.SEVERE, "NO TIENE REGISTRADO EL DRIVER");
            ex.printStackTrace();
        }

        try {

            //jdbc:sqlserver://localhost\SQLEXPRESS:1433;databaseName=db_nuevoROAC
            // String dbURL = "jdbc:sqlserver://172.24.204.40\\DIBAL";
            //   String dbURL = "jdbc:sqlserver://localhost:1433;databaseName=db_nuevoROAC";
            //jdbc:sqlserver://localhost\\NOMBRE_INSTANCIA;databaseName=BASE_DE_DATOS.
//             String dbURL = "jdbc:sqlserver://localhost\\db_nuevoROAC";
                   String dbURL ="jdbc:sqlserver://"+this.ipServer+"\\"+this.instance+";databaseName="+this.nombreBD;
        //    String user = "admin"; //icac_mad";
       //     String pass = "Junio01+";//Ic@c2013";
 LOG.log(Level.INFO, "conectando a "+dbURL);
 
            conn = DriverManager.getConnection(dbURL, username, password);
            if (conn != null) {
                DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
                LOG.log(Level.INFO, "Driver : {0}", dm.getDriverName());
                LOG.log(Level.INFO, "Driver version: {0}", dm.getDriverVersion());
                LOG.log(Level.INFO, "Product nombre: {0}", dm.getDatabaseProductName());
                LOG.log(Level.INFO, "Product version: {0}", dm.getDatabaseProductVersion());
            }

        } catch (SQLException ex) {
             LOG.log(Level.SEVERE, "Error CREANDO la conexion a la base de datos");
            ex.printStackTrace();
        } finally {
            cerrarConexion();
        }
        return conn;
    }

    public static void cerrarConexion() {
        LOG.log(Level.INFO, "cerrando la conexion>>>cerrarConexion()");
        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Error cerrando la conexion a la base de datos");
             LOG.log(Level.SEVERE, (Supplier<String>) ex);
            //ex.printStackTrace();
        }
    }

 
    public Connection getConnection() throws SQLException {

        if (conn != null) {
            if (conn.isClosed()) {
                conn = createConnection();
            } else {
                return conn;
            }
        } else {
            conn = createConnection();
        }
        return conn;
    }

    @Override
    public void destroy() throws Exception {
        LOG.log(Level.INFO, "******DESTRUYENDO EL EL BEAN destroy() ********");
        ConnectionServiceImpl.cerrarConexion();
    }

    public ConnectionConfiguration getConfiguracion() {
        return configuracion;
    }

    public void setConfiguracion(ConnectionConfiguration configuracion) {
        this.configuracion = configuracion;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        LOG.log(Level.INFO, "========  Estableciendo configuración de la conexion =====");

        this.ipServer = configuracion.getIpServer();//"localhost";
        this.instance = configuracion.getInstance();
        this.nombreBD = configuracion.getNombreBD();
        this.driver = configuracion.getDriver();
        this.username = configuracion.getUsername();
        this.password = configuracion.getPassword();

        LOG.log(Level.INFO, "ipServer: {0}", ipServer);
        LOG.log(Level.INFO, "instance: {0}", instance);
        LOG.log(Level.INFO, "nombreBD: {0}", nombreBD);
        LOG.log(Level.INFO, "driver: {0}", driver);
        LOG.log(Level.INFO, "username: {0}", username);
        LOG.log(Level.INFO, "password: {0}", password);

    }
        public void executeEjemplo(String sql) throws SQLException {

        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        ResultSetMetaData rsmd = rs.getMetaData();
        int cols = rsmd.getColumnCount();
        System.out.printf("La consulta carga %d columnas\n", cols);
        System.out.println("Estas columnas son: ");
        for (int i = 1; i <= cols; i++) {
            String colName = rsmd.getColumnName(i);
            String colType = rsmd.getColumnTypeName(i);
            System.out.println(colName + " de tipo" + colType);
        }

    }
    public ResultSet execute(String sql) throws SQLException {

        Statement stmt = conn.createStatement();
        return stmt.executeQuery(sql);


    }
}
