/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.icac.meh.pandora.services;

import es.icac.meh.pandora.modelo.Apunte;
import es.icac.meh.pandora.modelo.Recaudacion;
import es.icac.meh.pandora.modelo.TrabajoRealizado;

import es.icac.meh.pandora.services.daos.GestorApuntesServiceDAO;

import java.sql.SQLException;
import java.util.ArrayList;


import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author MAJIVIAL
 */
public class ServicioCruces {

    private static final Logger LOG = Logger.getLogger(ServicioCruces.class.getName());
        
    private es.icac.meh.pandora.services.daos.TrabajosRealizadosDAO trabajosDAO;
    private es.icac.meh.pandora.services.daos.RecaudacionesDAO cobrosDAO;
    private GestorApuntesServiceDAO apuntesDAO;
    
    public static void main(String[] args) {
        ServicioCruces ser=new ServicioCruces();
        ser.generarCruces("2019");
        
    }
    
    ArrayList<Apunte> diario;
    ArrayList<TrabajoRealizado> trabajos; 
    
    ServicioCruces(){
        
      
        ArrayList<Apunte> diario=new ArrayList();
    //    String anyoEjercicio="2019";
    }
    
    public void generarCruces(String anyoEjercicicio){
      
        try {
            trabajos = trabajosDAO.obtenerTrabajosEjercicio(anyoEjercicicio);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Error obteniendo los trabajos del DAO de trabajos _ trabajosDAO ", ex);
        }

        ArrayList<Recaudacion> cobros;
        try {
            cobros = cobrosDAO.obtenerRecaudacionesByEjercicio(anyoEjercicicio);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Error obteniendo los trabajos del DAO de trabajos _ trabajosDAO ", ex);
        }


        double saldo = 0;
        double saldoH = 0;
        double saldoD = 0;
        try {
            apuntesDAO.insertarApuntes(diario, "2020");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "ERROR INSERTANDO EL LOTE DE APUNTES", ex);
        }

        for (Apunte tmp : diario) {

            if (tmp.esDebe()) {
                //     System.out.print("\t" + tmp.getImporteString());
                saldoD += tmp.getImporte();
                saldo += tmp.getImporte();
            } else {
                //       System.out.print("\t\t\t" + tmp.getImporteString());
                saldoH += tmp.getImporte();
                saldo -= tmp.getImporte();
            }
        }
        System.out.println("\n*************TOTALES**************\t" + saldoD + "\t" + saldoH);
        System.out.println("\n**************SALDO***************\t" + saldo);
    }

    public es.icac.meh.pandora.services.daos.TrabajosRealizadosDAO getTrabajosDAO() {
        return trabajosDAO;
    }

    public void setTrabajosDAO(es.icac.meh.pandora.services.daos.TrabajosRealizadosDAO trabajosDAO) {
        this.trabajosDAO = trabajosDAO;
    }

    public es.icac.meh.pandora.services.daos.RecaudacionesDAO getRecaudacionsDAO() {
        return cobrosDAO;
    }

    public void setRecaudacionsDAO(es.icac.meh.pandora.services.daos.RecaudacionesDAO cobrosDAO) {
        this.cobrosDAO = cobrosDAO;
    }

    public GestorApuntesServiceDAO getApuntesDAO() {
        return apuntesDAO;
    }

    public void setApuntesDAO(GestorApuntesServiceDAO apuntesDAO) {
        this.apuntesDAO = apuntesDAO;
    }
}
/*




CREATE TABLE T_APUNTES (
            id INT NOT NULL IDENTITY PRIMARY KEY,
            FK_loteTrabajo INT,    
            codRoac VARCHAR(125),
            identificacion VARCHAR(255),
	    nifAuditor VARCHAR(125),
            documento VARCHAR(125),
            razonSocial VARCHAR(255),
            calcTrimestre INT,
            calcEjercicio INT,
            descripcionTipoTrabajo VARCHAR(255),
            informeDeCuentas VARCHAR(255),
            fechaInforme datetime,
            facturacionAuditoriaHonorarios FLOAT,
            coauditoria BIT,
            esInteresPublico BIT,
            tasa FLOAT,
            idTrabajoAtlas INT
	)


*/
