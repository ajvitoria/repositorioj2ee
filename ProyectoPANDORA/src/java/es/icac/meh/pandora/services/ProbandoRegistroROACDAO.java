
package es.icac.meh.pandora.services;

import es.icac.meh.pandora.modelo.RegistroROAC;
import es.icac.meh.pandora.services.impl.ConnectionServiceImpl;
import es.icac.meh.pandora.modelo.TrabajoRealizadoAtlas;

import es.icac.meh.pandora.services.ConnectionService;
import es.icac.meh.pandora.services.daos.DocumentoRoacDAO;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author MAJIVIAL
 */
public class ProbandoRegistroROACDAO {

    public static void main(String[] args) throws SQLException {
long tiempoInicio=new Date().getTime();
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("config/applicationContext.xml");
        System.out.println("=====LISTA DE BEANS CARGADOS=====");
        for (String tmp : applicationContext.getBeanDefinitionNames()) {
            System.out.println(" " + tmp);
        }

        ConnectionService servicio = applicationContext.getBean("service", ConnectionService.class);

  System.out.println("*******************TRABAJOS******************************");

        Connection conexionPrueba = servicio.getConnection();
        if (conexionPrueba != null) {
            
            DocumentoRoacDAO dao=   applicationContext.getBean("documentoRoacDAO", DocumentoRoacDAO.class);


            
          ArrayList<RegistroROAC>lista= dao.obtenerListaRoacs();
            for(RegistroROAC i: lista){
                  //  System.out.println(lista[i]);
                System.out.println(i);
            }
            
    Map<String, RegistroROAC> mapaByRoac = dao.obtenerMapaRoacsByCodRoac();
     for (String key : mapaByRoac.keySet()) {
                System.out.println(key + " - " + mapaByRoac.get(key));
            }
     
     
     
        for (String key : mapaByRoac.keySet())
            System.out.println(key + " - " + mapaByRoac.get(key));
     
        System.out.println();
                    Map<String, RegistroROAC> mapaByNif = dao.obtenerMapaRoacsByNif();
            for (String key : mapaByNif.keySet()) {
                System.out.println(key + " - " + mapaByNif.get(key));
            }
            
      
            System.out.println();

            String searchKey = "S2222";
            if (mapaByRoac.containsKey(searchKey)) {
                System.out.println("Encontrado AUDITOR por su ROAC " + mapaByRoac.get(searchKey) + " " + searchKey + " \n");
            } else {
                System.out.println("NO ENCONTRADO AUDITOR con ROAC" + searchKey);
            }


 
            String searcNIF = "B70114723";
            if (mapaByNif.containsKey(searcNIF)) {
                System.out.println("Encontrado AUDITOR por su NIF " + mapaByNif.get(searcNIF) + " " + searcNIF + " \n");
            } else {
                System.out.println("NO ENCONTRADO AUDITOR con nif" + searcNIF);
            }


        
        
        
        }//comprobar si la conexion esta bin
        
        
        
        
        
        
        
        
        
        System.out.println("########### AHORA A CERRAR LA APLICACION #########");
      //  applicationContext.close();
        applicationContext.registerShutdownHook();
        
        long tiempoFin=new Date().getTime();
          System.out.println("###########tiempo transcurrido="+((tiempoFin-tiempoInicio)/1000)+"segundos");
        

    }
    
    
    

}
