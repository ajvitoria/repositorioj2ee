
package es.icac.meh.pandora.services;

import es.icac.meh.pandora.modelo.ConnectionConfiguration;
import java.sql.Connection;
import java.sql.SQLException;
//un comentario
/**
 *
 * @author MAJIVIAL
 */
public interface ConnectionService {

    public Connection getConnection() throws SQLException;

    public ConnectionConfiguration getConfiguracion();
}
