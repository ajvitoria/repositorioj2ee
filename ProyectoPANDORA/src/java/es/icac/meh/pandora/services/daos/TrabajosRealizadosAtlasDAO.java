package es.icac.meh.pandora.services.daos;

import es.icac.meh.pandora.modelo.TrabajoRealizadoAtlas;
import es.icac.meh.pandora.services.ConnectionService;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MAJIVIAL
 */
public class TrabajosRealizadosAtlasDAO {

    private static final Logger LOG = Logger.getLogger(TrabajosRealizadosAtlasDAO.class.getName());

    private ConnectionService connection;

    public TrabajosRealizadosAtlasDAO(ConnectionService connection) {
        this.connection = connection;


    }

    
        String queryPrueba = "SELECT DISTINCT isnull(d.COD_ROAC, '') AS COD_ROAC, "
            + "CASE WHEN aud.Nombre is null or aud.Nombre = '' "
            + "THEN aud.Razon_Social\n"
            + "ELSE aud.Apellidos + ', ' + aud.Nombre\n"
            + "END as Identificacion,\n"
            + "isnull(i.Documento, '') AS Documento,\n"
            + "d.Ejercicio AS Ejercicio,"    
            + " isnull(REPLACE(i.RazonSocial,'\"',''), '') AS RazonSocial,\n"
            + "[dbo].[FormatearInformeCuentas](InformeDeCuentas) AS InformeDeCuentas,\n"
            + "InformeDeCuentas AS InformeDeCuentasReal	AS InformeDeCuentasReal, \n"
            + "dbo.[FormatearFecha](PrimerEjercicioAuditado) AS PrimerEjercicioAuditado,\n"
            + "dbo.[FormatearFecha](EjercicioAuditado) AS EjercicioAuditado,\n"
            + "dbo.[FormatearCharSiNo](Constitucion) AS Constitucion,\n"
            + "dbo.[FormatearFecha](EjercicioFinal) AS EjercicioFinal,\n"
            + "isnull(i.IdEntidad, '') AS IdEntidad,\n"
            + "isnull(e.Desc_Entidad, '') AS DescripcionEntidad,\n"
            + "dbo.[FormatearIntSiNo](ParticipesAuditado) AS ParticipesAuditado,\n"
            + "dbo.[FormatearIntSiNo](ParticipesAnterior) AS ParticipesAnterior,\n"
            + "ImporteNetoAuditado AS ImporteNetoAuditado,\n"
            + "ImporteNetoAnterior AS ImporteNetoAnterior,\n"
            + "isnull(PlantillaMediaAuditado, 0) AS PlantillaMediaAuditado,\n"
            + "isnull(PlantillaMediaAnterior, 0) AS PlantillaMediaAnterior,\n"
            + "isnull(i.IdTipoTrabajo, '') AS IdTipoTrabajo,\n"
            + "isnull(t.Desc_Trabajo, '') AS DescripcionTipoTrabajo,\n"
            + "dbo.[FormatearFecha](FechaInforme) AS FechaInforme,\n"
            + "isnull(ROACAuditorFirmante, '') AS ROACAuditorFirmante,\n"
            + "isnull(NombreAuditorFirmante, '') AS NombreAuditorFirmante,\n"
            + "dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe) AS Coauditoria,\n"
            + "MC.ROACCoAuditor AS ROACCoauditor,\n"
            + "isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion,\n"
            + "isnull(o.Desc_Opinion, 0) AS DescripcionTipoOpinion,\n"
            + "isnull(i.IdProvincia, 0) AS IdProvincia,\n"
            + "isnull(p.DescProvincia, '') AS Provincia,\n"
            + "FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios,\n"
            + "FacturacionAuditoriaHoras AS FacturacionAuditoriaHoras,\n"
            + "EM.Desc_Corta AS CNMV,\n"
            + "dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0)) AS InteresPublico,\n"
            + "ImporteCifraActivo AS ImporteCifraActivo,\n"
            + "ImporteCifraActivoAnterior AS ImporteCifraActivoAnterior,\n"
            + "i.HonorariosAuditoriaInterna,\n"
            + "i.HorasAuditoriaInterna,\n"
            + "i.HonorariosDiseno,\n"
            + "i.HorasDiseno,\n"
            + "i.HonorariosOtros,\n"
            + "i.HorasOtros\n"
            + "FROM [dbo].[T_Modelo_Informe] i\n"
            + "INNER JOIN [dbo].[T_Modelo_Datos] d \n"
            + "ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio  < 9999\n"
            + "INNER JOIN [dbo].[T_Modelo_Presentacion] pr \n"
            + "ON pr.Id_Modelo = d.Id_Modelo\n"
            + "INNER JOIN [dbo].[T_Auditores_Sociedades] aud \n"
            + "ON aud.COD_ROAC = pr.COD_ROAC\n"
            + "LEFT JOIN [dbo].[T_Provincias] p ON p.IdProvincia = i.IdProvincia\n"
            + "LEFT JOIN [dbo].[T_Entidades] e ON e.Cod_Entidad = i.IdEntidad\n"
            + "AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio)\n"
            + "OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio)\n"
            + "OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null))\n"
            + "LEFT JOIN [dbo].[T_Trabajo] t ON t.Cod_Trabajo = i.IdTipoTrabajo\n"
            + "LEFT JOIN [dbo].[T_Opinion] o ON o.Cod_Opinion = i.IdTipoOpinion\n"
            + "LEFT JOIN [dbo].[T_Modelo_Coauditor] MC ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe\n"
            + "LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV\n"
            + "INNER JOIN ( \n"
            + "    SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha\n"
            + "    FROM T_Modelo_Presentacion A\n"
            + "    INNER JOIN T_Modelo_Datos B\n"
            + "    ON A.Id_Modelo = B.Id_Modelo\n"
            + "    WHERE B.Ejercicio < 9999\n"
            + "    GROUP BY A.COD_ROAC\n"
            + ") MPMAX\n"
            + "ON pr.COD_ROAC = MPMAX.COD_ROAC\n"
            + "AND  pr.Fecha_Presentacion = MPMAX.fecha \n"
            + "WHERE d.COD_ROAC like '%'+'%'\n"
            + "AND (aud.Razon_Social like '%'+'%'\n"
            + "OR aud.Apellidos + ', ' + aud.Nombre like '%'+'%')\n"
            + "AND i.Documento like '%'+'%'\n"
            + "AND i.RazonSocial like '%'+'%'\n"
            + "order by 1,4";

        
        
    public TrabajoRealizadoAtlas[] obtenerTrabajosRealizadosKK() throws SQLException {

        PreparedStatement pstmt = connection.getConnection().prepareStatement(null);
        int anoLiquidacion = 2019;

        String COD_ROAC = "%";
        String Identificacion = "%";
        String CIF_Auditada = "%";
        String RazonSocial = "%";
        /*
        String COD_ROAC         = "'%'+$[Parameter:COD_ROAC]$+'%'";
        String Identificacion   = "'%'+$[Parameter:Identificacion]$+'%'";
        String CIF_Auditada     = "'%'+$[Parameter:CIF_Auditada]$+'%'";
        String RazonSocial      = " '%'+$[Parameter:RazonSocial]$+'%'";
         */

        pstmt.setInt(1, anoLiquidacion);
        pstmt.setInt(2, anoLiquidacion);
        pstmt.setString(3, COD_ROAC);
        pstmt.setString(4, Identificacion);
        pstmt.setString(5, Identificacion);
        pstmt.setString(6, CIF_Auditada);
        pstmt.setString(7, RazonSocial);

        //   System.out.println("app.TrabajosRealizadosDAO.obtenerTrabajosRealizados()"+pstmt.toString());
        ResultSet rs = pstmt.executeQuery();

        ArrayList<TrabajoRealizadoAtlas> trabajos = new ArrayList<>();

        if (rs != null) {
            while (rs.next()) {

                //DEBEN SER EXACTAMENTE LOS MISMOS CAMPOS QUE EL POJO
                String codRoac = rs.getString("COD_ROAC");

                LOG.log(Level.INFO, "Leyedo FILAS DE LA TABLA DE TRABAJOS para el Trabajo ID=" + codRoac);

                String identificacion = rs.getString("Identificacion");
                String documento = rs.getString("Documento");
                String razonSocial = rs.getString("RazonSocial");
                String informeDeCuentas = rs.getString("InformeDeCuentas");
                int informeDeCuentasReal = rs.getInt("InformeDeCuentasReal");
                String primerEjercicioAuditado = rs.getString("PrimerEjercicioAuditado");
                String ejercicioAuditado = rs.getString("EjercicioAuditado");
                String constitucion = rs.getString("Constitucion");

                String ejercicioFinal = rs.getString("EjercicioFinal");
                int idEntidad = rs.getInt("IdEntidad");
                String descripcionEntidad = rs.getString("DescripcionEntidad");

                String participesAuditado = rs.getString("ParticipesAuditado");
                String participesAnterior = rs.getString("ParticipesAnterior");

                double importeNetoAuditado = rs.getDouble("ImporteNetoAuditado");
                double importeNetoAnterior = rs.getDouble("ImporteNetoAnterior");

                int plantillaMediaAuditado = rs.getInt("PlantillaMediaAuditado");
                int plantillaMediaAnterior = rs.getInt("PlantillaMediaAnterior");

                int idTipoTrabajo = rs.getInt("IdTipoTrabajo");
                String descripcionTipoTrabajo = rs.getString("DescripcionTipoTrabajo");

                String fechaInforme = rs.getString("FechaInforme");
                /*es la fecha importante*/

                String roacAuditorFirmante = rs.getString("ROACAuditorFirmante");
                String nombreAuditorFirmante = rs.getString("NombreAuditorFirmante");
                String coauditoria = rs.getString("Coauditoria");
                String roacCoauditor = rs.getString("ROACCoauditor");

                int idTipoOpinion = rs.getInt("IdTipoOpinion");
                String descripcionTipoOpinion = rs.getString("DescripcionTipoOpinion");

                int idProvincia = rs.getInt("IdProvincia");
                String provincia = rs.getString("Provincia");

                double facturacionAuditoriaHonorarios = rs.getDouble("FacturacionAuditoriaHonorarios");
                double facturacionAuditoriaHoras = rs.getDouble("FacturacionAuditoriaHoras");

                String cnmv = rs.getString("CNMV");
                String esInteresPublico = rs.getString("InteresPublico");

                double importeCifraActivo = rs.getDouble("ImporteCifraActivo");
                double importeCifraActivoAnterior = rs.getDouble("ImporteCifraActivoAnterior");

                double honorariosAuditoriaInterna = rs.getDouble("HonorariosAuditoriaInterna");
                double horasAuditoriaInterna = rs.getDouble("HorasAuditoriaInterna");

                double honorariosDiseno = rs.getDouble("HonorariosDiseno");
                double horasDiseno = rs.getDouble("HorasDiseno");

                double honorariosOtros = rs.getDouble("HonorariosOtros");
                double horasOtros = rs.getDouble("HorasOtros");

                TrabajoRealizadoAtlas trabajo = null;
                try {
                    trabajo = new TrabajoRealizadoAtlas();
                    trabajo.setCodRoac(codRoac);
                    trabajo.setIdentificacion(identificacion);
                    trabajo.setDocumento(documento);
                    trabajo.setRazonSocial(razonSocial);
                    trabajo.setInformeDeCuentasReal(informeDeCuentasReal);
                    trabajo.setPrimerEjercicioAuditado(primerEjercicioAuditado);
                    trabajo.setEjercicioAuditado(ejercicioAuditado);
                    trabajo.setInformeDeCuentasReal(informeDeCuentasReal);
                    
                    trabajo.setConstitucion(constitucion);
                    trabajo.setEjercicioFinal(ejercicioFinal);
                    trabajo.setIdEntidad(idEntidad);
                    trabajo.setDescripcionEntidad(descripcionEntidad);

                    trabajo.setParticipesAuditado(participesAuditado);
                    trabajo.setParticipesAnterior(participesAnterior);

                    trabajo.setImporteNetoAuditado(importeNetoAuditado);
                    trabajo.setImporteNetoAnterior(importeNetoAnterior);

                    trabajo.setPlantillaMediaAnterior(plantillaMediaAnterior);
                    trabajo.setPlantillaMediaAuditado(plantillaMediaAuditado);

                    trabajo.setIdTipoTrabajo(idTipoTrabajo);
                    trabajo.setDescripcionTipoTrabajo(descripcionTipoTrabajo);

                    trabajo.setFechaInforme(fechaInforme);
                    trabajo.setRoacAuditorFirmante(roacAuditorFirmante);
                    trabajo.setNombreAuditorFirmante(nombreAuditorFirmante);
                    trabajo.setCoauditoria(coauditoria);
                    trabajo.setRoacCoauditor(roacCoauditor);

                    trabajo.setIdTipoOpinion(idTipoOpinion);
                    trabajo.setDescripcionTipoOpinion(descripcionTipoOpinion);

                    trabajo.setIdProvincia(idProvincia);
                    trabajo.setProvincia(provincia);

                    trabajo.setFacturacionAuditoriaHonorarios(facturacionAuditoriaHonorarios);
                    trabajo.setFacturacionAuditoriaHoras(facturacionAuditoriaHoras);
                    trabajo.setCnmv(cnmv);
                    trabajo.setEsInteresPublico(esInteresPublico);
                    trabajo.setImporteCifraActivo(importeCifraActivo);
                    trabajo.setImporteCifraActivoAnterior(importeCifraActivoAnterior);
                    trabajo.setHorasAuditoriaInterna(horasAuditoriaInterna);
                    trabajo.setHonorariosAuditoriaInterna(honorariosAuditoriaInterna);
                    trabajo.setHorasDiseno(horasDiseno);
                    trabajo.setHonorariosDiseno(honorariosDiseno);
                    trabajo.setHorasOtros(horasOtros);
                    trabajo.setHonorariosOtros(honorariosOtros);

//        System.out.println("trabajo->"+trabajo);
                    trabajos.add(trabajo);

                } catch (Exception creandoTrabajoException) {
                    LOG.log(Level.SEVERE, "==============================================");
                    LOG.log(Level.SEVERE, "ERROR CREANDO TRABAJO REALIZADO " + codRoac);
                    LOG.log(Level.SEVERE, "==============================================");
                    LOG.log(Level.SEVERE, "TRABAJO REALIZADO CONSTRUIDO HASTA EXCEPCION: ");
                    LOG.log(Level.SEVERE, trabajo.toString());
                    LOG.log(Level.SEVERE, "==============================================");
                }
            }

        } else {
            LOG.log(Level.WARNING, "Intentando obtener TrabajosRealizados desde una ResultSet y este es NULL");

        }

        pstmt.close();

        return (TrabajoRealizadoAtlas[]) trabajos.toArray();
    }
//ESTA ES LA CONSULTA BUENA
    public ArrayList<TrabajoRealizadoAtlas>obtenerTrabajosCompleta() throws SQLException {


        String sqlTodos = "SELECT DISTINCT isnull(d.COD_ROAC, '') AS COD_ROAC, \n"
                + "CASE WHEN aud.Nombre is null or aud.Nombre = '' \n"
                + "THEN aud.Razon_Social \n"
                + "ELSE aud.Apellidos + ', ' + aud.Nombre \n"
                + "END as Identificacion, \n"
                + " \n"
                + "isnull(i.Documento, '') AS Documento, \n"
                + "isnull(REPLACE(i.RazonSocial,'\"',''), '') AS RazonSocial, \n"
                + "dbo.[FormatearFecha](FechaInforme) AS FechaInforme, \n"
                + "InformeDeCuentas AS InformeDeCuentasReal AS InformeDeCuentasReal, \n"
                + "[dbo].[FormatearInformeCuentas](InformeDeCuentas) AS InformeDeCuentas, \n"
                + "dbo.[FormatearFecha](PrimerEjercicioAuditado) AS PrimerEjercicioAuditado, \n"
                + "dbo.[FormatearFecha](EjercicioAuditado) AS EjercicioAuditado, \n"
                + "dbo.[FormatearCharSiNo](Constitucion) AS Constitucion, \n"
                + "dbo.[FormatearFecha](EjercicioFinal) AS EjercicioFinal, \n"
                + "isnull(i.IdEntidad, '') AS IdEntidad, \n"
                + "isnull(e.Desc_Entidad, '') AS DescripcionEntidad, \n"
                + "dbo.[FormatearIntSiNo](ParticipesAuditado) AS ParticipesAuditado, \n"
                + "dbo.[FormatearIntSiNo](ParticipesAnterior) AS ParticipesAnterior, \n"
                + "ImporteNetoAuditado AS ImporteNetoAuditado, \n"
                + "ImporteNetoAnterior AS ImporteNetoAnterior, \n"
                + "isnull(PlantillaMediaAuditado, 0) AS PlantillaMediaAuditado, \n"
                + "isnull(PlantillaMediaAnterior, 0) AS PlantillaMediaAnterior, \n"
                + "isnull(i.IdTipoTrabajo, '') AS IdTipoTrabajo, \n"
                + "isnull(t.Desc_Trabajo, '') AS DescripcionTipoTrabajo, \n"
                + "isnull(ROACAuditorFirmante, '') AS ROACAuditorFirmante, \n"
                + "isnull(NombreAuditorFirmante, '') AS NombreAuditorFirmante, \n"
                + "dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe) AS Coauditoria, \n"
                + "MC.ROACCoAuditor AS ROACCoauditor, \n"
                + "isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion, \n"
                + "isnull(o.Desc_Opinion, 0) AS DescripcionTipoOpinion, \n"
                + "isnull(i.IdProvincia, 0) AS IdProvincia, \n"
                + "isnull(p.DescProvincia, '') AS Provincia, \n"
                + "FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios, \n"
                + "FacturacionAuditoriaHoras AS FacturacionAuditoriaHoras, \n"
                + "EM.Desc_Corta AS CNMV, \n"
                + "dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0)) AS InteresPublico, \n"
                + "ImporteCifraActivo AS ImporteCifraActivo, \n"
                + "ImporteCifraActivoAnterior AS ImporteCifraActivoAnterior, \n"
                + "i.HonorariosAuditoriaInterna, \n"
                + "i.HorasAuditoriaInterna, \n"
                + "i.HonorariosDiseno, \n"
                + "i.HorasDiseno, \n"
                + "i.HonorariosOtros, \n"
                + "i.HorasOtros \n"
                + "FROM [dbo].[T_Modelo_Informe] i \n"
                + "INNER JOIN [dbo].[T_Modelo_Datos] d		ON i.Id_Modelo = d.Id_Modelo  \n"
                + "INNER JOIN [dbo].[T_Modelo_Presentacion] pr	ON pr.Id_Modelo = d.Id_Modelo \n"
                + "INNER JOIN [dbo].[T_Auditores_Sociedades] aud	ON aud.COD_ROAC = pr.COD_ROAC \n"
                + "LEFT JOIN [dbo].[T_Provincias] p		ON p.IdProvincia = i.IdProvincia \n"
                + "LEFT JOIN [dbo].[T_Entidades] e		ON e.Cod_Entidad = i.IdEntidad AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio) OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio) OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null))  \n"
                + "LEFT JOIN [dbo].[T_Trabajo] t                 ON t.Cod_Trabajo = i.IdTipoTrabajo \n"
                + "LEFT JOIN [dbo].[T_Opinion] o			ON o.Cod_Opinion = i.IdTipoOpinion \n"
                + "LEFT JOIN [dbo].[T_Modelo_Coauditor] MC	ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe \n"
                + "LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM	ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV \n"
                + "INNER JOIN (  \n"
                + "    SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha \n"
                + "    FROM T_Modelo_Presentacion A \n"
                + "    INNER JOIN T_Modelo_Datos B \n"
                + "    ON A.Id_Modelo = B.Id_Modelo \n"
                + "    GROUP BY A.COD_ROAC \n"
                + ") MPMAX \n"
                + "ON pr.COD_ROAC = MPMAX.COD_ROAC AND  pr.Fecha_Presentacion = MPMAX.fecha  \n"
                + "            \n"
                + "order by 1,5";
        
        
        System.out.println("kk_>SQL=\n" + sqlTodos);

        Statement pstmt = connection.getConnection().createStatement();

        // int anoLiquidacion = 2019;
        ResultSet rs = pstmt.executeQuery(sqlTodos);

        ArrayList<TrabajoRealizadoAtlas> trabajos = new ArrayList<>();

        if (rs != null) {
            while (rs.next()) {

                //DEBEN SER EXACTAMENTE LOS MISMOS CAMPOS QUE EL POJO
                String codRoac = rs.getString("COD_ROAC");

                //         LOG.log(Level.INFO, "Leyedo FILAS DE LA TABLA DE TRABAJOS para el Trabajo ID="+codRoac);
                String identificacion = rs.getString("Identificacion");
                String documento = rs.getString("Documento");
                String razonSocial = rs.getString("RazonSocial");
                
               
            
                //        String primerEjercicioAuditado=rs.getString("PrimerEjercicioAuditado");
                String ejercicioAuditado = rs.getString("EjercicioAuditado");
                //        String constitucion=rs.getString("Constitucion");

                String ejercicioFinal = rs.getString("EjercicioFinal");
                //       int idEntidad=rs.getInt("IdEntidad");
                //       String descripcionEntidad=rs.getString("DescripcionEntidad");

                //      String participesAuditado=rs.getString("ParticipesAuditado");
                //       String participesAnterior=rs.getString("ParticipesAnterior");
                double importeNetoAuditado = rs.getDouble("ImporteNetoAuditado");
                //        double importeNetoAnterior=rs.getDouble("ImporteNetoAnterior");

                //          int plantillaMediaAuditado=rs.getInt("PlantillaMediaAuditado");           
                //         int plantillaMediaAnterior=rs.getInt("PlantillaMediaAnterior");
                int idTipoTrabajo = rs.getInt("IdTipoTrabajo");
                int informeDeCuentasReal= rs.getInt("IdTipoTrabajo");
                String descripcionTipoTrabajo = rs.getString("DescripcionTipoTrabajo");

                String fechaInforme = rs.getString("FechaInforme");
                /*es la fecha importante*/


                String roacAuditorFirmante = rs.getString("ROACAuditorFirmante");

                String nombreAuditorFirmante = rs.getString("NombreAuditorFirmante");
                String coauditoria = rs.getString("Coauditoria");
                String roacCoauditor = rs.getString("ROACCoauditor");

//    int idTipoOpinion=rs.getInt("IdTipoOpinion");
                String descripcionTipoOpinion = rs.getString("DescripcionTipoOpinion");

                //     int idProvincia=rs.getInt("IdProvincia");
                String provincia = rs.getString("Provincia");

                double facturacionAuditoriaHonorarios = rs.getDouble("FacturacionAuditoriaHonorarios");
                double facturacionAuditoriaHoras = rs.getDouble("FacturacionAuditoriaHoras");

                String cnmv = rs.getString("CNMV");
                String esInteresPublico = rs.getString("InteresPublico");
     
                double importeCifraActivo = rs.getDouble("ImporteCifraActivo");

                TrabajoRealizadoAtlas trabajo = null;
                try {
                    trabajo = new TrabajoRealizadoAtlas();
                    trabajo.setCodRoac(codRoac);
                    trabajo.setIdentificacion(identificacion);
                    trabajo.setDocumento(documento);
                    trabajo.setRazonSocial(razonSocial);
          trabajo.setInformeDeCuentasReal(informeDeCuentasReal);
                    trabajo.setEjercicioAuditado(ejercicioAuditado);

                    trabajo.setEjercicioFinal(ejercicioFinal);

                    trabajo.setImporteNetoAuditado(importeNetoAuditado);

                    trabajo.setIdTipoTrabajo(idTipoTrabajo);
                    trabajo.setDescripcionTipoTrabajo(descripcionTipoTrabajo);

                    trabajo.setFechaInforme(fechaInforme);
                    trabajo.setRoacAuditorFirmante(roacAuditorFirmante);
                    trabajo.setNombreAuditorFirmante(nombreAuditorFirmante);
                    trabajo.setCoauditoria(coauditoria);
                    trabajo.setEsInteresPublico(esInteresPublico);
                    trabajo.setRoacCoauditor(roacCoauditor);

                    trabajo.setDescripcionTipoOpinion(descripcionTipoOpinion);
                    trabajo.setProvincia(provincia);

                    trabajo.setFacturacionAuditoriaHonorarios(facturacionAuditoriaHonorarios);
                    trabajo.setFacturacionAuditoriaHoras(facturacionAuditoriaHoras);
                    trabajo.setCnmv(cnmv);
                    trabajo.setEsInteresPublico(esInteresPublico);
                    trabajo.setImporteCifraActivo(importeCifraActivo);

                    trabajos.add(trabajo);

                } catch (Exception creandoTrabajoException) {
                    LOG.log(Level.SEVERE, "==============================================");
                    LOG.log(Level.SEVERE, "ERROR CREANDO TRABAJO REALIZADO " + codRoac, creandoTrabajoException);
                    LOG.log(Level.SEVERE, "==============================================");
                    LOG.log(Level.SEVERE, "TRABAJO REALIZADO CONSTRUIDO HASTA EXCEPCION: ");
                    LOG.log(Level.SEVERE, trabajo != null ? trabajo.toString() : "NULL");
                    LOG.log(Level.SEVERE, "==============================================");
                }
            }
        } else {
            LOG.log(Level.WARNING, "Intentando obtener TrabajosRealizados desde una ResultSet y este es NULL");
        }
        pstmt.close();

        return trabajos;
    }  
    
    
    
    
//    String sql = "SELECT DISTINCT isnull(d.COD_ROAC, '') AS COD_ROAC, "
//            + "CASE WHEN aud.Nombre is null or aud.Nombre = '' "
//            + "THEN aud.Razon_Social "
//            + "ELSE aud.Apellidos + ', ' + aud.Nombre "
//            + "END as Identificacion, "
//            + "isnull(i.Documento, '') AS Documento, "
//            + " isnull(REPLACE(i.RazonSocial,'\"',''), '') AS RazonSocial, "
//            + "[dbo].[FormatearInformeCuentas](InformeDeCuentas) AS InformeDeCuentas, "
//            + "dbo.[FormatearFecha](PrimerEjercicioAuditado) AS PrimerEjercicioAuditado, "
//            + "dbo.[FormatearFecha](EjercicioAuditado) AS EjercicioAuditado, "
//            + "dbo.[FormatearCharSiNo](Constitucion) AS Constitucion, "
//            + "dbo.[FormatearFecha](EjercicioFinal) AS EjercicioFinal, "
//            + "isnull(i.IdEntidad, '') AS IdEntidad, "
//            + "isnull(e.Desc_Entidad, '') AS DescripcionEntidad, "
//            + "dbo.[FormatearIntSiNo](ParticipesAuditado) AS ParticipesAuditado, "
//            + "dbo.[FormatearIntSiNo](ParticipesAnterior) AS ParticipesAnterior, "
//            + "ImporteNetoAuditado AS ImporteNetoAuditado, "
//            + "ImporteNetoAnterior AS ImporteNetoAnterior, "
//            + "isnull(PlantillaMediaAuditado, 0) AS PlantillaMediaAuditado, "
//            + "isnull(PlantillaMediaAnterior, 0) AS PlantillaMediaAnterior, "
//            + "isnull(i.IdTipoTrabajo, '') AS IdTipoTrabajo, "
//            + "isnull(t.Desc_Trabajo, '') AS DescripcionTipoTrabajo, "
//            + "dbo.[FormatearFecha](FechaInforme) AS FechaInforme, "
//            + "isnull(ROACAuditorFirmante, '') AS ROACAuditorFirmante, "
//            + "isnull(NombreAuditorFirmante, '') AS NombreAuditorFirmante, "
//            + "dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe) AS Coauditoria, "
//            + "MC.ROACCoAuditor AS ROACCoauditor, "
//            + "isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion, "
//            + "isnull(o.Desc_Opinion, 0) AS DescripcionTipoOpinion, "
//            + "isnull(i.IdProvincia, 0) AS IdProvincia, "
//            + "isnull(p.DescProvincia, '') AS Provincia, "
//            + "FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios, "
//            + "FacturacionAuditoriaHoras AS FacturacionAuditoriaHoras, "
//            + "EM.Desc_Corta AS CNMV, "
//            + "dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0)) AS InteresPublico, "
//            + "ImporteCifraActivo AS ImporteCifraActivo, "
//            + "ImporteCifraActivoAnterior AS ImporteCifraActivoAnterior, "
//            + "i.HonorariosAuditoriaInterna, "
//            + "i.HorasAuditoriaInterna, "
//            + "i.HonorariosDiseno, "
//            + "i.HorasDiseno, "
//            + "i.HonorariosOtros, "
//            + "i.HorasOtros "
//            + "FROM [dbo].[T_Modelo_Informe] i "
//            + "INNER JOIN [dbo].[T_Modelo_Datos] d  "
//            + "ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio = ? "
//            + "INNER JOIN [dbo].[T_Modelo_Presentacion] pr  "
//            + "ON pr.Id_Modelo = d.Id_Modelo "
//            + "INNER JOIN [dbo].[T_Auditores_Sociedades] aud  "
//            + "ON aud.COD_ROAC = pr.COD_ROAC "
//            + "LEFT JOIN [dbo].[T_Provincias] p ON p.IdProvincia = i.IdProvincia "
//            + "LEFT JOIN [dbo].[T_Entidades] e ON e.Cod_Entidad = i.IdEntidad "
//            + "AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio) "
//            + "OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio) "
//            + "OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null)) "
//            + "LEFT JOIN [dbo].[T_Trabajo] t ON t.Cod_Trabajo = i.IdTipoTrabajo "
//            + "LEFT JOIN [dbo].[T_Opinion] o ON o.Cod_Opinion = i.IdTipoOpinion "
//            + "LEFT JOIN [dbo].[T_Modelo_Coauditor] MC ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe "
//            + "LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV "
//            + "INNER JOIN (  "
//            + "    SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha "
//            + "    FROM T_Modelo_Presentacion A "
//            + "    INNER JOIN T_Modelo_Datos B "
//            + "    ON A.Id_Modelo = B.Id_Modelo "
//            + "    WHERE B.Ejercicio = ? "
//            + "    GROUP BY A.COD_ROAC "
//            + ") MPMAX "
//            + "ON pr.COD_ROAC = MPMAX.COD_ROAC "
//            + "AND  pr.Fecha_Presentacion = MPMAX.fecha  "
//            + "WHERE d.COD_ROAC like ? "
//            + "AND (aud.Razon_Social like ? "
//            + "OR aud.Apellidos + ', ' + aud.Nombre like ? "
//            + "AND i.Documento like ? "
//            + "AND i.RazonSocial like ? "
//            + "order by 1,4 ";

    /*
"			 " +
"			PARAMETROS " +
"			 $[Parameter:EjercicioPresentado]$ " +
"			  $[Parameter:EjercicioPresentado]$ < SE REPITE " +
"			 '%'+$[Parameter:COD_ROAC]$+'%' " +
"AND (aud.Razon_Social like '%'+$[Parameter:Identificacion]$+'%' " +
"OR aud.Apellidos + ', ' + aud.Nombre like '%'+$[Parameter:Identificacion]$+'%') " +
"AND i.Documento like '%'+$[Parameter:CIF_Auditada]$+'%' " +
"AND i.RazonSocial like '%'+$[Parameter:RazonSocial]$+'%'";
     */
//    String sqlPreparada = "SELECT DISTINCT isnull(d.COD_ROAC, '') AS COD_ROAC, "
//            + "CASE WHEN aud.Nombre is null or aud.Nombre = '' "
//            + "THEN aud.Razon_Social "
//            + "ELSE aud.Apellidos + ', ' + aud.Nombre "
//            + "END as Identificacion, "
//            + "isnull(i.Documento, '') AS Documento, "
//            + " isnull(REPLACE(i.RazonSocial,'\"',''), '') AS RazonSocial, "
//            + "[dbo].[FormatearInformeCuentas](InformeDeCuentas) AS InformeDeCuentas, "
//            + "dbo.[FormatearFecha](PrimerEjercicioAuditado) AS PrimerEjercicioAuditado, "
//            + "dbo.[FormatearFecha](EjercicioAuditado) AS EjercicioAuditado, "
//            + "dbo.[FormatearCharSiNo](Constitucion) AS Constitucion, "
//            + "dbo.[FormatearFecha](EjercicioFinal) AS EjercicioFinal, "
//            + "isnull(i.IdEntidad, '') AS IdEntidad, "
//            + "isnull(e.Desc_Entidad, '') AS DescripcionEntidad, "
//            + "dbo.[FormatearIntSiNo](ParticipesAuditado) AS ParticipesAuditado, "
//            + "dbo.[FormatearIntSiNo](ParticipesAnterior) AS ParticipesAnterior, "
//            + "ImporteNetoAuditado AS ImporteNetoAuditado, "
//            + "ImporteNetoAnterior AS ImporteNetoAnterior, "
//            + "isnull(PlantillaMediaAuditado, 0) AS PlantillaMediaAuditado, "
//            + "isnull(PlantillaMediaAnterior, 0) AS PlantillaMediaAnterior, "
//            + "isnull(i.IdTipoTrabajo, '') AS IdTipoTrabajo, "
//            + "isnull(t.Desc_Trabajo, '') AS DescripcionTipoTrabajo, "
//            + "dbo.[FormatearFecha](FechaInforme) AS FechaInforme, "
//            + "isnull(ROACAuditorFirmante, '') AS ROACAuditorFirmante, "
//            + "isnull(NombreAuditorFirmante, '') AS NombreAuditorFirmante, "
//            + "dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe) AS Coauditoria, "
//            + "MC.ROACCoAuditor AS ROACCoauditor, "
//            + "isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion, "
//            + "isnull(o.Desc_Opinion, 0) AS DescripcionTipoOpinion, "
//            + "isnull(i.IdProvincia, 0) AS IdProvincia, "
//            + "isnull(p.DescProvincia, '') AS Provincia, "
//            + "FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios, "
//            + "FacturacionAuditoriaHoras AS FacturacionAuditoriaHoras, "
//            + "EM.Desc_Corta AS CNMV, "
//            + "dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0)) AS InteresPublico, "
//            + "ImporteCifraActivo AS ImporteCifraActivo, "
//            + "ImporteCifraActivoAnterior AS ImporteCifraActivoAnterior, "
//            + "i.HonorariosAuditoriaInterna, "
//            + "i.HorasAuditoriaInterna, "
//            + "i.HonorariosDiseno, "
//            + "i.HorasDiseno, "
//            + "i.HonorariosOtros, "
//            + "i.HorasOtros "
//            + "FROM [dbo].[T_Modelo_Informe] i "
//            + "INNER JOIN [dbo].[T_Modelo_Datos] d  "
//            + "ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio = ? "
//            + "INNER JOIN [dbo].[T_Modelo_Presentacion] pr  "
//            + "ON pr.Id_Modelo = d.Id_Modelo "
//            + "INNER JOIN [dbo].[T_Auditores_Sociedades] aud  "
//            + "ON aud.COD_ROAC = pr.COD_ROAC "
//            + "LEFT JOIN [dbo].[T_Provincias] p ON p.IdProvincia = i.IdProvincia "
//            + "LEFT JOIN [dbo].[T_Entidades] e ON e.Cod_Entidad = i.IdEntidad "
//            + "AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio) "
//            + "OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio) "
//            + "OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null)) "
//            + "LEFT JOIN [dbo].[T_Trabajo] t ON t.Cod_Trabajo = i.IdTipoTrabajo "
//            + "LEFT JOIN [dbo].[T_Opinion] o ON o.Cod_Opinion = i.IdTipoOpinion "
//            + "LEFT JOIN [dbo].[T_Modelo_Coauditor] MC ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe "
//            + "LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV "
//            + "INNER JOIN (  "
//            + "    SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha "
//            + "    FROM T_Modelo_Presentacion A "
//            + "    INNER JOIN T_Modelo_Datos B "
//            + "    ON A.Id_Modelo = B.Id_Modelo "
//            + "    WHERE B.Ejercicio =  ? "
//            + "    GROUP BY A.COD_ROAC "
//            + ") MPMAX "
//            + "ON pr.COD_ROAC = MPMAX.COD_ROAC "
//            + "AND  pr.Fecha_Presentacion = MPMAX.fecha "
//            + "WHERE d.COD_ROAC like ? "
//            + "AND (aud.Razon_Social like ? "
//            + "OR aud.Apellidos + ', ' + aud.Nombre like ? "
//            + "AND i.Documento like ? "
//            + "AND i.RazonSocial like ? ";

//           order by 1,4";
    /*
"			 " +
"			PARAMETROS " +
"			 $[Parameter:EjercicioPresentado]$ " +
"			  $[Parameter:EjercicioPresentado]$ < SE REPITE " +
"			 '%'+$[Parameter:COD_ROAC]$+'%' " +
"AND (aud.Razon_Social like '%'+$[Parameter:Identificacion]$+'%' " +
"OR aud.Apellidos + ', ' + aud.Nombre like '%'+$[Parameter:Identificacion]$+'%') " +
"AND i.Documento like '%'+$[Parameter:CIF_Auditada]$+'%' " +
"AND i.RazonSocial like '%'+$[Parameter:RazonSocial]$+'%'";
     */
//    public ArrayList<TrabajoRealizadoAtlas> obtenerTrabajosPrueba() throws SQLException {
//
//        Statement pstmt = connection.getConnection().createStatement();
//
//        //      System.out.println("app.TrabajosRealizadosDAO.obtenerTrabajosRealizados()"+pstmt.toString());
//        ResultSet rs = pstmt.executeQuery(queryPrueba);
//
//        ArrayList<TrabajoRealizadoAtlas> trabajos = new ArrayList<>();
//
//        if (rs != null) {
//            while (rs.next()) {
//
//                //DEBEN SER EXACTAMENTE LOS MISMOS CAMPOS QUE EL POJO
//                String codRoac = rs.getString("COD_ROAC");
//
//                //         LOG.log(Level.INFO, "Leyedo FILAS DE LA TABLA DE TRABAJOS para el Trabajo ID="+codRoac);
//                String identificacion = rs.getString("Identificacion");
//                String documento = rs.getString("Documento");
//                String razonSocial = rs.getString("RazonSocial");
//                String informeDeCuentas = rs.getString("InformeDeCuentas");
//                String primerEjercicioAuditado = rs.getString("PrimerEjercicioAuditado");
//                String ejercicioAuditado = rs.getString("EjercicioAuditado");
//                String constitucion = rs.getString("Constitucion");
//
//                String ejercicioFinal = rs.getString("EjercicioFinal");
//                int idEntidad = rs.getInt("IdEntidad");
//                String descripcionEntidad = rs.getString("DescripcionEntidad");
//
//                String participesAuditado = rs.getString("ParticipesAuditado");
//                String participesAnterior = rs.getString("ParticipesAnterior");
//
//                double importeNetoAuditado = rs.getDouble("ImporteNetoAuditado");
//                double importeNetoAnterior = rs.getDouble("ImporteNetoAnterior");
//
//                int plantillaMediaAuditado = rs.getInt("PlantillaMediaAuditado");
//                int plantillaMediaAnterior = rs.getInt("PlantillaMediaAnterior");
//
//                int idTipoTrabajo = rs.getInt("IdTipoTrabajo");
//                String descripcionTipoTrabajo = rs.getString("DescripcionTipoTrabajo");
//
//                String fechaInforme = rs.getString("FechaInforme");
//                /*es la fecha importante*/
//
//
//                String roacAuditorFirmante = rs.getString("ROACAuditorFirmante");
//
//                String nombreAuditorFirmante = rs.getString("NombreAuditorFirmante");
//                String coauditoria = rs.getString("Coauditoria");
//                String roacCoauditor = rs.getString("ROACCoauditor");
//
//                int idTipoOpinion = rs.getInt("IdTipoOpinion");
//                String descripcionTipoOpinion = rs.getString("DescripcionTipoOpinion");
//
//                int idProvincia = rs.getInt("IdProvincia");
//                String provincia = rs.getString("Provincia");
//
//                double facturacionAuditoriaHonorarios = rs.getDouble("FacturacionAuditoriaHonorarios");
//                double facturacionAuditoriaHoras = rs.getDouble("FacturacionAuditoriaHoras");
//
//                String cnmv = rs.getString("CNMV");
//                String esInteresPublico = rs.getString("InteresPublico");
//
//                double importeCifraActivo = rs.getDouble("ImporteCifraActivo");
//                double importeCifraActivoAnterior = rs.getDouble("ImporteCifraActivoAnterior");
//
//                double honorariosAuditoriaInterna = rs.getDouble("HonorariosAuditoriaInterna");
//                double horasAuditoriaInterna = rs.getDouble("HorasAuditoriaInterna");
//
//                double honorariosDiseno = rs.getDouble("HonorariosDiseno");
//                double horasDiseno = rs.getDouble("HorasDiseno");
//
//                double honorariosOtros = rs.getDouble("HonorariosOtros");
//                double horasOtros = rs.getDouble("HorasOtros");
//
//                TrabajoRealizadoAtlas trabajo = null;
//                try {
//                    trabajo = new TrabajoRealizadoAtlas();
//                    trabajo.setCodRoac(codRoac);
//                    trabajo.setIdentificacion(identificacion);
//                    trabajo.setDocumento(documento);
//                    trabajo.setRazonSocial(razonSocial);
//                    trabajo.setInformeDeCuentas(informeDeCuentas);
//                    trabajo.setPrimerEjercicioAuditado(primerEjercicioAuditado);
//                    trabajo.setEjercicioAuditado(ejercicioAuditado);
//
//                    trabajo.setConstitucion(constitucion);
//                    trabajo.setEjercicioFinal(ejercicioFinal);
//                    trabajo.setIdEntidad(idEntidad);
//                    trabajo.setDescripcionEntidad(descripcionEntidad);
//
//                    trabajo.setParticipesAuditado(participesAuditado);
//                    trabajo.setParticipesAnterior(participesAnterior);
//
//                    trabajo.setImporteNetoAuditado(importeNetoAuditado);
//                    trabajo.setImporteNetoAnterior(importeNetoAnterior);
//
//                    trabajo.setPlantillaMediaAnterior(plantillaMediaAnterior);
//                    trabajo.setPlantillaMediaAuditado(plantillaMediaAuditado);
//
//                    trabajo.setIdTipoTrabajo(idTipoTrabajo);
//                    trabajo.setDescripcionTipoTrabajo(descripcionTipoTrabajo);
//
//                    trabajo.setFechaInforme(fechaInforme);
//                    trabajo.setRoacAuditorFirmante(roacAuditorFirmante);
//                    trabajo.setNombreAuditorFirmante(nombreAuditorFirmante);
//                    trabajo.setCoauditoria(coauditoria);
//                    trabajo.setRoacCoauditor(roacCoauditor);
//
//                    trabajo.setIdTipoOpinion(idTipoOpinion);
//                    trabajo.setDescripcionTipoOpinion(descripcionTipoOpinion);
//
//                    trabajo.setIdProvincia(idProvincia);
//                    trabajo.setProvincia(provincia);
//
//                    trabajo.setFacturacionAuditoriaHonorarios(facturacionAuditoriaHonorarios);
//                    trabajo.setFacturacionAuditoriaHoras(facturacionAuditoriaHoras);
//                    trabajo.setCnmv(cnmv);
//                    trabajo.setEsInteresPublico(esInteresPublico);
//                    trabajo.setImporteCifraActivo(importeCifraActivo);
//                    trabajo.setImporteCifraActivoAnterior(importeCifraActivoAnterior);
//                    trabajo.setHorasAuditoriaInterna(horasAuditoriaInterna);
//                    trabajo.setHonorariosAuditoriaInterna(honorariosAuditoriaInterna);
//                    trabajo.setHorasDiseno(horasDiseno);
//                    trabajo.setHonorariosDiseno(honorariosDiseno);
//                    trabajo.setHorasOtros(horasOtros);
//                    trabajo.setHonorariosOtros(honorariosOtros);
//                    //        System.out.println("trabajo->"+trabajo);
//                    trabajos.add(trabajo);
//
//                } catch (Exception creandoTrabajoException) {
//                    LOG.log(Level.SEVERE, "==============================================");
//                    LOG.log(Level.SEVERE, "ERROR CREANDO TRABAJO REALIZADO " + codRoac, creandoTrabajoException);
//                    LOG.log(Level.SEVERE, "==============================================");
//                    LOG.log(Level.SEVERE, "TRABAJO REALIZADO CONSTRUIDO HASTA EXCEPCION: ");
//                    LOG.log(Level.SEVERE, trabajo != null ? trabajo.toString() : "NULL");
//                    LOG.log(Level.SEVERE, "==============================================");
//                }
//            }
//        } else {
//            LOG.log(Level.WARNING, "Intentando obtener TrabajosRealizados desde una ResultSet y este es NULL");
//        }
//        pstmt.close();
//
//        return trabajos;
//    }
//
//    String queryPrueba2 = "SELECT DISTINCT isnull(d.COD_ROAC, '') AS COD_ROAC, "
//            + "CASE WHEN aud.Nombre is null or aud.Nombre = '' "
//            + "THEN aud.Razon_Social\n"
//            + "ELSE aud.Apellidos + ', ' + aud.Nombre\n"
//            + "END as Identificacion,\n"
//            + "isnull(i.Documento, '') AS Documento,\n"
//            + " isnull(REPLACE(i.RazonSocial,'\"',''), '') AS RazonSocial,\n"
//            + "[dbo].[FormatearInformeCuentas](InformeDeCuentas) AS InformeDeCuentas,\n"
//            + "dbo.[FormatearFecha](PrimerEjercicioAuditado) AS PrimerEjercicioAuditado,\n"
//            + "dbo.[FormatearFecha](EjercicioAuditado) AS EjercicioAuditado,\n"
//            + "dbo.[FormatearCharSiNo](Constitucion) AS Constitucion,\n"
//            + "dbo.[FormatearFecha](EjercicioFinal) AS EjercicioFinal,\n"
//            + "isnull(i.IdEntidad, '') AS IdEntidad,\n"
//            + "isnull(e.Desc_Entidad, '') AS DescripcionEntidad,\n"
//            + "dbo.[FormatearIntSiNo](ParticipesAuditado) AS ParticipesAuditado,\n"
//            + "dbo.[FormatearIntSiNo](ParticipesAnterior) AS ParticipesAnterior,\n"
//            + "ImporteNetoAuditado AS ImporteNetoAuditado,\n"
//            + "ImporteNetoAnterior AS ImporteNetoAnterior,\n"
//            + "isnull(PlantillaMediaAuditado, 0) AS PlantillaMediaAuditado,\n"
//            + "isnull(PlantillaMediaAnterior, 0) AS PlantillaMediaAnterior,\n"
//            + "isnull(i.IdTipoTrabajo, '') AS IdTipoTrabajo,\n"
//            + "isnull(t.Desc_Trabajo, '') AS DescripcionTipoTrabajo,\n"
//            + "dbo.[FormatearFecha](FechaInforme) AS FechaInforme,\n"
//            + "isnull(ROACAuditorFirmante, '') AS ROACAuditorFirmante,\n"
//            + "isnull(NombreAuditorFirmante, '') AS NombreAuditorFirmante,\n"
//            + "dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe) AS Coauditoria,\n"
//            + "MC.ROACCoAuditor AS ROACCoauditor,\n"
//            + "isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion,\n"
//            + "isnull(o.Desc_Opinion, 0) AS DescripcionTipoOpinion,\n"
//            + "isnull(i.IdProvincia, 0) AS IdProvincia,\n"
//            + "isnull(p.DescProvincia, '') AS Provincia,\n"
//            + "FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios,\n"
//            + "FacturacionAuditoriaHoras AS FacturacionAuditoriaHoras,\n"
//            + "EM.Desc_Corta AS CNMV,\n"
//            + "dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0)) AS InteresPublico,\n"
//            + "ImporteCifraActivo AS ImporteCifraActivo,\n"
//            + "ImporteCifraActivoAnterior AS ImporteCifraActivoAnterior,\n"
//            + "i.HonorariosAuditoriaInterna,\n"
//            + "i.HorasAuditoriaInterna,\n"
//            + "i.HonorariosDiseno,\n"
//            + "i.HorasDiseno,\n"
//            + "i.HonorariosOtros,\n"
//            + "i.HorasOtros\n"
//            + "FROM [dbo].[T_Modelo_Informe] i\n"
//            + "INNER JOIN [dbo].[T_Modelo_Datos] d \n"
//            + "ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio  < 9999\n"
//            + "INNER JOIN [dbo].[T_Modelo_Presentacion] pr \n"
//            + "ON pr.Id_Modelo = d.Id_Modelo\n"
//            + "INNER JOIN [dbo].[T_Auditores_Sociedades] aud \n"
//            + "ON aud.COD_ROAC = pr.COD_ROAC\n"
//            + "LEFT JOIN [dbo].[T_Provincias] p ON p.IdProvincia = i.IdProvincia\n"
//            + "LEFT JOIN [dbo].[T_Entidades] e ON e.Cod_Entidad = i.IdEntidad\n"
//            + "AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio)\n"
//            + "OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio)\n"
//            + "OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null))\n"
//            + "LEFT JOIN [dbo].[T_Trabajo] t ON t.Cod_Trabajo = i.IdTipoTrabajo\n"
//            + "LEFT JOIN [dbo].[T_Opinion] o ON o.Cod_Opinion = i.IdTipoOpinion\n"
//            + "LEFT JOIN [dbo].[T_Modelo_Coauditor] MC ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe\n"
//            + "LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV\n"
//            + "INNER JOIN ( \n"
//            + "    SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha\n"
//            + "    FROM T_Modelo_Presentacion A\n"
//            + "    INNER JOIN T_Modelo_Datos B\n"
//            + "    ON A.Id_Modelo = B.Id_Modelo\n"
//            + "    WHERE B.Ejercicio < 9999\n"
//            + "    GROUP BY A.COD_ROAC\n"
//            + ") MPMAX\n"
//            + "ON pr.COD_ROAC = MPMAX.COD_ROAC\n"
//            + "AND  pr.Fecha_Presentacion = MPMAX.fecha \n"
//            + "WHERE d.COD_ROAC like '%'+'%'\n"
//            + "AND (aud.Razon_Social like '%'+'%'\n"
//            + "OR aud.Apellidos + ', ' + aud.Nombre like '%'+'%')\n"
//            + "AND i.Documento like '%'+'%'\n"
//            + "AND i.RazonSocial like '%'+'%'\n"
//            + "order by 1,4";
//
//    String queryParametrizadaKAO = "SELECT DISTINCT isnull(d.COD_ROAC, '') AS COD_ROAC, "
//            + "CASE WHEN aud.Nombre is null or aud.Nombre = '' "
//            + "THEN aud.Razon_Social "
//            + "ELSE aud.Apellidos + ', ' + aud.Nombre "
//            + "END as Identificacion, "
//            + "isnull(i.Documento, '') AS Documento, "
//            + " isnull(REPLACE(i.RazonSocial,'\"',''), '') AS RazonSocial, "
//            + "[dbo].[FormatearInformeCuentas](InformeDeCuentas) AS InformeDeCuentas, "
//            + "dbo.[FormatearFecha](PrimerEjercicioAuditado) AS PrimerEjercicioAuditado, "
//            + "dbo.[FormatearFecha](EjercicioAuditado) AS EjercicioAuditado, "
//            + "dbo.[FormatearCharSiNo](Constitucion) AS Constitucion, "
//            + "dbo.[FormatearFecha](EjercicioFinal) AS EjercicioFinal, "
//            + "isnull(i.IdEntidad, '') AS IdEntidad, "
//            + "isnull(e.Desc_Entidad, '') AS DescripcionEntidad, "
//            + "dbo.[FormatearIntSiNo](ParticipesAuditado) AS ParticipesAuditado, "
//            + "dbo.[FormatearIntSiNo](ParticipesAnterior) AS ParticipesAnterior, "
//            + "ImporteNetoAuditado AS ImporteNetoAuditado, "
//            + "ImporteNetoAnterior AS ImporteNetoAnterior, "
//            + "isnull(PlantillaMediaAuditado, 0) AS PlantillaMediaAuditado, "
//            + "isnull(PlantillaMediaAnterior, 0) AS PlantillaMediaAnterior, "
//            + "isnull(i.IdTipoTrabajo, '') AS IdTipoTrabajo, "
//            + "isnull(t.Desc_Trabajo, '') AS DescripcionTipoTrabajo, "
//            + "dbo.[FormatearFecha](FechaInforme) AS FechaInforme, "
//            + "isnull(ROACAuditorFirmante, '') AS ROACAuditorFirmante, "
//            + "isnull(NombreAuditorFirmante, '') AS NombreAuditorFirmante, "
//            + "dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe) AS Coauditoria, "
//            + "MC.ROACCoAuditor AS ROACCoauditor, "
//            + "isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion, "
//            + "isnull(o.Desc_Opinion, 0) AS DescripcionTipoOpinion, "
//            + "isnull(i.IdProvincia, 0) AS IdProvincia, "
//            + "isnull(p.DescProvincia, '') AS Provincia, "
//            + "FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios, "
//            + "FacturacionAuditoriaHoras AS FacturacionAuditoriaHoras, "
//            + "EM.Desc_Corta AS CNMV, "
//            + "dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0)) AS InteresPublico, "
//            + "ImporteCifraActivo AS ImporteCifraActivo, "
//            + "ImporteCifraActivoAnterior AS ImporteCifraActivoAnterior, "
//            + "i.HonorariosAuditoriaInterna, "
//            + "i.HorasAuditoriaInterna, "
//            + "i.HonorariosDiseno, "
//            + "i.HorasDiseno, "
//            + "i.HonorariosOtros, "
//            + "i.HorasOtros "
//            + "FROM [dbo].[T_Modelo_Informe] i "
//            + "INNER JOIN [dbo].[T_Modelo_Datos] d  "
//            + "ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio  = ?"
//            + /* PARAMETRO 1*/ "INNER JOIN [dbo].[T_Modelo_Presentacion] pr  "
//            + "ON pr.Id_Modelo = d.Id_Modelo "
//            + "INNER JOIN [dbo].[T_Auditores_Sociedades] aud  "
//            + "ON aud.COD_ROAC = pr.COD_ROAC "
//            + "LEFT JOIN [dbo].[T_Provincias] p ON p.IdProvincia = i.IdProvincia "
//            + "LEFT JOIN [dbo].[T_Entidades] e ON e.Cod_Entidad = i.IdEntidad "
//            + "AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio) "
//            + "OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio) "
//            + "OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null)) "
//            + "LEFT JOIN [dbo].[T_Trabajo] t ON t.Cod_Trabajo = i.IdTipoTrabajo "
//            + "LEFT JOIN [dbo].[T_Opinion] o ON o.Cod_Opinion = i.IdTipoOpinion "
//            + "LEFT JOIN [dbo].[T_Modelo_Coauditor] MC ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe "
//            + "LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV "
//            + "INNER JOIN (  "
//            + "    SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha "
//            + "    FROM T_Modelo_Presentacion A "
//            + "    INNER JOIN T_Modelo_Datos B "
//            + "    ON A.Id_Modelo = B.Id_Modelo "
//            + "    WHERE B.Ejercicio = ? "
//            + /* PARAMETRO 2*/ "    GROUP BY A.COD_ROAC "
//            + ") MPMAX "
//            + "ON pr.COD_ROAC = MPMAX.COD_ROAC "
//            + "AND  pr.Fecha_Presentacion = MPMAX.fecha  "
//            + "WHERE d.COD_ROAC like ? "
//            + /* PARAMETRO 3*/ "AND (aud.Razon_Social like ? "
//            + /* PARAMETRO 4*/ "OR aud.Apellidos + ', ' + aud.Nombre like ? "
//            + /* PARAMETRO 5*/ "AND i.Documento like ? "
//            + /* PARAMETRO 6*/ "AND i.RazonSocial like ? ";// + /* PARAMETRO 7         order by 1,4";
//


    public ArrayList<TrabajoRealizadoAtlas> obtenerTrabajos(String paramROAC, String paramEJERC) throws SQLException {

        String queryVER = "SELECT DISTINCT isnull(d.COD_ROAC, '')															AS COD_ROAC, \n"
                + "CASE WHEN aud.Nombre is null or aud.Nombre='' THEN \n"
                + "			aud.Razon_Social ELSE aud.Apellidos+', '+aud.Nombre END								AS Identificacion,  \n"
                + "isnull(i.Documento, '')																			AS Documento,   \n"
                + "isnull(REPLACE(i.RazonSocial,'\"',''), '')														AS RazonSocial,  \n"
                + "dbo.[FormatearFecha](EjercicioAuditado)															AS EjercicioAuditado,  \n"
                + "dbo.[FormatearFecha](EjercicioFinal)															AS EjercicioFinal,\n"
                + "ImporteNetoAuditado																				AS ImporteNetoAuditado,  \n"
                + "isnull(PlantillaMediaAnterior, 0)																AS PlantillaMediaAnterior,  \n"
                + "InformeDeCuentas AS InformeDeCuentasReal, \n"
                + "isnull(i.IdTipoTrabajo, '')																		AS IdTipoTrabajo,  \n"
                + "isnull(t.Desc_Trabajo, '')																		AS DescripcionTipoTrabajo,  \n"
                + "dbo.[FormatearFecha](FechaInforme)																AS FechaInforme,  \n"
                + "isnull(ROACAuditorFirmante, '')																	AS ROACAuditorFirmante,  \n"
                + "isnull(NombreAuditorFirmante, '')																AS NombreAuditorFirmante,   \n"
                + "dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe)													AS Coauditoria,  \n"
                + "MC.ROACCoAuditor																				AS ROACCoauditor,  \n"
                + "isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion,  isnull(o.Desc_Opinion, 0)							AS DescripcionTipoOpinion,  \n"
                + "isnull(i.IdProvincia, 0) AS IdProvincia,  isnull(p.DescProvincia, '')							AS Provincia,  \n"
                + "FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios,  FacturacionAuditoriaHoras	AS FacturacionAuditoriaHoras, \n"
                + "EM.Desc_Corta																					AS CNMV,  \n"
                + "dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0))												AS InteresPublico, \n"
                + "ImporteCifraActivo AS ImporteCifraActivo, "
                + "i.HorasOtros ,  i.HonorariosOtros  \n"
                + "\n"
                + "FROM [dbo].[T_Modelo_Informe] i  \n"
                + "INNER JOIN [dbo].[T_Modelo_Datos] d				ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio " /* ALEX AÑADE UN MAYOR O IGUAL PERO ERA SOLO UN IGUAL*/ + ">= " + paramEJERC
                + "INNER JOIN [dbo].[T_Modelo_Presentacion] pr		ON pr.Id_Modelo = d.Id_Modelo\n"
                + "INNER JOIN [dbo].[T_Auditores_Sociedades] aud	ON aud.COD_ROAC = pr.COD_ROAC\n"
                + "\n"
                + "LEFT JOIN [dbo].[T_Provincias] p				ON p.IdProvincia = i.IdProvincia   \n"
                + "LEFT JOIN [dbo].[T_Entidades] e					ON e.Cod_Entidad = i.IdEntidad  AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio)  OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio)  OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null))  LEFT JOIN [dbo].[T_Trabajo] t\n"
                + "												ON t.Cod_Trabajo = i.IdTipoTrabajo  \n"
                + "\n"
                + "LEFT JOIN [dbo].[T_Opinion] o					ON o.Cod_Opinion = i.IdTipoOpinion\n"
                + "LEFT JOIN [dbo].[T_Modelo_Coauditor] MC			ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe  \n"
                + "LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM		ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV\n"
                + "\n"
                + "INNER JOIN (SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha FROM T_Modelo_Presentacion A\n"
                + "			INNER JOIN T_Modelo_Datos B	ON A.Id_Modelo = B.Id_Modelo WHERE B.Ejercicio "/* ALEX AÑADE UN MAYOR O IGUAL PERO ERA SOLO UN IGUAL */ + ">= " + paramEJERC
                + " GROUP BY A.COD_ROAC  ) \n"
                + "	MPMAX ON pr.COD_ROAC = MPMAX.COD_ROAC  AND  pr.Fecha_Presentacion = MPMAX.fecha\n"
                + "	\n"
                + "WHERE d.COD_ROAC like '%" + paramROAC + "%'"
                + "\n order by 1,4";

        System.out.println("kk_>SQL=\n" + queryVER);

        Statement pstmt = connection.getConnection().createStatement();

        // int anoLiquidacion = 2019;
        ResultSet rs = pstmt.executeQuery(queryVER);

        ArrayList<TrabajoRealizadoAtlas> trabajos = new ArrayList<>();

        if (rs != null) {
            while (rs.next()) {

                //DEBEN SER EXACTAMENTE LOS MISMOS CAMPOS QUE EL POJO
                String codRoac = rs.getString("COD_ROAC");

                //         LOG.log(Level.INFO, "Leyedo FILAS DE LA TABLA DE TRABAJOS para el Trabajo ID="+codRoac);
                String identificacion = rs.getString("Identificacion");
                String documento = rs.getString("Documento");
                String razonSocial = rs.getString("RazonSocial");
                int informeDeCuentasReal=rs.getInt("InformeDeCuentasReal");
                //        String primerEjercicioAuditado=rs.getString("PrimerEjercicioAuditado");
                String ejercicioAuditado = rs.getString("EjercicioAuditado");
                //        String constitucion=rs.getString("Constitucion");

                String ejercicioFinal = rs.getString("EjercicioFinal");
                //       int idEntidad=rs.getInt("IdEntidad");
                //       String descripcionEntidad=rs.getString("DescripcionEntidad");

                //      String participesAuditado=rs.getString("ParticipesAuditado");
                //       String participesAnterior=rs.getString("ParticipesAnterior");
                double importeNetoAuditado = rs.getDouble("ImporteNetoAuditado");
                //        double importeNetoAnterior=rs.getDouble("ImporteNetoAnterior");

                //          int plantillaMediaAuditado=rs.getInt("PlantillaMediaAuditado");           
                //         int plantillaMediaAnterior=rs.getInt("PlantillaMediaAnterior");
                int idTipoTrabajo = rs.getInt("IdTipoTrabajo");
                String descripcionTipoTrabajo = rs.getString("DescripcionTipoTrabajo");

                String fechaInforme = rs.getString("FechaInforme");
                /*es la fecha importante*/


                String roacAuditorFirmante = rs.getString("ROACAuditorFirmante");

                String nombreAuditorFirmante = rs.getString("NombreAuditorFirmante");
                String coauditoria = rs.getString("Coauditoria");
                String roacCoauditor = rs.getString("ROACCoauditor");

//    int idTipoOpinion=rs.getInt("IdTipoOpinion");
                String descripcionTipoOpinion = rs.getString("DescripcionTipoOpinion");

                //     int idProvincia=rs.getInt("IdProvincia");
                String provincia = rs.getString("Provincia");

                double facturacionAuditoriaHonorarios = rs.getDouble("FacturacionAuditoriaHonorarios");
                double facturacionAuditoriaHoras = rs.getDouble("FacturacionAuditoriaHoras");

                String cnmv = rs.getString("CNMV");
                String esInteresPublico = rs.getString("InteresPublico");

                double importeCifraActivo = rs.getDouble("ImporteCifraActivo");

                TrabajoRealizadoAtlas trabajo = null;
                try {
                    trabajo = new TrabajoRealizadoAtlas();
                    trabajo.setCodRoac(codRoac);
                    trabajo.setIdentificacion(identificacion);
                    trabajo.setDocumento(documento);
                    trabajo.setRazonSocial(razonSocial);
                    trabajo.setInformeDeCuentasReal(informeDeCuentasReal);
                    trabajo.setEjercicioAuditado(ejercicioAuditado);

                    trabajo.setEjercicioFinal(ejercicioFinal);
                    trabajo.setInformeDeCuentasReal(informeDeCuentasReal);
                    trabajo.setImporteNetoAuditado(importeNetoAuditado);

                    trabajo.setIdTipoTrabajo(idTipoTrabajo);
                    trabajo.setDescripcionTipoTrabajo(descripcionTipoTrabajo);

                    trabajo.setFechaInforme(fechaInforme);
                    trabajo.setRoacAuditorFirmante(roacAuditorFirmante);
                    trabajo.setNombreAuditorFirmante(nombreAuditorFirmante);
                    trabajo.setCoauditoria(coauditoria);
                    trabajo.setRoacCoauditor(roacCoauditor);

                    trabajo.setDescripcionTipoOpinion(descripcionTipoOpinion);
                    trabajo.setProvincia(provincia);

                    trabajo.setFacturacionAuditoriaHonorarios(facturacionAuditoriaHonorarios);
                    trabajo.setFacturacionAuditoriaHoras(facturacionAuditoriaHoras);
                    trabajo.setCnmv(cnmv);
                    trabajo.setEsInteresPublico(esInteresPublico);
                    trabajo.setImporteCifraActivo(importeCifraActivo);

                    trabajos.add(trabajo);

                } catch (Exception creandoTrabajoException) {
                    LOG.log(Level.SEVERE, "==============================================");
                    LOG.log(Level.SEVERE, "ERROR CREANDO TRABAJO REALIZADO " + codRoac, creandoTrabajoException);
                    LOG.log(Level.SEVERE, "==============================================");
                    LOG.log(Level.SEVERE, "TRABAJO REALIZADO CONSTRUIDO HASTA EXCEPCION: ");
                    LOG.log(Level.SEVERE, trabajo != null ? trabajo.toString() : "NULL");
                    LOG.log(Level.SEVERE, "==============================================");
                }
            }
        } else {
            LOG.log(Level.WARNING, "Intentando obtener TrabajosRealizados desde una ResultSet y este es NULL");
        }
        pstmt.close();

        return trabajos;
    }
    
    
    //ESPERO QUE SEA ESTE
    public TrabajoRealizadoAtlas[] obtenerTrabajosPeriodo(String paramEJERC) throws SQLException {

        String queryVER = "SELECT DISTINCT isnull(d.COD_ROAC, '')															AS COD_ROAC, \n"
                + "CASE WHEN aud.Nombre is null or aud.Nombre='' THEN \n"
                + "			aud.Razon_Social ELSE aud.Apellidos+', '+aud.Nombre END								AS Identificacion,  \n"
                + " aud.Documento as NifAuditor, "
                + "isnull(i.Documento, '')																			AS Documento,   \n"
                + "isnull(REPLACE(i.RazonSocial,'\"',''), '')														AS RazonSocial,  \n"
                + "dbo.[FormatearFecha](EjercicioAuditado)															AS EjercicioAuditado,  \n"
                + "dbo.[FormatearFecha](EjercicioFinal)															AS EjercicioFinal,\n"
                + "ImporteNetoAuditado																				AS ImporteNetoAuditado,  \n"
                + "isnull(PlantillaMediaAnterior, 0)																AS PlantillaMediaAnterior,  \n"
                + "isnull(i.IdTipoTrabajo, '')																		AS IdTipoTrabajo,  \n"
                + "isnull(t.Desc_Trabajo, '')																		AS DescripcionTipoTrabajo,  \n"
                + "dbo.[FormatearFecha](FechaInforme)																AS FechaInforme,  \n"
                + "InformeDeCuentas                                                   AS InformeDeCuentasReal, \n"
                + "isnull(ROACAuditorFirmante, '')																	AS ROACAuditorFirmante,  \n"
                + "isnull(NombreAuditorFirmante, '')																AS NombreAuditorFirmante,   \n"
                + "dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe)													AS Coauditoria,  \n"
                + "MC.Id_Modelo_Informe as	Coauditoria2, \n"
                + "MC.ROACCoAuditor																				AS ROACCoauditor,  \n"
                + "isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion,  isnull(o.Desc_Opinion, 0)							AS DescripcionTipoOpinion,  \n"
                + "isnull(i.IdProvincia, 0) AS IdProvincia,  isnull(p.DescProvincia, '')							AS Provincia,  \n"
                + "FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios,  FacturacionAuditoriaHoras	AS FacturacionAuditoriaHoras, \n"
                + "EM.Desc_Corta																					AS CNMV,  \n"
                + "dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0))												AS InteresPublico, \n"
                + "ImporteCifraActivo AS ImporteCifraActivo, "
                + "i.HorasOtros ,  i.HonorariosOtros  \n"
                + "\n"
                + "FROM [dbo].[T_Modelo_Informe] i  \n"
                + "INNER JOIN [dbo].[T_Modelo_Datos] d			ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio = " + paramEJERC
                + " INNER JOIN [dbo].[T_Modelo_Presentacion] pr		ON pr.Id_Modelo = d.Id_Modelo \n"
                + " INNER JOIN [dbo].[T_Auditores_Sociedades] aud	ON aud.COD_ROAC = pr.COD_ROAC \n"
                + "\n"
                + "LEFT JOIN [dbo].[T_Provincias] p			ON p.IdProvincia = i.IdProvincia   \n"
                + "LEFT JOIN [dbo].[T_Entidades] e			ON e.Cod_Entidad = i.IdEntidad  AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio)  OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio)  OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null))  LEFT JOIN [dbo].[T_Trabajo] t\n"
                + "							ON t.Cod_Trabajo = i.IdTipoTrabajo  \n"
                + "\n"
                + "LEFT JOIN [dbo].[T_Opinion] o			ON o.Cod_Opinion = i.IdTipoOpinion \n"
                + "LEFT JOIN [dbo].[T_Modelo_Coauditor] MC		ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe  \n"
                + "LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM		ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV \n"
                + "\n"
                + "INNER JOIN (SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha FROM T_Modelo_Presentacion A\n"
                + "			INNER JOIN T_Modelo_Datos B	ON A.Id_Modelo = B.Id_Modelo WHERE B.Ejercicio = " + paramEJERC
                + " GROUP BY A.COD_ROAC  ) \n"
                + "	MPMAX ON pr.COD_ROAC = MPMAX.COD_ROAC  AND  pr.Fecha_Presentacion = MPMAX.fecha\n"
                + "	\n"
                + "\n order by 1,4";

 
       LOG.log(Level.INFO, "I>SQL=\n{0}", queryVER);
       
        TrabajoRealizadoAtlas[] trabajos;
        // int anoLiquidacion = 2019;
        try (Statement pstmt = connection.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY); // int anoLiquidacion = 2019;
                ResultSet rs = pstmt.executeQuery(queryVER)) {
            int contador = 0;
            if (rs.last()) {
                contador = rs.getRow();
                rs.beforeFirst(); // not rs.first() because the rs.next() below will move on, missing the first

            }
            final int TRAMO = 5;
            System.out.println("NUMERO DE TRABAJOS " + contador);
            int lapso = contador / TRAMO;
            //   rs.first();
            trabajos = new TrabajoRealizadoAtlas[contador++];
            int cuenta = -1;
            if (rs != null) {
                while (rs.next()) {
                    cuenta++;

                    //DEBEN SER EXACTAMENTE LOS MISMOS CAMPOS QUE EL POJO
                    String codRoac = rs.getString("COD_ROAC");

                    String identificacion = rs.getString("Identificacion");
                    String documento = rs.getString("Documento");
                    String nifAuditor = rs.getString("nifAuditor");
                    String razonSocial = rs.getString("RazonSocial");
                    //            String informeDeCuentas=rs.getString("InformeDeCuentas");
                    //        String primerEjercicioAuditado=rs.getString("PrimerEjercicioAuditado");
                    String ejercicioAuditado = rs.getString("EjercicioAuditado");
                    //        String constitucion=rs.getString("Constitucion");

                    String ejercicioFinal = rs.getString("EjercicioFinal");
                    //       int idEntidad=rs.getInt("IdEntidad");
                    //       String descripcionEntidad=rs.getString("DescripcionEntidad");

                    //      String participesAuditado=rs.getString("ParticipesAuditado");
                    //       String participesAnterior=rs.getString("ParticipesAnterior");
                    double importeNetoAuditado = rs.getDouble("ImporteNetoAuditado");
                    //        double importeNetoAnterior=rs.getDouble("ImporteNetoAnterior");

                    //          int plantillaMediaAuditado=rs.getInt("PlantillaMediaAuditado");
                    //         int plantillaMediaAnterior=rs.getInt("PlantillaMediaAnterior");
                    int idTipoTrabajo = rs.getInt("IdTipoTrabajo");
                    int informeDeCuentasReal = rs.getInt("informeDeCuentasReal");

                    String descripcionTipoTrabajo = rs.getString("DescripcionTipoTrabajo");

                    String fechaInforme = rs.getString("FechaInforme");
                    /*es la fecha importante*/

                    String roacAuditorFirmante = rs.getString("ROACAuditorFirmante");

                    String nombreAuditorFirmante = rs.getString("NombreAuditorFirmante");
                    //    String coauditoria = rs.getString("Coauditoria");
                    boolean coauditoria = rs.getBoolean("Coauditoria2");
                    String roacCoauditor = rs.getString("ROACCoauditor");

//    int idTipoOpinion=rs.getInt("IdTipoOpinion");
                    String descripcionTipoOpinion = rs.getString("DescripcionTipoOpinion");

//     int idProvincia=rs.getInt("IdProvincia");
                    String provincia = rs.getString("Provincia");

                    double facturacionAuditoriaHonorarios = rs.getDouble("FacturacionAuditoriaHonorarios");
                    double facturacionAuditoriaHoras = rs.getDouble("FacturacionAuditoriaHoras");

                    String cnmv = rs.getString("CNMV");
                    String esInteresPublico = rs.getString("InteresPublico");

                    double importeCifraActivo = rs.getDouble("ImporteCifraActivo");

                    TrabajoRealizadoAtlas trabajo = null;
                    try {
                        trabajo = new TrabajoRealizadoAtlas();
                        trabajo.setCodRoac(codRoac);
                        trabajo.setIdentificacion(identificacion);
                        trabajo.setDocumento(documento);
                        trabajo.setNifAuditor(nifAuditor);
                        trabajo.setRazonSocial(razonSocial);
                        trabajo.setInformeDeCuentasReal(informeDeCuentasReal);
                        trabajo.setEjercicioAuditado(ejercicioAuditado);

                        trabajo.setEjercicioFinal(ejercicioFinal);

                        trabajo.setImporteNetoAuditado(importeNetoAuditado);

                        trabajo.setIdTipoTrabajo(idTipoTrabajo);
                        trabajo.setDescripcionTipoTrabajo(descripcionTipoTrabajo);

                        trabajo.setFechaInforme(fechaInforme);
                        trabajo.setRoacAuditorFirmante(roacAuditorFirmante);
                        trabajo.setNombreAuditorFirmante(nombreAuditorFirmante);
                        if (coauditoria) {
                            int test = 22;
                        }
                        trabajo.setCoauditoria(coauditoria);
                        trabajo.setRoacCoauditor(roacCoauditor);

                        trabajo.setDescripcionTipoOpinion(descripcionTipoOpinion);
                        trabajo.setProvincia(provincia);

                        trabajo.setFacturacionAuditoriaHonorarios(facturacionAuditoriaHonorarios);
                        trabajo.setFacturacionAuditoriaHoras(facturacionAuditoriaHoras);
                        trabajo.setCnmv(cnmv);
                        trabajo.setEsInteresPublico(esInteresPublico);
                        trabajo.setImporteCifraActivo(importeCifraActivo);

                        trabajos[cuenta] = trabajo;

                    } catch (Exception creandoTrabajoException) {
                        LOG.log(Level.SEVERE, "==============================================");
                        LOG.log(Level.SEVERE, "ERROR CREANDO TRABAJO REALIZADO " + codRoac, creandoTrabajoException);
                        LOG.log(Level.SEVERE, "==============================================");
                        LOG.log(Level.SEVERE, "TRABAJO REALIZADO CONSTRUIDO HASTA EXCEPCION: ");
                        LOG.log(Level.SEVERE, trabajo != null ? trabajo.toString() : "NULL");
                        LOG.log(Level.SEVERE, "==============================================");
                    }
                }
            } else {
                LOG.log(Level.WARNING, "Intentando obtener TrabajosRealizados desde una ResultSet y este es NULL");
            }
        }

        return trabajos;
    }

 
    public ConnectionService getConnection() {
        return connection;
    }

    public void setConnection(ConnectionService connection) {
        this.connection = connection;
    }

    public ArrayList<TrabajoRealizadoAtlas> obtenerTrabajosPorPeriodoYRoac(String paramROAC, int paramEJERC) throws SQLException {

        //"WHERE d.COD_ROAC like '%S0530%' and FechaInforme between '20180401' and '20180430'\n"
        StringBuilder sb = new StringBuilder(" between ")
                .append("'").append(paramEJERC - 1).append("1001").append("'")
                .append(" and ")
                .append("'").append(paramEJERC).append("0930").append("'");
        String ejercicio = String.valueOf(paramEJERC);
        String periodo = sb.toString();

        String sql = "SELECT DISTINCT isnull(d.COD_ROAC, '')				AS COD_ROAC, \n"
                + "CASE WHEN aud.Nombre is null or aud.Nombre='' THEN \n"
                + "			aud.Razon_Social ELSE aud.Apellidos+', '+aud.Nombre END	AS Identificacion,  \n"
                + "isnull(i.Documento, '')							AS Documento,   \n"
                + "isnull(REPLACE(i.RazonSocial,'\"',''), '')					AS RazonSocial,  \n"
                + "dbo.[FormatearFecha](EjercicioAuditado)					AS EjercicioAuditado,  \n"
                + "dbo.[FormatearFecha](EjercicioFinal)						AS EjercicioFinal,\n"
                + "ImporteNetoAuditado								AS ImporteNetoAuditado,  \n"
                + "isnull(PlantillaMediaAnterior, 0)						AS PlantillaMediaAnterior,  \n"
                + "isnull(i.IdTipoTrabajo, '')							AS IdTipoTrabajo,  \n"
                + "isnull(t.Desc_Trabajo, '')							AS DescripcionTipoTrabajo,  \n"
                + "InformeDeCuentas                                                             AS InformeDeCuentasReal, \n"
                + "dbo.[FormatearFecha](FechaInforme)						AS FechaInforme,  \n"
                + "FechaInforme									AS FechaInformeBrut0,  \n"
                + "isnull(ROACAuditorFirmante, '')						AS ROACAuditorFirmante,  \n"
                + "isnull(NombreAuditorFirmante, '')						AS NombreAuditorFirmante, \n"
                + "dbo.MC.Id_Modelo_Informe                                                     AS Coauditoria2, \n"
                + "dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe)					AS Coauditoria, \n"
                + "MC.ROACCoAuditor								AS ROACCoauditor,  \n"
                + "isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion,  isnull(o.Desc_Opinion, 0)	AS DescripcionTipoOpinion,  \n"
                + "isnull(i.IdProvincia, 0) AS IdProvincia,  isnull(p.DescProvincia, '')	AS Provincia,  \n"
                + "FacturacionAuditoriaHonorarios           AS FacturacionAuditoriaHonorarios,  \n"
                + "FacturacionAuditoriaHoras    AS FacturacionAuditoriaHoras, \n"
                + "EM.Desc_Corta								AS CNMV,  \n"
                + "dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0))				AS InteresPublico, \n"
                + "ImporteCifraActivo AS ImporteCifraActivo, i.HorasOtros ,  i.HonorariosOtros  \n"
                + "\n"
                + "FROM [dbo].[T_Modelo_Informe] i  \n"
                + "INNER JOIN [dbo].[T_Modelo_Datos] d				ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio= '" + ejercicio + "'\n"
                + "INNER JOIN [dbo].[T_Modelo_Presentacion] pr      ON pr.Id_Modelo = d.Id_Modelo\n"
                + "INNER JOIN [dbo].[T_Auditores_Sociedades] aud    ON aud.COD_ROAC = pr.COD_ROAC\n"
                + "LEFT JOIN [dbo].[T_Provincias] p				ON p.IdProvincia = i.IdProvincia   \n"
                + "LEFT JOIN [dbo].[T_Entidades] e				ON e.Cod_Entidad = i.IdEntidad  AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio)  OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio)  OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null))  LEFT JOIN [dbo].[T_Trabajo] t\n"
                + "								ON t.Cod_Trabajo = i.IdTipoTrabajo  \n"
                + "LEFT JOIN [dbo].[T_Opinion] o				ON o.Cod_Opinion = i.IdTipoOpinion\n"
                + "LEFT JOIN [dbo].[T_Modelo_Coauditor] MC			ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe  \n"
                + "LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM      ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV\n"
                + "INNER JOIN (SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha FROM T_Modelo_Presentacion A\n"
                + "INNER JOIN T_Modelo_Datos B	ON A.Id_Modelo = B.Id_Modelo\n "
                + " WHERE B.Ejercicio= '" + ejercicio + "' GROUP BY A.COD_ROAC ) \n"
                + " MPMAX ON pr.COD_ROAC = MPMAX.COD_ROAC  AND  pr.Fecha_Presentacion = MPMAX.fecha\n"
                + "\n"
                + "WHERE d.COD_ROAC like '" + paramROAC + "' and FechaInforme " + periodo
                + " order by 1,4\n"
                + "";

        System.out.println("kk_>SQL=\n" + sql);

        Statement pstmt = connection.getConnection().createStatement();

        ResultSet rs = pstmt.executeQuery(sql);

        ArrayList<TrabajoRealizadoAtlas> trabajos = new ArrayList<>();

        if (rs != null) {
            while (rs.next()) {

                //DEBEN SER EXACTAMENTE LOS MISMOS CAMPOS QUE EL POJO
                String codRoac = rs.getString("COD_ROAC");

                String identificacion = rs.getString("Identificacion");
                String documento = rs.getString("Documento");
                String razonSocial = rs.getString("RazonSocial");
                String ejercicioAuditado = rs.getString("EjercicioAuditado");
                String ejercicioFinal = rs.getString("EjercicioFinal");
                double importeNetoAuditado = rs.getDouble("ImporteNetoAuditado");
                
                int informeDeCuentasReal = rs.getInt("InformeDeCuentasReal");//100-101 o 200- 201
                int idTipoTrabajo = rs.getInt("IdTipoTrabajo");
                String descripcionTipoTrabajo = rs.getString("DescripcionTipoTrabajo");

                String fechaInforme = rs.getString("FechaInforme");
                /*es la fecha importante*/

                String roacAuditorFirmante = rs.getString("ROACAuditorFirmante");
                String nombreAuditorFirmante = rs.getString("NombreAuditorFirmante");
                String coauditoria = rs.getString("Coauditoria");
                boolean coauditoriaBol = rs.getBoolean("Coauditoria2");
                
                String roacCoauditor = rs.getString("ROACCoauditor");
                String descripcionTipoOpinion = rs.getString("DescripcionTipoOpinion");
                String provincia = rs.getString("Provincia");
                double facturacionAuditoriaHonorarios = rs.getDouble("FacturacionAuditoriaHonorarios");
                double facturacionAuditoriaHoras = rs.getDouble("FacturacionAuditoriaHoras");
                String cnmv = rs.getString("CNMV");
                String esInteresPublico = rs.getString("InteresPublico");
                double importeCifraActivo = rs.getDouble("ImporteCifraActivo");

                TrabajoRealizadoAtlas trabajo = null;
                try {
                    trabajo = new TrabajoRealizadoAtlas();
                    trabajo.setCodRoac(codRoac);
                    trabajo.setIdentificacion(identificacion);
                    trabajo.setDocumento(documento);
                    trabajo.setRazonSocial(razonSocial);
                    trabajo.setEjercicioAuditado(ejercicioAuditado);
                    trabajo.setInformeDeCuentasReal(informeDeCuentasReal);
                    trabajo.setEjercicioFinal(ejercicioFinal);
                    trabajo.setImporteNetoAuditado(importeNetoAuditado);
                    trabajo.setIdTipoTrabajo(idTipoTrabajo);
                    trabajo.setDescripcionTipoTrabajo(descripcionTipoTrabajo);
                    trabajo.setFechaInforme(fechaInforme);
                    trabajo.setRoacAuditorFirmante(roacAuditorFirmante);
                    trabajo.setNombreAuditorFirmante(nombreAuditorFirmante);
                    trabajo.setCoauditoria(coauditoria);
                    trabajo.setRoacCoauditor(roacCoauditor);
                    trabajo.setDescripcionTipoOpinion(descripcionTipoOpinion);
                    trabajo.setProvincia(provincia);
                    trabajo.setFacturacionAuditoriaHonorarios(facturacionAuditoriaHonorarios);
                    trabajo.setFacturacionAuditoriaHoras(facturacionAuditoriaHoras);
                    trabajo.setCnmv(cnmv);
                    trabajo.setEsInteresPublico(esInteresPublico);
                    trabajo.setImporteCifraActivo(importeCifraActivo);

                    trabajos.add(trabajo);

                } catch (Exception creandoTrabajoException) {
                    LOG.log(Level.SEVERE, "==============================================");
                    LOG.log(Level.SEVERE, "ERROR CREANDO TRABAJO REALIZADO " + codRoac, creandoTrabajoException);
                    LOG.log(Level.SEVERE, "==============================================");
                    LOG.log(Level.SEVERE, "TRABAJO REALIZADO CONSTRUIDO HASTA EXCEPCION: ");
                    LOG.log(Level.SEVERE, trabajo != null ? trabajo.toString() : "NULL");
                    LOG.log(Level.SEVERE, "==============================================");
                }
            }
        } else {
            LOG.log(Level.WARNING, "Intentando obtener TrabajosRealizados desde una ResultSet y este es NULL");
        }
        pstmt.close();

        return trabajos;
    }

}
