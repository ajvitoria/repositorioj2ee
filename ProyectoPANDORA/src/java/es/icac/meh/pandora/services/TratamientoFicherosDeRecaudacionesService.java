package es.icac.meh.pandora.services;

import es.icac.meh.pandora.importacion.CampoImportado;
import es.icac.meh.pandora.modelo.Apunte;
import es.icac.meh.pandora.modelo.FicheroTraspasado;
import es.icac.meh.pandora.modelo.Recaudacion;
import es.icac.meh.pandora.modelo.Lote;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TratamientoFicherosDeRecaudacionesService {

 

    private CampoImportado tipoRegistro;
    private CampoImportado numeroSecuencial;
    private CampoImportado codigoTasa;
    private CampoImportado numeroJustificante;
    private CampoImportado subCampo1, subCampo2;
    private CampoImportado subCampo3;//valorAnyoEjercicio
    private CampoImportado subCampo4; // trimestre
    private CampoImportado subCampo5, subCampo6;
    private CampoImportado nif;
    private CampoImportado apellidosNombre;
    private CampoImportado fechaIngreso;
    private CampoImportado strImporte;
    private CampoImportado codigoEntiendadFinanciera;
    private CampoImportado codigoSucursal;
    private File ultimoFicheroTratado;

    private static final Logger LOG = Logger.getLogger(TratamientoFicherosDeRecaudacionesService.class.getName());

   // public static ArrayList<FicheroTraspasado> listaProcesados;
    
 
    
    public static int ficherosFallados;
    public static int ficherosCorrectos;
    public static int ficherosProcesados;
    public static int cobrosProcesadosPorLote;
    public static int cobrosFalladosPorLote;
    


    private ArrayList<String> obtenerLineasNuevoFichero(File fichero) {
        ArrayList<String> lineas = new ArrayList<>();
        FileReader fr = null;
        BufferedReader br = null;

        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).

            if (!fichero.exists()) {
                new FileNotFoundException();
            }

            LOG.log(Level.FINE, "procesando fichero {0} > {1}", new Object[]{fichero, new java.util.Date()});
 
            fr = new FileReader(fichero);
            br = new BufferedReader(fr);
            // Lectura del fichero
            String linea;
            while ((linea = br.readLine()) != null) {
                lineas.add(linea);
            }

        

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                LOG.log(Level.WARNING, "El fichero " + fichero + "no contiene lineas");
            }
            return lineas;
        }

    }

    private TratamientoFicherosDeRecaudacionesService() {
        generarEstructura();
   //     reiniciarProcesamiento();
       LOG.setLevel(Level.INFO);
    }

    private void generarEstructura() {
        this.tipoRegistro = new CampoImportado(1, 1);
        this.numeroSecuencial = new CampoImportado(2, 8);
        this.codigoTasa = new CampoImportado(9, 13);
        this.numeroJustificante = new CampoImportado(14, 26);

        subCampo1 = new CampoImportado(14, 17);
        subCampo2 = new CampoImportado(18, 20);
        subCampo3 = new CampoImportado(21, 21);
        subCampo4 = new CampoImportado(22, 22);
        subCampo5 = new CampoImportado(23, 25);
        subCampo6 = new CampoImportado(26, 26);

        this.nif = new CampoImportado(27, 35);
        this.apellidosNombre = new CampoImportado(36, 62);
        this.fechaIngreso = new CampoImportado(63, 70);
        this.strImporte = new CampoImportado(71, 82);
        this.codigoEntiendadFinanciera = new CampoImportado(83, 86);
        this.codigoSucursal = new CampoImportado(87, 90);
    }

    @Override
    public String toString() {
        return "RegistroDetalle{" + "tipoRegistro=" + tipoRegistro + ", numeroSecuencial=" + numeroSecuencial + ", codigoTasa=" + codigoTasa + ", numeroJustificante=" + numeroJustificante + ", nif=" + nif + ", apellidosNombre=" + apellidosNombre + ", fechaIngreso=" + fechaIngreso + ", importe=" + strImporte + ", codigoEntiendadFinanciera=" + codigoEntiendadFinanciera + ", codigoSucursal=" + codigoSucursal + ", fichero=" + ultimoFicheroTratado + '}';
    }
    
    final SimpleDateFormat SDF=new SimpleDateFormat("dd-MM-yyyy");
    private Lote procesarFichero(File fichero) {
        
        this.ultimoFicheroTratado=fichero;
        
        Lote loteRetorno=new Lote("LOTE "+fichero.getName(), SDF.format(new Date()));
        loteRetorno.setFichero(fichero);
             
        boolean tieneCabecera=false;
        boolean tieneRecaudacions=false;
        boolean tienePie=false;
        

        int cobrosProcesadosPorFichero = 0;
        
        int cobroNoProcesable=0; //aquellos cobros que no sean 791/608
        
        double importeRecaudacionsAcumuladosPorFichero = 0;

        int numeroLinea = 0;

        FicheroTraspasado ficheroProcesable = new FicheroTraspasado(fichero, false);
        int EJERCICIO=0;
        try {
           
            for (String linea : obtenerLineasNuevoFichero(fichero)) {
                numeroLinea++;

                String valorTipoRegistro = linea.substring(tipoRegistro.posIni - 1, tipoRegistro.posFin);
                String valorNumeroSecuencial = linea.substring(numeroSecuencial.posIni - 1, numeroSecuencial.posFin);

                switch (valorTipoRegistro) {

                    case "2":
                        Recaudacion cobro = null;
                        tieneRecaudacions=true;
                        try {
                            cobro = this.obtenerRecaudacionDeLinea(numeroLinea, linea);
                            if (cobro==null){
                                cobroNoProcesable++;
                                break;
                            }else{
                                ficheroProcesable.setEstaTraspasado(true);
                                cobrosProcesadosPorFichero++;
                                TratamientoFicherosDeRecaudacionesService.cobrosProcesadosPorLote++;
                                importeRecaudacionsAcumuladosPorFichero += (cobro.getImporteEuros());
                            }
                        } catch (Exception e) {
                            ficheroProcesable.setEstaTraspasado(false, e.getMessage());
                            TratamientoFicherosDeRecaudacionesService.cobrosFalladosPorLote++;
                            throw new ImpossibleProcessingException(fichero.getName(), numeroLinea, e.getMessage());

                        }
                        loteRetorno.anadirRecaudacions(cobro);
                        
                        break;

                    case "1":
                        tieneCabecera=true;
                        
                        EJERCICIO=leerCabecera(linea);
                        break;
                    case "3":
                        tienePie=true;
                       
              
                       loteRetorno.setImportePie(this.obtenerPieImporte(linea));
                       loteRetorno.setQuincenaAno(this.obtenerPieQuincenaAno(linea));
                       loteRetorno.setQuincenaTipo(this.obtenerPieQuincenaTipo(linea));
                       loteRetorno.setQuincenaMes(this.obtenerPieQuincenaMes(linea));
      
                     
                        

        }
    




            }//for String linea: obtenerLineas():
            
        } catch (ImpossibleProcessingException ex) {
            ex.printStackTrace();
               LOG.log(Level.SEVERE, "***NO SE HA PROCESADO correctamente el fichero {0}\nOBTENIENDO EL LOTE ={1}", new Object[]{fichero, loteRetorno});
        
            ficheroProcesable.setEstaTraspasado(false);
            cobrosProcesadosPorFichero = 0;
            importeRecaudacionsAcumuladosPorFichero = 0;
//            resultado = false; //esto significa que si uno falla... es resultado final del procesamiento global sera FALSE
      //      this.ficherosFallados++;
        }

        ficheroProcesable.setRecaudacionsProcesadosPorFichero(cobrosProcesadosPorFichero);
        ficheroProcesable.setImporteRecaudacionsAcumulados(importeRecaudacionsAcumuladosPorFichero);
       // listaProcesados.add(ficheroProcesable);
        loteRetorno.setCifraAcumulada(importeRecaudacionsAcumuladosPorFichero);
        loteRetorno.setEjercicio(EJERCICIO); 
//        if(resultado){ //procesado correcto
//            LOG.log(Level.INFO, "Se ha procesado correctamente el fichero {0}\nOBTENIENDO EL LOTE ={1}", new Object[]{fichero, loteRetorno});
//            LOG.log(Level.INFO, "Se han descartado {0} cobros por no ser ''791 & 608'' ", cobroNoProcesable);
//        }
        
         
        loteRetorno.setCifraAcumulada(importeRecaudacionsAcumuladosPorFichero);
        
        if(tieneCabecera && tieneRecaudacions && tienePie)
            loteRetorno.setValido(true);
        else
            loteRetorno.setValido(false);
        
        return loteRetorno;
        
        
    }



private String obtenerPieImporte(String linea) throws ImpossibleProcessingException {
    CampoImportado campo= new CampoImportado(31, 47);
    String cadenaExtraida = linea.substring(campo.posIni - 1, campo.posFin).trim();
     LOG.log(Level.INFO, "Extraido: obtenerPieImporte: {0}", cadenaExtraida);
    return cadenaExtraida;
}

private String obtenerPieQuincenaAno(String linea) throws ImpossibleProcessingException {
       CampoImportado campo= new CampoImportado(14, 17);
     String cadenaExtraida = linea.substring(campo.posIni - 1, campo.posFin).trim();
     LOG.log(Level.INFO, "Extraido:obtenerPieQuincenaAno: {0} ", cadenaExtraida);
     return cadenaExtraida;
}
private String obtenerPieQuincenaMes(String linea) throws ImpossibleProcessingException {
       CampoImportado campo= new CampoImportado(18, 19);
     String cadenaExtraida = linea.substring(campo.posIni - 1, campo.posFin).trim();
     LOG.log(Level.FINE, "Extraido: obtenerPieQuincenaMes:{0}", cadenaExtraida);
      return cadenaExtraida;
}
private String obtenerPieQuincenaTipo(String linea) throws ImpossibleProcessingException {
       CampoImportado campo= new CampoImportado(20, 21);
     String cadenaExtraida = linea.substring(campo.posIni - 1, campo.posFin).trim();
     LOG.log(Level.INFO, "Extraido obtenerPieQuincenaTipo: {0}", cadenaExtraida);
      return cadenaExtraida;
}

    private Recaudacion obtenerRecaudacionDeLinea(int numeroLinea, String linea) throws ImpossibleProcessingException {
        //OJITO   LA FUNCION substring de java inicia el recuento de los indices en 0 (cero) 
        // mientras que en el apartado de requisitos --> Diseño archivo de tasas (modelo 791).pdf
        //   a la posición inicial del indice desde se le quita UNA UNIDAD.
        //   a la posición DESDE del indice se la deja como esta.
        /*
                        INDICES    TIPO    NOMBRE DEL CAMPO
                       ========    =====   =============================================================
                        1-1        Núm.    Tipo de registro = 2
                        2 - 8      Núm.    Número secuencial de registro con ceros a la izquierda
                        9 - 13     Núm.    Código de Tasa
                       14 - 26     Núm.    Número de justificante
            
                                            Xxxyyyatvvvv
                                o	Siendo a: el año de ejercicio representado por su último dígito. Así, si el año del ejercicio es 2018, el valor de a será 8.
                                o	Siendo t: el valor para el trimestre de 1 a 4.

            
                       27 - 35     Alf.    NIF
                       36 - 62     Alf.    Apellidos y Nombre
                       63 - 70     Núm.    Fecha de ingreso (AAAAMMDD)
                       71 - 82     Núm.    Importe en céntimos de euro con ceros a la izquierda
                       83 - 86     Núm.    Código de la entidad financiera en que se produce el ingreso
                       87 - 90     Núm.    Código de la sucursal
         */

        String valorCodigoTasa = null;
        String valorNumeroJustificante = null;
//alexis ha quitado esto el 21 de mayo
        subCampo1 = new CampoImportado(14, 16);
        subCampo2 = new CampoImportado(17, 19);
        subCampo3 = new CampoImportado(20, 20);
        subCampo4 = new CampoImportado(21, 21);
        subCampo5 = new CampoImportado(22, 25);
        subCampo6 = new CampoImportado(26, 26);

        String valorSubcampo1 = null;
        String valorSubcampo2 = null;
        String valorAnyoEjercicio = null;
        String valorTrimestre = null;
        String valorSubcampo5 = null;
        String valorSubcampo6 = null;

        String valorNif = null;
        String valorApellidosNombre = null;
        String valorFechaIngreso = null;
        double valorImporteCentimos = 0;
        String valorCodigoEntidad = null;
        String valorCodigoSucursal = null;

        try {
            valorCodigoTasa = linea.substring(codigoTasa.posIni - 1, codigoTasa.posFin);
            valorNumeroJustificante = linea.substring(numeroJustificante.posIni - 1, numeroJustificante.posFin);
            valorSubcampo1 = linea.substring(subCampo1.posIni - 1, subCampo1.posFin);
            valorSubcampo2 = linea.substring(subCampo2.posIni - 1, subCampo2.posFin);            
            /*
                    SEGUN CHARLA MANTENIDA CON ANGEL 28/04/2020
                    UNICAMENTE SE PROCESARAN COBROS con estos valores:
                            valorSubcampo1=791 
                            valorSubcampo2=608
            */
            
            boolean PROCESAR_COBRO=(valorSubcampo1.equals("791") && valorSubcampo2.equals("608"));
            
            if(!PROCESAR_COBRO){
                LOG.log(Level.FINE, "DESCARTAR LINEA. NO ES UN 791 - 608 --> linea: "+linea+"");
                return null;
            }
            valorAnyoEjercicio = linea.substring(subCampo3.posIni - 1, subCampo3.posFin);
            valorTrimestre = linea.substring(subCampo4.posIni - 1, subCampo4.posFin);
            
            valorSubcampo5 = linea.substring(subCampo5.posIni - 1, subCampo5.posFin);
            valorSubcampo6 = linea.substring(subCampo6.posIni - 1, subCampo6.posFin);

            valorNif = linea.substring(nif.posIni - 1, nif.posFin);
            valorApellidosNombre = linea.substring(apellidosNombre.posIni - 1, apellidosNombre.posFin).trim();
            valorFechaIngreso = linea.substring(fechaIngreso.posIni - 1, fechaIngreso.posFin);

            String importeCentimos = linea.substring(strImporte.posIni - 1, strImporte.posFin);
            try {
                valorImporteCentimos = Double.parseDouble(importeCentimos);
            } catch (Exception r) {
                throw new ImpossibleProcessingException(numeroLinea, "****se ha intentado transformar CENTIMOS" + valorImporteCentimos + " en un numero double pero NO SE HA PODIDO.***************");
            }  
            valorCodigoEntidad = linea.substring(codigoEntiendadFinanciera.posIni - 1, codigoEntiendadFinanciera.posFin);
            valorCodigoSucursal = linea.substring(codigoSucursal.posIni - 1, codigoSucursal.posFin);
            TratamientoFicherosDeRecaudacionesService.cobrosProcesadosPorLote++;
        } catch (Exception e) {
            TratamientoFicherosDeRecaudacionesService.cobrosFalladosPorLote++;
            throw new ImpossibleProcessingException(numeroLinea, "Excepcion al procesar el cobro");
        }

       
            //System.out.println("tipoRegistro: "         + valorTipoRegistro);
            //System.out.println("valorNumeroSecuencial: "+ valorNumeroSecuencial);
             LOG.log(Level.FINEST, "codigo tasa: " + valorCodigoTasa);
             LOG.log(Level.FINEST, "numero justificante" + valorNumeroJustificante);
             LOG.log(Level.FINEST, "codigo valorSubcampo1: " + valorSubcampo1);
            LOG.log(Level.FINEST,"codigo valorSubcampo2: " + valorSubcampo2);
           LOG.log(Level.FINEST,"codigo AnyoEjercicio: " + valorAnyoEjercicio);
            LOG.log(Level.FINEST,"codigo valorTrimestre: " + valorTrimestre);
            LOG.log(Level.FINEST,"codigo valorSubcampo5: " + valorSubcampo5);
            LOG.log(Level.FINEST,"codigo valorSubcampo6: " + valorSubcampo6);
            LOG.log(Level.FINEST,"nif" + valorNif);
            LOG.log(Level.FINEST,"apellidosNombre: " + valorApellidosNombre);
            LOG.log(Level.FINEST,"fechaIngreso: " + valorFechaIngreso);
            LOG.log(Level.FINEST,"importe (en centimos))" + valorImporteCentimos);
            LOG.log(Level.FINEST,"entidad" + valorCodigoEntidad);
            LOG.log(Level.FINEST, "sucursal" + valorCodigoSucursal);
        



        //no usar constructor
        //Recaudacion cobro = new Recaudacion(valorCodigoTasa, valorNumeroJustificante, valorSubcampo1, valorSubcampo2, anyoCalculado, valorTrimestre, valorSubcampo5, valorSubcampo6, valorNif, valorApellidosNombre, fecha, valorImporteCentimos * 100, valorCodigoEntidad, valorCodigoSucursal);
        Recaudacion recaudacion = new Recaudacion();
        recaudacion.setCodigoTasa(valorCodigoTasa);
        recaudacion.setNumeroJustificante(valorNumeroJustificante);
        recaudacion.setCodigovalorSubcampo1(valorSubcampo1);
        recaudacion.setCodigovalorSubcampo2(valorSubcampo2);
        
        SimpleDateFormat SDF_TXT = new SimpleDateFormat("yyyyMMdd");
        Date fecha = null;
        try {
            fecha = SDF_TXT.parse(valorFechaIngreso);
           // ESTO YA NO SE HACE AQUÍ... SE HACE A POSTERIORI DESPUES DE LEER EL PIE DEL LOTE DE COBROS
           //  recaudacion.setCodigoAnyoEjercicio(Integer.parseInt(anyoCalculado));
            recaudacion.setFechaIngreso(fecha);  
            recaudacion.setCodigovalorTrimestre(Integer.parseInt(valorTrimestre));
            recaudacion.setCodigoAnyoEjercicio(Integer.parseInt(valorAnyoEjercicio));      
        } catch (NumberFormatException nfe){
            LOG.log(Level.SEVERE, "Los datos del trimestre={0}  y del ano: )valorAnyoEjercicio): {1} son incorrectos", new Object[]{valorTrimestre, valorAnyoEjercicio});
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "{0} NO PUDO SER CONVERTIDA A FECHA VALIDA!", valorFechaIngreso);
            fecha = new Date();
        }  
       
        recaudacion.setCodigovalorSubcampo5(valorSubcampo5);
        recaudacion.setCodigovalorSubcampo6(valorSubcampo6);
        recaudacion.setNif(valorNif);
        recaudacion.setApellidosNombre(valorApellidosNombre);

        recaudacion.setImporteEuros(valorImporteCentimos/100);
        recaudacion.setEntidad(valorCodigoEntidad);
        recaudacion.setSucursal(valorCodigoSucursal);
        recaudacion.setArchivoFuente(ultimoFicheroTratado.getName());
        
     
            LOG.log(Level.FINE, "cobro recibido: {0}", recaudacion);

        return recaudacion;
    }
//     public ArrayList<Lote> procesarLoteFicheros(String loteDeUno) {
//         String[]param=new String[1];
//         param[0]=loteDeUno;
//         return procesarLoteFicheros(param);
//         
//     }
    public ArrayList<Lote> procesarLoteFicheros(String[] loteUrls) {
        boolean resultadoGlobalProcesamiento = true;
          ArrayList<Lote> lotesResueltos=new ArrayList();
    //    TratamientoFicherosDeRecaudacionesService.reiniciarProcesamiento();
        TratamientoFicherosDeRecaudacionesService.ficherosProcesados = 0;
        int ejercicioProceso=Calendar.getInstance().get(Calendar.YEAR);
        
        
        for (String tmpUrl : loteUrls) {
            File fichero=new File(tmpUrl);
            Lote loteResuelto=null;
             int paramEjercicio=0;
            TratamientoFicherosDeRecaudacionesService.ficherosProcesados++;
            if (procesarFichero(fichero)!=null) {
                loteResuelto=procesarFichero(fichero);  
                LOG.log(Level.INFO, "Procesado archivo {0}", tmpUrl);
                paramEjercicio=loteResuelto.getEjercicio();
                paramEjercicio=2020;
                for (Recaudacion cobro: loteResuelto.getListaRecaudacions()){
                    cobro.setArchivoFuente(fichero.getName());
                   
                    try{
                        cobro.setAnyo(Integer.parseInt(loteResuelto.getQuincenaAno().trim()));
                    }catch(NumberFormatException e){
                           LOG.log(Level.SEVERE, "La recaudacion {0} del fichero {1} posee trimestre EXTRANO {2}", new Object[]{cobro.getNumeroJustificante(), cobro.getArchivoFuente(), loteResuelto.getQuincenaAno()});   
     
                    }
                    
                    cobro=establecerFechaPlazo(cobro);
                   
             
                    
                }
                
                TratamientoFicherosDeRecaudacionesService.ficherosCorrectos++;
            } else {
                TratamientoFicherosDeRecaudacionesService.ficherosFallados++;
                resultadoGlobalProcesamiento = false;
            }
//            loteResuelto.setResultado(resultadoGlobalProcesamiento);

             lotesResueltos.add(loteResuelto);
        }
        return lotesResueltos;
    }

//        File file = new File(url);
//        if (file.exists() && file.canRead()) {
//
//            return procesarFichero(file);
//        } else {
//            LOG.log(Level.SEVERE, "El fichero " + url + " ha desaparecido del servidor o no es accesible");
//        }
//            LOG.log(Level.SEVERE, "El fichero " + url + " NO SE HA PODIDO PROCESAR CORRECTAMENTE");
//            return null;
//    }

    
    
//       private String obtenerFechaFinalPlazo(Apunte tmp) {
//        int ejercicio = tmp.getEjercicio();
//        String fecha = "00";
//        String dia = "20/";
//        int trimestre = tmp.getTrimestre();
//      
//
////        switch (trimestre) {
////            case 1:
////                fecha = dia + "04/" + ejercicio;
////                break;
////            case 2:
////                fecha = dia + "07/" + ejercicio;
////                break;
////            case 3:
////                fecha = dia + "10/" + ejercicio;
////                break;
////            case 4:
////                fecha = dia + "01/" + (ejercicio + 1);
////                break;
////            default:
////                LOG.log(Level.WARNING, "La recaudacion {0} posee trimestre {1}", new Object[]{tmp.getDenominacion(), trimestre});
////        }
//        return fecha;
//    }




    private Recaudacion establecerFechaPlazo(Recaudacion recaudacion) {
        
        
     int codigoValorTrimestre=recaudacion.getCodigovalorTrimestre();
     int codigoAnyoEjercicio=recaudacion.getCodigoAnyoEjercicio();
     int anoPlazoPago=0;
     int mesPlazoPago=0;
     int diaPlazoPago=23;
 

     int anyo = recaudacion.getAnyo();
     /*
        MARAVILLOSA SOLUCION A LA CHAPUZA DEL AÑO DEL FICHERO DE HACIENDA
        ESTO ES YA LA GRAN TRACA. 
        PREMIO AL MEJOR PROGRAMDOR DEL AÑO.
        ESPERO QUE ESTO NO SE VEA NUNCA   
     */
     if (anyo <= 2020) {
            anoPlazoPago=2010+codigoAnyoEjercicio;
     }else{    
         anoPlazoPago=2020+codigoAnyoEjercicio;
     }
   
     switch (codigoValorTrimestre) {
            case 1:
                mesPlazoPago=Calendar.APRIL;
                break;
            case 2:
                mesPlazoPago=Calendar.JULY;
                break;
            case 3:
                mesPlazoPago=Calendar.OCTOBER;
                break;
            case 4:
                mesPlazoPago=Calendar.JANUARY;
                anoPlazoPago++; //se pagara en el siguiente año
                break;
            default:
                LOG.log(Level.WARNING, "La recaudacion {0} del fichero {1} posee trimestre EXTRANO {2}", new Object[]{recaudacion.getNumeroJustificante(), recaudacion.getArchivoFuente(), recaudacion.getCodigovalorTrimestre()});   
     }

       
     Calendar cal=new GregorianCalendar(anoPlazoPago, mesPlazoPago, diaPlazoPago);  
     recaudacion.setFechaPlazo(new Date(cal.getTimeInMillis()));     
     
/*
     Según conversación mantenida con Angel Luis el día 4/6/2020
     El año de pago --> (dato leido del registro es el que va a definir
     el ejercicio REAL QUE ESTÁ PAGADO EL auditor
     
     Es decir:
     
     Un auditor puede pagar en 2019 algo que tenía que haber pagado en 2016
     
     */
     recaudacion.setAnyo(anoPlazoPago);
        
        return recaudacion;
    }

    private int leerCabecera(String linea) {
            CampoImportado c=new CampoImportado(14, 17);
int retorno=0;
         String retorno2 = linea.substring(c.posIni - 1, c.posFin);
         retorno=Integer.parseInt(retorno2);
 return retorno;
    }



    private static class ImpossibleProcessingException extends Exception {

        private String fichero;

        public void setFichero(String fichero) {
            this.fichero = fichero;
        }

        public ImpossibleProcessingException(int numeroLinea, String error) {
            super("No se ha podido procesar el fichero 2 a partir de la linea " + numeroLinea + " >>> " + error);
        }

        public ImpossibleProcessingException(String fichero, int numeroLinea, String error) {

            super("No se ha podido procesar el fichero " + fichero + " a partir de la linea " + numeroLinea + " >>> " + error);
            this.fichero = fichero;
        }

        public ImpossibleProcessingException(String fichero, int numeroLinea) {
            this(fichero, numeroLinea, "Fallo de Procesamiento");
            this.fichero = fichero;
        }

    }
    

}
