
package es.icac.meh.pandora.services.daos;


import es.icac.meh.pandora.modelo.LoteTrabajoRealizado;
import es.icac.meh.pandora.modelo.TrabajoRealizado;
import es.icac.meh.pandora.modelo.TrabajoRealizadoAtlas;
import es.icac.meh.pandora.services.ConnectionService;
import es.icac.meh.pandora.services.impl.CalculoTarifaServiceImpl;
import es.icac.meh.pandora.utils.JDBCUtilities;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MAJIVIAL
 */
public class TrabajosRealizadosDAO {

    private static final Logger LOG = Logger.getLogger(TrabajosRealizadosDAO.class.getName());

    private ConnectionService connection;
    
    private CalculoTarifaServiceImpl tarificador;
    
    public TrabajosRealizadosDAO(ConnectionService connection){
        this.connection=connection;    
    
    
    }

    public TrabajoRealizado[] kkobtenerTrabajosRealizados() throws SQLException {

        PreparedStatement pstmt = connection.getConnection().prepareStatement(sqlPreparada);
        int anoLiquidacion = 2019;

        String COD_ROAC         = "%";
        String Identificacion   = "%";
        String CIF_Auditada     = "%";
        String RazonSocial      = "%";
        /*
        String COD_ROAC         = "'%'+$[Parameter:COD_ROAC]$+'%'";
        String Identificacion   = "'%'+$[Parameter:Identificacion]$+'%'";
        String CIF_Auditada     = "'%'+$[Parameter:CIF_Auditada]$+'%'";
        String RazonSocial      = " '%'+$[Parameter:RazonSocial]$+'%'";
        */
        
        
        pstmt.setInt(1, anoLiquidacion);
        pstmt.setInt(2, anoLiquidacion);
        pstmt.setString(3, COD_ROAC);
        pstmt.setString(4, Identificacion);
        pstmt.setString(5, Identificacion);
        pstmt.setString(6, CIF_Auditada);
        pstmt.setString(7, RazonSocial);
        
     //   System.out.println("app.TrabajosRealizadosDAO.obtenerTrabajosRealizados()"+pstmt.toString());

        ResultSet rs = pstmt.executeQuery();

        ArrayList<TrabajoRealizado> trabajos = new ArrayList<>();

        if (rs != null) {
            while (rs.next()) {

                //DEBEN SER EXACTAMENTE LOS MISMOS CAMPOS QUE EL POJO
                String codRoac = rs.getString("COD_ROAC");

                 LOG.log(Level.INFO, "Leyedo FILAS DE LA TABLA DE TRABAJOS para el Trabajo ID="+codRoac);
                 
                String identificacion=rs.getString("Identificacion");
                String documento=rs.getString("Documento");
                String razonSocial=rs.getString("RazonSocial");
                String informeDeCuentas=rs.getString("informeDeCuentas");
                
                String primerEjercicioAuditado=rs.getString("PrimerEjercicioAuditado");
                String ejercicioAuditado=rs.getString("EjercicioAuditado");
                String constitucion=rs.getString("Constitucion");
                
                String ejercicioFinal=rs.getString("EjercicioFinal");
                int idEntidad=rs.getInt("IdEntidad");
                String descripcionEntidad=rs.getString("DescripcionEntidad");
                
                String participesAuditado=rs.getString("ParticipesAuditado");
                String participesAnterior=rs.getString("ParticipesAnterior");
                
                double importeNetoAuditado=rs.getDouble("ImporteNetoAuditado");
                double importeNetoAnterior=rs.getDouble("ImporteNetoAnterior");
                
                int plantillaMediaAuditado=rs.getInt("PlantillaMediaAuditado");           
                int plantillaMediaAnterior=rs.getInt("PlantillaMediaAnterior");
                
                int idTipoTrabajo=rs.getInt("IdTipoTrabajo");
                String descripcionTipoTrabajo=rs.getString("DescripcionTipoTrabajo");
                
                String fechaInforme=rs.getString("FechaInforme");       /*es la fecha importante*/
                
                String roacAuditorFirmante=rs.getString("ROACAuditorFirmante");
                String nombreAuditorFirmante=rs.getString("NombreAuditorFirmante");                
                String coauditoria=rs.getString("Coauditoria");                
                String roacCoauditor=rs.getString("ROACCoauditor");
                
                int idTipoOpinion=rs.getInt("IdTipoOpinion");
                String descripcionTipoOpinion=rs.getString("DescripcionTipoOpinion");
                
                int idProvincia=rs.getInt("IdProvincia");
                String provincia=rs.getString("Provincia");
                
                double facturacionAuditoriaHonorarios=rs.getDouble("FacturacionAuditoriaHonorarios");
                double facturacionAuditoriaHoras=rs.getDouble("FacturacionAuditoriaHoras");
                
                String cnmv=rs.getString("CNMV");
                String esInteresPublico=rs.getString("InteresPublico");
                
                double importeCifraActivo=rs.getDouble("ImporteCifraActivo");
                double importeCifraActivoAnterior=rs.getDouble("ImporteCifraActivoAnterior");
                
                double honorariosAuditoriaInterna=rs.getDouble("HonorariosAuditoriaInterna");
                double horasAuditoriaInterna=rs.getDouble("HorasAuditoriaInterna");
                
                double honorariosDiseno=rs.getDouble("HonorariosDiseno");
                double horasDiseno=rs.getDouble("HorasDiseno");
                
                double honorariosOtros=rs.getDouble("HonorariosOtros");
                double horasOtros=rs.getDouble("HorasOtros");
                
                TrabajoRealizado trabajo = null;
                try{
                trabajo = new TrabajoRealizado();
                trabajo.setCodRoac(codRoac);
                trabajo.setIdentificacion(identificacion);
                
                trabajo.setInformeDeCuentasString(informeDeCuentas);
                
                trabajo.setDocumento(documento);
                trabajo.setRazonSocial(razonSocial);

            //    trabajo.setEjercicioAuditado(ejercicioAuditado);
                
                trabajo.setConstitucion(constitucion);
                trabajo.setEjercicioFinal(ejercicioFinal);


                trabajo.setDescripcionTipoTrabajo(descripcionTipoTrabajo);
                
                trabajo.setFechaInforme(fechaInforme);

                trabajo.setCoauditoria(coauditoria);


                trabajo.setFacturacionAuditoriaHonorarios(facturacionAuditoriaHonorarios);

                trabajo.setEsInteresPublico(esInteresPublico);                
                    
            //        System.out.println("trabajo->"+trabajo);
                trabajos.add(trabajo);
                
                } catch (Exception creandoTrabajoException) {
                    LOG.log(Level.SEVERE, "==============================================");
                    LOG.log(Level.SEVERE, "ERROR CREANDO TRABAJO REALIZADO " + codRoac);
                    LOG.log(Level.SEVERE, "==============================================");
                    LOG.log(Level.SEVERE, "TRABAJO REALIZADO CONSTRUIDO HASTA EXCEPCION: ");
                    LOG.log(Level.SEVERE, trabajo.toString());
                    LOG.log(Level.SEVERE, "==============================================");
                }
            }
            
        } else {
            LOG.log(Level.WARNING, "Intentando obtener TrabajosRealizados desde una ResultSet y este es NULL");
            
        }
        
        pstmt.close();
        
        return(TrabajoRealizado[])   trabajos.toArray();
    }
    
    
        
    String sql="SELECT DISTINCT isnull(d.COD_ROAC, '') AS COD_ROAC, " +
"            CASE WHEN aud.Nombre is null or aud.Nombre = '' " +
"            THEN aud.Razon_Social " +
"            ELSE aud.Apellidos + ', ' + aud.Nombre " +
"            END as Identificacion, " +
"            isnull(i.Documento, '') AS Documento, " +
"             isnull(REPLACE(i.RazonSocial,'\"',''), '') AS RazonSocial, " +
"            [dbo].[FormatearInformeCuentas](InformeDeCuentas) AS InformeDeCuentas, " +
"            dbo.[FormatearFecha](PrimerEjercicioAuditado) AS PrimerEjercicioAuditado, " +
"            dbo.[FormatearFecha](EjercicioAuditado) AS EjercicioAuditado, " +
"            dbo.[FormatearCharSiNo](Constitucion) AS Constitucion, " +
"            dbo.[FormatearFecha](EjercicioFinal) AS EjercicioFinal, " +
"            isnull(i.IdEntidad, '') AS IdEntidad, " +
"            isnull(e.Desc_Entidad, '') AS DescripcionEntidad, " +
"            dbo.[FormatearIntSiNo](ParticipesAuditado) AS ParticipesAuditado, " +
"            dbo.[FormatearIntSiNo](ParticipesAnterior) AS ParticipesAnterior, " +
"            ImporteNetoAuditado AS ImporteNetoAuditado, " +
"            ImporteNetoAnterior AS ImporteNetoAnterior, " +
"            isnull(PlantillaMediaAuditado, 0) AS PlantillaMediaAuditado, " +
"            isnull(PlantillaMediaAnterior, 0) AS PlantillaMediaAnterior, " +
"            isnull(i.IdTipoTrabajo, '') AS IdTipoTrabajo, " +
"            isnull(t.Desc_Trabajo, '') AS DescripcionTipoTrabajo, " +
"            dbo.[FormatearFecha](FechaInforme) AS FechaInforme, " +
"            isnull(ROACAuditorFirmante, '') AS ROACAuditorFirmante, " +
"            isnull(NombreAuditorFirmante, '') AS NombreAuditorFirmante,             " +
"            dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe) AS Coauditoria, " +
"            MC.ROACCoAuditor AS ROACCoauditor, " +
"            isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion, " +
"            isnull(o.Desc_Opinion, 0) AS DescripcionTipoOpinion, " +
"            isnull(i.IdProvincia, 0) AS IdProvincia, " +
"            isnull(p.DescProvincia, '') AS Provincia,             " +
"            FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios, " +
"            FacturacionAuditoriaHoras AS FacturacionAuditoriaHoras, " +
"            EM.Desc_Corta AS CNMV, " +
"            dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0)) AS InteresPublico, " +
"            ImporteCifraActivo AS ImporteCifraActivo, " +
"            ImporteCifraActivoAnterior AS ImporteCifraActivoAnterior, " +
"            i.HonorariosAuditoriaInterna, " +
"            i.HorasAuditoriaInterna, " +
"            i.HonorariosDiseno, " +
"            i.HorasDiseno, " +
"            i.HonorariosOtros, " +
"            i.HorasOtros " +
"            FROM [dbo].[T_Modelo_Informe] i " +
"            INNER JOIN [dbo].[T_Modelo_Datos] d  " +
"            ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio = ? " +
"            INNER JOIN [dbo].[T_Modelo_Presentacion] pr  " +
"            ON pr.Id_Modelo = d.Id_Modelo " +
"            INNER JOIN [dbo].[T_Auditores_Sociedades] aud  " +
"            ON aud.COD_ROAC = pr.COD_ROAC " +
"            LEFT JOIN [dbo].[T_Provincias] p ON p.IdProvincia = i.IdProvincia             " +
"            LEFT JOIN [dbo].[T_Entidades] e ON e.Cod_Entidad = i.IdEntidad " +
"            AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio) " +
"            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio) " +
"            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null)) " +
"            LEFT JOIN [dbo].[T_Trabajo] t ON t.Cod_Trabajo = i.IdTipoTrabajo " +
"            LEFT JOIN [dbo].[T_Opinion] o ON o.Cod_Opinion = i.IdTipoOpinion " +
"            LEFT JOIN [dbo].[T_Modelo_Coauditor] MC ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe " +
"            LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV " +
"            INNER JOIN (  " +
"                SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha " +
"                FROM T_Modelo_Presentacion A " +
"                INNER JOIN T_Modelo_Datos B " +
"                ON A.Id_Modelo = B.Id_Modelo " +
"                WHERE B.Ejercicio = ? " +
"                GROUP BY A.COD_ROAC " +
"            ) MPMAX " +
"            ON pr.COD_ROAC = MPMAX.COD_ROAC " +
"            AND  pr.Fecha_Presentacion = MPMAX.fecha  " +
"            WHERE d.COD_ROAC like ? " +
"            AND (aud.Razon_Social like ? " +
"            OR aud.Apellidos + ', ' + aud.Nombre like ? " +
"            AND i.Documento like ? " +
"            AND i.RazonSocial like ? " +
"            order by 1,4 ";
    
    
    /*
"			 " +
"			PARAMETROS " +
"			 $[Parameter:EjercicioPresentado]$ " +
"			  $[Parameter:EjercicioPresentado]$ < SE REPITE " +
"			 '%'+$[Parameter:COD_ROAC]$+'%' " +
"            AND (aud.Razon_Social like '%'+$[Parameter:Identificacion]$+'%' " +
"            OR aud.Apellidos + ', ' + aud.Nombre like '%'+$[Parameter:Identificacion]$+'%') " +
"            AND i.Documento like '%'+$[Parameter:CIF_Auditada]$+'%' " +
"            AND i.RazonSocial like '%'+$[Parameter:RazonSocial]$+'%'";
    */
    
    
    
    String sqlPreparada="SELECT DISTINCT isnull(d.COD_ROAC, '') AS COD_ROAC, " +
"            CASE WHEN aud.Nombre is null or aud.Nombre = '' " +
"            THEN aud.Razon_Social " +
"            ELSE aud.Apellidos + ', ' + aud.Nombre " +
"            END as Identificacion, " +
"            isnull(i.Documento, '') AS Documento, " +
"             isnull(REPLACE(i.RazonSocial,'\"',''), '') AS RazonSocial, " +
"            [dbo].[FormatearInformeCuentas](InformeDeCuentas) AS InformeDeCuentas, " +
"            InformeDeCuentas AS InformeDeCuentasReal, " +
"            dbo.[FormatearFecha](PrimerEjercicioAuditado) AS PrimerEjercicioAuditado, " +
"            dbo.[FormatearFecha](EjercicioAuditado) AS EjercicioAuditado, " +
"            dbo.[FormatearCharSiNo](Constitucion) AS Constitucion, " +
"            dbo.[FormatearFecha](EjercicioFinal) AS EjercicioFinal, " +
"            isnull(i.IdEntidad, '') AS IdEntidad, " +
"            isnull(e.Desc_Entidad, '') AS DescripcionEntidad, " +
"            dbo.[FormatearIntSiNo](ParticipesAuditado) AS ParticipesAuditado, " +
"            dbo.[FormatearIntSiNo](ParticipesAnterior) AS ParticipesAnterior, " +
"            ImporteNetoAuditado AS ImporteNetoAuditado, " +
"            ImporteNetoAnterior AS ImporteNetoAnterior, " +
"            isnull(PlantillaMediaAuditado, 0) AS PlantillaMediaAuditado, " +
"            isnull(PlantillaMediaAnterior, 0) AS PlantillaMediaAnterior, " +
"            isnull(i.IdTipoTrabajo, '') AS IdTipoTrabajo, " +
"            isnull(t.Desc_Trabajo, '') AS DescripcionTipoTrabajo, " +
"            dbo.[FormatearFecha](FechaInforme) AS FechaInforme, " +
"            isnull(ROACAuditorFirmante, '') AS ROACAuditorFirmante, " +
"            isnull(NombreAuditorFirmante, '') AS NombreAuditorFirmante,             " +
"            dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe) AS Coauditoria, " +
"            MC.ROACCoAuditor AS ROACCoauditor, " +
"            isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion, " +
"            isnull(o.Desc_Opinion, 0) AS DescripcionTipoOpinion, " +
"            isnull(i.IdProvincia, 0) AS IdProvincia, " +
"            isnull(p.DescProvincia, '') AS Provincia,             " +
"            FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios, " +
"            FacturacionAuditoriaHoras AS FacturacionAuditoriaHoras, " +
"            EM.Desc_Corta AS CNMV, " +
"            dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0)) AS InteresPublico, " +
"            ImporteCifraActivo AS ImporteCifraActivo, " +
"            ImporteCifraActivoAnterior AS ImporteCifraActivoAnterior, " +
"            i.HonorariosAuditoriaInterna, " +
"            i.HorasAuditoriaInterna, " +
"            i.HonorariosDiseno, " +
"            i.HorasDiseno, " +
"            i.HonorariosOtros, " +
"            i.HorasOtros " +
"            FROM [dbo].[T_Modelo_Informe] i " +
"            INNER JOIN [dbo].[T_Modelo_Datos] d  " +
"            ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio = ? " +
"            INNER JOIN [dbo].[T_Modelo_Presentacion] pr  " +
"            ON pr.Id_Modelo = d.Id_Modelo " +
"            INNER JOIN [dbo].[T_Auditores_Sociedades] aud  " +
"            ON aud.COD_ROAC = pr.COD_ROAC " +
"            LEFT JOIN [dbo].[T_Provincias] p ON p.IdProvincia = i.IdProvincia             " +
"            LEFT JOIN [dbo].[T_Entidades] e ON e.Cod_Entidad = i.IdEntidad " +
"            AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio) " +
"            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio) " +
"            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null)) " +
"            LEFT JOIN [dbo].[T_Trabajo] t ON t.Cod_Trabajo = i.IdTipoTrabajo " +
"            LEFT JOIN [dbo].[T_Opinion] o ON o.Cod_Opinion = i.IdTipoOpinion " +
"            LEFT JOIN [dbo].[T_Modelo_Coauditor] MC ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe " +
"            LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV " +
"            INNER JOIN (  " +
"                SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha " +
"                FROM T_Modelo_Presentacion A " +
"                INNER JOIN T_Modelo_Datos B " +
"                ON A.Id_Modelo = B.Id_Modelo " +
"                WHERE B.Ejercicio =  ? " +
"                GROUP BY A.COD_ROAC " +
"            ) MPMAX " +
"            ON pr.COD_ROAC = MPMAX.COD_ROAC " +
"            AND  pr.Fecha_Presentacion = MPMAX.fecha " +
"            WHERE d.COD_ROAC like ? " +
"            AND (aud.Razon_Social like ? " +
"            OR aud.Apellidos + ', ' + aud.Nombre like ? " +
"            AND i.Documento like ? " +
"            AND i.RazonSocial like ? ";

//           order by 1,4";
    
    
    /*
"			 " +
"			PARAMETROS " +
"			 $[Parameter:EjercicioPresentado]$ " +
"			  $[Parameter:EjercicioPresentado]$ < SE REPITE " +
"			 '%'+$[Parameter:COD_ROAC]$+'%' " +
"            AND (aud.Razon_Social like '%'+$[Parameter:Identificacion]$+'%' " +
"            OR aud.Apellidos + ', ' + aud.Nombre like '%'+$[Parameter:Identificacion]$+'%') " +
"            AND i.Documento like '%'+$[Parameter:CIF_Auditada]$+'%' " +
"            AND i.RazonSocial like '%'+$[Parameter:RazonSocial]$+'%'";
    */
    
//        public ArrayList<TrabajoRealizado>obtenerTrabajosPrueba() throws SQLException {
//
//        Statement pstmt = connection.getConnection().createStatement();
//       
//        
//
//  //      System.out.println("app.TrabajosRealizadosDAO.obtenerTrabajosRealizados()"+pstmt.toString());
//
//        ResultSet rs = pstmt.executeQuery(queryPrueba);
//
//        ArrayList<TrabajoRealizado> trabajos = new ArrayList<>();
//
//        if (rs != null) {
//            while (rs.next()) {
//
//                //DEBEN SER EXACTAMENTE LOS MISMOS CAMPOS QUE EL POJO
//                String codRoac = rs.getString("COD_ROAC");
//
//        //         LOG.log(Level.INFO, "Leyedo FILAS DE LA TABLA DE TRABAJOS para el Trabajo ID="+codRoac);
//                 
//                String identificacion=rs.getString("Identificacion");
//                String documento=rs.getString("Documento");
//                String razonSocial=rs.getString("RazonSocial");
//                String informeDeCuentas=rs.getString("InformeDeCuentas");
//                String primerEjercicioAuditado=rs.getString("PrimerEjercicioAuditado");
//                String ejercicioAuditado=rs.getString("EjercicioAuditado");
//                String constitucion=rs.getString("Constitucion");
//                
//                String ejercicioFinal=rs.getString("EjercicioFinal");
//                int idEntidad=rs.getInt("IdEntidad");
//                String descripcionEntidad=rs.getString("DescripcionEntidad");
//                
//                String participesAuditado=rs.getString("ParticipesAuditado");
//                String participesAnterior=rs.getString("ParticipesAnterior");
//                
//                double importeNetoAuditado=rs.getDouble("ImporteNetoAuditado");
//                double importeNetoAnterior=rs.getDouble("ImporteNetoAnterior");
//                
//                int plantillaMediaAuditado=rs.getInt("PlantillaMediaAuditado");           
//                int plantillaMediaAnterior=rs.getInt("PlantillaMediaAnterior");
//                
//                int idTipoTrabajo=rs.getInt("IdTipoTrabajo");
//                String descripcionTipoTrabajo=rs.getString("DescripcionTipoTrabajo");
//                
//                String fechaInforme=rs.getString("FechaInforme");       /*es la fecha importante*/
//                
//               
//                String roacAuditorFirmante=rs.getString("ROACAuditorFirmante");
//         
//                String nombreAuditorFirmante=rs.getString("NombreAuditorFirmante");                
//                String coauditoria=rs.getString("Coauditoria");                
//                String roacCoauditor=rs.getString("ROACCoauditor");
//                
//                int idTipoOpinion=rs.getInt("IdTipoOpinion");
//                String descripcionTipoOpinion=rs.getString("DescripcionTipoOpinion");
//                
//                int idProvincia=rs.getInt("IdProvincia");
//                String provincia=rs.getString("Provincia");
//                
//                double facturacionAuditoriaHonorarios=rs.getDouble("FacturacionAuditoriaHonorarios");
//                double facturacionAuditoriaHoras=rs.getDouble("FacturacionAuditoriaHoras");
//                
//                String cnmv=rs.getString("CNMV");
//                String esInteresPublico=rs.getString("InteresPublico");
//                
//                double importeCifraActivo=rs.getDouble("ImporteCifraActivo");
//                double importeCifraActivoAnterior=rs.getDouble("ImporteCifraActivoAnterior");
//                
//                double honorariosAuditoriaInterna=rs.getDouble("HonorariosAuditoriaInterna");
//                double horasAuditoriaInterna=rs.getDouble("HorasAuditoriaInterna");
//                
//                double honorariosDiseno=rs.getDouble("HonorariosDiseno");
//                double horasDiseno=rs.getDouble("HorasDiseno");
//                
//                double honorariosOtros=rs.getDouble("HonorariosOtros");
//                double horasOtros=rs.getDouble("HorasOtros");
//                
//                TrabajoRealizado trabajo = null;
//                try {
//                    trabajo = new TrabajoRealizado();
//                    trabajo.setCodRoac(codRoac);
//                    trabajo.setIdentificacion(identificacion);
//                    trabajo.setDocumento(documento);
//                    trabajo.setRazonSocial(razonSocial);
//
//        //            trabajo.setEjercicioAuditado(ejercicioAuditado);
//
//                    trabajo.setConstitucion(constitucion);
//                    trabajo.setEjercicioFinal(ejercicioFinal);
//
//
//
//                    trabajo.setDescripcionTipoTrabajo(descripcionTipoTrabajo);
//
//                    trabajo.setFechaInforme(fechaInforme);
//
//                    trabajo.setCoauditoria(coauditoria);
//     
//                    trabajo.setFacturacionAuditoriaHonorarios(facturacionAuditoriaHonorarios);
//                                     trabajo.setEsInteresPublico(esInteresPublico);
//                  
//                    trabajos.add(trabajo);
//
//                } catch (Exception creandoTrabajoException) {
//                    LOG.log(Level.SEVERE, "==============================================");              
//                    LOG.log(Level.SEVERE, "ERROR CREANDO TRABAJO REALIZADO " + codRoac, creandoTrabajoException);
//                    LOG.log(Level.SEVERE, "==============================================");
//                    LOG.log(Level.SEVERE, "TRABAJO REALIZADO CONSTRUIDO HASTA EXCEPCION: ");
//                    LOG.log(Level.SEVERE, trabajo!=null?trabajo.toString():"NULL");
//                    LOG.log(Level.SEVERE, "==============================================");
//                }
//            }            
//        } else {
//            LOG.log(Level.WARNING, "Intentando obtener TrabajosRealizados desde una ResultSet y este es NULL");            
//        }        
//        pstmt.close();
//        
//        return  trabajos;
//    } 
    public boolean borrarDatosTabla() throws SQLException {

        Statement pstmt = connection.getConnection().createStatement();
       
        return pstmt.execute("TRUNCATE TABLE T_TRABAJOS_REALIZADOS");   
    }    
    public boolean borrarDatosTabla(String pEjercicio) throws SQLException {

        Statement pstmt = connection.getConnection().createStatement();

        String sql = "DELETE FROM T_TRABAJOS_REALIZADOS WHERE calcEjercicio=" + pEjercicio;
      
        LOG.log(Level.WARNING, "Se han borrado trabajos del ejercicio {0} " + pEjercicio);
 return pstmt.execute(sql);
  

    }
    String queryPrueba = "SELECT DISTINCT isnull(d.COD_ROAC, '') AS COD_ROAC, "
            + "            CASE WHEN aud.Nombre is null or aud.Nombre = '' "
            + "            THEN aud.Razon_Social\n"
            + "            ELSE aud.Apellidos + ', ' + aud.Nombre\n"
            + "            END as Identificacion,\n"
            + "            isnull(i.Documento, '') AS Documento,\n"
            + "             isnull(REPLACE(i.RazonSocial,'\"',''), '') AS RazonSocial,\n"
            + "            [dbo].[FormatearInformeCuentas](InformeDeCuentas) AS InformeDeCuentas,\n"
            + "            dbo.[FormatearFecha](PrimerEjercicioAuditado) AS PrimerEjercicioAuditado,\n"
            + "            dbo.[FormatearFecha](EjercicioAuditado) AS EjercicioAuditado,\n"
            + "            dbo.[FormatearCharSiNo](Constitucion) AS Constitucion,\n"
            + "            dbo.[FormatearFecha](EjercicioFinal) AS EjercicioFinal,\n"
            + "            isnull(i.IdEntidad, '') AS IdEntidad,\n"
            + "            isnull(e.Desc_Entidad, '') AS DescripcionEntidad,\n"
            + "            dbo.[FormatearIntSiNo](ParticipesAuditado) AS ParticipesAuditado,\n"
            + "            dbo.[FormatearIntSiNo](ParticipesAnterior) AS ParticipesAnterior,\n"
            + "            ImporteNetoAuditado AS ImporteNetoAuditado,\n"
            + "            ImporteNetoAnterior AS ImporteNetoAnterior,\n"
            + "            isnull(PlantillaMediaAuditado, 0) AS PlantillaMediaAuditado,\n"
            + "            isnull(PlantillaMediaAnterior, 0) AS PlantillaMediaAnterior,\n"
            + "            isnull(i.IdTipoTrabajo, '') AS IdTipoTrabajo,\n"
            + "            isnull(t.Desc_Trabajo, '') AS DescripcionTipoTrabajo,\n"
            + "            dbo.[FormatearFecha](FechaInforme) AS FechaInforme,\n"
            + "            isnull(ROACAuditorFirmante, '') AS ROACAuditorFirmante,\n"
            + "            isnull(NombreAuditorFirmante, '') AS NombreAuditorFirmante,            \n"
            + "            dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe) AS Coauditoria,\n"
            + "            MC.ROACCoAuditor AS ROACCoauditor,\n"
            + "            isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion,\n"
            + "            isnull(o.Desc_Opinion, 0) AS DescripcionTipoOpinion,\n"
            + "            isnull(i.IdProvincia, 0) AS IdProvincia,\n"
            + "            isnull(p.DescProvincia, '') AS Provincia,            \n"
            + "            FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios,\n"
            + "            FacturacionAuditoriaHoras AS FacturacionAuditoriaHoras,\n"
            + "            EM.Desc_Corta AS CNMV,\n"
            + "            dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0)) AS InteresPublico,\n"
            + "            ImporteCifraActivo AS ImporteCifraActivo,\n"
            + "            ImporteCifraActivoAnterior AS ImporteCifraActivoAnterior,\n"
            + "            i.HonorariosAuditoriaInterna,\n"
            + "            i.HorasAuditoriaInterna,\n"
            + "            i.HonorariosDiseno,\n"
            + "            i.HorasDiseno,\n"
            + "            i.HonorariosOtros,\n"
            + "            i.HorasOtros\n"
            + "            FROM [dbo].[T_Modelo_Informe] i\n"
            + "            INNER JOIN [dbo].[T_Modelo_Datos] d \n"
            + "            ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio  < 9999\n"
            + "            INNER JOIN [dbo].[T_Modelo_Presentacion] pr \n"
            + "            ON pr.Id_Modelo = d.Id_Modelo\n"
            + "            INNER JOIN [dbo].[T_Auditores_Sociedades] aud \n"
            + "            ON aud.COD_ROAC = pr.COD_ROAC\n"
            + "            LEFT JOIN [dbo].[T_Provincias] p ON p.IdProvincia = i.IdProvincia            \n"
            + "            LEFT JOIN [dbo].[T_Entidades] e ON e.Cod_Entidad = i.IdEntidad\n"
            + "            AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio)\n"
            + "            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio)\n"
            + "            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null))\n"
            + "            LEFT JOIN [dbo].[T_Trabajo] t ON t.Cod_Trabajo = i.IdTipoTrabajo\n"
            + "            LEFT JOIN [dbo].[T_Opinion] o ON o.Cod_Opinion = i.IdTipoOpinion\n"
            + "            LEFT JOIN [dbo].[T_Modelo_Coauditor] MC ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe\n"
            + "            LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV\n"
            + "            INNER JOIN ( \n"
            + "                SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha\n"
            + "                FROM T_Modelo_Presentacion A\n"
            + "                INNER JOIN T_Modelo_Datos B\n"
            + "                ON A.Id_Modelo = B.Id_Modelo\n"
            + "                WHERE B.Ejercicio < 9999\n"
            + "                GROUP BY A.COD_ROAC\n"
            + "            ) MPMAX\n"
            + "            ON pr.COD_ROAC = MPMAX.COD_ROAC\n"
            + "            AND  pr.Fecha_Presentacion = MPMAX.fecha \n"
            + "            WHERE d.COD_ROAC like '%'+'%'\n"
            + "            AND (aud.Razon_Social like '%'+'%'\n"
            + "            OR aud.Apellidos + ', ' + aud.Nombre like '%'+'%')\n"
            + "            AND i.Documento like '%'+'%'\n"
            + "            AND i.RazonSocial like '%'+'%'\n"
            + "            order by 1,4";

    String queryParametrizadaKAO = "SELECT DISTINCT isnull(d.COD_ROAC, '') AS COD_ROAC, "
            + "            CASE WHEN aud.Nombre is null or aud.Nombre = '' "
            + "            THEN aud.Razon_Social "
            + "            ELSE aud.Apellidos + ', ' + aud.Nombre "
            + "            END as Identificacion, "
            + "            isnull(i.Documento, '') AS Documento, "
            + "             isnull(REPLACE(i.RazonSocial,'\"',''), '') AS RazonSocial, "
            + "            [dbo].[FormatearInformeCuentas](InformeDeCuentas) AS InformeDeCuentas, "
            + "            dbo.[FormatearFecha](PrimerEjercicioAuditado) AS PrimerEjercicioAuditado, "
            + "            dbo.[FormatearFecha](EjercicioAuditado) AS EjercicioAuditado, "
            + "            dbo.[FormatearCharSiNo](Constitucion) AS Constitucion, "
            + "            dbo.[FormatearFecha](EjercicioFinal) AS EjercicioFinal, "
            + "            isnull(i.IdEntidad, '') AS IdEntidad, "
            + "            isnull(e.Desc_Entidad, '') AS DescripcionEntidad, "
            + "            dbo.[FormatearIntSiNo](ParticipesAuditado) AS ParticipesAuditado, "
            + "            dbo.[FormatearIntSiNo](ParticipesAnterior) AS ParticipesAnterior, "
            + "            ImporteNetoAuditado AS ImporteNetoAuditado, "
            + "            ImporteNetoAnterior AS ImporteNetoAnterior, "
            + "            isnull(PlantillaMediaAuditado, 0) AS PlantillaMediaAuditado, "
            + "            isnull(PlantillaMediaAnterior, 0) AS PlantillaMediaAnterior, "
            + "            isnull(i.IdTipoTrabajo, '') AS IdTipoTrabajo, "
            + "            isnull(t.Desc_Trabajo, '') AS DescripcionTipoTrabajo, "
            + "            dbo.[FormatearFecha](FechaInforme) AS FechaInforme, "
            + "            isnull(ROACAuditorFirmante, '') AS ROACAuditorFirmante, "
            + "            isnull(NombreAuditorFirmante, '') AS NombreAuditorFirmante,             "
            + "            dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe) AS Coauditoria, "
            + "            MC.ROACCoAuditor AS ROACCoauditor, "
            + "            isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion, "
            + "            isnull(o.Desc_Opinion, 0) AS DescripcionTipoOpinion, "
            + "            isnull(i.IdProvincia, 0) AS IdProvincia, "
            + "            isnull(p.DescProvincia, '') AS Provincia,             "
            + "            FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios, "
            + "            FacturacionAuditoriaHoras AS FacturacionAuditoriaHoras, "
            + "            EM.Desc_Corta AS CNMV, "
            + "            dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0)) AS InteresPublico, "
            + "            ImporteCifraActivo AS ImporteCifraActivo, "
            + "            ImporteCifraActivoAnterior AS ImporteCifraActivoAnterior, "
            + "            i.HonorariosAuditoriaInterna, "
            + "            i.HorasAuditoriaInterna, "
            + "            i.HonorariosDiseno, "
            + "            i.HorasDiseno, "
            + "            i.HonorariosOtros, "
            + "            i.HorasOtros "
            + "            FROM [dbo].[T_Modelo_Informe] i "
            + "            INNER JOIN [dbo].[T_Modelo_Datos] d  "
            + "            ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio  = ?"
            + /* PARAMETRO 1*/ "            INNER JOIN [dbo].[T_Modelo_Presentacion] pr  "
            + "            ON pr.Id_Modelo = d.Id_Modelo "
            + "            INNER JOIN [dbo].[T_Auditores_Sociedades] aud  "
            + "            ON aud.COD_ROAC = pr.COD_ROAC "
            + "            LEFT JOIN [dbo].[T_Provincias] p ON p.IdProvincia = i.IdProvincia             "
            + "            LEFT JOIN [dbo].[T_Entidades] e ON e.Cod_Entidad = i.IdEntidad "
            + "            AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio) "
            + "            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio) "
            + "            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null)) "
            + "            LEFT JOIN [dbo].[T_Trabajo] t ON t.Cod_Trabajo = i.IdTipoTrabajo "
            + "            LEFT JOIN [dbo].[T_Opinion] o ON o.Cod_Opinion = i.IdTipoOpinion "
            + "            LEFT JOIN [dbo].[T_Modelo_Coauditor] MC ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe "
            + "            LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV "
            + "            INNER JOIN (  "
            + "                SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha "
            + "                FROM T_Modelo_Presentacion A "
            + "                INNER JOIN T_Modelo_Datos B "
            + "                ON A.Id_Modelo = B.Id_Modelo "
            + "                WHERE B.Ejercicio = ? "
            + /* PARAMETRO 2*/ "                GROUP BY A.COD_ROAC "
            + "            ) MPMAX "
            + "            ON pr.COD_ROAC = MPMAX.COD_ROAC "
            + "            AND  pr.Fecha_Presentacion = MPMAX.fecha  "
            + "            WHERE d.COD_ROAC like ? "
            + /* PARAMETRO 3*/ "            AND (aud.Razon_Social like ? "
            + /* PARAMETRO 4*/ "            OR aud.Apellidos + ', ' + aud.Nombre like ? "
            + /* PARAMETRO 5*/ "            AND i.Documento like ? "
            + /* PARAMETRO 6*/ "            AND i.RazonSocial like ? ";// + /* PARAMETRO 7         order by 1,4";

//ESTA ES LA CONSULTA BUENA 09/05/2020 ALEXIS
    public int insertarTrabajosRealizadoDesdeAtlas(TrabajoRealizadoAtlas[] listaTrabajos, LoteTrabajoRealizado lote, int ejercicio) {

        int[] trabajosInsertados = null;

        String sqlPreparada = "INSERT INTO T_TRABAJOS_REALIZADOS( "
                + " FK_loteTrabajo, "
                + " codRoac,  "
                + " identificacion,  "
                + " documento, "
                + " nifAuditor,"
                + " razonSocial, "
                + " calcTrimestre, "
                + " calcEjercicio,"
                + " descripcionTipoTrabajo,  "
                + " informeDeCuentas, "
                + " fechaInforme, "
                + " facturacionAuditoriaHonorarios, "
                + " coauditoria, "
                + " esInteresPublico,"
                + " tasa,"
                + " idTrabajoAtlas,"
                + " ejercicio"
                + ") "
                + "VALUES("
                + "?,?,?,?,"
                + "?,?,?,?,"
                + "?,?,?,?,"
                + "?,?,?,?,?"
                + ")";

        boolean retorno = true;
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.getConnection().prepareStatement(sqlPreparada);

            for (TrabajoRealizadoAtlas trabajo : listaTrabajos) {

                pstmt.setLong(1, 0);  //es el lote    lote.getId()    CAMBIAR
                pstmt.setString(2, trabajo.getCodRoac());
                pstmt.setString(3, trabajo.getIdentificacion());
                pstmt.setString(4, trabajo.getDocumento());
                pstmt.setString(5, trabajo.getNifAuditor());
                pstmt.setString(6, trabajo.getRazonSocial());
                pstmt.setInt(7, trabajo.getCalcTrimestre());
                pstmt.setInt(8, trabajo.getCalcEjercicio());
                pstmt.setString(9, trabajo.getDescripcionTipoTrabajo());
                pstmt.setString(10, trabajo.getInformeDeCuentas());// 100 indiviual 200-201 consolidado

                java.sql.Date date = new java.sql.Date(trabajo.getFechaInforme().getTime());
                pstmt.setDate(11, date);
                pstmt.setDouble(12, trabajo.getFacturacionAuditoriaHonorarios());
                pstmt.setBoolean(13, trabajo.isCoauditoria());
                pstmt.setBoolean(14, trabajo.isEsInteresPublico());

                ////arrreglar
                pstmt.setDouble(15, trabajo.getTasa());
                // pstmt.setDouble(15,DDDDDDDDDD tarificador.damePrecio(trabajo));

                pstmt.setLong(16, 99);
                pstmt.setInt(17, ejercicio);
                pstmt.addBatch();

            }
            trabajosInsertados = pstmt.executeBatch();

            LOG.log(Level.INFO, "Insertados {0} trabajos", trabajosInsertados.length);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Error en la insercion de TRABAJOS DESDE ATLAS  \n" , ex);
            try {
                JDBCUtilities.printSqlStatement(pstmt, sqlPreparada);
            } catch (SQLException ex1) {
                LOG.log(Level.SEVERE, "error mostrando sentencia preparda ", ex1);
            }
            retorno = false;
        }

        return trabajosInsertados.length;

    }

    public ArrayList<TrabajoRealizado> obtenerTrabajosPorRoacEjerc(String paramROAC, String paramEJERC) throws SQLException {

//        String COD_ROAC = "'%'+'%'";
//        String Identificacion = "'%AMO%'";
//        String CIF_Auditada = "'%'+'%'";
//        String RazonSocial = "'%'+'%'";
//        int ejercicio = 2018;
        String queryVER = "SELECT "
                + "            id,\n"
                + "            FK_loteTrabajo,    \n"
                + "            codRoac ,\n"
                + "            identificacion,\n"
                + "            documento,\n"
                + "            razonSocial,\n"
                + "            calcTrimestre,\n"
                + "            calcEjercicio,\n"
                + "            descripcionTipoTrabajo,\n"
                + "            informeDeCuentas,\n"
                + "            fechaInforme,\n"
                + "            facturacionAuditoriaHonorarios,\n"
                + "            coauditoria,\n"
                + "            esInteresPublico,\n"
                + "            tasa,\n"
                + "            idTrabajoAtlas\n"
                + " FROM t_TRABAJOS_REALIZADOS; ";

        ArrayList<TrabajoRealizado> trabajos;
        try (Statement pstmt = connection.getConnection().createStatement()) {
            ResultSet rs = pstmt.executeQuery(queryVER);
            trabajos = new ArrayList<>();
            if (rs != null) {
                while (rs.next()) {
                    //DEBEN SER EXACTAMENTE LOS MISMOS CAMPOS QUE EL POJO
                    String codRoac = rs.getString("codRoac");
                    //LOG.log(Level.INFO, "Leyedo FILAS DE LA TABLA DE TRABAJOS para el Trabajo ID="+codRoac);
                    String identificacion = rs.getString("identificacion");
                    String documento = rs.getString("documento");
                    String razonSocial = rs.getString("razonSocial");

                    int trimestre = rs.getInt("calcTrimestre");
                    int ejercicio = rs.getInt("calcEjercicio");

                    String descripcionTipoTrabajo = rs.getString("descripcionTipoTrabajo");
                    String informeDeCuentas = rs.getString("informeDeCuentas");
                    Date fechaInforme = rs.getDate("FechaInforme");
                    /*es la fecha importante*/

                    double facturacionAuditoriaHonorarios = rs.getDouble("FacturacionAuditoriaHonorarios");
                    double tasa = rs.getDouble("tasa");

                    TrabajoRealizado trabajo = null;
                    try {
                        trabajo = new TrabajoRealizado();
                        trabajo.setCodRoac(codRoac);
                        trabajo.setIdentificacion(identificacion);
                        trabajo.setDocumento(documento);
                        trabajo.setRazonSocial(razonSocial);
                        trabajo.setInformeDeCuentasString(informeDeCuentas);
                        trabajo.setDescripcionTipoTrabajo(descripcionTipoTrabajo);
                        trabajo.setFechaInforme(fechaInforme);
                        trabajo.setFacturacionAuditoriaHonorarios(facturacionAuditoriaHonorarios);
                        trabajo.setTasa(tasa);
                        trabajos.add(trabajo);

                    } catch (Exception creandoTrabajoException) {
                        LOG.log(Level.SEVERE, "==============================================");
                        LOG.log(Level.SEVERE, "ERROR CREANDO TRABAJO REALIZADO " + codRoac, creandoTrabajoException);
                        LOG.log(Level.SEVERE, "==============================================");
                        LOG.log(Level.SEVERE, "TRABAJO REALIZADO CONSTRUIDO HASTA EXCEPCION: ");
                        LOG.log(Level.SEVERE, trabajo != null ? trabajo.toString() : "NULL");
                        LOG.log(Level.SEVERE, "==============================================");
                    }
                }
            } else {
                LOG.log(Level.WARNING, "Intentando obtener TrabajosRealizados desde una ResultSet y este es NULL");
            }
            if (rs == null) {
                rs.close();
            }
        }
        return trabajos;
    }

    public ConnectionService getConnection() {
        return connection;
    }

    public void setConnection(ConnectionService connection) {
        this.connection = connection;
    }

//    public ArrayList<TrabajoRealizado> KKobtenerTrabajosPorPeriodo(String paramROAC, int paramEJERC) throws SQLException {
//
//        //"WHERE d.COD_ROAC like '%S0530%' and FechaInforme between '20180401' and '20180430'\n"
//        StringBuilder sb = new StringBuilder(" between ")
//                .append("'").append(paramEJERC - 1).append("1001").append("'")
//                .append(" and ")
//                .append("'").append(paramEJERC).append("0930").append("'");
//        String ejercicio = String.valueOf(paramEJERC);
//        String periodo = sb.toString();
//
//        String sql = "SELECT DISTINCT isnull(d.COD_ROAC, '')				AS COD_ROAC, \n"
//                + "CASE WHEN aud.Nombre is null or aud.Nombre='' THEN \n"
//                + "			aud.Razon_Social ELSE aud.Apellidos+', '+aud.Nombre END	AS Identificacion,  \n"
//                + "isnull(i.Documento, '')							AS Documento,   \n"
//                + "isnull(REPLACE(i.RazonSocial,'\"',''), '')					AS RazonSocial,  \n"
//                + "dbo.[FormatearFecha](EjercicioAuditado)					AS EjercicioAuditado,  \n"
//                + "dbo.[FormatearFecha](EjercicioFinal)						AS EjercicioFinal,\n"
//                + "ImporteNetoAuditado								AS ImporteNetoAuditado,  \n"
//                + "isnull(PlantillaMediaAnterior, 0)						AS PlantillaMediaAnterior,  \n"
//                + "isnull(i.IdTipoTrabajo, '')							AS IdTipoTrabajo,  \n"
//                + "isnull(t.Desc_Trabajo, '')							AS DescripcionTipoTrabajo,  \n"
//                + "dbo.[FormatearFecha](FechaInforme)						AS FechaInforme,  \n"
//                + "FechaInforme									AS FechaInformeBrut0,  \n"
//                + "isnull(ROACAuditorFirmante, '')						AS ROACAuditorFirmante,  \n"
//                + "isnull(NombreAuditorFirmante, '')						AS NombreAuditorFirmante,   \n"
//                + "dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe)					AS Coauditoria,  \n"
//                + "MC.ROACCoAuditor								AS ROACCoauditor,  \n"
//                + "isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion,  isnull(o.Desc_Opinion, 0)	AS DescripcionTipoOpinion,  \n"
//                + "isnull(i.IdProvincia, 0) AS IdProvincia,  isnull(p.DescProvincia, '')	AS Provincia,  \n"
//                + "FacturacionAuditoriaHonorarios                                               AS FacturacionAuditoriaHonorarios,  \n"
//                + "FacturacionAuditoriaHoras                                                    AS FacturacionAuditoriaHoras, \n"
//                + "EM.Desc_Corta								AS CNMV,  \n"
//                + "dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0))				AS InteresPublico, \n"
//                + "ImporteCifraActivo AS ImporteCifraActivo, i.HorasOtros ,  i.HonorariosOtros  \n"
//                + "\n"
//                + "FROM [dbo].[T_Modelo_Informe] i  \n"
//                + "INNER JOIN [dbo].[T_Modelo_Datos] d				ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio= '" + ejercicio + "'\n"
//                + "INNER JOIN [dbo].[T_Modelo_Presentacion] pr                  ON pr.Id_Modelo = d.Id_Modelo\n"
//                + "INNER JOIN [dbo].[T_Auditores_Sociedades] aud                ON aud.COD_ROAC = pr.COD_ROAC\n"
//                + "LEFT JOIN [dbo].[T_Provincias] p				ON p.IdProvincia = i.IdProvincia   \n"
//                + "LEFT JOIN [dbo].[T_Entidades] e				ON e.Cod_Entidad = i.IdEntidad  AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio)  OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio)  OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null))  LEFT JOIN [dbo].[T_Trabajo] t\n"
//                + "								ON t.Cod_Trabajo = i.IdTipoTrabajo  \n"
//                + "LEFT JOIN [dbo].[T_Opinion] o				ON o.Cod_Opinion = i.IdTipoOpinion\n"
//                + "LEFT JOIN [dbo].[T_Modelo_Coauditor] MC			ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe  \n"
//                + "LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM                  ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV\n"
//                + "INNER JOIN (SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha FROM T_Modelo_Presentacion A\n"
//                + "INNER JOIN T_Modelo_Datos B	ON A.Id_Modelo = B.Id_Modelo\n "
//                + " WHERE B.Ejercicio= '" + ejercicio + "' GROUP BY A.COD_ROAC ) \n"
//                + " MPMAX ON pr.COD_ROAC = MPMAX.COD_ROAC  AND  pr.Fecha_Presentacion = MPMAX.fecha\n"
//                + "\n"
//                + "WHERE d.COD_ROAC like '" + paramROAC + "' and FechaInforme " + periodo
//                + " order by 1,4\n"
//                + "";
//
//        System.out.println("kk_>SQL=\n" + sql);
//
//        ArrayList<TrabajoRealizado> trabajos;
//        try (Statement pstmt = connection.getConnection().createStatement()) {
//            ResultSet rs = pstmt.executeQuery(sql);
//            trabajos = new ArrayList<>();
//            if (rs != null) {
//                while (rs.next()) {
//
//                    //DEBEN SER EXACTAMENTE LOS MISMOS CAMPOS QUE EL POJO
//                    String codRoac = rs.getString("COD_ROAC");
//                    String identificacion = rs.getString("Identificacion");
//                    String documento = rs.getString("Documento");
//                    String nifAuditor = rs.getString("nifAuditor");
//                    String razonSocial = rs.getString("RazonSocial");
//                    String ejercicioAuditado = rs.getString("EjercicioAuditado");
//                    String ejercicioFinal = rs.getString("EjercicioFinal");
//                    double importeNetoAuditado = rs.getDouble("ImporteNetoAuditado");
//                    int idTipoTrabajo = rs.getInt("IdTipoTrabajo");
//                    String descripcionTipoTrabajo = rs.getString("DescripcionTipoTrabajo");
//
//                    String fechaInforme = rs.getString("FechaInforme");
//                    /*es la fecha importante*/
//
//                    String roacAuditorFirmante = rs.getString("ROACAuditorFirmante");
//                    String nombreAuditorFirmante = rs.getString("NombreAuditorFirmante");
//                    String coauditoria = rs.getString("Coauditoria");
//                    String roacCoauditor = rs.getString("ROACCoauditor");
//                    String descripcionTipoOpinion = rs.getString("DescripcionTipoOpinion");
//                    String provincia = rs.getString("Provincia");
//                    double facturacionAuditoriaHonorarios = rs.getDouble("FacturacionAuditoriaHonorarios");
//                    double facturacionAuditoriaHoras = rs.getDouble("FacturacionAuditoriaHoras");
//                    String cnmv = rs.getString("CNMV");
//                    String esInteresPublico = rs.getString("InteresPublico");
//                    double importeCifraActivo = rs.getDouble("ImporteCifraActivo");
//
//                    TrabajoRealizado trabajo = null;
//                    try {
//                        trabajo = new TrabajoRealizado();
//                        trabajo.setCodRoac(codRoac);
//                        trabajo.setIdentificacion(identificacion);
//                        trabajo.setDocumento(documento);
//                        trabajo.setNif(nifAuditor);
//                        trabajo.setRazonSocial(razonSocial);
//                        // trabajo.setEjercicioAuditado(ejercicioAuditado);
//                        // trabajo.setEjercicioFinal(ejercicioFinal);
//                        trabajo.setDescripcionTipoTrabajo(descripcionTipoTrabajo);
//                        trabajo.setFechaInforme(fechaInforme);
//
//                        trabajo.setCoauditoria(coauditoria);
//                        trabajo.setFacturacionAuditoriaHonorarios(facturacionAuditoriaHonorarios);
//                        trabajo.setEsInteresPublico(esInteresPublico);
//
//                        trabajos.add(trabajo);
//
//                    } catch (Exception creandoTrabajoException) {
//                        LOG.log(Level.SEVERE, "==============================================");
//                        LOG.log(Level.SEVERE, "ERROR CREANDO TRABAJO REALIZADO " + codRoac, creandoTrabajoException);
//                        LOG.log(Level.SEVERE, "==============================================");
//                        LOG.log(Level.SEVERE, "TRABAJO REALIZADO CONSTRUIDO HASTA EXCEPCION: ");
//                        LOG.log(Level.SEVERE, trabajo != null ? trabajo.toString() : "NULL");
//                        LOG.log(Level.SEVERE, "==============================================");
//                    }
//                }
//            } else {
//                LOG.log(Level.WARNING, "Intentando obtener TrabajosRealizados desde una ResultSet y este es NULL");
//            }
//        } //auto-cierre del objeto statement
//
//        return trabajos;
//    }

    public boolean crearTabla() throws SQLException {
        String sql = "	 CREATE TABLE T_TRABAJOS_REALIZADOS (\n" +
"            id INT NOT NULL IDENTITY PRIMARY KEY,\n" +
"            FK_loteTrabajo INT,    \n" +
"            codRoac VARCHAR(125),\n" +
"            identificacion VARCHAR(255),\n" +
"            documento VARCHAR(125),\n" +
"            nifAuditor VARCHAR(125),\n" +
"	     informeDeCuentas VARCHAR(125),\n" +
"            razonSocial VARCHAR(255),\n" +
"            calcTrimestre INT,\n" +
"            calcEjercicio INT,\n" +
"            descripcionTipoTrabajo VARCHAR(255),\n" +
"            fechaInforme datetime,\n" +
"            facturacionAuditoriaHonorarios FLOAT,\n" +
"            coauditoria BIT,\n" +
"            esInteresPublico BIT,\n" +
"            tasa FLOAT,\n" +
"            idTrabajoAtlas INT,\n" +
"            ejercicio INT\n" +
"	)";

        Statement pstmt = connection.getConnection().createStatement();

        return pstmt.execute(sql);

    }

    public int obtenerUltimoId() throws SQLException {
        String sql = "SELECT MAX(id) FROM T_TRABAJOS_REALIZADOS";
        Statement pstmt = connection.getConnection().createStatement();
        ResultSet rs = pstmt.executeQuery(sql);
        if (rs != null) {
            rs.next();
            return rs.getInt(1);
        } else {
            LOG.log(Level.WARNING, "La obtención del ultimo registro NO HA PRODUCIDO RESULTADOS (ESTA VACIA?)");
            return 0;
        }
    }

    public CalculoTarifaServiceImpl getTarificador() {
        return tarificador;
    }

    public void setTarificador(CalculoTarifaServiceImpl tarificador) {
        this.tarificador = tarificador;
    }

    //ESTA ES LA CONSULTA QUE SE ESTÁ UTILIZANDO
    public ArrayList<TrabajoRealizado> obtenerTrabajos() throws SQLException {

        String queryVER = "SELECT "
                + "            id,\n"
                + "            FK_loteTrabajo,    \n"
                + "            codRoac ,\n"
                + "            identificacion,\n"
                + "            nifAuditor,"
                + "            informeDeCuentas,"
                + "            documento,\n"
                + "            razonSocial,\n"
                + "            calcTrimestre,\n"
                + "            calcEjercicio,\n"
                + "            descripcionTipoTrabajo,\n"
                + "            fechaInforme,\n"
                + "            facturacionAuditoriaHonorarios,\n"
                + "            coauditoria,\n"
                + "            esInteresPublico,\n"
                + "            tasa,\n"
                + "            idTrabajoAtlas\n"
                + "            ejercicio"
                + " FROM t_TRABAJOS_REALIZADOS; ";

        System.out.println("kk_>SQL=\n" + queryVER);
        ArrayList<TrabajoRealizado> trabajos;
        try (Statement pstmt = connection.getConnection().createStatement()) {
            ResultSet rs = pstmt.executeQuery(queryVER);
            trabajos = new ArrayList<>();
            if (rs != null) {
                while (rs.next()) {
                    long id=rs.getLong("id");
                    String codRoac = rs.getString("codRoac");
                    String identificacion = rs.getString("identificacion");
                    String documento = rs.getString("documento");
                    String nifAuditor = rs.getString("nifAuditor");
                    String razonSocial = rs.getString("razonSocial");
                    String informeDeCuentas = rs.getString("informeDeCuentas");
                    int trimestre = rs.getInt("calcTrimestre");
                    int ejercicio = rs.getInt("ejercicio");
                    String descripcionTipoTrabajo = rs.getString("descripcionTipoTrabajo");
                    String fechaInforme = rs.getString("FechaInforme");//YYYY-mm-dd
                    SimpleDateFormat SDF_SQL = new SimpleDateFormat("yyyy-MM-dd");
                    Date fechaInformeDate = null;
                    try {
                        fechaInformeDate = SDF_SQL.parse(fechaInforme);
                    } catch (ParseException ex) {
                        Logger.getLogger(TrabajosRealizadosDAO.class.getName()).log(Level.SEVERE, "obtenerTrabajos()", ex);
                    }

                    /*es la fecha importante*/
                    double facturacionAuditoriaHonorarios = rs.getDouble("FacturacionAuditoriaHonorarios");
                    double tasa = rs.getDouble("tasa");

                    TrabajoRealizado trabajo = null;
                    try {
                        trabajo = new TrabajoRealizado();
                        trabajo.setId(id);
                        trabajo.setCodRoac(codRoac);
                        trabajo.setIdentificacion(identificacion);
                        trabajo.setDocumento(documento);
                        trabajo.setNif(nifAuditor);
                        trabajo.setRazonSocial(razonSocial);
                        trabajo.setInformeDeCuentasString(informeDeCuentas);
                        trabajo.setTasa(tasa);
                        trabajo.setDescripcionTipoTrabajo(descripcionTipoTrabajo);
                        trabajo.setFechaInforme(fechaInformeDate);
                        trabajo.setFacturacionAuditoriaHonorarios(facturacionAuditoriaHonorarios);
                        trabajo.setEjercicio(ejercicio);
                        trabajos.add(trabajo);

                    } catch (Exception creandoTrabajoException) {
                        LOG.log(Level.SEVERE, "==============================================");
                        LOG.log(Level.SEVERE, "ERROR CREANDO TRABAJO REALIZADO " + codRoac, creandoTrabajoException);
                        LOG.log(Level.SEVERE, "==============================================");
                        LOG.log(Level.SEVERE, "TRABAJO REALIZADO CONSTRUIDO HASTA EXCEPCION: ");
                        LOG.log(Level.SEVERE, trabajo != null ? trabajo.toString() : "NULL");
                        LOG.log(Level.SEVERE, "==============================================");
                    }
                }
            } else {
                LOG.log(Level.WARNING, "Intentando obtener TrabajosRealizados desde una ResultSet y este es NULL");
            }
        }
        return trabajos;
    }

    public ArrayList<TrabajoRealizado> obtenerTrabajosEjercicio(String anyoEjercicio) throws SQLException {

        String queryVER = "SELECT "
                + "            id,\n"
                + "            FK_loteTrabajo,    \n"
                + "            codRoac ,\n"
                + "            identificacion,\n"
                + "            nifAuditor,"
                + "            informeDeCuentas,"
                + "            documento,\n"
                + "            razonSocial,\n"
                + "            calcTrimestre,\n"
                + "            calcEjercicio,\n"
                + "            descripcionTipoTrabajo,\n"
                + "            fechaInforme,\n"
                + "            facturacionAuditoriaHonorarios,\n"
                + "            coauditoria,\n"
                + "            esInteresPublico,\n"
                + "            tasa,\n"
                + "            idTrabajoAtlas,\n"
                + "            ejercicio"
                + " FROM t_TRABAJOS_REALIZADOS "
                + " WHERE ejercicio=" + anyoEjercicio;

        System.out.println(">>>>>>SQL=\n" + queryVER);
        ArrayList<TrabajoRealizado> trabajos;
        
        try (Statement pstmt = connection.getConnection().createStatement();ResultSet rs = pstmt.executeQuery(queryVER);) {
            
            trabajos = new ArrayList<>();
            if (rs != null) {
                while (rs.next()) {
                    long id=rs.getLong("id");
                    String codRoac = rs.getString("codRoac");
                    String identificacion = rs.getString("identificacion");
                    String documento = rs.getString("documento");
                    String nifAuditor = rs.getString("nifAuditor");
                    String razonSocial = rs.getString("razonSocial");
                    String informeDeCuentas = rs.getString("informeDeCuentas");
                    int trimestre = rs.getInt("calcTrimestre");
                    int anyo = rs.getInt("calcEjercicio");
                    int ejercicio = rs.getInt("ejercicio");
                    String descripcionTipoTrabajo = rs.getString("descripcionTipoTrabajo");
                    Date fechaInforme = rs.getDate("FechaInforme");//YYYY-mm-dd
                    SimpleDateFormat SDF_SQL = new SimpleDateFormat("yyyy-MM-dd");
//                    Date fechaInformeDate = null;
//                    try {
//                        fechaInformeDate = SDF_SQL.parse(fechaInforme);
//                    } catch (ParseException ex) {
//                        LOG.log(Level.SEVERE, "obtenerTrabajos()", ex);
//                    }

                    /*es la fecha importante*/
                    double facturacionAuditoriaHonorarios = rs.getDouble("FacturacionAuditoriaHonorarios");
                    double tasa = rs.getDouble("tasa");

                    TrabajoRealizado trabajo = null;
                    try {
                        trabajo = new TrabajoRealizado();
                        trabajo.setId(id);
                        trabajo.setCodRoac(codRoac);
                        trabajo.setIdentificacion(identificacion);
                        trabajo.setDocumento(documento);
                        trabajo.setNif(nifAuditor);
                        trabajo.setRazonSocial(razonSocial);
                        trabajo.setInformeDeCuentasString(informeDeCuentas);
                        trabajo.setTasa(tasa);
                        trabajo.setEjercicio(ejercicio);
           
                        trabajo.setDescripcionTipoTrabajo(descripcionTipoTrabajo);
                        trabajo.setFechaInforme(fechaInforme);
                        trabajo.setFacturacionAuditoriaHonorarios(facturacionAuditoriaHonorarios);
                        trabajos.add(trabajo);

                    } catch (Exception creandoTrabajoException) {
                        LOG.log(Level.SEVERE, "==============================================");
                        LOG.log(Level.SEVERE, "ERROR CREANDO TRABAJO REALIZADO " + codRoac, creandoTrabajoException);
                        LOG.log(Level.SEVERE, "==============================================");
                        LOG.log(Level.SEVERE, "TRABAJO REALIZADO CONSTRUIDO HASTA EXCEPCION: ");
                        LOG.log(Level.SEVERE, trabajo != null ? trabajo.toString() : "NULL");
                        LOG.log(Level.SEVERE, "==============================================");
                    } finally {
                        if (pstmt == null) {
                            pstmt.close();
                        }
                        if (rs == null) {
                            rs.close();
                        }
                    }
                }
            } else {
                LOG.log(Level.WARNING, "Intentando obtener TrabajosRealizados desde una ResultSet y este es NULL");
            }
        }
        return trabajos;
    }

    public int getNumeroTrabajos(String ejercicio) {
int resultado = 0;
       Statement pstmt = null;
        try {
            String sql = "select count(*) as count from T_TRABAJOS_REALIZADOS where calcEjercicio=" + ejercicio;
            LOG.log(Level.INFO, "sql={0}", sql);
            pstmt =  connection.getConnection().createStatement();
                      
            ResultSet rs = pstmt.executeQuery(sql);
        
           int rowCount = 0;
           while(rs.next()) {
               resultado = rs.getInt("count");        
           }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
}
