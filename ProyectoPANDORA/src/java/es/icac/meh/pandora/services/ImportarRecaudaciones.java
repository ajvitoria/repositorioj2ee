
package es.icac.meh.pandora.services;

import java.io.File;
import java.util.ArrayList;

public class ImportarRecaudaciones {

    private String url;

    private static ArrayList<File> listaFicherosTratados;
    private static ImportarRecaudaciones miconfigurador;
    private static YaTratadoException yaTratada;
    
    public static ImportarRecaudaciones tratarFicheroRecaudacion(File fichero) throws YaTratadoException {

        if (!listaFicherosTratados.contains(fichero)) {
            listaFicherosTratados.add(fichero);
        } else {
            yaTratada=new YaTratadoException("el fichero " + fichero + " ya ha sido previamente tratado!");    ;
            throw yaTratada;
        }

        if (miconfigurador == null) {

            if (fichero.exists()) {
                miconfigurador = new ImportarRecaudaciones();
                listaFicherosTratados = new ArrayList();
            }

        }
        return miconfigurador;
    }

    public static void listarFicherosTratados() {
        System.out.println("=========FICHEROS TRATADOS==========");
        for (File tmp : listaFicherosTratados) {
            System.out.println("\t" + tmp.getAbsolutePath());
        }
    }

    private ImportarRecaudaciones() throws YaTratadoException {

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private static class YaTratadoException extends Exception {

        public YaTratadoException(String string) {
        }
    }

}
