/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.icac.meh.pandora.services.impl;

import es.icac.meh.pandora.modelo.Tarifa;
import es.icac.meh.pandora.modelo.TrabajoRealizado;
import es.icac.meh.pandora.modelo.TrabajoRealizadoAtlas;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MAJIVIAL
 */
public class CalculoTarifaServiceImpl {
    
    
    
    private boolean DEBUG=false;
        
        
    ArrayList<Tarifa> listaTarifas;
    private double TRAMO_30M=30000;
    private double INFINITO=999999999999999999999999D;
    
    private static final Logger LOG = Logger.getLogger(CalculoTarifaServiceImpl.class.getName());
      final SimpleDateFormat SDF=new SimpleDateFormat("dd-MM-yyyy");


    public CalculoTarifaServiceImpl(){
        listaTarifas=new ArrayList();
        generarTarifas();
       //stem.out.println("********TARIFAS********");
        LOG.log(Level.INFO, ("********TARIFAS********"));
        for(Tarifa tmp: listaTarifas){
            System.out.printf("Desde %s hasta %s se cobraran %f %n",tmp.getFechaInicioAplicacionString(), tmp.getFechaFinAplicacionString(), tmp.getPrecioTarifa());
            
        
        
        }
        
    
    }
    

//        public double damePrecio(TrabajoRealizado trabajo){
//   
//        //cuando funcione QUITAR ESTOS CAMPOS 
//        Date fechaInforme=trabajo.getFechaInforme();
//        double importe=trabajo.getFacturacionAuditoriaHonorarios();//<<<<< espero que SEA ESTE EL CAMPO  
//        double precio=tarificarTrabajo(trabajo, fechaInforme, importe);  
//        return precio;
//        
//    }
        
        
    public double damePrecio(TrabajoRealizadoAtlas trabajo) {

        //cuando funcione QUITAR ESTOS CAMPOS 
        Date fechaInforme = trabajo.getFechaInforme();
        double importe = trabajo.getFacturacionAuditoriaHonorarios();//<<<<< espero que SEA ESTE EL CAMPO 


        double retorno = 0;
        if (fechaInforme == null || importe < 0) {
            LOG.log(Level.WARNING, "No se puede tarificar una fecha a NULL o IMPORTE INFERIOR A 0");
            return 0;
        }
        for (Tarifa tarifa : listaTarifas) {
            boolean verSiLaFechaEsAntes = fechaInforme.before(tarifa.getFechaFinAplicacion()) || fechaInforme.equals(tarifa.getFechaFinAplicacion());
            boolean verSiLaFechaEsIgualODespues = fechaInforme.after(tarifa.getFechaInicioAplicacion()) || fechaInforme.equals(tarifa.getFechaInicioAplicacion());

            boolean verSiEsDeInteresPublico = tarifa.isEsDeInteresPublico() == trabajo.isEsInteresPublico();
            //es "MENOR" Y NO "MENOR O IGUAL"   PORQUE EL REQUISITO REQFUNC2-C habla que el tramo es importe facturado <= 30000

            boolean verSiEntraEnPrecio = trabajo.getFacturacionAuditoriaHonorarios() >= tarifa.getImporteDesde() && trabajo.getFacturacionAuditoriaHonorarios() <= tarifa.getImporteHasta();
            if (trabajo.getFacturacionAuditoriaHonorarios() > 30000) {
                int kaka = 1;
            }

            if (verSiLaFechaEsAntes && verSiLaFechaEsIgualODespues && verSiEsDeInteresPublico && verSiEntraEnPrecio) {
                retorno = tarifa.getPrecioTarifa();
                
                if(DEBUG){
                StringBuilder sb = new StringBuilder("a la fecha ")
                        .append(SDF.format(fechaInforme))
                        .append(" se aplica la tarifa ").append(tarifa).append(" con importe de ").append(retorno);
                LOG.log(Level.INFO, sb.toString());
                }
                break;

            }
        }
        if (retorno == 0) {
            String msg = "No se ha encontrado TARIFA para el trabajo: \n" + trabajo;
            LOG.log(Level.SEVERE, msg);
        }

        if (trabajo.isCoauditoria()) {
            retorno /= 2; //SI ES UNA COAUDITORIA EL COSTE SE REPARTE ENTRE DOS
        }

        return retorno;
    }


    
    
    private void generarTarifas() {
        Tarifa t1_2016A=generarTarifa(Tipo.T1, 2016, 123.40, "01-01","31-12");
        Tarifa t2_2016A=generarTarifa(Tipo.T2, 2016, 246.90, "01-01","31-12");
        Tarifa t3_2016A=generarTarifa(Tipo.T3, 2016, 246.90, "01-01","31-12");
        Tarifa t4_2016A=generarTarifa(Tipo.T4, 2016, 493.80, "01-01","31-12");

        listaTarifas.add(t1_2016A);
        listaTarifas.add(t2_2016A);
        listaTarifas.add(t3_2016A);
        listaTarifas.add(t4_2016A);
        

                
        Tarifa t1_2017A=generarTarifa(Tipo.T1, 2017, 123.40, "01-01","28-06");
        Tarifa t2_2017A=generarTarifa(Tipo.T2, 2017, 246.90, "01-01","28-06");
        Tarifa t3_2017A=generarTarifa(Tipo.T3, 2017, 246.90, "01-01","28-06");
        Tarifa t4_2017A=generarTarifa(Tipo.T4, 2017, 493.80, "01-01","28-06");
        listaTarifas.add(t1_2017A);
        listaTarifas.add(t2_2017A);
        listaTarifas.add(t3_2017A);
        listaTarifas.add(t4_2017A);
                
        Tarifa t1_2017B=generarTarifa(Tipo.T1, 2017, 124.63, "29-06","31-12");
        Tarifa t2_2017B=generarTarifa(Tipo.T2, 2017, 249.37, "29-06","31-12");
        Tarifa t3_2017B=generarTarifa(Tipo.T3, 2017, 249.37, "29-06","31-12");
        Tarifa t4_2017B=generarTarifa(Tipo.T4, 2017, 498.74, "29-06","31-12");
        listaTarifas.add(t1_2017B);
        listaTarifas.add(t2_2017B);
        listaTarifas.add(t3_2017B);
        listaTarifas.add(t4_2017B);
                
        Tarifa t1_2018A=generarTarifa(Tipo.T1, 2018, 124.63, "01-01","04-07");
        Tarifa t2_2018A=generarTarifa(Tipo.T2, 2018, 249.37, "01-01","04-07");
        Tarifa t3_2018A=generarTarifa(Tipo.T3, 2018, 249.37, "01-01","04-07");
        Tarifa t4_2018A=generarTarifa(Tipo.T4, 2018, 498.74, "01-01","04-07");
                
        listaTarifas.add(t1_2018A);
        listaTarifas.add(t2_2018A);
        listaTarifas.add(t3_2018A);
        listaTarifas.add(t4_2018A);
        
        Tarifa t1_2018B=generarTarifa(Tipo.T1, 2018, 125.88, "05-07","31-12");
        Tarifa t2_2018B=generarTarifa(Tipo.T2, 2018, 251.86, "05-07","31-12");
        Tarifa t3_2018B=generarTarifa(Tipo.T3, 2018, 251.86, "05-07","31-12");
        Tarifa t4_2018B=generarTarifa(Tipo.T4, 2018, 503.73, "05-07","31-12");
                
        listaTarifas.add(t1_2018B);
        listaTarifas.add(t2_2018B);
        listaTarifas.add(t3_2018B);
        listaTarifas.add(t4_2018B);
        
        Tarifa t1_2019A=generarTarifa(Tipo.T1, 2019, 125.88, "01-01","31-12");
        Tarifa t2_2019A=generarTarifa(Tipo.T2, 2019, 251.86, "01-01","31-12");
        Tarifa t3_2019A=generarTarifa(Tipo.T3, 2019, 251.86, "01-01","31-12");
        Tarifa t4_2019A=generarTarifa(Tipo.T4, 2019, 503.73, "01-01","31-12");
                
        listaTarifas.add(t1_2019A);
        listaTarifas.add(t2_2019A);
        listaTarifas.add(t3_2019A);
        listaTarifas.add(t4_2019A);        
   
        
    }

    private Tarifa generarTarifa(Tipo tipo, int ejercicio, double importe, String desde, String hasta){
        
        Tarifa tarifaRetorno=new Tarifa();        
        tarifaRetorno.setNombre(tipo.toString());
        tarifaRetorno.setAnyoEjercicioAplicacion(ejercicio);
        String fechaInicio=desde+"-"+String.valueOf(ejercicio);
        tarifaRetorno.setFechaInicioAplicacion(fechaInicio);
        String fechaFin=hasta+"-"+String.valueOf(ejercicio);
        tarifaRetorno.setFechaFinAplicacion(fechaFin);
   
        
        switch(tipo){
            case T1: 
                tarifaRetorno.setEsDeInteresPublico(false);
                tarifaRetorno.setImporteDesde(0);
                tarifaRetorno.setImporteHasta(TRAMO_30M);
                break;
            case T2:
                tarifaRetorno.setEsDeInteresPublico(false);
                tarifaRetorno.setImporteDesde(TRAMO_30M);
                tarifaRetorno.setImporteHasta(INFINITO);
                break;     
            case T3: 
                tarifaRetorno.setEsDeInteresPublico(true);
                tarifaRetorno.setImporteDesde(0);
                tarifaRetorno.setImporteHasta(TRAMO_30M);
                break;
            case T4:
                tarifaRetorno.setEsDeInteresPublico(true);
                tarifaRetorno.setImporteDesde(TRAMO_30M);
                tarifaRetorno.setImporteHasta(INFINITO);
                break;
            default:
                LOG.log(Level.SEVERE, "Se ha producico un error al generar la tarifa con "+tipo);
        }
       
        tarifaRetorno.setPrecioTarifa(importe);
        
        return tarifaRetorno;  
    }
    private Tarifa generarTarifakk(Tipo tipo, int ejercicio, double importe){
        Tarifa tarifaRetorno=new Tarifa();        
        tarifaRetorno.setNombre(tipo.toString());
        tarifaRetorno.setAnyoEjercicioAplicacion(ejercicio);
        String fechaInicio="01-01-"+String.valueOf(ejercicio);
        tarifaRetorno.setFechaInicioAplicacion(fechaInicio);
        String fechaFin="31-12-"+String.valueOf(ejercicio);
        tarifaRetorno.setFechaFinAplicacion(fechaFin);
        tarifaRetorno.setImporteDesde(0);
        tarifaRetorno.setImporteHasta(TRAMO_30M);
        tarifaRetorno.setEsDeInteresPublico(false);
        tarifaRetorno.setPrecioTarifa(importe);
        
        return tarifaRetorno;  
    }

//    private double tarificarTrabajo(TrabajoRealizadoAtlas trabajo, Date fechaInforme, double importe) {
//        double retorno=0;
//        if (fechaInforme==null || importe<0){
//            LOG.log(Level.WARNING, "No se puede tarificar una fecha a NULL o IMPORTE INFERIOR A 0");
//            return 0;
//        }
//        for(Tarifa tarifa: listaTarifas){
//            boolean verSiLaFechaEsAntes=fechaInforme.before(tarifa.getFechaFinAplicacion()) || fechaInforme.equals(tarifa.getFechaFinAplicacion());
//            boolean verSiLaFechaEsIgualODespues=fechaInforme.after(tarifa.getFechaInicioAplicacion()) || fechaInforme.equals(tarifa.getFechaInicioAplicacion());
//          
//            boolean verSiEsDeInteresPublico=tarifa.isEsDeInteresPublico()==trabajo.isEsInteresPublico();
//            //es "MENOR" Y NO "MENOR O IGUAL"   PORQUE EL REQUISITO REQFUNC2-C habla que el tramo es importe facturado <= 30000
////            if(trabajo.getFacturacionAuditoriaHonorarios()==0){
////                System.out.println("precio CERO");
////            }
//            boolean verSiEntraEnPrecio=trabajo.getFacturacionAuditoriaHonorarios() >= tarifa.getImporteDesde() && trabajo.getFacturacionAuditoriaHonorarios()<= tarifa.getImporteHasta();
//           if(trabajo.getFacturacionAuditoriaHonorarios()>30000){
//               int kaka=1;
//           }
//            
//            if (verSiLaFechaEsAntes &&  verSiLaFechaEsIgualODespues && verSiEsDeInteresPublico && verSiEntraEnPrecio) {
//                retorno = tarifa.getPrecioTarifa();
////                StringBuilder sb=new StringBuilder("a la fecha ")
////                        .append(SDF.format(fechaInforme))
////                        .append(" se aplica la tarifa ").append(tarifa).append(" con importe de ").append(retorno);
////                LOG.log(Level.INFO,  sb.toString());
//                break;
//
//            }
//        }
//        if (retorno==0){
//            String msg="No se ha encontrado TARIFA para el trabajo: \n"+trabajo;
//             LOG.log(Level.SEVERE, msg);
//        }
//        return retorno;
//    }
    
    
    
        enum Tipo {
        T1,T2,T3,T4
    }
    
}
