/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.icac.meh.pandora.vista;

/**
 *
 * @author MAJIVIAL
 */
public class Componentes {
    public String estilo;
    

   
    public static String estiloBotonReset="input[type=reset] {\n" +
"                 color:#fff;\n" +
"                font-family:arial;font-size:13px;font-weight:normal;\n" +
"                 background-color:#ff0000;\n" +
"                -moz-border-radius:4px;\n" +
"                -webkit-border-radius:4px;\n" +
"                border-radius:4px;display:inline-block;\n" +
"                padding:5px 15px; \n" +
"                border:0 none;\n" +
"                cursor:pointer;\n" +
"                -webkit-border-radius: 5px;\n" +
"                border-radius: 5px; \n" +
"            };";
    public static String estiloBotonSubmit="input[type=submit] {\n" +
"                 color:#fff;\n" +
"                font-family:arial;font-size:13px;font-weight:normal;\n" +
"                 background-color:#00A800;\n" +
"                -moz-border-radius:4px;\n" +
"                -webkit-border-radius:4px;\n" +
"                border-radius:4px;display:inline-block;\n" +
"                padding:5px 15px; \n" +
"                border:0 none;\n" +
"                cursor:pointer;\n" +
"                -webkit-border-radius: 5px;\n" +
"                border-radius: 5px; \n" +
"            };";
    
    public static String estiloComunBotonesSubmitCancelar="<style>\n"
            +estiloBotonReset
            +estiloBotonSubmit
            +"</style>\n";

public static String estiloTablaCebra="table tr:nth-child(even) {"
                    + "background-color: #eee;"
                    + "}"
                    + "table tr:nth-child(odd) {"
                    + "background-color: #fff;"
                    + "}\n"
        +"td{"
        +"\tpadding-left: 10px; padding-right: 10px;\n"
        +"\tfont-family: Verdana, Arial, Helvetica, sans-serif;\n" 
        +"\tfont-size:17px;\n"
        + "}\n";
}
