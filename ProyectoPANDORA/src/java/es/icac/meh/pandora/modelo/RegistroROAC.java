/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.icac.meh.pandora.modelo;

/**
 *
 * @author MAJIVIAL
 */
public class RegistroROAC {
    private String codRoac;
    private String documento;
    private String denominacion;
    
    public RegistroROAC(String codRoac, String documento, String denominacion ) {
        this.documento = documento;
        this.codRoac = codRoac;
        this.denominacion= denominacion;
    }
    
    
    public void setCodRoac(String codRoac) {
        this.codRoac = codRoac;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }
    
    public String getCodRoac() {
        return codRoac;
    }

    public String getDocumento() {
        return documento;
    }

    public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }

    @Override
    public String toString() {
        return "RegistroROAC{" + "codRoac=" + codRoac + ", documento=" + documento + ", denominacion=" + denominacion + '}';
    }
    
    
}
