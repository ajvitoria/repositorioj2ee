package es.icac.meh.pandora.modelo;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MAJIVIAL
 */
public class Recaudacion implements Apunte {

    //  final SimpleDateFormat SDF=new SimpleDateFormat("dd-MM-yyyy");
    private static final Logger LOG = Logger.getLogger(Recaudacion.class.getName());

    private long id;

    private String codigoTasa;
    private String numeroJustificante;
    private String codigovalorSubcampo1;
    private String codigovalorSubcampo2;
    private int codigoAnyoEjercicio;
    private int codigovalorTrimestre;
    private String codigovalorSubcampo5;
    private String codigovalorSubcampo6;
    private String nif;
    private String apellidosNombre;
    private Date fechaIngreso;
    private double importeEuros;

    private String entidad;
    private String sucursal;
    private String roac;

    private int anyo;
    private int ejercicio;
    
    private String archivoFuente;
    private Date fechaPlazo;


//    public Recaudacion(String valorCodigoTasa, String valorNumeroJustificante, String valorSubcampo1, 
//            String valorSubcampo2, String valorAnyoEjercicio, 
//            String valorTrimestre, String valorSubcampo5, String valorSubcampo6, 
//            String valorNif, String valorApellidosNombre, Date valorFechaIngreso, double importe, String valorCodigoEntidad, String valorCodigoSucursal) {
//          this.id=0;
//        this.codigoTasa = valorCodigoTasa;
//        this.numeroJustificante = valorNumeroJustificante;
//        this.codigovalorSubcampo1 = valorSubcampo1;
//        this.codigovalorSubcampo2 = valorSubcampo2;
//        this.codigoAnyoEjercicio = Integer.parseInt(valorAnyoEjercicio);
//        this.codigovalorTrimestre = Integer.parseInt(valorTrimestre);
//        this.codigovalorSubcampo5 = valorSubcampo5;
//        this.codigovalorSubcampo6 = valorSubcampo6;
//        this.nif = valorNif;
//        this.apellidosNombre = valorApellidosNombre;
//        this.fechaIngreso = valorFechaIngreso;
//        this.importeEuros = importe;
//        this.entidad = valorCodigoEntidad;
//        this.sucursal = valorCodigoSucursal; 
//    }

    public Recaudacion() {
    }
//    @Override
//    public String toString() {
//        return "Recaudacion{" + "codigoTasa=" + codigoTasa + ", numeroJustificante=" + numeroJustificante + ", codigovalorSubcampo1=" + codigovalorSubcampo1 + ", codigovalorSubcampo2=" + codigovalorSubcampo2 + ", codigoAnyoEjercicio=" + codigoAnyoEjercicio + ", codigovalorTrimestre=" + codigovalorTrimestre + ", codigovalorSubcampo5=" + codigovalorSubcampo5 + ", codigovalorSubcampo6=" + codigovalorSubcampo6 + ", nif=" + nif + ", apellidosNombre=" + apellidosNombre + ", fechaIngreso=" + fechaIngreso + ", importeEuros=" + importeEuros + ", entidad=" + entidad + ", sucursal=" + sucursal + '}';
//    }

    @Override
    public String toString() {
        return "Recaudacion{" + "id=" + id + ", codigoTasa=" + codigoTasa + ", numeroJustificante=" + numeroJustificante + ", codigovalorSubcampo1=" + codigovalorSubcampo1 + ", codigovalorSubcampo2=" + codigovalorSubcampo2 + ", codigoAnyoEjercicio=" + codigoAnyoEjercicio + ", codigovalorTrimestre=" + codigovalorTrimestre + ", codigovalorSubcampo5=" + codigovalorSubcampo5 + ", codigovalorSubcampo6=" + codigovalorSubcampo6 + ", nif=" + nif + ", apellidosNombre=" + apellidosNombre + ", fechaIngreso=" + fechaIngreso + ", importeEuros=" + importeEuros + ", entidad=" + entidad + ", sucursal=" + sucursal + ", roac=" + roac + ", anyo=" + anyo + ", ejercicio=" + ejercicio + ", archivoFuente=" + archivoFuente + ", fechaPlazo=" + fechaPlazo + '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCodigoTasa() {
        return codigoTasa;
    }

    public void setCodigoTasa(String codigoTasa) {
        this.codigoTasa = codigoTasa;
    }

    public String getNumeroJustificante() {
        return numeroJustificante;
    }

    public void setNumeroJustificante(String numeroJustificante) {
        this.numeroJustificante = numeroJustificante;
    }

    public String getCodigovalorSubcampo1() {
        return codigovalorSubcampo1;
    }

    public void setCodigovalorSubcampo1(String codigovalorSubcampo1) {
        this.codigovalorSubcampo1 = codigovalorSubcampo1;
    }

    public String getCodigovalorSubcampo2() {
        return codigovalorSubcampo2;
    }

    public void setCodigovalorSubcampo2(String codigovalorSubcampo2) {
        this.codigovalorSubcampo2 = codigovalorSubcampo2;
    }

    public int getCodigoAnyoEjercicio() {
        return codigoAnyoEjercicio;
    }

    public void setCodigoAnyoEjercicio(int codigoAnyoEjercicio) {
        this.codigoAnyoEjercicio = codigoAnyoEjercicio;
    }

    public int getCodigovalorTrimestre() {
        return codigovalorTrimestre;
    }

    public void setCodigovalorTrimestre(int codigovalorTrimestre) {
        this.codigovalorTrimestre = codigovalorTrimestre;
    }

    public String getCodigovalorSubcampo5() {
        return codigovalorSubcampo5;
    }

    public void setCodigovalorSubcampo5(String codigovalorSubcampo5) {
        this.codigovalorSubcampo5 = codigovalorSubcampo5;
    }

    public String getCodigovalorSubcampo6() {
        return codigovalorSubcampo6;
    }

    public void setCodigovalorSubcampo6(String codigovalorSubcampo6) {
        this.codigovalorSubcampo6 = codigovalorSubcampo6;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getApellidosNombre() {
        return apellidosNombre;
    }

    public void setApellidosNombre(String apellidosNombre) {
        this.apellidosNombre = apellidosNombre;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
    public void setFechaIngreso(String fechaIngreso) {
        setFechaIngreso(convertStringToDate("fechaIngreso", fechaIngreso));
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public void setImporteEuros(double importeEuros) {
        this.importeEuros = importeEuros;
    }

    public double getImporteEuros() {
        return importeEuros;
    }
    
        private String convertDateToString(String campo, Date valorParametro){
        String retorno=null;
         try{
            retorno=SDF.format(valorParametro);
        }catch(Exception ex){
            String msg="Se ha intentado obtener el campo "+campo
                    + ". Intentado convertir la fecha "+valorParametro 
                    +" a una cadena válida SIN EXITO";
            LOG.log(Level.SEVERE,msg,ex );
        }       
        return retorno;
        
    }
    private Date convertStringToDate(String campo, String valorParametro){
            Date retorno=null;
        try{
            retorno=SDF.parse(valorParametro.trim());
        }catch(Exception ex){
            String msg="Se ha intentado obtener el campo "+campo
                    + ". Intentado convertir el valor "+valorParametro 
                    +" a una fecha válida SIN EXITO";
            LOG.log(Level.SEVERE,msg, ex );
        }
        return retorno;
    }

    @Override
    public Date getFecha() {
        return fechaIngreso;
    }
    public String getFechaIngresoString() {
        return convertDateToString("FechaIngreso", fechaIngreso);
    }
    @Override
    public String getDenominacion() {
       return "Recaudacion Recibido "+numeroJustificante;
    }

    @Override
    public String getImporteString() {
        return formatea.format(this.importeEuros);
    }
    @Override
    public double getImporte() {
        return this.importeEuros;
    }
    @Override
    public boolean esDebe() {
        return false; //voy a poner el el trabajo al debe y el cobro al haber
    }

    @Override
    public int getTrimestre() {
       return codigovalorTrimestre;
    }

    @Override
    public String getRoac() {
        return roac;
    }

    public void setRoac(String roac) {
        this.roac = roac;
    }

    @Override
    public String getCampo1() {
       return nif;
    }

    @Override
    public String getCampo2() {
       return this.getDenominacion();
    }

    @Override
    public String getCampo3() {
      return this.getNombreAuditor();
    }

    @Override
    public String getNifAuditor() {
        return this.nif;
    }

    @Override
    public String getNombreAuditor() {
        return this.apellidosNombre;
    }

    public String getArchivoFuente() {
        return archivoFuente;
    }

    public void setArchivoFuente(String archivoFuente) {
        this.archivoFuente = archivoFuente;
    }

    public Date getFechaPlazo() {
        return fechaPlazo;
    }

    public void setFechaPlazo(Date fechaPlazo) {
        this.fechaPlazo = fechaPlazo;
    }
  
    public void setAnyo(int anyo) {
        this.anyo = anyo;
    }
    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }
    
    @Override
    public int getEjercicio() {
       return ejercicio;
    }
    
    @Override
    public int getAnyo() {
        return anyo;
    }

    @Override
    public long getFkEntidad() {
        return getId();
    }

    @Override
    public Date getFechaPago() {
        return fechaPlazo;
    }

  
}