/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.icac.meh.pandora.modelo;

import java.util.Date;

/**
 *
 * @author MAJIVIAL
 */
public class InformeFueraDePlazo {
    
   private String roac;
   private String modelo;
   private Date fechaFinDePlazo;
   private Date fechaIngreso;
   
   private double importe;
   private double tipo;
   private double recargo;
   private double interesDemora;
   private double recargoTotal;
   private double recargoRED;
   private String nif;
   private String identificacion;

    public String getRoac() {
        return roac;
    }

    public void setRoac(String roac) {
        this.roac = roac;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Date getFechaFinDePlazo() {
        return fechaFinDePlazo;
    }

    public void setFechaFinDePlazo(Date fechaFinDePlazo) {
        this.fechaFinDePlazo = fechaFinDePlazo;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public double getTipo() {
        return tipo;
    }

    public void setTipo(double tipo) {
        this.tipo = tipo;
    }

    public double getRecargo() {
        return recargo;
    }

    public void setRecargo(double recargo) {
        this.recargo = recargo;
    }

    public double getInteresDemora() {
        return interesDemora;
    }

    public void setInteresDemora(double interesDemora) {
        this.interesDemora = interesDemora;
    }

    public double getRecargoTotal() {
        return recargoTotal;
    }

    public void setRecargoTotal(double recargoTotal) {
        this.recargoTotal = recargoTotal;
    }

    public double getRecargoRED() {
        return recargoRED;
    }

    public void setRecargoRED(double recargoRED) {
        this.recargoRED = recargoRED;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
   
}
