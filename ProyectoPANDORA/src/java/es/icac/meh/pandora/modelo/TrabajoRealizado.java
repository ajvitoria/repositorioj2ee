/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.icac.meh.pandora.modelo;


import static es.icac.meh.pandora.modelo.Apunte.formatea;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MAJIVIAL
 */
public class TrabajoRealizado implements Apunte{ 
    
    private static final Logger LOG = Logger.getLogger(TrabajoRealizado.class.getName());
    final SimpleDateFormat SDF=new SimpleDateFormat("dd-MM-yyyy");
    
    
    private long id;

    private String codRoac;
    private String identificacion;
    private String documento;
    private String nif;
    
    private String razonSocial;

    //ESTOS CAMPOS NO TIENEN SET PORQUE SE CALCULAN AL EJECUTAR EL setFechaInforme()
    private int calcTrimestre;
    private int calcEjercicio;
    
    private int ejercicio;


       
    private String descripcionTipoTrabajo;

    private String informeDeCuentasString;
    
    private Date fechaInforme;
    /* esta es la fecha importante*/

    private Date fechaPrevisionPago;
    
    private double facturacionAuditoriaHonorarios;
    private boolean coauditoria;

    private boolean esInteresPublico;

    private double tasa;
    private long idTrabajoAtlas;
    private long idLoteTrabajo;
 

    public String getCodRoac() {
        return codRoac;
    }

    public void setCodRoac(String cod_roac) {
        this.codRoac = cod_roac;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }


    public void setConstitucion(String constitucion) {
           convertStringToBoolean("constitucion", constitucion);    
    }

    public void setEjercicioFinal(String ejercicioFinal) {
       convertStringToDate("ejercicioFinal", ejercicioFinal);  
    }

    public String getDescripcionTipoTrabajo() {
        return descripcionTipoTrabajo;
    }

    public void setDescripcionTipoTrabajo(String descripcionTipoTrabajo) {
        this.descripcionTipoTrabajo = descripcionTipoTrabajo;
    }

    public Date getFechaInforme() {
        return fechaInforme;
    }


    public void setFechaInforme(Date fechaInforme) {
        this.fechaInforme = fechaInforme;
        //actualizarPlazoPrevisonDePago(fechaInforme);
        actualizarPlazoPrevisonDePago();
        
    }
    public void setFechaInforme(String fechaInforme) {
        this.setFechaInforme(convertStringToDate("fechaInforme", fechaInforme));  
    }

    public boolean isCoauditoria() {
        return coauditoria;
    }
    
    public void setCoauditoria(boolean coauditoria) {
        this.coauditoria = coauditoria;
    }
    public void setCoauditoria(String coauditoria) {
               convertStringToBoolean("coauditoria", coauditoria);   
    }

    public boolean isEsInteresPublico() {
        return esInteresPublico;
    }
    
    public void setEsInteresPublico(boolean esInteresPublico) {
        this.esInteresPublico = esInteresPublico;
    }
    public void setEsInteresPublico(String esInteresPublico) {
        convertStringToBoolean("esInteresPublico", esInteresPublico); 
    }

    private String convertDateToString(String campo, Date valorParametro){
        String retorno=null;
         try{
            retorno=SDF.format(valorParametro);
        }catch(Exception ex){
            String msg="Se ha intentado obtener el campo "+campo
                    + ". Intentado convertir la fecha "+valorParametro 
                    +" a una cadena válida SIN EXITO";
            LOG.log(Level.SEVERE,msg );
        }       
        return retorno;     
    }
    
    private Date convertStringToDate(String campo, String valorParametro){
            Date retorno=null;
        try{
            retorno=SDF.parse(valorParametro.trim());
        }catch(Exception ex){
            String msg="Se ha intentado obtener el campo "+campo
                    + ". Intentado convertir el valor "+valorParametro 
                    +" a una fecha válida SIN EXITO";
            LOG.log(Level.SEVERE,msg );
        }
        return retorno;
    }
  
    private boolean convertStringToBoolean(String campo, String valorParametro) {
        boolean retorno = false;

        switch (valorParametro.toUpperCase()) {

            case "SI": case "SÍ": case "YES": case "TRUE":
                retorno = true;
                break;

            case "NO": case "FALSE":
                retorno = false;
                break;

            default:
                LOG.log(Level.SEVERE, "Se ha intentado obtener el campo "+campo
                    + ". Intentado convertir el valor "+valorParametro 
                    +" a una expresion booleana SIN EXITO");
        }
        return retorno;
    }
        
    @Override
    public String toString() {
        return "TrabajoRealizado{"
                + "codRoac=" + codRoac
                + ", fechaInforme=" + getFechaInformeString()
                + ", EJERCICIO:"+ calcEjercicio +" TRIMESTRE:" + calcTrimestre
                + ", facturacionAuditoriaHonorarios=" + facturacionAuditoriaHonorarios
                + ", esInteresPublico=" + esInteresPublico
                + '}';
    }

   

//    private void actualizarPlazoPrevisonDePago(Date fechaInforme) {
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(fechaInforme);
//        switch (cal.get(Calendar.MONTH)) {
//            case Calendar.JANUARY:
//            case Calendar.FEBRUARY:
//            case Calendar.MARCH:
//                this.calcTrimestre = 1;
//                this.calcEjercicio = cal.get(Calendar.YEAR);
//                break;
//
//            case Calendar.APRIL:
//            case Calendar.MAY:
//            case Calendar.JUNE:
//                this.calcTrimestre = 2;
//                this.calcEjercicio = cal.get(Calendar.YEAR);
//                break;
//            case Calendar.JULY:
//            case Calendar.AUGUST:
//            case Calendar.SEPTEMBER:
//                this.calcTrimestre = 3;
//                this.calcEjercicio = cal.get(Calendar.YEAR);
//                break;
//
//            case Calendar.OCTOBER:
//            case Calendar.NOVEMBER:
//            case Calendar.DECEMBER:
//                this.calcTrimestre = 4;
//                this.calcEjercicio = cal.get(Calendar.YEAR) + 1;
//                break;
//            default:
//                LOG.log(Level.SEVERE, "NO HA RECONOCIDO CORRECTAMENTE LA FECHA DE INFORME Y NO CALCULA EL EJERCICIO NI EL TRIMESTREnO ");
//        }
//    }

    public int getCalcTrimestre() {
        return calcTrimestre;
    }

    public int getCalcEjercicio() {
        return calcEjercicio;
    }

    @Override
    public Date getFecha() {
     return fechaInforme;
    }
    public String getFechaInformeString() {
        return convertDateToString("FechaInforme", fechaInforme);
    }
    @Override
    public String getDenominacion() {
        return "Trabajo Realizado";
    }

    @Override
    public String getImporteString() {
         return formatea.format(this.tasa);//esto seria la TASA   --> MODFICAR
    }
    @Override
    public double getImporte() {
         return this.tasa;//esto seria la TASA   --> MODFICAR
    }
    @Override
    public boolean esDebe() {
              return true; //voy a poner el el trabajo al debe y el cobro al haber
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdTrabajoAtlas() {
        return idTrabajoAtlas;
    }

    public void setIdTrabajoAtlas(long idTrabajoAtlas) {
        this.idTrabajoAtlas = idTrabajoAtlas;
    }

    public double getFacturacionAuditoriaHonorarios() {
        return facturacionAuditoriaHonorarios;
    }

    public void setFacturacionAuditoriaHonorarios(double facturacionAuditoriaHonorarios) {
        this.facturacionAuditoriaHonorarios = facturacionAuditoriaHonorarios;
    }

    public double getTasa() {
        return tasa;
    }
 
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }


    public long getIdLoteTrabajo() {
        return idLoteTrabajo;
    }

    public void setIdLoteTrabajo(long idLoteTrabajo) {
        this.idLoteTrabajo = idLoteTrabajo;
    }


    @Override
    public int getTrimestre() {
     return calcTrimestre;
    }

    @Override
    public String getRoac() {
    return codRoac;
    }

    //nif empresa auditada
    @Override
    public String getCampo1() {
        return documento;
    }

    //Individual o consolidado
    @Override
    public String getCampo2() {
       return informeDeCuentasString;//getInformeDeCuentas()
    }

    //importe
    @Override
    public String getCampo3() {
       return formatea.format(this.facturacionAuditoriaHonorarios);
    }
 
    public void setInformeDeCuentasString(String informeDeCuentasString) {
        this.informeDeCuentasString = informeDeCuentasString;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    @Override
    public String getNifAuditor() {
        return nif;
    }

    @Override
    public String getNombreAuditor() {
       return identificacion;        
        
    }

    @Override
    public long getFkEntidad() {
        return id;
    }

    @Override
    public int getAnyo() {
         return calcEjercicio;        
    }
    
    @Override
    public int getEjercicio() {
        
        return this.ejercicio;
    }
    //solo debería ejercutarse desde el DAO
    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }

    public Date getFechaPrevisionPago() {
        return fechaPrevisionPago;
    }

    public void setFechaPrevisionPago(Date fechaPrevisionPago) {
        this.fechaPrevisionPago = fechaPrevisionPago;
    }

    @Override
    public Date getFechaPago() {
        return fechaPrevisionPago;
    }
    
    
    private void actualizarPlazoPrevisonDePago() {
        
  
        
          Calendar cal=Calendar.getInstance();
          cal.setTime(fechaInforme);
          
        switch (cal.get(Calendar.MONTH)) {
            case Calendar.JANUARY:
            case Calendar.FEBRUARY:
            case Calendar.MARCH:
                this.calcTrimestre = 1;
                this.calcEjercicio = cal.get(Calendar.YEAR);
                break;

            case Calendar.APRIL:
            case Calendar.MAY:
            case Calendar.JUNE:
                this.calcTrimestre = 2;
                this.calcEjercicio = cal.get(Calendar.YEAR);
                break;
            case Calendar.JULY:
            case Calendar.AUGUST:
            case Calendar.SEPTEMBER:
                this.calcTrimestre = 3;
                this.calcEjercicio = cal.get(Calendar.YEAR);
                break;

            case Calendar.OCTOBER:
            case Calendar.NOVEMBER:
            case Calendar.DECEMBER:
                this.calcTrimestre = 4;
                this.calcEjercicio = cal.get(Calendar.YEAR) + 1;
                break;
            default:
                LOG.log(Level.SEVERE, "NO HA RECONOCIDO CORRECTAMENTE LA FECHA DE INFORME Y NO CALCULA EL EJERCICIO NI EL TRIMESTREnO ");
        }

     int mesPlazoPago=0;
     int diaPlazoPago=23;
    
     switch (calcTrimestre) {
            case 1:
                mesPlazoPago=Calendar.APRIL;
                break;
            case 2:
                mesPlazoPago=Calendar.JULY;
                break;
            case 3:
                mesPlazoPago=Calendar.OCTOBER;
                break;
            case 4:
                mesPlazoPago=Calendar.JANUARY;
                calcEjercicio++; //se pagara en el siguiente año
                break;
            default:
                LOG.log(Level.WARNING, "El informe  {0} del fichero {1} posee trimestre EXTRANO {2}", new Object[]{"trabajo", "recaudacion.getArchivoFuente()", "recaudacion.getCodigovalorTrimestre()"});   
     }

       
     Calendar calResultado=new GregorianCalendar(calcEjercicio, mesPlazoPago, diaPlazoPago);  
     setFechaPrevisionPago(new Date(calResultado.getTimeInMillis()));
    
   
/*
     Según conversación mantenida con Angel Luis el día 4/6/2020
     El año de pago --> (dato leido del registro es el que va a definir
     el ejercicio REAL QUE ESTÁ PAGADO EL auditor
     
     Es decir:
     
     Un auditor puede pagar en 2019 algo que tenía que haber pagado en 2016
     
     */
           
        
// *******************   ANTIGUA IMPLEMENTACION DE ESTE PROCEDIMIENTO (VOID) ************************        
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(fechaInforme);
//        switch (cal.get(Calendar.MONTH)) {
//            case Calendar.JANUARY:
//            case Calendar.FEBRUARY:
//            case Calendar.MARCH:
//                this.calcTrimestre = 1;
//                this.calcEjercicio = cal.get(Calendar.YEAR);
//                break;
//
//            case Calendar.APRIL:
//            case Calendar.MAY:
//            case Calendar.JUNE:
//                this.calcTrimestre = 2;
//                this.calcEjercicio = cal.get(Calendar.YEAR);
//                break;
//            case Calendar.JULY:
//            case Calendar.AUGUST:
//            case Calendar.SEPTEMBER:
//                this.calcTrimestre = 3;
//                this.calcEjercicio = cal.get(Calendar.YEAR);
//                break;
//
//            case Calendar.OCTOBER:
//            case Calendar.NOVEMBER:
//            case Calendar.DECEMBER:
//                this.calcTrimestre = 4;
//                this.calcEjercicio = cal.get(Calendar.YEAR) + 1;
//                break;
//            default:
//                LOG.log(Level.SEVERE, "NO HA RECONOCIDO CORRECTAMENTE LA FECHA DE INFORME Y NO CALCULA EL EJERCICIO NI EL TRIMESTREnO ");
//        }

    }
    
    
}
