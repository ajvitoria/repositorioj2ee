
package es.icac.meh.pandora.modelo;

import java.util.Properties;

/**
 *
 * @author MAJIVIAL
 */
public class ConnectionConfiguration {
    
    private Properties properties;
    
    private String url;
    private String username;
    private String password;
    private String instance;
    private String nombreBD;
    private String driver;
    private String ipServer;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
       return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public String getNombreBD() {
        return nombreBD;
    }

    public void setNombreBD(String nombreBD) {
        this.nombreBD = nombreBD;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getIpServer() {
        return ipServer;
    }

    public void setIpServer(String ipServer) {
        this.ipServer = ipServer;
    }

    public Properties getProperties() {
        return properties;
    }
}
