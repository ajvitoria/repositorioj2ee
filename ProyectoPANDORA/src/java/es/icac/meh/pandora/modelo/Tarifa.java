/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.icac.meh.pandora.modelo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MAJIVIAL
 */

public class Tarifa {
    
    
    final SimpleDateFormat SDF=new SimpleDateFormat("dd-MM-yyyy");
    private static final Logger LOG = Logger.getLogger(Tarifa.class.getName());
    
    
    private int id;
    private String nombre;
    private int anyoEjercicioAplicacion;
    private Date fechaInicioAplicacion;
    private Date fechaFinAplicacion;
    private double importeDesde;
    private double importeHasta;
    private boolean esDeInteresPublico;
    private double precioTarifa;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getAnyoEjercicioAplicacion() {
        return anyoEjercicioAplicacion;
    }

    public void setAnyoEjercicioAplicacion(int anyoEjercicioAplicacion) {
        this.anyoEjercicioAplicacion = anyoEjercicioAplicacion;
    }

    public Date getFechaInicioAplicacion() {
        return fechaInicioAplicacion;
    }
    public String getFechaInicioAplicacionString() {
        return convertDateToString("fechaInicioAplicacion", fechaInicioAplicacion);
    }

    public void setFechaInicioAplicacion(Date fechaInicioAplicacion) {
        this.fechaInicioAplicacion = fechaInicioAplicacion;
    }

    public void setFechaInicioAplicacion(String fechaInicioAplicacion) {
        this.setFechaInicioAplicacion(convertStringToDate("fechaInicioAplicacion", fechaInicioAplicacion));;
    }


    
    
    
    public Date getFechaFinAplicacion() {
        return fechaFinAplicacion;
    }

    public String getFechaFinAplicacionString() {
        return convertDateToString("fechaFinAplicacion", fechaFinAplicacion);
    }

    public void setFechaFinAplicacion(Date fechaFinAplicacion) {
        this.fechaFinAplicacion = fechaFinAplicacion;
    }

    public void setFechaFinAplicacion(String fechaFinAplicacion) {
        this.setFechaFinAplicacion(convertStringToDate("fechaFinAplicacion", fechaFinAplicacion));;
    }  
    
    public double getImporteDesde() {
        return importeDesde;
    }

    public void setImporteDesde(double importeDesde) {
        this.importeDesde = importeDesde;
    }

    public double getImporteHasta() {
        return importeHasta;
    }

    public void setImporteHasta(double importeHasta) {
        this.importeHasta = importeHasta;
    }

    public boolean isEsDeInteresPublico() {
        return esDeInteresPublico;
    }

    public void setEsDeInteresPublico(boolean esDeInteresPublico) {
        this.esDeInteresPublico = esDeInteresPublico;
    }

    public double getPrecioTarifa() {
        return precioTarifa;
    }

    public void setPrecioTarifa(double precioTarifa) {
        this.precioTarifa = precioTarifa;
    }

    private String convertDateToString(String campo, Date valorParametro) {
        String retorno = null;
        try {
            retorno = SDF.format(valorParametro);
        } catch (Exception ex) {
            String msg = "Se ha intentado obtener el campo " + campo
                    + ". Intentado convertir la fecha " + valorParametro
                    + " a una cadena válida SIN EXITO";
            LOG.log(Level.SEVERE, msg);
        }
        return retorno;

    }

    private Date convertStringToDate(String campo, String valorParametro) {
        Date retorno = null;
        try {
            retorno = SDF.parse(valorParametro.trim());
        } catch (Exception ex) {
            String msg = "Se ha intentado obtener el campo " + campo
                    + ". Intentado convertir el valor " + valorParametro
                    + " a una fecha válida SIN EXITO";
            LOG.log(Level.SEVERE, msg);
        }
        return retorno;
    }

    @Override
    public String toString() {
        return nombre + "("+anyoEjercicioAplicacion+"}";
    }

}
