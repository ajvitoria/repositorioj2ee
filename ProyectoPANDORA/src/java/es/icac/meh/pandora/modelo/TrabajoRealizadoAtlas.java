package es.icac.meh.pandora.modelo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MAJIVIAL
 */
public class TrabajoRealizadoAtlas { 
    
    private static final Logger LOG = Logger.getLogger(TrabajoRealizadoAtlas.class.getName());
    final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");

    private String codRoac;
    private String identificacion;
    private String documento;
    private String razonSocial;
    private String nifAuditor;

    public String getNifAuditor() {
        return nifAuditor;
    }

    public void setNifAuditor(String nifAuditor) {
        this.nifAuditor = nifAuditor;
    }
    
    private Date primerEjercicioAuditado;
    private Date ejercicioAuditado;
    private boolean constitucion;
    
        private Date ejercicioFinal;
    private int idEntidad;
    private String descripcionEntidad;

    private boolean participesAuditado;
    private boolean participesAnterior;

    private double importeNetoAuditado;
    private double importeNetoAnterior;

    private int plantillaMediaAuditado;

    private int plantillaMediaAnterior;

    private int idTipoTrabajo;

    private String descripcionTipoTrabajo;

    private Date fechaInforme;
    /* esta es la fecha importante*/
    private String roacAuditorFirmante;
    private String nombreAuditorFirmante;

    private boolean coauditoria;
    /*SIEMPRE FALSE 'No'*/
    private String roacCoauditor;
    /*NO SE HA RELLLENADO NUNCA*/
    private int idTipoOpinion;
    private String descripcionTipoOpinion;
    private int idProvincia;
    private String provincia;
    private double facturacionAuditoriaHonorarios;
    private double facturacionAuditoriaHoras;
    private String cnmv;
    private boolean esInteresPublico;
    private double importeCifraActivo;
    private double importeCifraActivoAnterior;
    private double honorariosAuditoriaInterna;
    private double horasAuditoriaInterna;
    private double honorariosDiseno;
    private double horasDiseno;
    private double honorariosOtros;
    private double horasOtros;


    //ESTOS CAMPOS NO TIENEN SET PORQUE SE CALCULAN AL EJECUTAR EL setFechaInforme()
    private int calcTrimestre;
    private int calcEjercicio;
    private String informeDeCuentas;
    
    private int informeDeCuentasReal;
    
    private double tasa;
    
    public int getInformeDeCuentasReal() {
        return informeDeCuentasReal;
    }
    public String getInformeDeCuentas() {
        if (informeDeCuentasReal < 200) {
            return "Invidual";
        } else {
            return "Consolidado";
        }
    }


    public void setInformeDeCuentasReal(int informeDeCuentasReal) {
        this.informeDeCuentasReal = informeDeCuentasReal;
        if(informeDeCuentasReal<200){
            this.informeDeCuentas="Invidual";
        }else{
            this.informeDeCuentas="Consolidado";
        }
            
    }
    
    


    public String getCodRoac() {
        return codRoac;
    }

    public void setCodRoac(String cod_roac) {
        this.codRoac = cod_roac;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Date getPrimerEjercicioAuditado() {
        return primerEjercicioAuditado;
    }
    


    public void setPrimerEjercicioAuditado(String primerEjercicioAuditado) {

    }
    public void setPrimerEjercicioAuditado(Date primerEjercicioAuditado) {
        this.primerEjercicioAuditado = primerEjercicioAuditado;
    }

    public Date getEjercicioAuditado() {
        return ejercicioAuditado;
    }

    public void setEjercicioAuditado(Date ejercicioAuditado) {
        this.ejercicioAuditado = ejercicioAuditado;
    }

    public void setEjercicioAuditado(String ejercicioAuditado) {
        convertStringToDate("ejercicioAuditado", ejercicioAuditado);
    }

    public boolean isConstitucion() {
        return constitucion;
    }

    public void setConstitucion(boolean constitucion) {
        this.constitucion = constitucion;
    }
    public void setConstitucion(String constitucion) {
           this.constitucion =convertStringToBoolean("constitucion", constitucion);    
    }

    public Date getEjercicioFinal() {
        return ejercicioFinal;
    }
    
    public void setEjercicioFinal(String ejercicioFinal) {
       convertStringToDate("ejercicioFinal", ejercicioFinal);  
    }
    public void setEjercicioFinal(Date ejercicioFinal) {
        this.ejercicioFinal = ejercicioFinal;
    }

    public int getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(int idEntidad) {
        this.idEntidad = idEntidad;
    }

    public String getDescripcionEntidad() {
        return descripcionEntidad;
    }

    public void setDescripcionEntidad(String descripcionEntidad) {
        this.descripcionEntidad = descripcionEntidad;
    }

    public boolean isParticipesAuditado() {
        return participesAuditado;
    }
    
    public void setParticipesAuditado(boolean participesAuditado) {
        this.participesAuditado = participesAuditado;
    }
    public void setParticipesAuditado(String participesAuditado) {
            this.participesAuditado =convertStringToBoolean("participesAuditado", participesAuditado);    
    }

    public boolean isParticipesAnterior() {
        return participesAnterior;
    }

    public void setParticipesAnterior(String participesAnterior) {
       this.participesAnterior= convertStringToBoolean("participesAnterior", participesAnterior);
    }

    public void setParticipesAnterior(boolean participesAnterior) {
        this.participesAnterior = participesAnterior;
    }

    public double getImporteNetoAuditado() {
        return importeNetoAuditado;
    }

    public void setImporteNetoAuditado(double importeNetoAuditado) {
        this.importeNetoAuditado = importeNetoAuditado;
    }

    public double getImporteNetoAnterior() {
        return importeNetoAnterior;
    }

    public void setImporteNetoAnterior(double importeNetoAnterior) {
        this.importeNetoAnterior = importeNetoAnterior;
    }

    public int getPlantillaMediaAuditado() {
        return plantillaMediaAuditado;
    }

    public void setPlantillaMediaAuditado(int plantillaMediaAuditado) {
        this.plantillaMediaAuditado = plantillaMediaAuditado;
    }

    public int getPlantillaMediaAnterior() {
        return plantillaMediaAnterior;
    }

    public void setPlantillaMediaAnterior(int plantillaMediaAnterior) {
        this.plantillaMediaAnterior = plantillaMediaAnterior;
    }

    public int getIdTipoTrabajo() {
        return idTipoTrabajo;
    }

    public void setIdTipoTrabajo(int idTipoTrabajo) {
        this.idTipoTrabajo = idTipoTrabajo;
    }

    public String getDescripcionTipoTrabajo() {
        return descripcionTipoTrabajo;
    }

    public void setDescripcionTipoTrabajo(String descripcionTipoTrabajo) {
        this.descripcionTipoTrabajo = descripcionTipoTrabajo;
    }

    public Date getFechaInforme() {
        return fechaInforme;
    }
    public String getFechaInformeString() {
        return convertDateToString("FechaInforme", fechaInforme);
    }

    public void setFechaInforme(Date fechaInforme) {
        this.fechaInforme = fechaInforme;     
    }
    public void setFechaInforme(String fechaInforme) {
        this.setFechaInforme(convertStringToDate("fechaInforme", fechaInforme));  
    }

    public String getRoacAuditorFirmante() {
        return roacAuditorFirmante;
    }

    public void setRoacAuditorFirmante(String ROACAuditorFirmante) {
        this.roacAuditorFirmante = ROACAuditorFirmante;
    }

    public String getNombreAuditorFirmante() {
        return nombreAuditorFirmante;
    }

    public void setNombreAuditorFirmante(String nombreAuditorFirmante) {
        this.nombreAuditorFirmante = nombreAuditorFirmante;
    }

    public boolean isCoauditoria() {
        return coauditoria;
    }
    
    public void setCoauditoria(boolean coauditoria) {
        this.coauditoria = coauditoria;
    }

    public void setCoauditoria(String coauditoria) {
        this.coauditoria = convertStringToBoolean("coauditoria", coauditoria);
    }

    public String getRoacCoauditor() {
        return roacCoauditor;
    }

    public void setRoacCoauditor(String roacCoauditor) {
        this.roacCoauditor = roacCoauditor;
    }

    public int getIdTipoOpinion() {
        return idTipoOpinion;
    }

    public void setIdTipoOpinion(int idTipoOpinion) {
        this.idTipoOpinion = idTipoOpinion;
    }

    public String getDescripcionTipoOpinion() {
        return descripcionTipoOpinion;
    }

    public void setDescripcionTipoOpinion(String descripcionTipoOpinion) {
        this.descripcionTipoOpinion = descripcionTipoOpinion;
    }

    public int getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(int idProvincia) {
        this.idProvincia = idProvincia;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public double getFacturacionAuditoriaHonorarios() {
        return facturacionAuditoriaHonorarios;
    }

    public void setFacturacionAuditoriaHonorarios(double facturacionAuditoriaHonorarios) {
        this.facturacionAuditoriaHonorarios = facturacionAuditoriaHonorarios;
    }

    public double getFacturacionAuditoriaHoras() {
        return facturacionAuditoriaHoras;
    }

    public void setFacturacionAuditoriaHoras(double facturacionAuditoriaHoras) {
        this.facturacionAuditoriaHoras = facturacionAuditoriaHoras;
    }

    public String getCnmv(){
        return this.cnmv;
    }
            
    public void setCnmv(String cnmv) {
            this.cnmv=cnmv;   
    }

    public boolean isEsInteresPublico() {
        return esInteresPublico;
    }
    
    public void setEsInteresPublico(boolean esInteresPublico) {
        this.esInteresPublico = esInteresPublico;
    }
    public void setEsInteresPublico(String esInteresPublico) {
        this.esInteresPublico=convertStringToBoolean("esInteresPublico", esInteresPublico); 
    }

    public double getImporteCifraActivo() {
        return importeCifraActivo;
    }

    public void setImporteCifraActivo(double importeCifraActivo) {
        this.importeCifraActivo = importeCifraActivo;
    }

    public double getImporteCifraActivoAnterior() {
        return importeCifraActivoAnterior;
    }

    public void setImporteCifraActivoAnterior(double importeCifraActivoAnterior) {
        this.importeCifraActivoAnterior = importeCifraActivoAnterior;
    }

    public double getHonorariosAuditoriaInterna() {
        return honorariosAuditoriaInterna;
    }

    public void setHonorariosAuditoriaInterna(double honorariosAuditoriaInterna) {
        this.honorariosAuditoriaInterna = honorariosAuditoriaInterna;
    }

    public double getHorasAuditoriaInterna() {
        return horasAuditoriaInterna;
    }

    public void setHorasAuditoriaInterna(double horasAuditoriaInterna) {
        this.horasAuditoriaInterna = horasAuditoriaInterna;
    }

    public double getHonorariosDiseno() {
        return honorariosDiseno;
    }

    public void setHonorariosDiseno(double honorariosDiseno) {
        this.honorariosDiseno = honorariosDiseno;
    }

    public double getHorasDiseno() {
        return horasDiseno;
    }

    public void setHorasDiseno(double horasDiseno) {
        this.horasDiseno = horasDiseno;
    }

    public double getHonorariosOtros() {
        return honorariosOtros;
    }

    public void setHonorariosOtros(double honorariosOtros) {
        this.honorariosOtros = honorariosOtros;
    }

    public double getHorasOtros() {
        return horasOtros;
    }

    public void setHorasOtros(double horasOtros) {
        this.horasOtros = horasOtros;
    }
    private String convertDateToString(String campo, Date valorParametro){
        String retorno=null;
         try{
            retorno=SDF.format(valorParametro);
        }catch(Exception ex){
            String msg="Se ha intentado obtener el campo "+campo
                    + ". Intentado convertir la fecha "+valorParametro 
                    +" a una cadena válida SIN EXITO";
            LOG.log(Level.SEVERE,msg );
        }       
        return retorno;
        
    }
    private Date convertStringToDate(String campo, String valorParametro){
            Date retorno=null;
        try{
            retorno=SDF.parse(valorParametro.trim());
        }catch(Exception ex){
            String msg="Se ha intentado obtener el campo "+campo
                    + ". Intentado convertir el valor "+valorParametro 
                    +" a una fecha válida SIN EXITO";
            LOG.log(Level.SEVERE,msg );
        }
        return retorno;
    }
  
    private boolean convertStringToBoolean(String campo, String valorParametro) {
        boolean retorno = false;

        switch (valorParametro.toUpperCase()) {

            case "SI": case "SÍ": case "YES": case "TRUE":
                retorno = true;
                break;

            case "NO": case "FALSE":
                retorno = false;
                break;

            default:
                LOG.log(Level.SEVERE, "Se ha intentado obtener el campo "+campo
                    + ". Intentado convertir el valor "+valorParametro 
                    +" a una expresion booleana SIN EXITO");
        }
        return retorno;
    }
        
    @Override
    public String toString() {
        return "TrabajoRealizado{"
                + "codRoac=" + codRoac
                + ", fechaInforme=" + getFechaInformeString()
                + ", EJERCICIO:"+ calcEjercicio +" TRIMESTRE:" + calcTrimestre
                + ", facturacionAuditoriaHonorarios=" + facturacionAuditoriaHonorarios
                + ", esInteresPublico=" + esInteresPublico
                + '}';
    }

    public String toStringFULL() {
        return "TrabajoRealizado{"
                + "codRoac=" + codRoac
                + ", identificacion=" + identificacion
                + ", documento=" + documento
                + ", razonSocial=" + razonSocial
                + ", informeDeCuentas=" + informeDeCuentas
                + ", primerEjercicioAuditado=" + primerEjercicioAuditado
                + ", ejercicioAuditado=" + ejercicioAuditado
                + ", constitucion=" + constitucion
                + ", ejercicioFinal=" + ejercicioFinal
                + ", idEntidad=" + idEntidad
                + ", descripcionEntidad=" + descripcionEntidad
                + ", participesAuditado=" + participesAuditado
                + ", participesAnterior=" + participesAnterior
                + ", importeNetoAuditado=" + importeNetoAuditado
                + ", importeNetoAnterior=" + importeNetoAnterior
                + ", plantillaMediaAuditado=" + plantillaMediaAuditado
                + ", plantillaMediaAnterior=" + plantillaMediaAnterior
                + ", idTipoTrabajo=" + idTipoTrabajo
                + ", descripcionTipoTrabajo=" + descripcionTipoTrabajo
                + ", fechaInforme=" + fechaInforme
                + ", EJERCICIO:"+ calcEjercicio +" TRIMESTRE:" + calcTrimestre
                + ", roacAuditorFirmante=" + roacAuditorFirmante
                + ", nombreAuditorFirmante=" + nombreAuditorFirmante
                + ", coauditoria=" + coauditoria
                + ", roacCoauditor=" + roacCoauditor
                + ", idTipoOpinion=" + idTipoOpinion
                + ", descripcionTipoOpinion=" + descripcionTipoOpinion
                + ", idProvincia=" + idProvincia
                + ", provincia=" + provincia
                + ", facturacionAuditoriaHonorarios=" + facturacionAuditoriaHonorarios
                + ", facturacionAuditoriaHoras=" + facturacionAuditoriaHoras
                + ", CNMV=" + cnmv
                + ", esInteresPublico=" + esInteresPublico
                + ", importeCifraActivo=" + importeCifraActivo
                + ", importeCifraActivoAnterior=" + importeCifraActivoAnterior
                + ", honorariosAuditoriaInterna=" + honorariosAuditoriaInterna
                + ", horasAuditoriaInterna=" + horasAuditoriaInterna
                + ", honorariosDiseno=" + honorariosDiseno
                + ", horasDiseno=" + horasDiseno
                + ", honorariosOtros=" + honorariosOtros
                + ", horasOtros=" + horasOtros + '}';
    }

    

    public int getCalcTrimestre() {
        return calcTrimestre;
    }

    public int getCalcEjercicio() {
        return calcEjercicio;
    }

    public double getTasa() {
        return tasa;
    }

    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    
}
