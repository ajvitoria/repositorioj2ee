
package es.icac.meh.pandora.modelo;

import java.util.Date;

/**
 *
 * @author MAJIVIAL
 */
public interface Movimiento {

    public long getMovimientoId();//     id INT NOT NULL IDENTITY PRIMARY KEY,

    public String getRoac();// roac VARCHAR(35),

    public Date getFecha();// datetime,

    public Date getFechaPlazo(); //datetime;

    public String getNifAuditor(); //VARCHAR(35),

    public String getNombreAuditor();// VARCHAR( 145),	

    public int getEjercicioMovi();// INT,

    public int getTrimestreMovi();// INT,

    public boolean esDebe();//BIT,

    public int getFKRecaudacionID();// INT,

    public int getFKTrabajoID();// INT,

    public double getImporte();// FLOAT,

    public int getEjercicioCruce();// INT
}
