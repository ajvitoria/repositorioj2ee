package es.icac.meh.pandora.modelo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.File;

/**
 *
 * @author MAJIVIAL
 */
public class Lote {

    final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");
    private static final Logger LOG = Logger.getLogger(Lote.class.getName());

    private Date fechaProcesado;
    private int id;
    private String descripcion;
    private int numeroRecaudacions;
    private double importePorLote;
    private double cifraAcumulada;
    private double cifraPie;
    private int ejercicio;
    
    private double importePie;
                        private String quincenaAno;
                        private String quincenaTipo;
                        private String quincenaMes;
    
    private ArrayList<Recaudacion> listaRecaudacions;
    private boolean valido;


    
    private File fichero;

    public Lote(String descripcion, String fecha) {
        this(0, descripcion);
        setFechaProcesado(fecha);
    }

    public Lote(int id, String descripcion, String fecha) {
        this(id, descripcion);
        setFechaProcesado(fecha);
    }

    public Lote(int id, String descripcion, Date fecha) {
        this(id, descripcion);
        setFechaProcesado(SDF.format(fecha));
        listaRecaudacions = new ArrayList();
    }

    public Lote(int id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
        listaRecaudacions=new ArrayList();
    }

    public Date getFechaProcesado() {
        return fechaProcesado;
    }

    public String getFechaProcesadoString() {
        return SDF.format(getFechaProcesado());
    }

    public void setFechaProcesado(String fechaProcesado) {
        Date param = new Date();
        try {
            param = SDF.parse(fechaProcesado);

        } catch (Exception e) {

            param = new Date();
            LOG.log(Level.WARNING, "La fecha '" + fechaProcesado + "' de procesado del lote no ha podido convertirse");
        }
        this.setFechaProcesado(param);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Recaudacion> getListaRecaudacions() {
 // "Nunca usar este array para añadir sobre EL COBROS... usar anadirRecaudacions()");
        return listaRecaudacions;
    }

    public void anadirRecaudacions(Recaudacion nuevoRecaudacion) {

        this.listaRecaudacions.add(nuevoRecaudacion);

        double acumulado = 0;
//        acumulado = this.listaRecaudacions.stream().map((tmp) -> tmp.getImporteEuros()).reduce(acumulado, (accumulator, _item) -> accumulator + _item);
        for (Recaudacion tmp : this.listaRecaudacions) {
            acumulado += tmp.getImporteEuros();
        }
/*
        
        */
        this.importePorLote = acumulado;
        this.numeroRecaudacions = listaRecaudacions.size();

    }
    
    public double getCifraAcumulada() {
        return cifraAcumulada;
    }

    public void setCifraAcumulada(double cifraAcumulada) {
        this.cifraAcumulada = cifraAcumulada;
    }
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Lote{" + "id=" + getId() + ", fechaProcesado=" + getFechaProcesado() + ", descripcion=" + getDescripcion() + ", listaRecaudacions=" + getListaRecaudacions() + '}';
    }

    /**
     * @param fechaProcesado the fechaProcesado to set
     */
    public void setFechaProcesado(Date fechaProcesado) {
        this.fechaProcesado = fechaProcesado;
    }

    /**
     * @return the numeroRecaudacions
     */
    public int getNumeroRecaudacions() {
        return numeroRecaudacions;
    }

    /**
     * @param numeroRecaudacions the numeroRecaudacions to set
     */
    public void setNumeroRecaudacions(int numeroRecaudacions) {
        this.numeroRecaudacions = numeroRecaudacions;
    }

    /**
     * @return the importePorLote
     */
    public double getImportePorLote() {
        return importePorLote;
    }

    /**
     * @param importePorLote the importePorLote to set
     */
    public void setImportePorLote(double importePorLote) {
        this.importePorLote = importePorLote;
    }
    
    public File getFichero() {
        return fichero;
    }

    public void setFichero(File fichero) {
        this.fichero = fichero;
    }
    public boolean isValido() {
        return valido;
    }

    public void setValido(boolean resultado) {
        this.valido = resultado;
    }

    public double getCifraPie() {
        return cifraPie;
    }

    public void setCifraPie(double cifraPie) {
        this.cifraPie = cifraPie;
    }

    public double getImportePie() {
        return importePie;
    }
  public void setImportePie(String cadenaImportePie) {      
        this.importePie = Double.parseDouble(cadenaImportePie);
    }
    public void setImportePie(double importePie) {
        this.importePie = importePie;
    }

    public String getQuincenaAno() {
        return quincenaAno;
    }

    public void setQuincenaAno(String quincenaAno) {
        this.quincenaAno = quincenaAno;
    }

    public String getQuincenaTipo() {
        return quincenaTipo;
    }

    public void setQuincenaTipo(String quincenaTipo) {
        this.quincenaTipo = quincenaTipo;
    }

    public String getQuincenaMes() {
        return quincenaMes;
    }

    public void setQuincenaMes(String quincenaMes) {
        this.quincenaMes = quincenaMes;
    }

    public void setEjercicio(int ejercicio) {
        this.ejercicio=ejercicio;
    }

    public int getEjercicio() {
        return ejercicio;
    }
}
