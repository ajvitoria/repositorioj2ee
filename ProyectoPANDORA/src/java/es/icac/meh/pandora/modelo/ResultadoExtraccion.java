
package es.icac.meh.pandora.modelo;

    public class ResultadoExtraccion{
        
        private long tiempoProceso;
        private int numeroDatosExtraidos;
        

        public int getNumeroDatosExtraidos() {
            return numeroDatosExtraidos;
        }

        public void setNumeroDatosExtraidos(int numeroDatosExtraidos) {
            this.numeroDatosExtraidos = numeroDatosExtraidos;
        }

        public long getTiempoProceso() {
            return tiempoProceso;
        }

        public void setTiempoProceso(long tiempoProceso) {
            this.tiempoProceso = tiempoProceso;
        }
        
        

    }  