/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.icac.meh.pandora.modelo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author MAJIVIAL
 */
public class LoteTrabajoRealizado {

    final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");
    private static final Logger LOG = Logger.getLogger(LoteTrabajoRealizado.class.getName());
    
    
    private int id;
    private Date fechaProcesado;

    private String descripcion;
    private int numeroRecaudacions;
    private double importePorLote;
    private double cifraAcumulada;
    private double cifraPie;

    private ArrayList<Recaudacion> listaRecaudacions;
    private boolean valido;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFechaProcesado() {
        return fechaProcesado;
    }

    public void setFechaProcesado(Date fechaProcesado) {
        this.fechaProcesado = fechaProcesado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getNumeroRecaudacions() {
        return numeroRecaudacions;
    }

    public void setNumeroRecaudacions(int numeroRecaudacions) {
        this.numeroRecaudacions = numeroRecaudacions;
    }

    public double getImportePorLote() {
        return importePorLote;
    }

    public void setImportePorLote(double importePorLote) {
        this.importePorLote = importePorLote;
    }

    public double getCifraAcumulada() {
        return cifraAcumulada;
    }

    public void setCifraAcumulada(double cifraAcumulada) {
        this.cifraAcumulada = cifraAcumulada;
    }

    public double getCifraPie() {
        return cifraPie;
    }

    public void setCifraPie(double cifraPie) {
        this.cifraPie = cifraPie;
    }

    public ArrayList<Recaudacion> getListaRecaudacions() {
        return listaRecaudacions;
    }

    public void setListaRecaudacions(ArrayList<Recaudacion> listaRecaudacions) {
        this.listaRecaudacions = listaRecaudacions;
    }

    public boolean isValido() {
        return valido;
    }

    public void setValido(boolean valido) {
        this.valido = valido;
    }
}
