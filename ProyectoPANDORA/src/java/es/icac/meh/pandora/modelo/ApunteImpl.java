/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.icac.meh.pandora.modelo;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author MAJIVIAL
 */
public class ApunteImpl implements Apunte{
    final  SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");
    private long id;
    private double importe;
    private boolean esDebe;
    private String campo1;
    private int trimestre;
    private int ejercicio;
    private int anyo;
    
    private String nombreAuditor;
    private String nifAuditor;
    private String roac;
    private String importeString;
    private String denominacion;
    private Date fecha;
    private String campo2;
    private String campo3;
    private Date fechaPag;

    public Date getFechaPag() {
        return fechaPag;
    }
    @Override
    public Date getFechaPago() {
        return fechaPag;
    }
    public void setFechaPag(Date fechaPag) {
        this.fechaPag = fechaPag;
    }
    
    public void setImporte(double importe) {
        this.importe = importe;
    }

    public void setEsDebe(boolean esDebe) {
        this.esDebe = esDebe;
    }

    public void setCampo1(String campo1) {
        this.campo1 = campo1;
    }

    public void setTrimestre(int trimestre) {
        this.trimestre = trimestre;
    }

    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }

    public void setNombreAuditor(String nombreAuditor) {
        this.nombreAuditor = nombreAuditor;
    }

    public void setNifAuditor(String nifAuditor) {
        this.nifAuditor = nifAuditor;
    }

    public void setRoac(String roac) {
        this.roac = roac;
    }

    public void setImporteString(String importeString) {
        this.importeString = importeString;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setCampo2(String campo2) {
        this.campo2 = campo2;
    }

    public void setCampo3(String campo3) {
        this.campo3 = campo3;
    }

    @Override
    public Date getFecha() {
        return fecha;
    }

    public String getFechaString() {
        return SDF.format(getFecha());
    }


    @Override
    public String getDenominacion() {
       return denominacion;
    }

    @Override
    public String getImporteString() {
       return importeString;
    }

    @Override
    public String getRoac() {
       return roac;
    }

    @Override
    public String getNifAuditor() {
       return nifAuditor;
    }

    @Override
    public String getNombreAuditor() {
       return nombreAuditor;
    }


    @Override
    public double getImporte() {
       return importe;
    }

    @Override
    public boolean esDebe() {
       return esDebe;
    }

    @Override
    public String getCampo1() {
       return campo1;
    }

    @Override
    public String getCampo2() {
       return campo2;
    }

    @Override
    public String getCampo3() {
       return campo3;
    }

    @Override
    public long getFkEntidad() {
        return id;
    }
    
    @Override
    public int getEjercicio() {
       return ejercicio;
    }

    @Override
    public int getTrimestre() {
       return trimestre;
    }

    @Override
    public int getAnyo() {
       return this.anyo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setAnyo(int anyo) {
        this.anyo = anyo;
    }


    
}
