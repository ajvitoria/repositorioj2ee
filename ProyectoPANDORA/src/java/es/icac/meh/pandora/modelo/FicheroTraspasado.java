package es.icac.meh.pandora.modelo;

import java.io.File;
import java.util.Date;

/**
 *
 * @author MAJIVIAL
 */
public class FicheroTraspasado {

    private String url;
    private String nombre;
    private Date fechaTraspaso;
    private Date fechaCreacion;
    private boolean estaTraspasado;
    private String mensaje;

    private int cobrosProcesadosPorFichero = 0;
    private double importeRecaudacionsAcumulados = 0;

    public FicheroTraspasado(File targetFile, boolean estaTraspasado) {
        this(targetFile, estaTraspasado, "realizado");
    }

    public FicheroTraspasado(File targetFile, boolean estaTraspasado, String mensaje){
        this.url=targetFile.getAbsolutePath();
        this.nombre=targetFile.getName();
        fechaTraspaso=new Date();
        this.fechaCreacion=new Date(targetFile.lastModified());
        this.estaTraspasado=estaTraspasado;
        this.mensaje=mensaje;
    }

    public void setEstaTraspasado(boolean estaTraspasado) {
        if (!estaTraspasado) {
            mensaje = "ERROR AL TRASPASAR";
        } else {
            mensaje = "OK";
        }
        this.setEstaTraspasado(estaTraspasado, mensaje);
    }

    public void setEstaTraspasado(boolean estaTraspasado, String mensaje) {
         this.estaTraspasado = estaTraspasado;
         this.mensaje=mensaje;

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaTraspaso() {
        return fechaTraspaso;
    }

    public void setFechaTraspaso(Date fechaTraspaso) {
        this.fechaTraspaso = fechaTraspaso;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }


    public int getRecaudacionsProcesadosPorFichero() {
        return cobrosProcesadosPorFichero;
    }

    public void setRecaudacionsProcesadosPorFichero(int cobrosProcesadosPorFichero) {
        this.cobrosProcesadosPorFichero = cobrosProcesadosPorFichero;
    }

    public double getImporteRecaudacionsAcumulados() {
        return importeRecaudacionsAcumulados;
    }

    public void setImporteRecaudacionsAcumulados(double importeRecaudacionsAcumulados) {
        this.importeRecaudacionsAcumulados = importeRecaudacionsAcumulados;
    }

    public boolean estaTraspasado() {
        return estaTraspasado;
    }
    
 
}
