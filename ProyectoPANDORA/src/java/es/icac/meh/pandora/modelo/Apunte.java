/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.icac.meh.pandora.modelo;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author MAJIVIAL
 */
public interface Apunte {
    
    /**
     *
     */
    final SimpleDateFormat SDF=new SimpleDateFormat("dd-MM-yyyy");
    final DecimalFormat formatea = new DecimalFormat("###,###,###.##");  
    
    public Date     getFecha();
    public String   getDenominacion();
    public String   getImporteString();
    public String   getRoac();
    public String   getNifAuditor();
    public String   getNombreAuditor();
    
    public long     getFkEntidad();
    public int      getAnyo();
    
    public int      getEjercicio();    
    public int      getTrimestre();
    public double   getImporte();
    public boolean  esDebe();
    public String   getCampo1();
    public String   getCampo2();
    public String   getCampo3();
    
    public Date     getFechaPago();


}
