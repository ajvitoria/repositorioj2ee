
package es.icac.meh.pandora.exportacion;


import es.icac.meh.pandora.modelo.Apunte;

import es.icac.meh.pandora.modelo.TrabajoRealizadoAtlas;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


/**
 *
 * @author MAJIVIAL
 */
public class ExportarTrabajos {
    

     public static void main(String args[]) {
         
              ArrayList<Apunte> diario=new ArrayList();
              
              
                 TrabajoRealizadoAtlas tr0=new TrabajoRealizadoAtlas();
             tr0.setCodRoac("trabajo tr0");
        tr0.setFechaInforme("01-01-2016");
        tr0.setFacturacionAuditoriaHonorarios(400);
        

        
        TrabajoRealizadoAtlas tr1=new TrabajoRealizadoAtlas();
        tr1.setCodRoac("trabajo tr1");
        tr1.setFechaInforme("01-01-2017");
        tr1.setFacturacionAuditoriaHonorarios(100);

        
        TrabajoRealizadoAtlas tr2=new TrabajoRealizadoAtlas();
            tr2.setCodRoac("trabajo tr2");
        tr2.setFechaInforme("01-01-2017");
        tr2.setFacturacionAuditoriaHonorarios(100);
       
        //DURANTE LA TARIFA A APLICAR
               
        TrabajoRealizadoAtlas tr3=new TrabajoRealizadoAtlas();
            tr3.setCodRoac("trabajo tr3");
        tr3.setFechaInforme("01-01-2018");
        tr3.setFacturacionAuditoriaHonorarios(100);

              
        TrabajoRealizadoAtlas tr4=new TrabajoRealizadoAtlas();
            tr4.setCodRoac("trabajo tr4");
        tr4.setFechaInforme("01-06-2018");
        tr4.setFacturacionAuditoriaHonorarios(100);

              
        TrabajoRealizadoAtlas tr5=new TrabajoRealizadoAtlas();
            tr5.setCodRoac("trabajo tr5");
        tr5.setFechaInforme("31-12-2018");
        tr5.setFacturacionAuditoriaHonorarios(100);
//        diario.add(tr5);
//        
//        
//        
//
//      
//        
//        
//        //DESPUES DEL PERIODO DE LA TARIFA A APLICAR
//        TrabajoRealizadoAtlas tr6=new TrabajoRealizadoAtlas();
//            tr6.setCodRoac("trabajo tr6");
//        tr6.setFechaInforme("01-01-2018");
//        tr6.setFacturacionAuditoriaHonorarios(100);
//               
//        TrabajoRealizadoAtlas tr7=new TrabajoRealizadoAtlas();
//            tr7.setCodRoac("trabajo tr7");
//        tr7.setFechaInforme("01-01-2018");
//        tr7.setFacturacionAuditoriaHonorarios(100);
//                
//        TrabajoRealizadoAtlas tr8=new TrabajoRealizadoAtlas();
//            tr8.setCodRoac("trabajo tr8");
//        tr8.setFechaInforme("01-01-2018");
//        tr8.setFacturacionAuditoriaHonorarios(240);
//        
//        
//       
//
//
//        
//
//        
//        Recaudacion cobro1=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro2=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro3=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro4=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro5=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro6=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro7=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro8=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//     
//        diario.add(tr0);
//        diario.add(cobro1);
//        diario.add(cobro2);
//
//        diario.add(tr1);
//        diario.add(tr2);
//        diario.add(cobro3);
//
//        diario.add(tr3);
//        diario.add(tr4);
//        diario.add(cobro4);
//        diario.add(cobro5);
//        diario.add(cobro6);
//
//        diario.add(tr5);
//
//        diario.add(cobro7);
//        diario.add(cobro8);

       

        



       
               
        double saldo=0;
        double saldoH=0;
        double saldoD=0;
        
        for (Apunte tmp : diario) {
            System.out.print("\n" + tmp.getFecha() + "\t" + tmp.getDenominacion());
            if (tmp.esDebe()) {
                System.out.print("\t" + tmp.getImporteString());
                saldoD +=tmp.getImporte();
                saldo +=tmp.getImporte();
            } else {
                System.out.print("\t\t\t" + tmp.getImporteString());
                saldoH += tmp.getImporte();
                saldo -=tmp.getImporte();
            }
        }
        System.out.println("\n*************TOTALES**************\t"+saldoD+"\t"+saldoH);
        System.out.println("\n**************SALDO***************\t"+saldo);
    
         new ExportarTrabajos().generarExcel("c:\\javam\\trabajos.xls", diario);
     }
     
     public static void generarExcel(String nombreFichero, ArrayList<Apunte>lista){
         
         
        //Crear libro de trabajo en blanco
        int numeroHojas=0;
        XSSFWorkbook  libro = new XSSFWorkbook ();
        ArrayList<Sheet> hojas=new ArrayList();
        
        boolean termina=false;
        
     
        
        //Crea hoja nueva
        XSSFSheet  hoja = libro.createSheet("Hoja de datos 0");
        //Por cada línea se crea un arreglo de objetos (Object[])
 
        int numeroLinea = 1;
        
        double saldo=0;
        double saldoH=0;
        double saldoD=0;
        
        Iterator<Apunte> enumeracion=lista.iterator();
        
        String[]listaRot={
            "ID",
            "FECHA", 
            "DESCRIPCION________", 
            "ROAC",
            "AÑO",
            "TRIM",
            "DEBE",
            "HABER",
            "NIF____",
            "RAZON SOCIAL",
            "NIF",
            "TIPO INFORME",
            "FACTURACION"
        };
        Row rowCab = hoja.createRow(0);
        for(int i=0; i<listaRot.length;i++){
         Cell celdaDenominacion=rowCab.createCell(i, CellType.STRING);
            celdaDenominacion.setCellValue(listaRot[i]);
        }
            
            
        
        while(enumeracion.hasNext()){
                Apunte key=enumeracion.next();


               
            Row row = hoja.createRow(numeroLinea++);
            

            
            Cell celdaId=row.createCell(0, CellType.NUMERIC);
            celdaId.setCellValue(numeroLinea);
            
            Cell celdaFecha=row.createCell(1, CellType.STRING);
            SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
            String fec=sdf.format(key.getFecha());
                    
            
            celdaFecha.setCellValue(fec);
            
            Cell celdaDenominacion=row.createCell(2, CellType.STRING);
            celdaDenominacion.setCellValue(key.getDenominacion());
            
            Cell celdaRoac=row.createCell(3, CellType.STRING);
            celdaRoac.setCellValue(key.getRoac());
            
            Cell celdaEjercicio=row.createCell(4, CellType.NUMERIC);
            celdaEjercicio.setCellValue(key.getEjercicio());
            
            Cell celdaTrimestre=row.createCell(5, CellType.NUMERIC);
            celdaTrimestre.setCellValue(key.getTrimestre());
            
            if(key.esDebe()){
                Cell celdaDEBE=row.createCell(6, CellType.NUMERIC);
                celdaDEBE.setCellValue(key.getImporte());
                Cell celdaHABER=row.createCell(7, CellType.STRING);
                 celdaHABER.setCellValue("");
                 saldoD += key.getImporte();
                 saldo +=key.getImporte();
               
            }else{
                 Cell celdaDEBE=row.createCell(6, CellType.STRING);
                 celdaDEBE.setCellValue("");  
                 Cell celdaHABER=row.createCell(7, CellType.NUMERIC);
                 celdaHABER.setCellValue(key.getImporte());
                
                 saldoH += key.getImporte();
                 saldo -=key.getImporte();
            }    
       
            Cell celdaNifAuditor = row.createCell(8, CellType.STRING);
            celdaNifAuditor.setCellValue(key.getNifAuditor());

            Cell celdaNombreAuditor = row.createCell(9, CellType.STRING);
            celdaNombreAuditor.setCellValue(key.getNombreAuditor());

            Cell celdaCampo1 = row.createCell(10, CellType.STRING);
            celdaCampo1.setCellValue(key.getCampo1());

            Cell celdaCampo2 = row.createCell(11, CellType.STRING);
            celdaCampo2.setCellValue(key.getCampo2());

            Cell celdaCampo3 = row.createCell(12, CellType.STRING);
            celdaCampo3.setCellValue(key.getCampo3());


        
        if (numeroLinea>=3365000){
                Row rowTotales = hoja.createRow(numeroLinea++);
              //  rowTotales.setRowStyle(cs);

                Cell celdaTOTALES=rowTotales.createCell(6, CellType.STRING);
                celdaTOTALES.setCellValue("TOTALES:");

                Cell celdaDEBE=rowTotales.createCell(6, CellType.NUMERIC);
                celdaDEBE.setCellValue(saldoD);
                Cell celdaHABER=rowTotales.createCell(7, CellType.NUMERIC);
                celdaHABER.setCellValue(saldoH);

                Cell celdaSALDO=rowTotales.createCell(8, CellType.NUMERIC);
                celdaSALDO.setCellValue(saldo);            
            
                hojas.add(hoja);
                numeroLinea=0;
                hoja = libro.createSheet("Hoja de datos "+(++numeroHojas));
        }else{
            continue;
        }
    }
         
        try {
            //Se genera el documento
            FileOutputStream out = new FileOutputStream(new File(nombreFichero));
            libro.write(out);
            out.close();
            System.out.println("es.icac.meh.pandora.exportacion.ExportarTrabajos.main()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     
      
     public void generarExcelFake(String nombreFichero, ArrayList<Apunte>lista){
         
         
        //Crear libro de trabajo en blanco
        Workbook workbook = new HSSFWorkbook();
        //Crea hoja nueva
        Sheet sheet = workbook.createSheet("Hoja de datos");
        //Por cada línea se crea un arreglo de objetos (Object[])
        Map<String, Object[]> datos = new TreeMap<String, Object[]>();
        datos.put("1", new Object[]{"Identificador", "Nombre", "Apellidos"});
        datos.put("2", new Object[]{1, "María", "Remen"});
        datos.put("3", new Object[]{2, "David", "Allos"});
        datos.put("4", new Object[]{3, "Carlos", "Caritas"});
        datos.put("5", new Object[]{4, "Luisa", "Vitz"});
        //Iterar sobre datos para escribir en la hoja
        Set<String> keyset = datos.keySet();
        int numeroLinea = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(numeroLinea++);
            Object[] arregloObjetos = datos.get(key);
            int numeroCelda = 0;
            for (Object obj : arregloObjetos) {
                Cell cell = row.createCell(numeroCelda++);
                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }
            }
        }
        try {
            //Se genera el documento
            FileOutputStream out = new FileOutputStream(new File(nombreFichero));
            workbook.write(out);
            out.close();
            System.out.println("es.icac.meh.pandora.exportacion.ExportarTrabajos.main()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
