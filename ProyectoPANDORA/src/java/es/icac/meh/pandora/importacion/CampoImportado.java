/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.icac.meh.pandora.importacion;

public class CampoImportado {

    public int posIni, posFin    ;

    /*
        CampoImportado, por defecto será de tipo AlfaNumerico
    */
    public CampoImportado(int posIni, int posFin) {
        this(posIni, posFin, Tipo.ALFANUM);
    }
    

    public CampoImportado(int posIni, int posFin, Tipo tipo) {
        this.posIni = posIni;
        this.posFin = posFin;
    }
    
    private enum Tipo{
        NUMERO, ALFANUM
    }

}