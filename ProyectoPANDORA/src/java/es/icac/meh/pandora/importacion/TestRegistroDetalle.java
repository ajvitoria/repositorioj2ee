/*
1-1 Núm. Tipo de registro = 2
2 - 8 Núm. Número secuencial de registro con ceros a la izquierda
9 - 13 Núm. Código de Tasa
14 - 26 Núm. Número de justificante
27 - 35 Alf. NIF
36 - 62 Alf. Apellidos y Nombre
63 - 70 Núm. Fecha de ingreso (AAAAMMDD)
71 - 82 Núm. Importe en céntimos de euro con ceros a la izquierda
83 - 86 Núm. Código de la entidad financiera en que se produce el ingreso
87 - 90 Núm. Código de la sucursal
 */
package es.icac.meh.pandora.importacion;

import app.ImportarFicheroRecaudaciones;
import java.io.BufferedReader;
import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestRegistroDetalle {

    private CampoImportado tipoRegistro;
    private CampoImportado numeroSecuencial;
    private CampoImportado codigoTasa;
    private CampoImportado numeroJustificante;
    private CampoImportado nif;
    private CampoImportado apellidosNombre;
    private CampoImportado fechaIngreso;
    private CampoImportado importe;
    private CampoImportado codigoEntiendadFinanciera;
    private CampoImportado codigoSucursal;
    private File fichero;

    private ArrayList<String> lineas;

    public TestRegistroDetalle(String fis) throws FileNotFoundException {
        FileReader fr = null;
        BufferedReader br = null;
        lineas = new ArrayList<>();
        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).

            fichero = new File(fis);
            if (!fichero.exists()) {
                new FileNotFoundException();
            } else {
                fichero = new File(fis);
            }
            System.out.println("procesando fichero " + fichero);
            fr = new FileReader(fichero);
            br = new BufferedReader(fr);
            // Lectura del fichero
            String linea;
            while ((linea = br.readLine()) != null) {
                lineas.add(linea);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        this.generarEstructura();
        this.procesarFichero();
    }

    private void generarEstructura() {
        this.tipoRegistro = new CampoImportado(1, 1);
        this.numeroSecuencial = new CampoImportado(2, 8);
        this.codigoTasa = new CampoImportado(9, 13);
        this.numeroJustificante = new CampoImportado(14, 26);
        this.nif = new CampoImportado(27, 35);
        this.apellidosNombre = new CampoImportado(36, 62);
        this.fechaIngreso = new CampoImportado(63, 70);
        this.importe = new CampoImportado(71, 82);
        this.codigoEntiendadFinanciera = new CampoImportado(83, 86);
        this.codigoSucursal = new CampoImportado(87, 90);
    }

    @Override
    public String toString() {
        return "RegistroDetalle{" + "tipoRegistro=" + tipoRegistro + ", numeroSecuencial=" + numeroSecuencial + ", codigoTasa=" + codigoTasa + ", numeroJustificante=" + numeroJustificante + ", nif=" + nif + ", apellidosNombre=" + apellidosNombre + ", fechaIngreso=" + fechaIngreso + ", importe=" + importe + ", codigoEntiendadFinanciera=" + codigoEntiendadFinanciera + ", codigoSucursal=" + codigoSucursal + ", fichero=" + fichero + '}';
    }

    private void procesarFichero() {
        for (String linea : this.lineas) {

            //OJITO   LA FUNCION substring de java inicia el recuento de los indices en 0 (cero) 
               // mientras que en el apartado de requisitos --> Diseño archivo de tasas (modelo 791).pdf
               
               
            //   a la posición inicial del indice desde se le quita UNA UNIDAD.
            //   a la posición DESDE del indice se la deja como esta.
            
         
            /*
                        INDICES    TIPO    NOMBRE DEL CAMPO
                       ========    =====   =============================================================
                        1-1        Núm.    Tipo de registro = 2
                        2 - 8      Núm.    Número secuencial de registro con ceros a la izquierda
                        9 - 13     Núm.    Código de Tasa
                       14 - 26     Núm.    Número de justificante
                       27 - 35     Alf.    NIF
                       36 - 62     Alf.    Apellidos y Nombre
                       63 - 70     Núm.    Fecha de ingreso (AAAAMMDD)
                       71 - 82     Núm.    Importe en céntimos de euro con ceros a la izquierda
                       83 - 86     Núm.    Código de la entidad financiera en que se produce el ingreso
                       87 - 90     Núm.    Código de la sucursal
             */
            System.out.println("tipoRegistro: " + linea.substring(tipoRegistro.posIni - 1, tipoRegistro.posFin ));
            System.out.println("codigo tasa: " + linea.substring(codigoTasa.posIni - 1, codigoTasa.posFin ));
            System.out.println("apellidosNombre: " + linea.substring(apellidosNombre.posIni - 1, apellidosNombre.posFin ));
            System.out.println("fechaIngreso: " + linea.substring(fechaIngreso.posIni - 1, fechaIngreso.posFin ));
        }
    }
    
        public static void main(String[] args) {
        try {
            System.out.println("funcionadno");
            TestRegistroDetalle app=new TestRegistroDetalle("d:\\pandora\\ficheroHacienda.dat");
            System.out.println(app);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImportarFicheroRecaudaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
