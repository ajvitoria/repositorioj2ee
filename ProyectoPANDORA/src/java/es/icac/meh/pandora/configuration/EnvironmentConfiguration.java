package es.icac.meh.pandora.configuration;

/*
 * Este bean manejara la configuracion del entorno donde se despliegue la 
 * aplicacion.

 */

/**
 *
 * @author MAJIVIAL
 */
public class EnvironmentConfiguration {
    
    private String environmentName;
    private String stageDirectory;
    private String treatmentDirectory;
    
    public String getEnvironmentName() {
        return environmentName;
    }

    public void setEnvironmentName(String environmentName) {
        this.environmentName = environmentName;
    }

    public String getStageDirectory() {
        return stageDirectory;
    }

    public void setStageDirectory(String stageDirectory) {
        this.stageDirectory = stageDirectory;
    }

    public String getTreatmentDirectory() {
        return treatmentDirectory;
    }

    public void setTreatmentDirectory(String treatmentDirectory) {
        this.treatmentDirectory = treatmentDirectory;
    }
    
}
