/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import es.icac.meh.pandora.modelo.TrabajoRealizado;
import es.icac.meh.pandora.services.impl.ConnectionServiceImpl;
import es.icac.meh.pandora.modelo.TrabajoRealizadoAtlas;
import es.icac.meh.pandora.services.ConnectionService;
import es.icac.meh.pandora.services.ConnectionService;
import es.icac.meh.pandora.services.daos.TrabajosRealizadosDAO;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author MAJIVIAL
 */
public class ProbandoDAOTrabajos {

    public static void main(String[] args) throws SQLException {
long tiempoInicio=new Date().getTime();
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("config/applicationContext.xml");
        System.out.println("=====LISTA DE BEANS CARGADOS=====");
        for (String tmp : applicationContext.getBeanDefinitionNames()) {
            System.out.println(" " + tmp);
        }

        ConnectionService servicio = applicationContext.getBean("service", ConnectionService.class);

  System.out.println("*******************TRABAJOS******************************");

        Connection conexionPrueba = servicio.getConnection();
        if (conexionPrueba != null) {
            
TrabajosRealizadosDAO dao=   applicationContext.getBean("trabajosDAO", TrabajosRealizadosDAO.class);


          ArrayList<TrabajoRealizado>lista= dao.obtenerTrabajos();
            for(int i=0; i<23; i++){
                  //  System.out.println(lista[i]);
                System.out.println(lista.get(i));
            }
            
            
            try {
                DatabaseMetaData dm = (DatabaseMetaData) conexionPrueba.getMetaData();
                System.out.println("Driver : " + dm.getDriverName());
                System.out.println("Driver version: " + dm.getDriverVersion());
                System.out.println("Product nombre: " + dm.getDatabaseProductName());
                System.out.println("Product version: " + dm.getDatabaseProductVersion());
            } catch (SQLException ex) {
                Logger.getLogger(ConnectionServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
           //conexionPrueba.close();
            }
            
        }
        System.out.println("########### AHORA A CERRAR LA APLICACION #########");
      //  applicationContext.close();
        applicationContext.registerShutdownHook();
        
        long tiempoFin=new Date().getTime();
          System.out.println("###########tiempo transcurrido="+((tiempoFin-tiempoInicio)/1000)+"segundos");
        

    }
    
    
    

}
