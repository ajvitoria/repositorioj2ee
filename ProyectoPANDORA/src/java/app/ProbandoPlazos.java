/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MAJIVIAL
 * 
 * 
 * 
 * 
Este informe se obtiene a partir del fichero procedente de la Agencia Tributaria. 

Para que un pago entre dentro del plazo, tiene que haberse efectuado 
* dentro de los 25 primeros días del mes siguiente a finalizar el trimestre natural. 
* Ejp.  si un auditor ha realizado los trabajos en el primer trimestre (1-ene hasta 30-mar) 
* el pago lo tiene que realizar en el periodo desde el 1 de abril hasta el 25 de abril. 
* Si paga posteriormente se considera que ha pagado fuera de plazo.

El informe resultante deberá contener al menos los siguientes campos:

* 
* 
* 
* 
 */
public class ProbandoPlazos {

    private final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");

    public static void main(String[] args) {
       // new ProbandoPlazos().iniciar();
        
        Date hoy=new Date();
        Calendar cal=Calendar.getInstance();
        imprimir(cal);
        
        SimpleDateFormat SDF=new SimpleDateFormat("dd-MM-yyyy");
        Date otra=null;
        try {
            otra=SDF.parse("16-05-1971");
            Calendar ca=Calendar.getInstance();
            ca.setTime(otra);
            imprimir(ca);
            
            GregorianCalendar ca2=new GregorianCalendar(1971, Calendar.MAY, 16);
            
            imprimir(ca2);
            
        } catch (ParseException ex) {
            Logger.getLogger(ProbandoPlazos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public static void imprimir(Calendar cal){
        System.out.println("mes:"+cal.get(Calendar.MONTH));
    
    }
    public void iniciar() {
        System.out.println("mete una fecha:");
        Scanner sc = new Scanner(System.in);

        String fecha = null;
        do {
            fecha = sc.nextLine();

            String resultado = sumarDias(fecha, 25);
            System.out.println("app.ProbandoPlazos.iniciar()-->" + resultado);

        } while (!fecha.equals(""));
    }

    public String sumarDias(String fechaYHora, int dias) {
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd-MM-yyyy");


        //convert String to LocalDate
        LocalDate localDate = LocalDate.parse(fechaYHora, formateador);
        // Lo convertimos a objeto para poder trabajar con él
     //   LocalDateTime fechaYHoraLocal = LocalDateTime.parse(fechaYHora, formateador);

        // Sumar los años indicados
        localDate = localDate.plusDays(dias);

        //Formatear de nuevo y regresar como cadena
        return localDate.format(formateador);
    }
}
