/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import es.icac.meh.pandora.modelo.Recaudacion;
import es.icac.meh.pandora.modelo.Lote;
import es.icac.meh.pandora.services.daos.RecaudacionesDAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author MAJIVIAL
 */
public class ProbandoDAORecaudacions{

    public static void main(String[] args) throws SQLException {
        long tiempoInicio = new Date().getTime();
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("config/applicationContext.xml");
        System.out.println("=====LISTA DE BEANS CARGADOS=====");
        for (String tmp : applicationContext.getBeanDefinitionNames()) {
            System.out.println(" " + tmp);
        }

        RecaudacionesDAO dao = applicationContext.getBean("recaudacionesDAO", RecaudacionesDAO.class);
       Lote lote1 = new Lote(1, "descripcion de mi nuevo lote 1", "16-03-2020");
//   //     ArrayList<Recaudacion> lista = dao.obtenerRecaudacionsPorLote(lote1);
//        System.out.println("app.ProbandoDAORecaudacions.main()--> LISTADO DE cobros del lote1");
//        for (Recaudacion tmp : lista) {
//            System.out.println(">>" + tmp);
//        }

        System.out.println("app.ProbandoDAOLotes.main()-> obteniendo uno");

        Lote lote2 = new Lote(2, "descripcion de mi nuevo lote 2", "16-03-2020");
    //    Recaudacion cobro1=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 1200.06, "2038", "1920");
////        System.out.println("insertado nuevo lote:" + dao.insertarRecaudacion(cobro1, lote2));

        Recaudacion cobroRecuperado = dao.obtenerRecaudacionById(1);

        System.out.println("uLTIMO REGISTRO INSERTADO ES=" + dao.obtenerUltimoId());
        System.out.println("recuperado obteniendo uno" + cobroRecuperado);

        
        Recaudacion cobroAActualizar=null;
        cobroAActualizar.setId(15);
        
        
         dao.moverRecaudacionAOtroLote(cobroAActualizar,lote2, lote1);
        
        
//        System.out.println("borrando lote 2");
//        dao.borrarLote(2);
//        System.out.println("uLTIMO REGISTRO INSERTADO ES=" + dao.obtenerUltimoId());
//        lista = dao.obtenerLotes();
//        for (Lote tmp : lista) {
//            System.out.println(">>" + tmp);
//        }

        long tiempoFin = new Date().getTime();
        System.out.println("###########tiempo transcurrido=" + ((tiempoFin - tiempoInicio) / 1000) + "segundos");

    }

}
