/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import es.icac.meh.pandora.modelo.Lote;
import es.icac.meh.pandora.services.impl.ConnectionServiceImpl;
import es.icac.meh.pandora.modelo.TrabajoRealizadoAtlas;
import es.icac.meh.pandora.services.ConnectionService;
import es.icac.meh.pandora.services.ConnectionService;
import es.icac.meh.pandora.services.daos.LoteDAO;
import es.icac.meh.pandora.services.daos.TrabajosRealizadosDAO;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author MAJIVIAL
 */
public class ProbandoDAOLotes {

    public static void main(String[] args) throws SQLException {
        long tiempoInicio = new Date().getTime();
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("config/applicationContext.xml");
        System.out.println("=====LISTA DE BEANS CARGADOS=====");
        for (String tmp : applicationContext.getBeanDefinitionNames()) {
            System.out.println(" " + tmp);
        }

        LoteDAO dao = applicationContext.getBean("loteDAO", LoteDAO.class);

        ArrayList<Lote> lista = dao.obtenerLotes();
        for (Lote tmp : lista) {
            System.out.println(">>" + tmp);
        }

        System.out.println("app.ProbandoDAOLotes.main()-> obteniendo uno");

        Lote loteNuevo = new Lote(0, "descripcion de mi nuevo lote", "31-01-2020");
        System.out.println("insertado nuevo lote:" + dao.insertarLote(loteNuevo));

        Lote lote2 = dao.obtenerLoteById(1);

        System.out.println("uLTIMO REGISTRO INSERTADO ES=" + dao.obtenerUltimoId());
        System.out.println("recuperado obteniendo uno" + lote2);

        System.out.println("borrando lote 2");
        dao.borrarLote(2);
        System.out.println("uLTIMO REGISTRO INSERTADO ES=" + dao.obtenerUltimoId());
        lista = dao.obtenerLotes();
        for (Lote tmp : lista) {
            System.out.println(">>" + tmp);
        }

        long tiempoFin = new Date().getTime();
        System.out.println("###########tiempo transcurrido=" + ((tiempoFin - tiempoInicio) / 1000) + "segundos");

    }

}
