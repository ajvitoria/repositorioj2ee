/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import es.icac.meh.pandora.modelo.TrabajoRealizadoAtlas;
import es.icac.meh.pandora.services.impl.CalculoTarifaServiceImpl;
import java.util.ArrayList;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author MAJIVIAL
 */
public class ProbandoTarifas {

    private static final Logger LOG = Logger.getLogger(ProbandoTarifas.class.getName());
    
    
    public static void main(String[] args) {
        
        
        //forma antigua. CalculoTarifaServiceImpl calculadora=new CalculoTarifaServiceImpl();
                    Handler consoleHandler = new ConsoleHandler();
                 consoleHandler.setLevel(Level.WARNING);
                    LOG.addHandler(consoleHandler);
             
     
         ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("config/applicationContext.xml");
           
         CalculoTarifaServiceImpl  calculadora=        (CalculoTarifaServiceImpl) applicationContext.getBean("tarificador"); 
        //ANTES DE LA TARIFA A APLICAR
        
        TrabajoRealizadoAtlas tr0=new TrabajoRealizadoAtlas();
             tr0.setCodRoac("trabajo tr0");
        tr0.setFechaInforme("01-01-2016");
        tr0.setFacturacionAuditoriaHonorarios(240);
        
        TrabajoRealizadoAtlas tr1=new TrabajoRealizadoAtlas();
        tr1.setCodRoac("trabajo tr1");
        tr1.setFechaInforme("01-01-2017");
        tr1.setFacturacionAuditoriaHonorarios(240);
                
        TrabajoRealizadoAtlas tr2=new TrabajoRealizadoAtlas();
            tr2.setCodRoac("trabajo tr2");
        tr2.setFechaInforme("01-01-2017");
        tr2.setFacturacionAuditoriaHonorarios(240);
        
        //DURANTE LA TARIFA A APLICAR
               
        TrabajoRealizadoAtlas tr3=new TrabajoRealizadoAtlas();
            tr3.setCodRoac("trabajo tr3");
        tr3.setFechaInforme("01-01-2018");
        tr3.setFacturacionAuditoriaHonorarios(32240);
                
        TrabajoRealizadoAtlas tr4=new TrabajoRealizadoAtlas();
            tr4.setCodRoac("trabajo tr4");
        tr4.setFechaInforme("01-06-2018");
        tr4.setFacturacionAuditoriaHonorarios(240);
                
        TrabajoRealizadoAtlas tr5=new TrabajoRealizadoAtlas();
            tr5.setCodRoac("trabajo tr5");
        tr5.setFechaInforme("31-12-2018");
        tr5.setFacturacionAuditoriaHonorarios(30000);
        
        //DURANTE LA TARIFA A APLICAR Y CON INTERES PUBLICO
        TrabajoRealizadoAtlas tr3CIP=new TrabajoRealizadoAtlas();
            tr3CIP.setCodRoac("trabajo tr3CIP");
        tr3CIP.setFechaInforme("01-01-2018");
        tr3CIP.setFacturacionAuditoriaHonorarios(32240);
        tr3CIP.setEsInteresPublico(true);
        
        TrabajoRealizadoAtlas tr4CIP=new TrabajoRealizadoAtlas();
            tr4CIP.setCodRoac("trabajo tr4CIP");
        tr4CIP.setFechaInforme("01-06-2018");
        tr4CIP.setFacturacionAuditoriaHonorarios(240);
        tr4CIP.setEsInteresPublico(true);

        //reparado
        TrabajoRealizadoAtlas tr5CIP=new TrabajoRealizadoAtlas();
            tr5CIP.setCodRoac("trabajo tr5CIP");
        tr5CIP.setFechaInforme("31-12-2018");
        tr5CIP.setFacturacionAuditoriaHonorarios(30000);
        tr5CIP.setEsInteresPublico(true);        
        
        
        //DESPUES DEL PERIODO DE LA TARIFA A APLICAR
        TrabajoRealizadoAtlas tr6=new TrabajoRealizadoAtlas();
            tr6.setCodRoac("trabajo tr6");
        tr6.setFechaInforme("01-01-2018");
        tr6.setFacturacionAuditoriaHonorarios(240);
               
        TrabajoRealizadoAtlas tr7=new TrabajoRealizadoAtlas();
            tr7.setCodRoac("trabajo tr7");
        tr7.setFechaInforme("01-01-2018");
        tr7.setFacturacionAuditoriaHonorarios(240);
                
        TrabajoRealizadoAtlas tr8=new TrabajoRealizadoAtlas();
            tr8.setCodRoac("trabajo tr8");
        tr8.setFechaInforme("01-01-2018");
        tr8.setFacturacionAuditoriaHonorarios(240);
        
        
                TrabajoRealizadoAtlas trx=new TrabajoRealizadoAtlas();
            trx.setCodRoac("trabajo x");
        trx.setFechaInforme("01-01-2018");
        trx.setFacturacionAuditoriaHonorarios(30000);
        trx.setEsInteresPublico(false);
        
        
       //PROBAR CAMBIO DE AÑO: 31/12/17, 01/01/18, 31/12/18 y 01/01/19 
       // SIN INTERES PUBLICO
        TrabajoRealizadoAtlas trA=new TrabajoRealizadoAtlas();
        trA.setCodRoac("trabajo trA");
        trA.setFechaInforme("31-01-2017");
        trA.setFacturacionAuditoriaHonorarios(1000);
        trA.setEsInteresPublico(false);
        
        TrabajoRealizadoAtlas trB=new TrabajoRealizadoAtlas();
        trB.setCodRoac("trabajo trB");
        trB.setFechaInforme("01-01-2018");
        trB.setFacturacionAuditoriaHonorarios(1000);
        trB.setEsInteresPublico(false);
        
        TrabajoRealizadoAtlas trC=new TrabajoRealizadoAtlas();
        trC.setCodRoac("trabajo trC");
        trC.setFechaInforme("31-12-2018");
        trC.setFacturacionAuditoriaHonorarios(1000);
        trC.setEsInteresPublico(false);
        
        TrabajoRealizadoAtlas trD=new TrabajoRealizadoAtlas();
        trD.setCodRoac("trabajo trD");
        trD.setFechaInforme("01-01-2019");
        trD.setFacturacionAuditoriaHonorarios(1000);
        trD.setEsInteresPublico(false);

        
           
       
        ArrayList<TrabajoRealizadoAtlas> trabajos=new ArrayList();
        trabajos.add(tr0);
        trabajos.add(tr1);
        trabajos.add(tr2);
        trabajos.add(tr3);
        trabajos.add(tr4);
        trabajos.add(tr5);
        trabajos.add(tr3CIP);
        trabajos.add(tr4CIP);
        trabajos.add(tr5CIP);
        
        trabajos.add(tr6);
        trabajos.add(tr7);
        trabajos.add(tr8);
        
        trabajos.add(trx);
        
        trabajos.add(trA);
        trabajos.add(trB);
        trabajos.add(trC);
        trabajos.add(trD);
        
        
        for(TrabajoRealizadoAtlas tmp: trabajos){
           
          //  System.out.println("Para el trabajo: \n"+tmp+"\nse cobrara una comision de "+calculadora.damePrecio(tmp));
            LOG.log(Level.INFO, ("Para el trabajo: \n"+tmp+"\nse cobrara una comision de "+calculadora.damePrecio(tmp)));
        
        }
        
    }
}
