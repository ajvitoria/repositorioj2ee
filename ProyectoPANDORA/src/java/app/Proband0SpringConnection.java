/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import es.icac.meh.pandora.services.impl.ConnectionServiceImpl;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author MAJIVIAL
 */
public class Proband0SpringConnection {

    public static void main(String[] args) throws SQLException {

        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("config/applicationContext.xml");
        System.out.println("=====LISTA DE BEANS CARGADOS=====");
        for (String tmp : applicationContext.getBeanDefinitionNames()) {
            System.out.println(" " + tmp);
        }

        ConnectionServiceImpl servicio = applicationContext.getBean("service", ConnectionServiceImpl.class);

        System.out.println(">>>>>URL :" + servicio.getConfiguracion().getUrl());
        System.out.println(">>>>>USUARIO :" + servicio.getConfiguracion().getUsername());
        System.out.println(">>>>>CONSTRASENA :" + servicio.getConfiguracion().getPassword());
        System.out.println(">>>>>IP SERVIDOR :" + servicio.getConfiguracion().getIpServer());
        System.out.println(">>>>>DRIVER:" + servicio.getConfiguracion().getDriver());
        System.out.println(">>>>>INSTANCIA:" + servicio.getConfiguracion().getInstance());
        System.out.println(">>>>>BD:" + servicio.getConfiguracion().getNombreBD());

        Connection conexionPrueba = servicio.getConnection();
        if (conexionPrueba != null) {

            try {
                DatabaseMetaData dm = (DatabaseMetaData) conexionPrueba.getMetaData();
                System.out.println("Driver : " + dm.getDriverName());
                System.out.println("Driver version: " + dm.getDriverVersion());
                System.out.println("Product nombre: " + dm.getDatabaseProductName());
                System.out.println("Product version: " + dm.getDatabaseProductVersion());
            } catch (SQLException ex) {
                Logger.getLogger(ConnectionServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
           //conexionPrueba.close();
            }
            
        }
        System.out.println("########### AHORA A CERRAR LA APLICACION #########");
      //  applicationContext.close();
        applicationContext.registerShutdownHook();

    }
}
