/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import es.icac.meh.pandora.exportacion.ExportarTrabajos;
import es.icac.meh.pandora.modelo.Apunte;
import es.icac.meh.pandora.modelo.Recaudacion;
import es.icac.meh.pandora.modelo.TrabajoRealizado;
import es.icac.meh.pandora.services.daos.TrabajosRealizadosDAO;
import es.icac.meh.pandora.services.daos.RecaudacionesDAO;
import es.icac.meh.pandora.services.daos.TrabajosDTO;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author MAJIVIAL
 */
public class ProbandoCruceRecaudacionsTrabajosDAO {

    private static final Logger LOG = Logger.getLogger(ProbandoCruceRecaudacionsTrabajosDAO.class.getName());
    
    
    public static void main(String[] args) {
        ProbandoCruceRecaudacionsTrabajosDAO calculadora=new ProbandoCruceRecaudacionsTrabajosDAO();
                    Handler consoleHandler = new ConsoleHandler();
                 consoleHandler.setLevel(Level.WARNING);
                    LOG.addHandler(consoleHandler);
             
     
                    
    }
    ProbandoCruceRecaudacionsTrabajosDAO(){
        
        
        ArrayList<Apunte> diario=new ArrayList();
                ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("config/applicationContext.xml");
        System.out.println("=====LISTA DE BEANS CARGADOS=====");
        for (String tmp : applicationContext.getBeanDefinitionNames()) {
            System.out.println(" " + tmp);
        }

        
     TrabajosDTO dto=   applicationContext.getBean("trabajoDTO", TrabajosDTO.class);
        String ejercicio="2018";
        
  //   dto.importarTrabajosAPartirEjerciciosConfiguracion();
dto.extraerTrabajos(ejercicio, true);

        TrabajosRealizadosDAO servicioTrabajos = applicationContext.getBean("trabajosDAO", TrabajosRealizadosDAO.class);
        RecaudacionesDAO servicioRecaudacions=applicationContext.getBean("recaudacionesDAO", RecaudacionesDAO.class);
        Collection<? extends Apunte> listaTrabajos=null;
        try {
            listaTrabajos = servicioTrabajos.obtenerTrabajos();
        } catch (SQLException ex) {
            Logger.getLogger(ProbandoCruceRecaudacionsTrabajosDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

       diario.addAll(listaTrabajos);
 Collection<? extends Apunte> listaRecaudacions=null;
        try {
            listaRecaudacions = servicioRecaudacions.obtenerRecaudacionesByEjercicio(ejercicio);
        } catch (SQLException ex) {
            Logger.getLogger(ProbandoCruceRecaudacionsTrabajosDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
     diario.addAll(listaRecaudacions);
       
//ExportarTrabajos.generarExcel("c:\\javam\\listaApuntes.xlsx", diario);
//System.out.println("realizado el trabajo");

       

        



       
               
        double saldo=0;
        double saldoH=0;
        double saldoD=0;
        
        for (Apunte tmp : diario) {
            System.out.print("\n" + tmp.getFecha() + "\t" + tmp.getDenominacion() + "\t" + tmp.getRoac());

            if (tmp.esDebe()) {
                System.out.print("\t" + tmp.getImporteString());
                saldoD +=tmp.getImporte();
                saldo +=tmp.getImporte();
            } else {
                System.out.print("\t\t\t" + tmp.getImporteString());
                saldoH += tmp.getImporte();
                saldo -=tmp.getImporte();
            }
        }
        System.out.println("\n*************TOTALES**************\t"+saldoD+"\t"+saldoH);
        System.out.println("\n**************SALDO***************\t"+saldo);
    }
}
