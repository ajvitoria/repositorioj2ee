/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import es.icac.meh.pandora.modelo.Apunte;
import es.icac.meh.pandora.modelo.Recaudacion;
import es.icac.meh.pandora.modelo.TrabajoRealizadoAtlas;
import es.icac.meh.pandora.services.impl.CalculoTarifaServiceImpl;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MAJIVIAL
 */
public class ProbandoCruceRecaudacionsTrabajos {

    private static final Logger LOG = Logger.getLogger(ProbandoCruceRecaudacionsTrabajos.class.getName());
    
    
    public static void main(String[] args) {
        ProbandoCruceRecaudacionsTrabajos calculadora=new ProbandoCruceRecaudacionsTrabajos();
                    Handler consoleHandler = new ConsoleHandler();
                 consoleHandler.setLevel(Level.WARNING);
                    LOG.addHandler(consoleHandler);
             
     
                    
    }
    ProbandoCruceRecaudacionsTrabajos(){
        
        
        ArrayList<Apunte> diario=new ArrayList();
        
        TrabajoRealizadoAtlas tr0=new TrabajoRealizadoAtlas();
             tr0.setCodRoac("trabajo tr0");
        tr0.setFechaInforme("01-01-2016");
        tr0.setFacturacionAuditoriaHonorarios(400);
        

        
        TrabajoRealizadoAtlas tr1=new TrabajoRealizadoAtlas();
        tr1.setCodRoac("trabajo tr1");
        tr1.setFechaInforme("01-01-2017");
        tr1.setFacturacionAuditoriaHonorarios(100);

        
        TrabajoRealizadoAtlas tr2=new TrabajoRealizadoAtlas();
            tr2.setCodRoac("trabajo tr2");
        tr2.setFechaInforme("01-01-2017");
        tr2.setFacturacionAuditoriaHonorarios(100);
       
        //DURANTE LA TARIFA A APLICAR
               
        TrabajoRealizadoAtlas tr3=new TrabajoRealizadoAtlas();
            tr3.setCodRoac("trabajo tr3");
        tr3.setFechaInforme("01-01-2018");
        tr3.setFacturacionAuditoriaHonorarios(100);

              
        TrabajoRealizadoAtlas tr4=new TrabajoRealizadoAtlas();
            tr4.setCodRoac("trabajo tr4");
        tr4.setFechaInforme("01-06-2018");
        tr4.setFacturacionAuditoriaHonorarios(100);

              
        TrabajoRealizadoAtlas tr5=new TrabajoRealizadoAtlas();
            tr5.setCodRoac("trabajo tr5");
        tr5.setFechaInforme("31-12-2018");
        tr5.setFacturacionAuditoriaHonorarios(100);

        
        
        

      
        
        
        //DESPUES DEL PERIODO DE LA TARIFA A APLICAR
        TrabajoRealizadoAtlas tr6=new TrabajoRealizadoAtlas();
            tr6.setCodRoac("trabajo tr6");
        tr6.setFechaInforme("01-01-2018");
        tr6.setFacturacionAuditoriaHonorarios(100);
               
        TrabajoRealizadoAtlas tr7=new TrabajoRealizadoAtlas();
            tr7.setCodRoac("trabajo tr7");
        tr7.setFechaInforme("01-01-2018");
        tr7.setFacturacionAuditoriaHonorarios(100);
                
        TrabajoRealizadoAtlas tr8=new TrabajoRealizadoAtlas();
            tr8.setCodRoac("trabajo tr8");
        tr8.setFechaInforme("01-01-2018");
        tr8.setFacturacionAuditoriaHonorarios(240);
        
        
       


        

        
//        Recaudacion cobro1=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro2=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro3=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro4=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro5=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro6=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro7=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//       Recaudacion cobro8=new Recaudacion("tasa","justif","sc1","sc2","2019","3","sc5", "sc6", "a-48021729","jimenez vitoria alejandro", new Date(), 100, "2038", "1920");
//     
//  //      diario.add(tr0);
//        diario.add(cobro1);
//        diario.add(cobro2);
//
//  //      diario.add(tr1);
//  //      diario.add(tr2);
//        diario.add(cobro3);
//
//   //     diario.add(tr3);
//   //     diario.add(tr4);
//        diario.add(cobro4);
//        diario.add(cobro5);
//        diario.add(cobro6);
//
//    //    diario.add(tr5);
//
//        diario.add(cobro7);
//        diario.add(cobro8);

       

        



       
               
        double saldo=0;
        double saldoH=0;
        double saldoD=0;
        
        for (Apunte tmp : diario) {
            System.out.print("\n" + tmp.getFecha() + "\t" + tmp.getDenominacion());
            if (tmp.esDebe()) {
                System.out.print("\t" + tmp.getImporteString());
                saldoD +=tmp.getImporte();
                saldo +=tmp.getImporte();
            } else {
                System.out.print("\t\t\t" + tmp.getImporteString());
                saldoH += tmp.getImporte();
                saldo -=tmp.getImporte();
            }
        }
        System.out.println("\n*************TOTALES**************\t"+saldoD+"\t"+saldoH);
        System.out.println("\n**************SALDO***************\t"+saldo);
    }
}
