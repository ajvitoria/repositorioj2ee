/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import es.icac.meh.pandora.services.daos.GestorApuntesServiceDAO;
import es.icac.meh.pandora.services.daos.MovimientosDTO;
import es.icac.meh.pandora.services.daos.RecaudacionesDAO;
import es.icac.meh.pandora.services.daos.TrabajosRealizadosDAO;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.catalina.connector.Response;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class GenerarCruces extends HttpServlet {

     MovimientosDTO movimientosDTO;

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.

        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());

        movimientosDTO= webApplicationContext.getBean("movimientosDTO", MovimientosDTO.class);
  

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

                        String paramEjercicio = request.getParameter("ejercicio");
        if (paramEjercicio!=null){
            if(paramEjercicio.trim().equals(""))
                response.sendError(Response.SC_PRECONDITION_FAILED,"Debe enviar EJERCICIO");
            else{
               generarResultado(paramEjercicio, response);
            }
        }else{
            response.sendError(Response.SC_PRECONDITION_FAILED,"Debe enviar EJERCICIO");
        }
    }
    
    private void generarResultado(String ejercicio,HttpServletResponse response )throws ServletException, IOException{
        //solicitar ano / regeneracion
               //saber cuantos cobros hay
      
        int numRecaudaciones=movimientosDTO.getRecaudacionesDAO().getNumeroRecaudaciones(ejercicio);
          //saber cuandos informes hay
        int numTrabajos=movimientosDTO.getTrabajosDAO().getNumeroTrabajos(ejercicio);
        
        int registros=numRecaudaciones+numTrabajos;
        movimientosDTO.reseteaApuntes();
        movimientosDTO.borrarApuntesEjercicio(ejercicio);
 int pagosInsertados=movimientosDTO.insertarRecaudaciones(ejercicio);
   movimientosDTO.reseteaApuntes();
 int trabajosInsertaos=movimientosDTO.insertarTrabajos(ejercicio);
       movimientosDTO.reseteaApuntes();
        //cuaantificar el numero de registros
        //borrar apuntes pr
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GenerarCruces</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Resumen resultados Ejercicio: " + ejercicio + "</h1>");
            out.println("<fieldset>");
            out.println("<legend>Datos cruzados</legend>");
                  out.println("<h2>Recaudaciones: " + pagosInsertados + "</h2>");
            out.println("<h2>Intormes:" + trabajosInsertaos + "</h2>");
//            out.println("<h2>Recaudaciones: " + numRecaudaciones + "</h2>");
//            out.println("<h2>Intormes:" + numTrabajos + "</h2>");
            out.println("</fieldset>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
