package controller;

import es.icac.meh.pandora.modelo.Apunte;
import es.icac.meh.pandora.modelo.ApunteImpl;
import es.icac.meh.pandora.modelo.Recaudacion;
import es.icac.meh.pandora.services.daos.GestorApuntesServiceDAO;
import es.icac.meh.pandora.services.daos.RecaudacionesDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import static java.time.temporal.ChronoUnit.MONTHS;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author MAJIVIAL
 */
public class ListadoFueraPlazo extends HttpServlet {

    WebApplicationContext applicationContext;
    RecaudacionesDAO servicio;
    ArrayList<Recaudacion> listaApuntes;
    private static final Logger LOG = Logger.getLogger(ListadoFueraPlazo.class.getName());
    private SimpleDateFormat SDF=new SimpleDateFormat("dd/MM/yyyy");
        DecimalFormat formatea = new DecimalFormat("###,###.##");
        
        
    String CSS="<style>"
            +".loader {\n" +
"    position: fixed;\n" +
"    left: 0px;\n" +
"    top: 0px;\n" +
"    width: 100%;\n" +
"    height: 100%;\n" +
"    z-index: 9999;\n" +
"    background: url('/ProyectoPANDORA/images/loading.gif') 50% 50% no-repeat rgb(249,249,249);\n" +
"    opacity: .2;\n" +
"}"+"</style>";
    String SCRIPT="<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>";
    
    String SCRIPT_CARGADOR="<script type=\"text/javascript\">\n" +
"$(window).load(function() {\n" +
"    $(\".loader\").fadeOut(\"slow\");\n" +
"});\n" +
"</script>";
    
    String DIV="<div class=\"loader\"></div>";
    
    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        applicationContext = ContextLoader.getCurrentWebApplicationContext();
      //  servicio = applicationContext.getBean("servicioApuntesDAO", GestorApuntesServiceDAO.class);
        servicio = applicationContext.getBean("recaudacionesDAO", RecaudacionesDAO.class);

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String paramEjercicio = request.getParameter("ejercicio");
        try{
            if(paramEjercicio ==null ||paramEjercicio.equals(""))
                paramEjercicio="2021";
            }catch(Exception e){
                paramEjercicio="2021";  
        }
        
     
        processResponse(response, paramEjercicio);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected void processResponse(HttpServletResponse response, String paramAno)
            throws ServletException, IOException {





        try {
            listaApuntes = servicio.obtenerRecaudacionesByEjercicio(paramAno);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "ERROR EN EL DAO DE APUNTES", ex);
        }

        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1 
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
        response.setDateHeader("Expires", 0); //prevents caching at the proxy server  

       
        String formulario = "        <form name=\"filtroRoac\" action=\"/ProyectoPANDORA//ListadoFueraPlazo\" method=\"GET\">\n"
                + "            \n"
                + "            <INPUT TYPE=\"TEXT\" NAME=\"ejercicio\" size=\"4\" value='"+paramAno+"'/>\n"
                //        + "            <INPUT TYPE=\"TEXT\" NAME=\"roac\" size=\"10\" value='"+paramRoac+"'/>\n"
                + "            <input type=\"submit\" value=\"Consultar Ejercicio\"/>\n"
                + "        </form>";
        response.setBufferSize(4096/21);
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("   <title>Fuera de Plazo</title>");
            out.println("   <link rel=\"stylesheet\" type=\"text/css\" href=\"/ProyectoPANDORA/css/zebra.css\">");
            out.println(CSS);
            out.println(SCRIPT);
            out.println("</head>");
            out.println("<body>");

            out.println("<h1>LISTADO de RECAUDACIONES FUERA DE PLAZO:  "+"</h1>");
           
            
            out.println("<h4>Año:" + paramAno + "+ ROAC: " + "</H4>");
            out.println("<FIELDSET><LEGEND>Filtrado:</LEGEND>");
            out.println(formulario);
            out.println("</FIELDSET><HR/>");
             out.println(DIV);
            out.println("<table border='1' class='blueTable'>");

            out.println("<tr>");
            out.println("<th width='3%'></th>");
            out.println("<th width='10%'>Archivo</th>");
            out.println("<th width='10%'>Justificante</th>");
            out.println("<th width='4%'>Año</th>");
            out.println("<th width='6%' style='font-size: 13px'>Trimestre</th>");
           
            out.println("<th width='6%'>Roac</th>");
            out.println("<th width='6%'>NIF</th>");
            out.println("<th width='25%'>Nombre Auditor</th>");
            out.println("<th width='6%'>Importe</th>");
            out.println("<th width='5%'>Fecha Ingreso</th>");
            out.println("<th width='5%'>Fecha Fin de Plazo</th>");
            out.println("<th width='5%'>"
                    + "Diferencia<br/>"
                    + "<div style='font-size: 11px'>"
                    + "<b><SUB>+ Retraso<br/>"
                    + "- Anticipado</b></SUB>"
                    + "</div>"
                    + "</th>");
            out.println("</tr>");

            TreeSet listaRoacs = new TreeSet();
            for (Recaudacion tmp : listaApuntes) {
                listaRoacs.add(tmp.getRoac());
            }
            int ejercicio = 0, trimestre = 0;
            Calendar cal = Calendar.getInstance();
         
            String nombreAuditor = null;
            String nifAuditor = null;
            String iRoac = null;
            String fechaIngreso = null;
            String fechaFinalPlazo = null;
            double importeAcumulado = 0;
            double importe=0;
            long retraso = 0;
            String fichero=null;
            int contador=0;
            
            for (Recaudacion tmp : listaApuntes) {
                                
                if(tmp.getTrimestre()==0 || tmp.esDebe()) continue;
                
                contador++;
                
                if(contador==100){
           
                    LOG.log(Level.SEVERE, "*******************aumento el buffer*****************");
                    //   response.setBufferSize(16384);//8192);
                    out.flush();
                    
                }
                nombreAuditor = tmp.getNombreAuditor().trim();
                nifAuditor = tmp.getNifAuditor();
                iRoac = tmp.getRoac();
               
              
                trimestre = tmp.getTrimestre();
                fechaIngreso=SDF.format(tmp.getFechaIngreso());
                fechaFinalPlazo=SDF.format(tmp.getFechaPlazo());
                importe = tmp.getImporte();          
                ejercicio=tmp.getEjercicio();
                
                importeAcumulado += importe;
                
                retraso = obtenerRetraso(tmp.getFechaIngreso(), tmp.getFechaPlazo());
                fichero=tmp.getArchivoFuente();
                
                out.println("<tr>");
                out.println("<td>" + contador + "</td>");
                out.println("<td>" + tmp.getArchivoFuente() + "</td>");
                out.println("<td>" + tmp.getNumeroJustificante() + "</td>");
                out.println("<td>" + getEjercicioObjetoRecaudacion(tmp) + "</td>");
                out.println("<td>" + trimestre + "</td>");             
                out.println("<td>" + iRoac + "</td>");
                out.println("<td>" + nifAuditor + "</td>");
                out.println("<td>" + nombreAuditor + "</td>");
                out.println("<td>" + formatea.format(importe) + "</td>");
                out.println("<td>" + fechaIngreso + "</td>");
                out.println("<td>" + fechaFinalPlazo + "</td>");
           //     out.println("<td>" + (retraso ==0?"":(retraso)*(-1)) + "</td>");
                out.println("<td>" + obtenerRetraso2(tmp.getFechaIngreso(), tmp.getFechaPlazo()) + "</td>");
                  
                out.println("</tr>");

            }
            /*
            out.println("<tr><td colspan='3'></td>");

            out.println("<td><b>" + formatea.format(importeAcumulado) + "</b></td><td colspan='4'></td></tr>");
*/
            out.println("</table>");
             out.println(SCRIPT_CARGADOR);
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    
    private int getEjercicioObjetoRecaudacion(Recaudacion recaudacion) {

        int codigoAnyoEjercicio = recaudacion.getCodigoAnyoEjercicio();
        int anoPlazoPago = 0;

        int anyo = recaudacion.getAnyo();
        /*
        MARAVILLOSA SOLUCION A LA CHAPUZA DEL AÑO DEL FICHERO DE HACIENDA
        ESTO ES YA LA GRAN TRACA. 
        PREMIO AL MEJOR PROGRAMDOR DEL AÑO.
        ESPERO QUE ESTO NO SE VEA NUNCA   
         */
        if (anyo <= 2020) {
            anoPlazoPago = 2010 + codigoAnyoEjercicio;
        } else {
            anoPlazoPago = 2020 + codigoAnyoEjercicio;
        }
        return anoPlazoPago;
    }
    private String obtenerRetraso2(Date dFechaIngreso, Date dFechaFinalPlazo){
            Calendar fechaIngreso=Calendar.getInstance();
            fechaIngreso.setTime(dFechaIngreso);
                    
            Calendar fechaFinPlazo=Calendar.getInstance();
            fechaFinPlazo.setTime(dFechaFinalPlazo);
            
            
//        double resultado=0;
//        if(fechaIngreso.after(fechaFinPlazo)){
//         
//            int diaIngreso=fechaIngreso.get(Calendar.DATE);
//            int diaFinPlazo=fechaFinPlazo.get(Calendar.DATE);
//             
//                
//                
//            if(diaIngreso-diaFinPlazo>0){
//                double a=(fechaIngreso.get(Calendar.YEAR)-fechaFinPlazo.get(Calendar.YEAR))*12.0;
//                double b=fechaIngreso.get(Calendar.MONTH)-fechaFinPlazo.get(Calendar.MONTH);
//                double c=(fechaIngreso.get(Calendar.DATE)-fechaFinPlazo.get(Calendar.DATE))/30.0;
//                resultado=a+b+c;
//            }else{
//                double a=(fechaIngreso.get(Calendar.YEAR)-fechaFinPlazo.get(Calendar.YEAR))*12.0;
//                double b=fechaIngreso.get(Calendar.MONTH)-fechaFinPlazo.get(Calendar.MONTH)-1;
//                double c=((1+fechaIngreso.get(Calendar.DATE)-fechaFinPlazo.get(Calendar.DATE))/30.0);
//                resultado=a+b+c;
//            }
//            
//        }else{
//            System.out.println("es igual o antes. ESTA EN PLAZO");
//        }
//        
//         System.out.println("LA DIFERENCIA ES "+resultado);
         
         
              double aa=(fechaIngreso.get(Calendar.YEAR)-fechaFinPlazo.get(Calendar.YEAR))*12.0;
                double bb=fechaIngreso.get(Calendar.MONTH)-fechaFinPlazo.get(Calendar.MONTH);
                double cc=(fechaIngreso.get(Calendar.DATE)-fechaFinPlazo.get(Calendar.DATE))/30.0;
                double resultado2=aa+bb+cc;
                
            
                     DecimalFormat df = new DecimalFormat("#.00");
return formatea.format(resultado2);
       
    
        }
        private long obtenerRetraso(Date fechaIngreso, Date fechaFinalPlazo) {
        

     //       DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        try {
          //  LocalDate dateBefore = LocalDate.parse(fechaIngreso, formatter);
          
//   
//Instant instant = fechaIngreso.toInstant();
//ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
//LocalDate dateBefore = zdt.toLocalDate();
//         
//Instant instantFinal = fechaFinalPlazo.toInstant();
//ZonedDateTime zdtFinal = instantFinal.atZone(ZoneId.systemDefault());
//LocalDate dateAfter = zdtFinal.toLocalDate();


Instant instantBefore = Instant.ofEpochMilli(fechaIngreso.getTime());
LocalDateTime localDateTime = LocalDateTime.ofInstant(instantBefore, ZoneId.systemDefault()); 
LocalDate dateBefore = localDateTime.toLocalDate();

Instant instantAfter = Instant.ofEpochMilli(fechaFinalPlazo.getTime());
LocalDateTime localDateTimeAfter = LocalDateTime.ofInstant(instantAfter, ZoneId.systemDefault()); 
LocalDate dateAfter = localDateTimeAfter.toLocalDate();



        //    LocalDate.parse(fechaFinalPlazo, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        //    LocalDate dateAfter = LocalDate.parse(fechaFinalPlazo, formatter);
       //      LocalDate dateAfter = fechaFinalPlazo.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        if (dateBefore.getDayOfMonth() > 28) {
            dateBefore = dateBefore.minusDays(5);
        } else if (dateAfter.getDayOfMonth() > 28) {
            dateAfter = dateAfter.minusDays(5);
        }
        return ChronoUnit.MONTHS.between(dateBefore, dateAfter);
    
         //   return MONTHS.between(dateBefore, dateAfter);
        } catch (Exception e) {
            e.printStackTrace();
     //       LOG.log(Level.WARNING, "Error calculando el RETRASO para las fechas  {0} y {1}", new Object[]{fechaIngreso, fechaFinalPlazo});

        }
        return 0;
    }
        
        
//    private String buscarNombreByNif() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
 
}
