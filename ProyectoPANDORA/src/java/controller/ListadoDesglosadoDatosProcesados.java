
package controller;

import es.icac.meh.pandora.modelo.Apunte;
import es.icac.meh.pandora.modelo.ApunteImpl;
import es.icac.meh.pandora.modelo.RegistroROAC;
import es.icac.meh.pandora.services.daos.DocumentoRoacDAO;
import es.icac.meh.pandora.services.daos.GestorApuntesServiceDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

import java.util.Iterator;
import java.util.Map;

import java.util.TreeSet;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author MAJIVIAL
 */
public class ListadoDesglosadoDatosProcesados extends HttpServlet {

    WebApplicationContext applicationContext;
    GestorApuntesServiceDAO servicio;
    ArrayList<ApunteImpl> listaApuntes;
//    DocumentoRoacDAO servicioROAC;
    private static final Logger LOG = Logger.getLogger(ListadoDesglosadoDatosProcesados.class.getName());

    TreeSet listaRoacs;

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        applicationContext = ContextLoader.getCurrentWebApplicationContext();

        servicio = applicationContext.getBean("servicioApuntesDAO", GestorApuntesServiceDAO.class);
      //  servicioROAC = applicationContext.getBean("documentoRoacDAO", DocumentoRoacDAO.class);

        try {
            listaApuntes = servicio.obtenerApuntes("2019");
            listaRoacs = new TreeSet();

        } catch (SQLException ex) {

        }
        int contador = 0;
        String x = null;
   //     Map<String, RegistroROAC> mapaRoacs = servicioROAC.obtenerMapaRoacsByNif();

        for (ApunteImpl tmp : listaApuntes) {
            x = tmp.getRoac();
            if (x == null) {
                x = tmp.getNifAuditor();
                tmp.setRoac(x);
            }
            this.listaRoacs.add(x);

        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String paramEjercicio = request.getParameter("ejercicio");

        paramEjercicio = "2019";

        Calendar cal = Calendar.getInstance();
        int ayno = cal.get(Calendar.YEAR);
        int dia = cal.get(Calendar.DATE);
        int mes = cal.get(Calendar.MONTH) + 1;

        processResponse(response, paramEjercicio);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected void processResponse(HttpServletResponse response, String paramAno)
            throws ServletException, IOException {

        DecimalFormat formatea = new DecimalFormat("###,###.##");

        if (paramAno == null) {
            paramAno = "2019";
        }

        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1 
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
        response.setDateHeader("Expires", 0); //prevents caching at the proxy server  

        String formulario = "        <form name=\"filtroRoac\" action=\"/ProyectoPANDORA/TrabajosRealizadosServlet\" method=\"GET\">\n"
                + "            \n"
                //          + "            <INPUT TYPE=\"TEXT\" NAME=\"ano\" size=\"4\" value='"+paramAno+"'/>\n"
                //        + "            <INPUT TYPE=\"TEXT\" NAME=\"roac\" size=\"10\" value='"+paramRoac+"'/>\n"
                + "            <input type=\"submit\" value=\"Consultar ROAC\"/>\n"
                + "        </form>";
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("   <title>LISTADO DESGLOSADO</title>");
            out.println("   <link rel=\"stylesheet\" type=\"text/css\" href=\"/ProyectoPANDORA/css/zebra.css\">");
            out.println("</head>");
            out.println("<body>");

            out.println("<h1>Listado DESGLOSADO Datos Procesados:</h1>");
            out.println("<h4>Año:" + paramAno + "+ ROAC: " + "</H4>");
            out.println("<FIELDSET><LEGEND>Filtrado:</LEGEND>");
            out.println(formulario);
            out.println("</FIELDSET><HR/>");
            out.println("<table border='1' class='blueTable'>");

            out.println("<tr>");
            out.println("<th>CodRoac</th>");
            out.println("<th>Documento</th>");
            out.println("<th>Identificacion</th>");
            out.println("<th>Informes</th>");

            out.println("<th>Total<br/>Recaudado</th>");

            out.println("<th>Total<br/>a Pagar</th>");
            out.println("<th>Diferencia</th>");

            out.println("</tr>");

//             List listaSinDuplicados = listaApuntes.stream()
//      .map(item->item.getRoac())
//      .distinct()
//      .collect(Collectors.toList());
//             
            double importeRecaudado = 0, importeTasas = 0, diferencia = 0;

            int informes = 0;

            String nombreAuditor = null;
            String nifAuditor = null;

            for (Iterator it = listaRoacs.iterator(); it.hasNext();) {

                String iRoac = (String) it.next();
                informes = 0;
                importeRecaudado = 0;
                importeTasas = 0;

                nombreAuditor = null;
                nifAuditor = null;
                for (ApunteImpl tmp : listaApuntes) {
                    if (tmp.getRoac().equals(iRoac)) {
                        
                               out.println("<tr>");
                out.println("<td>" + iRoac + "</td>");
                out.println("<td>" + nifAuditor + "</td>");
                out.println("<td>" + nombreAuditor + "</td>");
                out.println("<td>" + informes + "</td>");

                out.println("<td>" + formatea.format(tmp.getImporte()) + "</td>");

                out.println("<td>" + formatea.format(importeTasas) + "</td>");
                out.println("<td>" + formatea.format(diferencia) + "</td>");

                out.println("</tr>");
                        if (nifAuditor != null) {
                            if (nifAuditor.equals("")) {
                                nifAuditor = tmp.getNifAuditor();
                            }
                        } else {
                            nifAuditor = tmp.getNifAuditor();
                        }

                        if (nombreAuditor == null) {
                            nombreAuditor = tmp.getNombreAuditor().trim();
                        }
                        if (nombreAuditor.equals("")) {
                            nombreAuditor = tmp.getNombreAuditor().trim();
                        }

                        if (tmp.esDebe()) {
                            importeTasas += tmp.getImporte();
                        } else {
                            importeRecaudado += tmp.getImporte();
                        }
                        informes++;
                    }
                    diferencia = importeTasas - importeRecaudado;
                out.println("<tr style='background-color: coral;'>");
                out.println("<td>" + iRoac + "</td>");
                out.println("<td>" + nifAuditor + "</td>");
                out.println("<td>" + nombreAuditor + "</td>");
                out.println("<td>" +  "</td>");

                out.println("<td>" + formatea.format(importeRecaudado) + "</td>");

                out.println("<td>" + formatea.format(importeTasas) + "</td>");
                out.println("<td>" + formatea.format(diferencia) + "</td>");

                out.println("</tr>");
                }

                out.println("<tr>");
                out.println("<td>" + iRoac + "</td>");
                out.println("<td>" + nifAuditor + "</td>");
                out.println("<td>" + nombreAuditor + "</td>");
                out.println("<td>" +  "</td>");

                out.println("<td>" + formatea.format(importeRecaudado) + "</td>");

                out.println("<td>" + formatea.format(importeTasas) + "</td>");
                out.println("<td>" + formatea.format(diferencia) + "</td>");

                out.println("</tr>");

            }
            out.println("<tr><td colspan='3'></td>");

            out.println("<td><b>" + formatea.format(23) + "</b></td><td colspan='4'></td></tr>");

            out.println("</table>");
            out.println("</body>");
            out.println("</html>");
        }
    }

}
