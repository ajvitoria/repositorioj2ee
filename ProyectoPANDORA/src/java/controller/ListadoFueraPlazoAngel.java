package controller;

import es.icac.meh.pandora.modelo.Apunte;
import es.icac.meh.pandora.modelo.ApunteImpl;
import es.icac.meh.pandora.services.daos.GestorApuntesServiceDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import static java.time.temporal.ChronoUnit.MONTHS;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author MAJIVIAL
 */

@WebServlet("/angel")
public class ListadoFueraPlazoAngel extends HttpServlet {

    WebApplicationContext applicationContext;
    GestorApuntesServiceDAO servicio;
    ArrayList<ApunteImpl> listaApuntes;
    private static final Logger LOG = Logger.getLogger(ListadoFueraPlazo.class.getName());
   SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        applicationContext = ContextLoader.getCurrentWebApplicationContext();

        servicio = applicationContext.getBean("servicioApuntesDAO", GestorApuntesServiceDAO.class);

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String paramEjercicio = request.getParameter("ejercicio");
        try{
            if(paramEjercicio ==null ||paramEjercicio.equals(""))
                paramEjercicio="2019";
            }catch(Exception e){
                paramEjercicio="2019";  
        }
        
     
        processResponse(response, paramEjercicio);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected void processResponse(HttpServletResponse response, String paramAno)
            throws ServletException, IOException {

        DecimalFormat formatea = new DecimalFormat("###,###.##");



        try {
            listaApuntes = servicio.obtenerApuntes(paramAno);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "ERROR EN EL DAO DE APUNTES", ex);
        }

        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1 
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
        response.setDateHeader("Expires", 0); //prevents caching at the proxy server  

        String formulario = "        <form name=\"filtroRoac\" action=\"/ProyectoPANDORA/TrabajosRealizadosServlet\" method=\"GET\">\n"
                + "            \n"
                //          + "            <INPUT TYPE=\"TEXT\" NAME=\"ano\" size=\"4\" value='"+paramAno+"'/>\n"
                //        + "            <INPUT TYPE=\"TEXT\" NAME=\"roac\" size=\"10\" value='"+paramRoac+"'/>\n"
                + "            <input type=\"submit\" value=\"Consultar ROAC\"/>\n"
                + "        </form>";
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("   <title>Fuera de Plazo</title>");
            out.println("   <link rel=\"stylesheet\" type=\"text/css\" href=\"/ProyectoPANDORA/css/zebra.css\">");
            out.println("</head>");
            out.println("<body>");

            out.println("<h1>LISTADO de RECAUDACIONES FUERA DE PLAZO:</h1>");
            out.println("<h4>Año:" + paramAno + "+ ROAC: " + "</H4>");
            out.println("<FIELDSET><LEGEND>Filtrado:</LEGEND>");
            out.println(formulario);
            out.println("</FIELDSET><HR/>");
            out.println("<table border='1' class='blueTable'>");

            out.println("<tr>");
            out.println("<th>Trimestre</th>");
            out.println("<th>Año</th>");
            out.println("<th>Roac</th>");
            out.println("<th>NIF</th>");

            out.println("<th>Nombre Auditor</th>");

            out.println("<th>Importe</th>");
            out.println("<th>Fecha Ingreso</th>");
            out.println("<th>Fecha Fin de Plazo</th>");
            out.println("<th>Retraso</th>");
            out.println("</tr>");

            TreeSet listaRoacs = new TreeSet();
            for (Apunte tmp : listaApuntes) {
                listaRoacs.add(tmp.getRoac());
            }
            int ejercicio = 0, trimestre = 0;

            String nombreAuditor = null;
            String nifAuditor = null;
            String iRoac = null;
            String fechaIngreso = null;
            String fechaFinalPlazo = null;
            double importe = 0;
            long retraso = 0;
            for (ApunteImpl tmp : listaApuntes) {
                
                
                if(tmp.getTrimestre()==0 || tmp.esDebe()) continue;
                
                nombreAuditor = tmp.getNombreAuditor().trim();
                nifAuditor = tmp.getNifAuditor();
                iRoac = tmp.getRoac();
               
                    fechaFinalPlazo = obtenerFechaFinalPlazo(tmp);
                    trimestre = tmp.getTrimestre();
                    importe += tmp.getImporte();
              
                    fechaIngreso = sdf.format(tmp.getFecha());
                    importe += tmp.getImporte();
                
                retraso = obtenerRetraso(fechaIngreso, fechaFinalPlazo);
                ejercicio = tmp.getEjercicio();
                out.println("<tr>");
                out.println("<td>" + trimestre + "</td>");
                out.println("<td>" + ejercicio + "</td>");
                out.println("<td>" + iRoac + "</td>");
                out.println("<td>" + nifAuditor + "</td>");
                out.println("<td>" + nombreAuditor + "</td>");
                 out.println("<td>" + tmp.getDenominacion() + "</td>");
                out.println("<td>" + formatea.format(importe) + "</td>");
                out.println("<td>" + fechaIngreso + "</td>");
                out.println("<td>" + fechaFinalPlazo + "</td>");
                out.println("<td>" + (retraso ==0?"":(retraso)*(-1)) + "</td>");
                out.println("</tr>");

            }
            out.println("<tr><td colspan='3'></td>");

            out.println("<td><b>" + formatea.format(23) + "</b></td><td colspan='4'></td></tr>");

            out.println("</table>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    private String obtenerFechaFinalPlazo(Apunte tmp) {
        int ejercicio = tmp.getEjercicio();
        String fecha = "00";
        String dia = "20/";
        int trimestre = tmp.getTrimestre();
      

        switch (trimestre) {
            case 1:
                fecha = dia + "04/" + ejercicio;
                break;
            case 2:
                fecha = dia + "07/" + ejercicio;
                break;
            case 3:
                fecha = dia + "10/" + ejercicio;
                break;
            case 4:
                fecha = dia + "01/" + (ejercicio + 1);
                break;
            default:
                LOG.log(Level.WARNING, "La recaudacion {0} posee trimestre {1}", new Object[]{tmp.getDenominacion(), trimestre});
        }
        return fecha;
    }

    private String buscarNombreByNif() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private long obtenerRetraso(String fechaIngreso, String fechaFinalPlazo) {
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        try {
            LocalDate dateBefore = LocalDate.parse(fechaIngreso, formatter);
        //    LocalDate.parse(fechaFinalPlazo, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            LocalDate dateAfter = LocalDate.parse(fechaFinalPlazo, formatter);

        if (dateBefore.getDayOfMonth() > 28) {
            dateBefore = dateBefore.minusDays(5);
        } else if (dateAfter.getDayOfMonth() > 28) {
            dateAfter = dateAfter.minusDays(5);
        }
        return ChronoUnit.MONTHS.between(dateBefore, dateAfter);
    
         //   return MONTHS.between(dateBefore, dateAfter);
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Error calculando el RETRASO para las fechas  {0} y {1}", new Object[]{fechaIngreso, fechaFinalPlazo});

        }
        return 0;
    }
}
