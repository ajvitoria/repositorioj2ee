/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import es.icac.meh.pandora.modelo.ApunteImpl;
import es.icac.meh.pandora.services.daos.DocumentoRoacDAO;
import es.icac.meh.pandora.services.daos.GestorApuntesServiceDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.catalina.connector.Response;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author MAJIVIAL
 */
public class ListadoResumenDatosProcesadosFiltroRoac extends HttpServlet {

    WebApplicationContext applicationContext;
    GestorApuntesServiceDAO servicio;
    ArrayList<ApunteImpl> listaApuntes;
    DocumentoRoacDAO servicioROAC;
    private static final Logger LOG = Logger.getLogger(ListadoResumenDatosProcesadosFiltroRoac.class.getName());
    DecimalFormat formatea = new DecimalFormat("###,###.#####");
    TreeSet listaRoacs;
    private boolean mostrarTotales=false;

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        applicationContext = ContextLoader.getCurrentWebApplicationContext();

        servicio = applicationContext.getBean("servicioApuntesDAO", GestorApuntesServiceDAO.class);
        servicioROAC = applicationContext.getBean("documentoRoacDAO", DocumentoRoacDAO.class);
    }
    
    private boolean procesarEjercio(String ejercicio){
        try {
            listaApuntes = servicio.obtenerApuntes(ejercicio);
           listaRoacs = new TreeSet();

        } catch (SQLException ex) {
              LOG.log(Level.SEVERE, "ERROR SQL", ex);
              return false;
        }
        int contador = 0;
        String x = null;
    //    Map<String, RegistroROAC> mapaRoacs = servicioROAC.obtenerMapaRoacsByNif();

        for (ApunteImpl tmp : listaApuntes) {
            x = tmp.getRoac();
            if (x == null) {
                x = tmp.getNifAuditor();
                tmp.setRoac(x);
            }
            this.listaRoacs.add(x);

        }
        return true;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        
        String paramEjercicio = request.getParameter("ejercicio");                
        try{
            if(paramEjercicio ==null ||paramEjercicio.equals(""))
                paramEjercicio="2019";
            }catch(Exception e){
                paramEjercicio="2019";  
        }
               
       String paramMostrarTotales = request.getParameter("totales");                
        try{
            if(paramMostrarTotales !=null)
                mostrarTotales=true;
            }catch(Exception e){
                 mostrarTotales=false;
        }
        
        if (paramEjercicio!=null){
            if(paramEjercicio.trim().equals(""))
                response.sendError(Response.SC_PRECONDITION_FAILED,"Debe enviar EJERCICIO");
            else{
                if(!procesarEjercio(paramEjercicio))
                    response.sendError(Response.SC_SERVICE_UNAVAILABLE,"Error de servidor");
                else
                   procesarRespuesta(paramEjercicio, response);
            }
        }else{
            response.sendError(Response.SC_PRECONDITION_FAILED,"Debe enviar EJERCICIO");
        }



    }


    @Override
    public String getServletInfo() {
        
        try {
            Thread.sleep(0);
        } catch (InterruptedException ex) {
            Logger.getLogger(ListadoResumenDatosProcesadosFiltroRoac.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Short description";
    }// </editor-fold>
    

String scriptFiltro="$(\"#buscaRoac\").keyup(function() {\n" +
"  _this = this;\n" +
"  // Show only matching TR, hide rest of them\n" +
"  $.each($(\"table tr\"), function() {\n" +
"    if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)\n" +
"      $(this).hide();\n" +
"    else\n" +
"      $(this).show();\n" +
"  });\n" +
"});";


    private void procesarRespuesta(String paramEjercicio,HttpServletResponse response) throws IOException{
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1 
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
        response.setDateHeader("Expires", 0); //prevents caching at the proxy server  
        String IMPORT_SCRIPT="<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>";
        String BUSQUEDA_SCRIPT="<script>\n $(\"#buscaRoac\").keyup(function() {\n" +
"  _this = this;\n" +
"  // Show only matching TR, hide rest of them\n" +
"  $.each($(\"table tr\"), function() {\n" +
"    if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)\n" +
"      $(this).hide();\n" +
"    else\n" +
"      $(this).show();\n" +
"  });\n" +
"});\n"
                +"</script>";
        
        String formulario = "        <form name=\"filtroRoac\" action=\"/ProyectoPANDORA//ListadoResumenDatosProcesados\" method=\"GET\">\n"
                + "            \n"
                + "            <INPUT TYPE=\"TEXT\" NAME=\"ejercicio\" size=\"4\" value='"+paramEjercicio+"'/>\n"
                + "            <INPUT TYPE=\"TEXT\" NAME=\"buscaRoac\" size=\"10\" value=''/>\n"
                + "            <input type=\"submit\" value=\"Consultar Ejercicio\"/>\n"
                + "        </form>";
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("   <title>Resumen Datos Procesados</title>");
            out.println("   <link rel=\"stylesheet\" type=\"text/css\" href=\"/ProyectoPANDORA/css/zebra.css\">");
            out.println(IMPORT_SCRIPT);
       
            
            out.println("</head>");
            out.println("<body>");
          out.println(BUSQUEDA_SCRIPT);
            out.println("<h1>Listado de Ingresos/Trabajos Realizados:</h1>");
            out.println("<h4>Año:" + paramEjercicio + "+ ROAC: " + "</H4>");
            out.println("<FIELDSET><LEGEND>Filtrado:</LEGEND>");
            out.println(formulario);
            out.println("</FIELDSET><HR/>");
            out.println("<table border='1' class='blueTable'>");

            out.println("<tr>");
            out.println("<th></th>");
            out.println("<th>CodRoac</th>");
            out.println("<th>Documento</th>");
            out.println("<th>Identificacion</th>");
            out.println("<th>Informes</th>");

            out.println("<th>Total Ingresado (AEAT)</th>");

            out.println("<th>Total Declarado");
            out.println("<span style='font-size: 13px'>Mod02&Mod03</span></th>");
            out.println("<th>Diferencia</th>");

            out.println("</tr>");

//             List listaSinDuplicados = listaApuntes.stream()
//      .map(item->item.getRoac())
//      .distinct()
//      .collect(Collectors.toList());
//             
            double importeRecaudado = 0, importeTasas = 0, diferencia = 0;

            int informes = 0;

            String nombreAuditor = null;
            String nifAuditor = null;

            double acumuladoRecaudaciones=0, acumuladoInformes=0, acumuladoDiferencia=0;
            
            int contador=0;
            
            for (Iterator it = listaRoacs.iterator(); it.hasNext();) {

                String iRoac = (String) it.next();
                informes = 0;
                importeRecaudado = 0;
                importeTasas = 0;

                nombreAuditor = null;
                nifAuditor = null;
                
                for (ApunteImpl tmp : listaApuntes) {
                    
                    if (tmp.getRoac().equals(iRoac)) {
                        if (nifAuditor != null) {
                            if (nifAuditor.equals("")) {
                                nifAuditor = tmp.getNifAuditor();
                            }
                        } else {
                            nifAuditor = tmp.getNifAuditor();
                        }

                        if (nombreAuditor == null) {
                            nombreAuditor = tmp.getNombreAuditor().trim();
                        }
                        if (nombreAuditor.equals("")) {
                            nombreAuditor = tmp.getNombreAuditor().trim();
                        }

                        if (tmp.esDebe()) {
                            importeTasas += tmp.getImporte();
                            informes++;
                        } else {
                            importeRecaudado += tmp.getImporte();
                        }
                        
                    }
                    //Angel Luis solicita que
                    diferencia = importeRecaudado-importeTasas ;
//                    if(diferencia==0d){
//                        diferencia=(int)diferencia;
//                    }

                }
                contador++;
                out.println("<tr>");
                out.println("<td>" + contador + "</td>");
                out.println("<td>" + iRoac + "</td>");
                out.println("<td>" + nifAuditor + "</td>");
                out.println("<td>" + nombreAuditor + "</td>");
                out.println("<td>" + informes + "</td>");

                out.println("<td>" + formatea.format(importeRecaudado) + "</td>");

                out.println("<td>" + formatea.format(importeTasas) + "</td>");
                
                out.println("<td>");
                
                String strDiferencia=formatea.format(diferencia);
                
                if(strDiferencia.equals("-0")){
                    out.println("o");//+ (diferencia!=0?formatea.format(diferencia):0) + "</td>");
                }else
                 out.println(strDiferencia);//+ (diferencia!=0?formatea.format(diferencia):0) + "</td>");
               
                out.println("</td>");
                out.println("</tr>");
                acumuladoRecaudaciones+=importeRecaudado;
                acumuladoInformes+=importeTasas;
                acumuladoDiferencia+=diferencia;
        
            }
            
            
            if(mostrarTotales){
                out.println("<tr><td colspan='4'></td>");


                out.println("<td><b>" + formatea.format(acumuladoRecaudaciones) + "</b></td>"
                        + "<td><b>" + formatea.format(acumuladoInformes) + "</b></td>"
                        + "<td><b>" + formatea.format(acumuladoDiferencia) + "</b></td>"
                        + "</tr>");
            }
            out.println("</table>");
            out.println("</body>");
            out.println("</html>");
        }
    }

}
