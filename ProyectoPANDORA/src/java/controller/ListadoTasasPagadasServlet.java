
package controller;

import es.icac.meh.pandora.modelo.Recaudacion;
import es.icac.meh.pandora.modelo.Lote;
import es.icac.meh.pandora.services.daos.RecaudacionesDAO;
import java.io.IOException;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author MAJIVIAL
 */
public class ListadoTasasPagadasServlet extends HttpServlet {


    
    	private String LISTA_ID_COBROS = "listaRecaudacions";
private RecaudacionesDAO dao;
    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
                ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("config/applicationContext.xml");
        System.out.println("=====LISTA DE BEANS CARGADOS=====");
        for (String tmp : applicationContext.getBeanDefinitionNames()) {
            System.out.println(" " + tmp);
        }

         dao = applicationContext.getBean("recaudacionesDAO", RecaudacionesDAO.class);

        
        
        
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
    response.setHeader("Pragma","no-cache"); //HTTP 1.0 
    response.setDateHeader ("Expires", 0); //prevents caching at the proxy server  
        
		List<Recaudacion> listaRecaudacions = null;
		try {
			listaRecaudacions = selectStudentInfo();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//System.out.println("======studentList :"+listaRecaudacions);
		String destination = "/WEB-INF/view/table-demo.jsp";
		request.setAttribute(LISTA_ID_COBROS, listaRecaudacions);

		RequestDispatcher rd = request.getRequestDispatcher(destination);
		rd.forward(request, response);

	}

	public List<Recaudacion> selectStudentInfo() throws SQLException {


       return dao.obtenerTodosRecaudaciones();



	}



    
    


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
