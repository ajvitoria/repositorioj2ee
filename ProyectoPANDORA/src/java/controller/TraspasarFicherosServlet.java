package controller;

import es.icac.meh.pandora.configuration.EnvironmentConfiguration;
import es.icac.meh.pandora.services.TratamientoFicherosDeRecaudacionesService;
import es.icac.meh.pandora.modelo.Lote;
import es.icac.meh.pandora.services.ImportarRecaudaciones;
import es.icac.meh.pandora.services.daos.LoteDAO;
import es.icac.meh.pandora.services.daos.RecaudacionesDAO;
import es.icac.meh.pandora.vista.Componentes;
import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author MAJIVIAL
 */
public class TraspasarFicherosServlet extends HttpServlet {

    private ArrayList<File> listaFicheros;
    WebApplicationContext webApplicationContext;
    private static final Logger LOG = Logger.getLogger(TraspasarFicherosServlet.class.getName());
    EnvironmentConfiguration environment;
    RecaudacionesDAO recaudacionesDAO;
    
    final String ESTILO = "<style>"
            + Componentes.estiloTablaCebra
            + Componentes.estiloBotonSubmit
            + Componentes.estiloBotonReset
            + " </style>";

    private String STYLE_BOTON_HOME = ".boton-personalizado {\n"
            + "text-decoration:none;\n"
            + "font-weight:600;\n"
            + "font-size:20px;\n"
            + "color:#ffffff;\n"
            + "padding-top:20px;\n"
            + "padding-bottom:20px;\n"
            + "padding-left:40px;\n"
            + "padding-right:40px;\n"
            + "background-color:#005BBB;\n"
            + "}";
    private String BOTON_HOME = "<A href='"
            + "/ProyectoPANDORA/index.html"
            + " '"
            + " style='"
            + "text-decoration:none;\n"
            + "font-weight:600;\n"
            + "font-size:20px;\n"
            + "color:#ffffff;\n"
            + "padding-top:15px;\n"
            + "padding-bottom:20px;\n"
            + "padding-left:40px;\n"
            + "padding-right:40px;\n"
            + "background-color:#005BBB;\n"
            + "}'"
            + ">"
            + " Ir a menú principal"
            + "</A>";
    private String BOTON_STAGE = "<A href='"
            + "/ProyectoPANDORA/listadoStageArea.jsp"
            + " '"
            + " style='"
            + "text-decoration:none;\n"
            + "font-weight:600;\n"
            + "font-size:20px;\n"
            + "color:#ffffff;\n"
            + "padding-top:15px;\n"
            + "padding-bottom:20px;\n"
            + "padding-left:40px;\n"
            + "padding-right:40px;\n"
            + "background-color:#005BBB;\n"
            + "}'"
            + ">"
            + " ir a casa"
            + "</A>";
    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
webApplicationContext=WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        System.out.println("***CONFIGURACION DEL SERVICIO DE FICHEROS*****creada lista de ficheros");

        environment = webApplicationContext.getBean("enviroment", EnvironmentConfiguration.class);
        recaudacionesDAO = webApplicationContext.getBean("recaudacionesDAO", RecaudacionesDAO.class);
    }


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        listaFicheros = new ArrayList();
        System.out.println("controller.TraspasarFicheros.processRequest()-->TRASPASO DE DATOS ");
        LOG.log(Level.INFO, "controller.TraspasarFicheros.processRequest()-->recibidos");
        String rutas[] = request.getParameterValues("respuesta[]");
        //String parametro2 = request.getParameter("respuesta[]");
        //System.out.println(parametro2);
        //String[] rutas = parametro2.split(",");
        
        if(rutas==null){
            nadaQueIncorporar(response);
        }else{
            if (rutas.length==0)
                nadaQueIncorporar(response);
        }
        LOG.log(Level.INFO, "rutas en el servlet!{0}", rutas.length);


        for (String url : rutas) {
            String tmp = environment.getStageDirectory() + File.pathSeparator + url;
            LOG.log(Level.INFO, "\t" + url);

        }
//        TratamientoFicherosDeRecaudacionesService.reiniciarProcesamiento();
        ArrayList<Lote> lotesProcesados = webApplicationContext.getBean("tratamientoRecaudacions", TratamientoFicherosDeRecaudacionesService.class).procesarLoteFicheros(rutas);
        LoteDAO dao = (LoteDAO) webApplicationContext.getBean("loteDAO");
        long startTime, finTime,totalSum=0;
        int registros=0;
        int totalRegistros=0;
        
        for (Lote lote : lotesProcesados) {   
            startTime = System.currentTimeMillis();
            File ficheroProcesable=lote.getFichero();
            registros=contarLineas(ficheroProcesable)-2;
            totalRegistros+=registros;
            
            LOG.log(Level.INFO, "Insertado {0} cobros", registros);
            
            dao.insertarLote(lote);    
            LOG.log(Level.INFO, "Insertado en BD {0}", ficheroProcesable.getName());
            
            finTime= (System.currentTimeMillis()-startTime);
            LOG.log(Level.INFO, "Lote procesado en Insertado en "+(finTime/1000)+ " segs");
            double promedio=(double)finTime/registros;
            LOG.log(Level.INFO, "Promedio "+promedio+ " registros/segundo"); 
            totalSum+=finTime;            
        }
        double promedioTotal=totalRegistros/totalSum;
            LOG.log(Level.INFO, "Promedio "+promedioTotal+ " registros/segundo"); 
        System.out.println("********FIN DEL TRASPASO MOSTRAR RESULTADOS FINALES ********");
//this.moverLoteFicherosStage2Procesados(rutas);

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Ficheros importados=  " + request.getContextPath() + " </title>");

            out.println(ESTILO);

            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Ficheros importados del lote de Gestión Recaudatoria" + "</h1>");

            out.println("<br/><br/>");

            out.println("<h1>lotes importados del lote " + "</h1>");
            out.println("<table border='2'>");
            int contador=0;
            SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
            for (Lote tmp : lotesProcesados) {
                out.println("<tr>");
                out.println("<td><b>" + (contador++)+ "</b></td>");
                out.println("<td>" + tmp.getFichero().getName()+ "</td>");
                out.println("<td>" + tmp.getFichero().getParent()+ "</td>");
                String fecha=sdf.format(new Date(tmp.getFichero().lastModified()));
                out.println("<td>" + fecha +"</td>");
                out.println("<td>" + tmp.getDescripcion() + "</td>");
  
                out.println("<td>" + tmp.getFechaProcesadoString() + "</td>");
                out.println("<td>" + tmp.getListaRecaudacions().size() + "</td>");
                out.println("<td>" + tmp.getImportePorLote() / 100 + "</td>");
                
                out.println("<td>" + tmp.getCifraAcumulada()+ "</td>");
                
                out.println("<td>");

                if (tmp.isValido()) {
                    out.println("<img src='" + request.getContextPath() + File.separator + "images/ok.png' witdh='50px' height='25px'/>");
                    moveFileSimple(tmp.getFichero());
                        //HABRIA QUE HACER ROLLBACK DE LOS DATOS INSERTADOS??????????
                    
                      
                } else {
                    out.println("<img src='" + request.getContextPath() + File.separator + "images/kao.png' witdh='50px' height='25px'"
                            + " title='" + tmp.isValido()+ "'" + "/>");
                    
                       recaudacionesDAO.borrarRecaudacionByArchivo(tmp.getFichero().getName());
                }
                out.println("</td>");
                
                
                out.println("</tr>");
                

            }
            out.println("</table>");
            out.println("<br/><br/>");

            out.println(BOTON_HOME);
            out.println("</body>");
            out.println("</html>");
        }

        LOG.log(Level.INFO, "controller.TraspasarFicheros.processRequest()--> fin de tratamiento");

    }

    private int contarLineas(File fichero) {
        int contadorL = 0;
        try {
            BufferedReader fich = new BufferedReader(new FileReader(fichero));            //Usamos la clase BufferReadeader para tener acceso a un metodo propio (readLine()) y asi mediante un contador contar las lineas.

            String linea;
            //En este caso la condicion final del while corresponde a null, para indicar el final de linea
            while ((linea = fich.readLine()) != null) {
                contadorL++;
            }
            System.out.println("El número de líneas :" + contadorL);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return contadorL;

    }
    private Map<String, String> tratarParametro(final String parametro2) {
        //        final String parametro2 = "{\"name\":\"Falco\",\"age\":3,\"bitable\":false}";
        JsonParser parser = Json.createParser(new StringReader(parametro2));
         LOG.log(Level.INFO,"=================== T R A T A M I E N T O ==============================");
        Map<String, String> mapa = new HashMap<String, String>();
        String key = null;
        String value = null;
        while (parser.hasNext()) {
            final JsonParser.Event event = parser.next();
            switch (event) {
                case KEY_NAME:
                    key = parser.getString();
                  //  System.out.println(key);
                    break;
                case VALUE_STRING:
                    value = parser.getString();
                  //  System.out.println(value);
                    break;
            }

            if (key != null) {
                if (key.equals("ruta")) {
                    mapa.put(value, key);
                }
            }

        }
        parser.close();

        return mapa;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Este servlet permite traspasar ficheros desde el stage area al Sistema de Tratamiento de Pandora";
    }// </editor-fold>

    private void tratarMapa(Map<String, String> mapa) {
        Iterator it = mapa.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            if (e.getKey() == null || e.getValue() == null) {
                continue;
            }
            String clave = e.getKey().toString();
            String valor = e.getValue().toString();
          //   LOG.log(Level.INFO,"tratando fichero " + clave + " valor:" + valor);
            listaFicheros.add(new File(clave));
        }
    

        for (File tmp : listaFicheros) {
            try {
               ImportarRecaudaciones.tratarFicheroRecaudacion(tmp);
            } catch (Exception ex) {
                LOG.log(Level.WARNING, "El fichero "+tmp+" ya ha sido tratado previamente", ex);
            }
            //cuando este operativo.
           // moveFileSimple(tmp);
        }
        ImportarRecaudaciones.listarFicherosTratados();
    }

    private boolean moveFileSimple(File fromFile) {
        
        String destiny = environment.getTreatmentDirectory();
                
        String toFile = fromFile.getName();
        File destinyFile = new File(destiny, toFile);
        
        if(destinyFile.exists()){
            LOG.log(Level.WARNING, "Ya estaba procesado el fichero " + destinyFile);
            destinyFile.delete();
        }
        LOG.log(Level.INFO, "moviendo desde ... " + fromFile + "hasta... " + destinyFile);
        return fromFile.renameTo(destinyFile);
    }

    private boolean moverLoteFicherosStage2Procesados(String[] rutas) {
        boolean operacionRealizada = true;

        for (String tmp : rutas) {
            if (!moveFileSimple(new File(tmp))) {
                operacionRealizada = false;
                LOG.log(Level.SEVERE, "Error moviendo al fichero "
                        + tmp
                        + " al directorio "
                        + webApplicationContext.getBean("enviroment", EnvironmentConfiguration.class).getTreatmentDirectory()
                        + "desde el directorio de stage "
                        + webApplicationContext.getBean("enviroment", EnvironmentConfiguration.class).getStageDirectory()
                );
            }
        }

        return operacionRealizada;
    }
    private void nadaQueIncorporar(HttpServletResponse response) throws IOException{
    try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Ficheros importados </title>");

            out.println(ESTILO);

            out.println("</head>");
            out.println("<body>");
            out.println("<h1>NO HAY Ficheros PARA IMPORTAR !" + "</h1>");

            out.println("<br/><br/>");



            out.println(BOTON_HOME);
            out.println("</body>");
            out.println("</html>");
        }

        LOG.log(Level.INFO, "controller.TraspasarFicheros.processRequest()--> fin de tratamiento");

    }


}
