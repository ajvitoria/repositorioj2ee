/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author MAJIVIAL
 */
public class TestSqlConnection extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(TestSqlConnection.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        LOG.log(Level.INFO, "Inicio servlet!");
        response.setContentType("text/html;charset=UTF-8");
        String url = request.getParameter("url");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String instance = request.getParameter("instance");
        String nombreBD = request.getParameter("nombreBD");
        String driver = request.getParameter("driver");
        String ipServer = request.getParameter("ipServer");

        try {
            PrintWriter out = response.getWriter();
            LOG.log(Level.INFO, "crea escritor servlet!");
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TestSqlConnection</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet TestSqlConnection at " + request.getContextPath() + "</h1>");

            out.printf("<h5> url : %s</h5>", url);
            out.printf("<h5> username : %s</h5>", username);
            out.printf("<h5> password : %s</h5>", password);
            out.printf("<h5> instance : %s</h5>", instance);
            out.printf("<h5> nombreBD : %s</h5>", nombreBD);
            out.printf("<h5> driver : %s</h5>", driver);
            out.printf("<h5> ipServer : %s</h5>", ipServer);

            Connection conn = null;

            try {
                Class.forName(driver);
            } catch (ClassNotFoundException ex) {
                System.err.println("NO TIENE REGISTRADO EL DRIVER");
                out.printf("<h1>PROBLEMA AL REGISTRAR EL DRIVER : %s</h1>", ex.getMessage());
                LOG.log(Level.SEVERE, "PROBLEMA AL REGISTRAR EL DRIVER ", ex);
            }

            try {
                if (url != null && !url.equals("")) {
                    String dbURL = url;
           
                      out.println("<h3>URL FINAL="+dbURL+"</h3>");
                    conn = DriverManager.getConnection(dbURL, username, password);
                } else {
                         out.println("<h1>URL DE CONEXION VACIA</h1>");
                }

                if (conn != null) {
                    DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();

                    out.println("<h2>Conexion establecida con  configuración del formulario!</h2>");
                    out.printf("<h5>Driver: %s</h5>", dm.getDriverName());
                    out.printf("<h5>Driver version: %s</h5>", dm.getDriverVersion());
                    out.printf("<h5>Product nombre: %s</h5>", dm.getDatabaseProductName());
                    out.printf("<h5>Product version: %s</h5>", dm.getDatabaseProductVersion());

                    System.out.println("Driver : " + dm.getDriverName());
                    System.out.println("Driver version: " + dm.getDriverVersion());
                    System.out.println("Product nombre: " + dm.getDatabaseProductName());
                    System.out.println("Product version: " + dm.getDatabaseProductVersion());
                }else{
                  out.println("<h1>NO SE HA CREADO LA CONEXION</h1>");
                }

            } catch (SQLException ex) {
                out.printf("<h1>SQLException : %s</h1>", ex.getMessage());
                ex.printStackTrace();
            } finally {
                try {
                    if (conn != null && !conn.isClosed()) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                    out.printf("<h1>SQLException cerrando conexion: %s</h1>", ex.getMessage());
                    ex.printStackTrace();
                }
            }

            out.println("</body>");
            out.println("</html>");

        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "FALLO EN INFRAESTRUCTURA TOMCAT", ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
