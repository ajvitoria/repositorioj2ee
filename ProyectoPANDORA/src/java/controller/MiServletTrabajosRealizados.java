package controller;


import es.icac.meh.pandora.modelo.Apunte;
import es.icac.meh.pandora.modelo.TrabajoRealizado;

import es.icac.meh.pandora.services.daos.TrabajosRealizadosDAO;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;


import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class MiServletTrabajosRealizados extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(MiServletTrabajosRealizados.class.getName());
    private ArrayList<TrabajoRealizado> lista;
    TrabajosRealizadosDAO servicio;
    
    @Override
    public void init() throws ServletException {
        super.init(); 
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());   
        servicio = webApplicationContext.getBean("trabajosDAO", TrabajosRealizadosDAO.class); 
    }

    private ArrayList<TrabajoRealizado> obtenerTrabajos(String codRoac, String codEjc){
        ArrayList<TrabajoRealizado >retorno=new ArrayList<>();
                
        try {
            //retorno = dao.obtenerTrabajosPorRoacEjerc(codRoac,codEjc);
            retorno=servicio.obtenerTrabajosEjercicio(codEjc);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Obteniendo trabajos ", ex);
        }
    
        return retorno;
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String ejercicio="2019";
        lista=obtenerTrabajos("",ejercicio);
        response.setContentType("text/html;charset=UTF-8");
        //response.setContentType("application/vnd.ms-excel");
        //response.setHeader("Content-Disposition", "attachment; filename=trabajosrealizados-"+ejercicio+".xls");

        try (PrintWriter out = response.getWriter()) {

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MiServlet</title>");            
            out.println("</head>");
            
            out.println("<body>");
            out.println("<h1>Trabajos Realizados</h1>");
            out.println("<table border='1'>");            
            for (Apunte tmp : lista) {
                out.println("<tr>");
                out.println("<td>" + tmp.getRoac()          + "</td>");
                out.println("<td>" + tmp.getNifAuditor()    + "</td>");
                out.println("<td>" + tmp.getCampo1()        + "</td>");
                out.println("<td>" + tmp.getDenominacion()  + "</td>");
                out.println("<td>" + tmp.getImporte()       + "</td>");
                out.println("<td>" + tmp.getEjercicio()     + "</td>");
                out.println("<td>" + tmp.getTrimestre()     + "</td>"); 
                out.println("<td>" + tmp.getFecha()         + "</td>");       
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

 
 
 
final String sqlPreparadakk="SELECT DISTINCT isnull(d.COD_ROAC, '') AS COD_ROAC,\n" +
"            CASE WHEN aud.Nombre is null or aud.Nombre = ''\n" +
"            THEN aud.Razon_Social\n" +
"            ELSE aud.Apellidos + ', ' + aud.Nombre\n" +
"            END as Identificacion,\n" +
"            isnull(i.Documento, '') AS Documento,\n" +
"             isnull(REPLACE(i.RazonSocial,'\"',''), '') AS RazonSocial,\n" +
"            [dbo].[FormatearInformeCuentas](InformeDeCuentas) AS InformeDeCuentas,\n" +
"            dbo.[FormatearFecha](PrimerEjercicioAuditado) AS PrimerEjercicioAuditado,\n" +
"            dbo.[FormatearFecha](EjercicioAuditado) AS EjercicioAuditado,\n" +
"            dbo.[FormatearCharSiNo](Constitucion) AS Constitucion,\n" +
"            dbo.[FormatearFecha](EjercicioFinal) AS EjercicioFinal,\n" +
"            isnull(i.IdEntidad, '') AS IdEntidad,\n" +
"            isnull(e.Desc_Entidad, '') AS DescripcionEntidad,\n" +
"            dbo.[FormatearIntSiNo](ParticipesAuditado) AS ParticipesAuditado,\n" +
"            dbo.[FormatearIntSiNo](ParticipesAnterior) AS ParticipesAnterior,\n" +
"            ImporteNetoAuditado AS ImporteNetoAuditado,\n" +
"            ImporteNetoAnterior AS ImporteNetoAnterior,\n" +
"            isnull(PlantillaMediaAuditado, 0) AS PlantillaMediaAuditado,\n" +
"            isnull(PlantillaMediaAnterior, 0) AS PlantillaMediaAnterior,\n" +
"            isnull(i.IdTipoTrabajo, '') AS IdTipoTrabajo,\n" +
"            isnull(t.Desc_Trabajo, '') AS DescripcionTipoTrabajo,\n" +
"            dbo.[FormatearFecha](FechaInforme) AS FechaInforme,\n" +
"            isnull(ROACAuditorFirmante, '') AS ROACAuditorFirmante,\n" +
"            isnull(NombreAuditorFirmante, '') AS NombreAuditorFirmante,\n" +
"            dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe) AS Coauditoria,\n" +
"            MC.ROACCoAuditor AS ROACCoauditor,\n" +
"            isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion,\n" +
"            isnull(o.Desc_Opinion, 0) AS DescripcionTipoOpinion,\n" +
"            isnull(i.IdProvincia, 0) AS IdProvincia,\n" +
"            isnull(p.DescProvincia, '') AS Provincia,            \n" +
"            FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios,\n" +
"            FacturacionAuditoriaHoras AS FacturacionAuditoriaHoras,\n" +
"            EM.Desc_Corta AS CNMV,\n" +
"            dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0)) AS InteresPublico,\n" +
"            ImporteCifraActivo AS ImporteCifraActivo,\n" +
"            ImporteCifraActivoAnterior AS ImporteCifraActivoAnterior,\n" +
"            i.HonorariosAuditoriaInterna,\n" +
"            i.HorasAuditoriaInterna,\n" +
"            i.HonorariosDiseno,\n" +
"            i.HorasDiseno,\n" +
"            i.HonorariosOtros,\n" +
"            i.HorasOtros\n" +
"            FROM [dbo].[T_Modelo_Informe] i\n" +
"            INNER JOIN [dbo].[T_Modelo_Datos] d \n" +
"            ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio = ? \n"               /*$[Parameter:EjercicioPresentado] */+
"            INNER JOIN [dbo].[T_Modelo_Presentacion] pr \n" +
"            ON pr.Id_Modelo = d.Id_Modelo\n" +
"            INNER JOIN [dbo].[T_Auditores_Sociedades] aud \n" +
"            ON aud.COD_ROAC = pr.COD_ROAC\n" +
"            LEFT JOIN [dbo].[T_Provincias] p ON p.IdProvincia = i.IdProvincia\n" +
"            LEFT JOIN [dbo].[T_Entidades] e ON e.Cod_Entidad = i.IdEntidad\n" +
"            AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio)\n" +
"            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio)\n" +
"            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null))\n" +
"            LEFT JOIN [dbo].[T_Trabajo] t ON t.Cod_Trabajo = i.IdTipoTrabajo\n" +
"            LEFT JOIN [dbo].[T_Opinion] o ON o.Cod_Opinion = i.IdTipoOpinion\n" +
"            LEFT JOIN [dbo].[T_Modelo_Coauditor] MC ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe\n" +
"            LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV\n" +
"            INNER JOIN ( \n" +
"                SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha\n" +
"                FROM T_Modelo_Presentacion A\n" +
"                INNER JOIN T_Modelo_Datos B\n" +
"                ON A.Id_Modelo = B.Id_Modelo\n" +
"                WHERE B.Ejercicio = ?\n" +                                     /* $[Parameter:EjercicioPresentado] */
"                GROUP BY A.COD_ROAC\n" +
"            ) MPMAX\n" +
"            ON pr.COD_ROAC = MPMAX.COD_ROAC\n" +
"            AND  pr.Fecha_Presentacion = MPMAX.fecha \n" +
"            WHERE d.COD_ROAC like ? \n" +                                      /*  '%'+$[Parameter:COD_ROAC]$+'%'  */
"            AND (aud.Razon_Social like ?\n" +                                  /*  '%'+$[Parameter:Identificacion]$+'%'    */
"            OR aud.Apellidos + ', ' + aud.Nombre like ? )\n" +                 /*  '%'+$[Parameter:Identificacion]$+'%'                          */
"            AND i.Documento like ? \n" +                                       /*  '%'+$[Parameter:CIF_Auditada]$+'%'     */
"            AND i.RazonSocial like ? \n" +                                     /*  '%'+$[Parameter:RazonSocial]$+'%'      */
"            order by 1,4";



final String sqlOriginal="SELECT DISTINCT isnull(d.COD_ROAC, '') AS COD_ROAC,\n" +
"            CASE WHEN aud.Nombre is null or aud.Nombre = ''\n" +
"            THEN aud.Razon_Social\n" +
"            ELSE aud.Apellidos + ', ' + aud.Nombre\n" +
"            END as Identificacion,\n" +
"            isnull(i.Documento, '') AS Documento,\n" +
"             isnull(REPLACE(i.RazonSocial,'\"',''), '') AS RazonSocial,\n" +
"            [dbo].[FormatearInformeCuentas](InformeDeCuentas) AS InformeDeCuentas,\n" +
"            dbo.[FormatearFecha](PrimerEjercicioAuditado) AS PrimerEjercicioAuditado,\n" +
"            dbo.[FormatearFecha](EjercicioAuditado) AS EjercicioAuditado,\n" +
"            dbo.[FormatearCharSiNo](Constitucion) AS Constitucion,\n" +
"            dbo.[FormatearFecha](EjercicioFinal) AS EjercicioFinal,\n" +
"            isnull(i.IdEntidad, '') AS IdEntidad,\n" +
"            isnull(e.Desc_Entidad, '') AS DescripcionEntidad,\n" +
"            dbo.[FormatearIntSiNo](ParticipesAuditado) AS ParticipesAuditado,\n" +
"            dbo.[FormatearIntSiNo](ParticipesAnterior) AS ParticipesAnterior,\n" +
"            ImporteNetoAuditado AS ImporteNetoAuditado,\n" +
"            ImporteNetoAnterior AS ImporteNetoAnterior,\n" +
"            isnull(PlantillaMediaAuditado, 0) AS PlantillaMediaAuditado,\n" +
"            isnull(PlantillaMediaAnterior, 0) AS PlantillaMediaAnterior,\n" +
"            isnull(i.IdTipoTrabajo, '') AS IdTipoTrabajo,\n" +
"            isnull(t.Desc_Trabajo, '') AS DescripcionTipoTrabajo,\n" +
"            dbo.[FormatearFecha](FechaInforme) AS FechaInforme,\n" +
"            isnull(ROACAuditorFirmante, '') AS ROACAuditorFirmante,\n" +
"            isnull(NombreAuditorFirmante, '') AS NombreAuditorFirmante,            \n" +
"            dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe) AS Coauditoria,\n" +
"            MC.ROACCoAuditor AS ROACCoauditor,\n" +
"            isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion,\n" +
"            isnull(o.Desc_Opinion, 0) AS DescripcionTipoOpinion,\n" +
"            isnull(i.IdProvincia, 0) AS IdProvincia,\n" +
"            isnull(p.DescProvincia, '') AS Provincia,            \n" +
"            FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios,\n" +
"            FacturacionAuditoriaHoras AS FacturacionAuditoriaHoras,\n" +
"            EM.Desc_Corta AS CNMV,\n" +
"            dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0)) AS InteresPublico,\n" +
"            ImporteCifraActivo AS ImporteCifraActivo,\n" +
"            ImporteCifraActivoAnterior AS ImporteCifraActivoAnterior,\n" +
"            i.HonorariosAuditoriaInterna,\n" +
"            i.HorasAuditoriaInterna,\n" +
"            i.HonorariosDiseno,\n" +
"            i.HorasDiseno,\n" +
"            i.HonorariosOtros,\n" +
"            i.HorasOtros\n" +
"            FROM [dbo].[T_Modelo_Informe] i\n" +
"            INNER JOIN [dbo].[T_Modelo_Datos] d \n" +
"            ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio = $[Parameter:EjercicioPresentado]$\n" +
"            INNER JOIN [dbo].[T_Modelo_Presentacion] pr \n" +
"            ON pr.Id_Modelo = d.Id_Modelo\n" +
"            INNER JOIN [dbo].[T_Auditores_Sociedades] aud \n" +
"            ON aud.COD_ROAC = pr.COD_ROAC\n" +
"            LEFT JOIN [dbo].[T_Provincias] p ON p.IdProvincia = i.IdProvincia            \n" +
"            LEFT JOIN [dbo].[T_Entidades] e ON e.Cod_Entidad = i.IdEntidad\n" +
"            AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio)\n" +
"            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio)\n" +
"            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null))\n" +
"            LEFT JOIN [dbo].[T_Trabajo] t ON t.Cod_Trabajo = i.IdTipoTrabajo\n" +
"            LEFT JOIN [dbo].[T_Opinion] o ON o.Cod_Opinion = i.IdTipoOpinion\n" +
"            LEFT JOIN [dbo].[T_Modelo_Coauditor] MC ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe\n" +
"            LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV\n" +
"            INNER JOIN ( \n" +
"                SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha\n" +
"                FROM T_Modelo_Presentacion A\n" +
"                INNER JOIN T_Modelo_Datos B\n" +
"                ON A.Id_Modelo = B.Id_Modelo\n" +
"                WHERE B.Ejercicio = $[Parameter:EjercicioPresentado]$\n" +
"                GROUP BY A.COD_ROAC\n" +
"            ) MPMAX\n" +
"            ON pr.COD_ROAC = MPMAX.COD_ROAC\n" +
"            AND  pr.Fecha_Presentacion = MPMAX.fecha \n" +
"            WHERE d.COD_ROAC like '%'+$[Parameter:COD_ROAC]$+'%'\n" +
"            AND (aud.Razon_Social like '%'+$[Parameter:Identificacion]$+'%'\n" +
"            OR aud.Apellidos + ', ' + aud.Nombre like '%'+$[Parameter:Identificacion]$+'%')\n" +
"            AND i.Documento like '%'+$[Parameter:CIF_Auditada]$+'%'\n" +
"            AND i.RazonSocial like '%'+$[Parameter:RazonSocial]$+'%'\n" +
"            order by 1,4";



}