
package controller;

import app.ProbandoCruceRecaudacionsTrabajosDAO;
import es.icac.meh.pandora.exportacion.ExportarTrabajos;
import es.icac.meh.pandora.modelo.Apunte;
import es.icac.meh.pandora.modelo.ApunteImpl;
import es.icac.meh.pandora.modelo.TrabajoRealizado;
import es.icac.meh.pandora.services.daos.RecaudacionesDAO;
import es.icac.meh.pandora.services.daos.GestorApuntesServiceDAO;
import es.icac.meh.pandora.services.daos.TrabajosDTO;
import es.icac.meh.pandora.services.daos.TrabajosRealizadosDAO;
import es.icac.meh.pandora.services.impl.GeneracionCrucesServicioImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.catalina.connector.Response;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;




public class ListadoApuntes extends HttpServlet {
 WebApplicationContext  applicationContext;
 GestorApuntesServiceDAO servicio;
 ArrayList<ApunteImpl> listaApuntes;
 GeneracionCrucesServicioImpl servicioCruces;
    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
       applicationContext = ContextLoader.getCurrentWebApplicationContext();

       servicio=applicationContext.getBean("servicioApuntesDAO", GestorApuntesServiceDAO.class);
 
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String paramEjercicio = request.getParameter("ejercicio");
        if (paramEjercicio!=null){
            if(paramEjercicio.trim().equals(""))
                response.sendError(Response.SC_PRECONDITION_FAILED,"Debe enviar EJERCICIO");
        }

    
        
        
        String paramDescripcion = request.getParameter("descripcion");
        String paramBorrar = request.getParameter("borrado");

//        if (paramEjercicio == null || paramEjercicio.equals("")) {            
//            String url = request.getContextPath() + "/generarImportacion.jsp";
//            response.sendRedirect(url);
//        } 
        boolean truncate=false;
         if (paramBorrar != null)             
            if(paramBorrar.equals("on"))
                truncate=true;
         
         Calendar cal=Calendar.getInstance();
         int ayno=cal.get(Calendar.YEAR);
         int dia=cal.get(Calendar.DATE);
         int mes=cal.get(Calendar.MONTH)+1;
 
         if (paramDescripcion== null || paramDescripcion.equals("")){
             try{
             ayno=Integer.parseInt(paramEjercicio);
             }catch(Exception e){
             ayno=cal.get(Calendar.YEAR);
             }
            paramDescripcion=String.valueOf(ayno)
                       .concat(" ")
                       .concat(String.valueOf(dia))
                       .concat("/")
                       .concat(String.valueOf(mes))
                       .concat("/")
                       .concat(String.valueOf(ayno));
            }            
         
            processResponse(response,paramEjercicio);

    }

//    private void tratarPeticion(String pEjer, String pDescripcion, boolean pBorr, HttpServletResponse response) throws IOException {
//
//       // boolean resultado=servicio.procesarCruces(pEjer, pBorr);
//
//    
//         
//         
//         response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet ListadoApuntes</title>");
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>pEjer at " + pEjer + "</h1>");
//            out.println("<h1>pDescripcion at " + pDescripcion + "</h1>");
//            out.println("<h1>pBorrat " + pBorr + "</h1>");
//
//            if (servicio.getDiario().size() > 1) {
//                out.println("<li>RESULTADO DEL PROCESAMIENTO OK</li>");
//            } else {
//                out.println("<li>RESULTADO DEL PROCESAMIENTO MAL :(</li>");
//            }
//
//            out.println("</body>");
//            out.println("</html>");
//        }
//    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    
     protected void processResponse(HttpServletResponse response, String paramAno)
            throws ServletException, IOException {


 
        if (paramAno == null) {
            paramAno = "2019";
        }
        
                  try {
         listaApuntes=servicio.obtenerApuntes(paramAno);
     } catch (SQLException ex) {
         Logger.getLogger(ListadoApuntes.class.getName()).log(Level.SEVERE, "ERROR EN EL DAO DE APUNTES", ex);
     }
                  
                  
        response.setContentType("text/html;charset=UTF-8");
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
    response.setHeader("Pragma","no-cache"); //HTTP 1.0 
    response.setDateHeader ("Expires", 0); //prevents caching at the proxy server  

        String formulario = "        <form name=\"filtroRoac\" action=\"/ProyectoPANDORA/TrabajosRealizadosServlet\" method=\"GET\">\n"
                + "            \n"
      //          + "            <INPUT TYPE=\"TEXT\" NAME=\"ano\" size=\"4\" value='"+paramAno+"'/>\n"
        //        + "            <INPUT TYPE=\"TEXT\" NAME=\"roac\" size=\"10\" value='"+paramRoac+"'/>\n"
                + "            <input type=\"submit\" value=\"Consultar ROAC\"/>\n"
                + "        </form>";
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("   <title>Servlet MiServlet</title>");
            out.println("   <link rel=\"stylesheet\" type=\"text/css\" href=\"/ProyectoPANDORA/css/zebra.css\">");
            out.println("</head>");
            out.println("<body>");

            out.println("<h1>Listado de Movimientos</h1>");
            out.println("<h4>Año:" + paramAno + "+ ROAC: " +  "</H4>");
            out.println("<FIELDSET><LEGEND>Filtrado:</LEGEND>");
            out.println(formulario);
            out.println("</FIELDSET><HR/>");
            out.println("<table border='1' class='blueTable'>");

            out.println("<tr>");
            out.println("<th>CodRoac</th>");
            out.println("<th>Documento</th>");
            out.println("<th>Identificacion</th>");
            out.println("<th>Concepto</th>");

            out.println("<th>Fecha</th>");

            out.println("<th>Ejercicio</th>");
            out.println("<th>Trimestre</th>");

            out.println("<th>Importe</th>");
           // out.println("<th></th>");
          //  out.println("<th></th>");
            out.println("</tr>");
            double acumulado = 0;
            for (Apunte tmp : listaApuntes) {

                out.println("<tr>");
                out.println("<td>" + tmp.getRoac()+ "</td>");
                out.println("<td>" + tmp.getNifAuditor()+ "</td>");
                out.println("<td>" + tmp.getNombreAuditor()+ "</td>");
                out.println("<td>" + tmp.getDenominacion()+ "</td>");

                out.println("<td>" + tmp.getFecha()+ "</td>");

                out.println("<td>" + tmp.getEjercicio()+ "</td>");
                out.println("<td>" + tmp.getTrimestre()+ "</td>");
                   out.println("<td>" + tmp.getFechaPago()+ "</td>");
                   
                 out.println("<td>" + tmp.getImporte()+ "</td>");
                acumulado += tmp.getImporte();
     //     out.println("<td>" + tmp.getCampo1()+ "</td>");
     //     out.println("<td>" + tmp.getCampo2()+ "</td>");
                out.println("</tr>");
            }
            out.println("<tr><td colspan='6'></td>");

            DecimalFormat formatea = new DecimalFormat("###,###.##");

            out.println("<td><b>" + formatea.format(acumulado) + "</b></td><td colspan='2'></td></tr>");
            out.println("</table>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
      protected void processResponse2(HttpServletResponse response, String paramAno)
            throws ServletException, IOException {


 
        response.setContentType("text/html;charset=UTF-8");
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
    response.setHeader("Pragma","no-cache"); //HTTP 1.0 
    response.setDateHeader ("Expires", 0); //prevents caching at the proxy server  

       
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("   <title>Servlet MiServlet</title>");
            out.println("   <link rel=\"stylesheet\" type=\"text/css\" href=\"/ProyectoPANDORA/css/zebra.css\">");
            out.println("</head>");
            out.println("<body>");

            out.println("<h1>GENERADOS CRUCES DEL PERIDODO "+paramAno+"</h1>");
           out.println("</body>");
  out.println("</HTML>");
        }
    }
    
}
