package controller;

import es.icac.meh.pandora.modelo.ResultadoExtraccion;
import es.icac.meh.pandora.modelo.TrabajoRealizadoAtlas;
import es.icac.meh.pandora.services.daos.TrabajosDTO;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;

import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.catalina.connector.Response;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;


public class GenerarExtraccion extends HttpServlet {
    
  private ArrayList<TrabajoRealizadoAtlas> lista;
    TrabajosDTO dto;
    private static final Logger LOG = Logger.getLogger(GenerarExtraccion.class.getName());
    
    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.

        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        dto =webApplicationContext.getBean("trabajoDTO", TrabajosDTO.class);

    }  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
          boolean regenerar=true;
          
            ResultadoExtraccion resultado=null;
        
          
                String paramEjercicio = request.getParameter("ejercicio");
        if (paramEjercicio!=null){
            if(paramEjercicio.trim().equals(""))
                response.sendError(Response.SC_PRECONDITION_FAILED,"Debe enviar EJERCICIO");
            else{
               resultado = dto.extraerTrabajos(paramEjercicio, regenerar);
            }
        }else{
            response.sendError(Response.SC_PRECONDITION_FAILED,"Debe enviar EJERCICIO");
        }
        


               

      

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GenerarExtraccion</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>GENERADA EXTRACCIÓN: "+new java.util.Date()+"</h1>");
            out.println("<h2>Duración de la extracción: "+resultado.getTiempoProceso()+" segundos </h2>");
            out.println("<h2>Registros Extraidos: "+resultado.getNumeroDatosExtraidos()+" registros </h2>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
