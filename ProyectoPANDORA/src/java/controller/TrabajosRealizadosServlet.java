package controller;

import es.icac.meh.pandora.modelo.TrabajoRealizado;
import es.icac.meh.pandora.modelo.TrabajoRealizadoAtlas;
import es.icac.meh.pandora.services.ConnectionService;
import es.icac.meh.pandora.services.daos.TrabajosRealizadosDAO;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class TrabajosRealizadosServlet extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(TrabajosRealizadosServlet.class.getName());

    private ArrayList<TrabajoRealizado> lista;
    TrabajosRealizadosDAO dao;

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.

        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());

        dao = webApplicationContext.getBean("trabajosDAO",TrabajosRealizadosDAO.class);

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String paramRoac = request.getParameter("roac");
        String paramAno = request.getParameter("ano");
        if (paramRoac == null) {
            paramRoac = "";
        }
        if (paramAno == null) {
            paramAno = "2019";
        }
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
    response.setHeader("Pragma","no-cache"); //HTTP 1.0 
    response.setDateHeader ("Expires", 0); //prevents caching at the proxy server  
        try {
            lista = dao.obtenerTrabajosPorRoacEjerc(paramRoac, paramAno);
        } catch (SQLException ex) {
            //   Logger.getLogger(MiServletTrabajosRealizados.class.getName()).log(Level.SEVERE, null, ex);
            LOG.log(Level.SEVERE, "ERROR SQL", ex);
        }

        String formulario = "        <form name=\"filtroRoac\" action=\"/ProyectoPANDORA/TrabajosRealizadosServlet\" method=\"GET\">\n"
                + "            \n"
                + "            <INPUT TYPE=\"TEXT\" NAME=\"ano\" size=\"4\" value='"+paramAno+"'/>\n"
                + "            <INPUT TYPE=\"TEXT\" NAME=\"roac\" size=\"10\" value='"+paramRoac+"'/>\n"
                + "            <input type=\"submit\" value=\"Consultar ROAC\"/>\n"
                + "        </form>";
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("   <title>Servlet MiServlet</title>");
            out.println("   <link rel=\"stylesheet\" type=\"text/css\" href=\"/ProyectoPANDORA/css/zebra.css\">");
            out.println("</head>");
            out.println("<body>");

            out.println("<h1>Trabajos realizados:</h1>");
            out.println("<h4>Año:" + paramAno + "+ ROAC: " + paramRoac + "</H4>");
            out.println("<FIELDSET><LEGEND>Filtrado:</LEGEND>");
            out.println(formulario);
            out.println("</FIELDSET><HR/>");
            out.println("<table border='1' class='blueTable'>");

            out.println("<tr>");
            out.println("<th>CodRoac</th>");
            out.println("<th>Identificacion</th>");
            out.println("<th>Documento</th>");
            out.println("<th>RazonSocial</th>");

            out.println("<th>FechaInforme</th>");
            out.println("<th>Tasa</th>");
            out.println("<th>Ejercicio</th>");
            out.println("<th>Trim.</th>");
            out.println("<th>Fact. Audit</th>");

            out.println("<th>InteresPublico</th>");
            out.println("<th>Coauditoria</th>");
            out.println("</tr>");
            double acumulado = 0;
            for (TrabajoRealizado tmp : lista) {

                out.println("<tr>");
                out.println("<td>" + tmp.getCodRoac() + "</td>");
                out.println("<td>" + tmp.getIdentificacion() + "</td>");
                out.println("<td>" + tmp.getDocumento() + "</td>");
                out.println("<td>" + tmp.getRazonSocial() + "</td>");

                out.println("<td>" + tmp.getFecha() + "</td>");
                out.println("<td>" + tmp.getTasa()+ "</td>");
                out.println("<td>" + tmp.getCalcEjercicio()+ "</td>");
                out.println("<td>" + tmp.getCalcTrimestre()+ "</td>");
                 out.println("<td>" + tmp.getFacturacionAuditoriaHonorarios()+ "</td>");
                acumulado += tmp.getFacturacionAuditoriaHonorarios();
                out.println("<td>" + tmp.isEsInteresPublico() + "</td>");
                out.println("<td>" + tmp.isCoauditoria() + "</td>");
                out.println("</tr>");
            }
            out.println("<tr><td colspan='6'></td>");

            DecimalFormat formatea = new DecimalFormat("###,###.##");

            out.println("<td><b>" + formatea.format(acumulado) + "</b></td><td colspan='2'></td></tr>");
            out.println("</table>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    
}
