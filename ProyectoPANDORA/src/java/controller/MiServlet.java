/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import es.icac.meh.pandora.services.ConnectionService;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author MAJIVIAL
 */
public class MiServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
         WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
 System.out.println("=====LISTA DE BEANS CARGADOS=====");
         for(String tmp: webApplicationContext.getBeanDefinitionNames()){
             System.out.println(" "+tmp);
         }

             Conexion x1=webApplicationContext.getBean("conexionVisibleFileSystem", Conexion.class);
             Conexion x2=webApplicationContext.getBean("conexionVisibleClassPath", Conexion.class);
             
     //       Conexion y1=webApplicationContext.getBean("conexionOcultaFileSystem", Conexion.class);
     //       Conexion y2=webApplicationContext.getBean("conexionOcultaClassPath", Conexion.class);
    //         Conexion sorpresa=webApplicationContext.getBean("conexionOcultaNOSE", Conexion.class);
             
            ConnectionService javaBean=webApplicationContext.getBean("service", ConnectionService.class);
      
//Properties propertiesOcultas=(Properties) webApplicationContext.getBean("propiedadesOCULTAS");
Properties propiedadesVISIBLESclasspath=(Properties) webApplicationContext.getBean("propiedadesVISIBLESclasspath");


        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MiServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MiServlet at " + request.getContextPath() + "</h1>");
   
         //   out.println("<h1>URL OCULTA="+propertiesOcultas.getProperty("db.url")+ "</h1>");
            out.println("<h1>URL VISIBLE="+propiedadesVISIBLESclasspath.getProperty("db.url")+ "</h1>");
            out.println("<h1>CONEXION visible CLASSPATH del bean=" + x2.getUrl() + "</h1>");
  //          out.println("<h1>CONEXION oculta FILESYSTEM del bean=" + y1.getUrl() + "</h1>");
            out.println("<h1>CONEXION visible FILESYSTEM del bean=" + x1.getUrl() + "</h1>");
 //           out.println("<h1>CONEXION oculta CLASSPATH del bean=" + y2.getUrl() + "</h1>");
   //         out.println("<h1>CONEXION sorpresa >>>>la pilla del filesystem=" + sorpresa.getUrl() + "</h1>");
            out.println("<h1>CONEXION javaBean.url CLASSPATH del bean=" + javaBean.getConfiguracion().getUrl() + "</h1>");
            out.println("<h1>CONEXION javaBean.username CLASSPATH del bean=" + javaBean.getConfiguracion().getUsername() + "</h1>");
            out.println("<h1>CONEXION javaBean.password CLASSPATH del bean=" + javaBean.getConfiguracion().getPassword() + "</h1>");
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
SELECT DISTINCT isnull(d.COD_ROAC, '') AS COD_ROAC,
            CASE WHEN aud.Nombre is null or aud.Nombre = ''
            THEN aud.Razon_Social
            ELSE aud.Apellidos + ', ' + aud.Nombre
            END as Identificacion,
            isnull(i.Documento, '') AS Documento,
             isnull(REPLACE(i.RazonSocial,'"',''), '') AS RazonSocial,
            [dbo].[FormatearInformeCuentas](InformeDeCuentas) AS InformeDeCuentas,
            dbo.[FormatearFecha](PrimerEjercicioAuditado) AS PrimerEjercicioAuditado,
            dbo.[FormatearFecha](EjercicioAuditado) AS EjercicioAuditado,
            dbo.[FormatearCharSiNo](Constitucion) AS Constitucion,
            dbo.[FormatearFecha](EjercicioFinal) AS EjercicioFinal,
            isnull(i.IdEntidad, '') AS IdEntidad,
            isnull(e.Desc_Entidad, '') AS DescripcionEntidad,
            dbo.[FormatearIntSiNo](ParticipesAuditado) AS ParticipesAuditado,
            dbo.[FormatearIntSiNo](ParticipesAnterior) AS ParticipesAnterior,
            ImporteNetoAuditado AS ImporteNetoAuditado,
            ImporteNetoAnterior AS ImporteNetoAnterior,
            isnull(PlantillaMediaAuditado, 0) AS PlantillaMediaAuditado,
            isnull(PlantillaMediaAnterior, 0) AS PlantillaMediaAnterior,
            isnull(i.IdTipoTrabajo, '') AS IdTipoTrabajo,
            isnull(t.Desc_Trabajo, '') AS DescripcionTipoTrabajo,
            dbo.[FormatearFecha](FechaInforme) AS FechaInforme,
            isnull(ROACAuditorFirmante, '') AS ROACAuditorFirmante,
            isnull(NombreAuditorFirmante, '') AS NombreAuditorFirmante,            
            dbo.[FormatearIntSiNo](MC.Id_Modelo_Informe) AS Coauditoria,
            MC.ROACCoAuditor AS ROACCoauditor,
            isnull(i.IdTipoOpinion, 0) AS IdTipoOpinion,
            isnull(o.Desc_Opinion, 0) AS DescripcionTipoOpinion,
            isnull(i.IdProvincia, 0) AS IdProvincia,
            isnull(p.DescProvincia, '') AS Provincia,            
            FacturacionAuditoriaHonorarios AS FacturacionAuditoriaHonorarios,
            FacturacionAuditoriaHoras AS FacturacionAuditoriaHoras,
            EM.Desc_Corta AS CNMV,
            dbo.[FormatearIntSiNo](ISNULL(InteresPublico, 0)) AS InteresPublico,
            ImporteCifraActivo AS ImporteCifraActivo,
            ImporteCifraActivoAnterior AS ImporteCifraActivoAnterior,
            i.HonorariosAuditoriaInterna,
            i.HorasAuditoriaInterna,
            i.HonorariosDiseno,
            i.HorasDiseno,
            i.HonorariosOtros,
            i.HorasOtros
            FROM [dbo].[T_Modelo_Informe] i
            INNER JOIN [dbo].[T_Modelo_Datos] d 
            ON i.Id_Modelo = d.Id_Modelo AND d.Ejercicio = $[Parameter:EjercicioPresentado]$
            INNER JOIN [dbo].[T_Modelo_Presentacion] pr 
            ON pr.Id_Modelo = d.Id_Modelo
            INNER JOIN [dbo].[T_Auditores_Sociedades] aud 
            ON aud.COD_ROAC = pr.COD_ROAC
            LEFT JOIN [dbo].[T_Provincias] p ON p.IdProvincia = i.IdProvincia            
            LEFT JOIN [dbo].[T_Entidades] e ON e.Cod_Entidad = i.IdEntidad
            AND ((e.Ejercicio_Inicio is null AND e.Ejercicio_Fin >= d.Ejercicio)
            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin >= d.Ejercicio)
            OR (e.Ejercicio_Inicio <= d.Ejercicio AND e.Ejercicio_Fin is null))
            LEFT JOIN [dbo].[T_Trabajo] t ON t.Cod_Trabajo = i.IdTipoTrabajo
            LEFT JOIN [dbo].[T_Opinion] o ON o.Cod_Opinion = i.IdTipoOpinion
            LEFT JOIN [dbo].[T_Modelo_Coauditor] MC ON MC.Id_Modelo_Informe = i.Id_Modelo_Informe
            LEFT JOIN [dbo].[T_Tipo_Entidad_Emisora] EM ON EM.Cod_Tipo_Entidad_Emisora = i.CNMV
            INNER JOIN ( 
                SELECT A.COD_ROAC , max(Fecha_Presentacion) as fecha
                FROM T_Modelo_Presentacion A
                INNER JOIN T_Modelo_Datos B
                ON A.Id_Modelo = B.Id_Modelo
                WHERE B.Ejercicio = $[Parameter:EjercicioPresentado]$
                GROUP BY A.COD_ROAC
            ) MPMAX
            ON pr.COD_ROAC = MPMAX.COD_ROAC
            AND  pr.Fecha_Presentacion = MPMAX.fecha 
            WHERE d.COD_ROAC like '%'+$[Parameter:COD_ROAC]$+'%'
            AND (aud.Razon_Social like '%'+$[Parameter:Identificacion]$+'%'
            OR aud.Apellidos + ', ' + aud.Nombre like '%'+$[Parameter:Identificacion]$+'%')
            AND i.Documento like '%'+$[Parameter:CIF_Auditada]$+'%'
            AND i.RazonSocial like '%'+$[Parameter:RazonSocial]$+'%'
            order by 1,4

*/