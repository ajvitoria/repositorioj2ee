<%-- 
    Document   : newjsp
    Created on : 11-mar-2020, 10:36:09
    Author     : MAJIVIAL
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Stage</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <style>
 
        </style>
    </head>
    <body>
        <h1>Hello World!</h1>
        <input type="text" id="search-text" class="search-box">
        <span class="list-count"></span>
        <ul id="list">

  <li>List Item 1</li>
  <li>List Item 2</li>
  <li>List Item 3</li>



  <li>List Item n</li>

  <span class="empty-item">no results</span>
  
  <script>
      $(document).ready(function() {

  var jobCount = $('#list .in').length;
  $('.list-count').text(jobCount + ' items');
  
  $("#search-text").keyup(function () {
  
    var searchTerm = $("#search-text").val();
    var listItem = $('#list').children('li');
   
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
    
    $.extend($.expr[':'], {
      'containsi': function(elem, i, match, array) {

        return (elem.textContent || elem.innerText || '').toLowerCase()
        .indexOf((match[3] || "").toLowerCase()) >= 0;
      }
  });   
    
  $("#list li").not(":containsi('" + searchSplit + "')").each(function(e)   {
    $(this).addClass('hiding out').removeClass('in');
    setTimeout(function() {
      $('.out').addClass('hidden');
    }, 300);
  });
    
  $("#list li:containsi('" + searchSplit + "')").each(function(e) {
    $(this).removeClass('hidden out').addClass('in');
    setTimeout(function() {
      $('.in').removeClass('hiding');
    }, 1);
  });  
  
  var jobCount = $('#list .in').length;
    $('.list-count').text(jobCount + ' items');
    
    if(jobCount == '0') {
      $('#list').addClass('empty');
    }
    else {
      $('#list').removeClass('empty');
    }
    
  });
     
  function searchList() {                

    var listArray = [];
  
    $("#list li").each(function() {
      var listText = $(this).text().trim();
        listArray.push(listText);
    });
    
    $('#search-text').autocomplete({
      source: listArray
    });  
    
  }
                                   
  searchList();
                  
});
  </script>

</ul>
    </body>
</html>
