var studentTable;

jQuery(document).ready(function() {

	studentTable = jQuery('#listaCobrosTable').dataTable({
	
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"iDisplayLength": 10,
		"bProcessing" : true,
		"bServerSide" : true,
		"sAjaxSource" : basePath +"/controller/ListadoCobrosServlet.java",
			
		"aoColumns" : [ {"bSearchable" : false,	"bVisible" : false,	"asSorting" : [ "asc" ]	},
    	    {"sWidth" : "10%","bSortable" : true },
     	   {"sWidth" : "75%","bSortable" : true },
     	   {"sWidth" : "5%","bSortable" : true },
     	   {"sWidth" : "5%","bSortable" : true },
     	   {"sWidth" : "5%","bSortable" : false }
	    ]
	});
	jQuery(".ui-corner-br").addClass('ui-widget-header_custom ');

});