<%@page import="es.icac.meh.pandora.modelo.Recaudacion"%>
<%@page import="controller.*,java.util.*"%><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
	var basePath = '${pageContext.request.contextPath}';
   
</script>
<link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/assets/datatable/css/demo_table_jui.css">
<!--link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/assets/datatable/css/datatables.css" -->
<link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/assets/jquery-ui/css/redmond/jquery-ui-1.8.11.custom.css" >
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery-1.5.2.min.js" ></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery-ajax-form-plugin/jquery.form.js" ></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/datatable/dataTables.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/datatable/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/jquery-ui/js/jquery-ui-1.8.11.custom.min.js" ></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/table-demo.js"></script> 	

<title>Listado de Tasas Pagadas</title>
</head>
<body>
	<form>
		<div class="titleDiv">Listado de TASAS PAGADAS</div>
		<div class="clearfix"></div>
		
		<div class="formDiv">
			<table border="0" margin="0" padding="0" width="100%"
				class="dataTables_wrapper" id="listaRecaudacionsTable">
				<thead>
					<tr>
						<th>Id</th>
						<th>CIF</th>
						<th>Nombre</th>
                                                <th>Fecha</th>
						<th>Importe</th>						
						<th>Justificante</th>
					</tr>
				</thead>
				<tbody>
					<%
					  if(request.getAttribute("listaRecaudacions") != null){
						List<Recaudacion> listaRecaudacions = (List<Recaudacion>)request.getAttribute("listaRecaudacions");
					%>
					<% 
						 if( listaRecaudacions != null ){		
							for (Recaudacion cobro: listaRecaudacions){	
					%>	
							<tr>							
						 		<td><%=cobro.getCodigoTasa()%></td>
								<td><%=cobro.getNif()%></td>
								<td><%=cobro.getApellidosNombre()%></td>
								<td><%=cobro.getFechaIngresoString()%></td>
								<td><%=cobro.getImporteEuros()%></td>
								<td><%=cobro.getNumeroJustificante()%></td>									
							</tr>
					       <%}
					     }					
					  }%> 				
					</tbody>
			</table>
		</div>
	</form>
</body>
</html>
