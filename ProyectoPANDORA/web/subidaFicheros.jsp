<%-- 
    Document   : subidaFicheros
    Created on : 18-mar-2020, 18:48:26
    Author     : MAJIVIAL
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        
        <script
			  src="https://code.jquery.com/jquery-3.4.1.js"
			  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
			  crossorigin="anonymous"></script>
                          
                          
<!--script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script-->
        <style>
            #dragandrophandler{
                border: 2px dashed #92AAB0;
                width: 650px;
                height: 300px;
                color: #92AAB0;
                text-align: center;
                vertical-align: middle;
                padding: 10px 0px 10px 10px;
                font-size:200%;
                display: table-cell;

            }   

            #dragandrophandlerkk
            {
                border:2px dotted #0B85A1;
                width:400px;
                color:#92AAB0;
                text-align:left;vertical-align:middle;
                padding:10px 10px 10 10px;
                margin-bottom:10px;
                font-size:200%;
            }
            .progressBar {
                width: 200px;
                height: 22px;
                border: 1px solid #ddd;
                border-radius: 5px; 
                overflow: hidden;
                display:inline-block;
                margin:0px 10px 5px 5px;
                vertical-align:top;
            }

            .progressBar div {
                height: 100%;
                color: #fff;
                text-align: right;
                line-height: 22px; /* same as #progressBar height if we want text middle aligned */
                width: 0;
                background-color: #0ba1b5; border-radius: 3px; 
            }
            .statusbar{
                border-top:1px solid #A9CCD1;
                min-height:25px;
                width:100%;
                padding:10px 10px 0px 10px;
                vertical-align:top;
            }
            .statusbar:nth-child(odd){
                background:#EBEFF0;
            }
            .filename{
                display:inline-block;
                vertical-align:top;
                width:250px;
            }
            .filesize{
                display:inline-block;
                vertical-align:top;
                color:#30693D;
                width:100px;
                margin-left:10px;
                margin-right:5px;
            }
            .abort{
                background-color:#A8352F;
                -moz-border-radius:4px;
                -webkit-border-radius:4px;
                border-radius:4px;display:inline-block;
                color:#fff;
                font-family:arial;font-size:13px;font-weight:normal;
                padding:4px 15px;
                cursor:pointer;
                vertical-align:top
            }
            .botonCerrar{
                background-color:#A8352F;
                -moz-border-radius:4px;
                -webkit-border-radius:4px;
                border-radius:4px;display:inline-block;
                color:#fff;
                font-family:arial;font-size:13px;font-weight:normal;
                padding:4px 15px;
                cursor:pointer;
                vertical-align:top;
                text-align: center;
                width: 100%;
            }
        </style>
    </head>

    <body>
        <div id="dragandrophandler">Arrastra los archivos a este recuadro</div>
        <br><h1 id="contador"></h1><br/>
        <hr/><br/>
        <div id="status1"></div>
        <hr/>
        <br/>
        <div id="botonCerrar" class="botonCerrar">Cerrar</div>
        <script>
            var ultimoFicheroSubido;
            const listaFicheros = [];


            function sendFileToServer(formData, status) {
                var uploadURL = "UploadServlet"; //Upload URL
                var extraData = {}; //Extra Data.
                var jqXHR = $.ajax({
                    xhr: function () {
                        var xhrobj = $.ajaxSettings.xhr();
                        if (xhrobj.upload) {
                            xhrobj.upload.addEventListener('progress', function (event) {
                                var percent = 0;
                                var position = event.loaded || event.position;
                                var total = event.total;
                                if (event.lengthComputable) {
                                    percent = Math.ceil(position / total * 100);
                                }
                                //Set progress
                                status.setProgress(percent);
                            }, false);
                        }
                        return xhrobj;
                    },
                    url: uploadURL,
                    type: "POST",
                    contentType: false,
                    overrideMimeType: "text/plain; charset=UTF-8",
                    processData: false,
                    cache: false,
                    data: formData,
                    success: function (data) {
                        status.setProgress(100);
                        console.log("subido");

                        let indice=$('.statusbar').length;
                        $("#status1").append("Fichero subido indice " + indice + "</br>");
                        $("#status1").append("Fichero subido " + listaFicheros[indice].name + "</br>");
                        
                        console.log(jqXHR);
                    }
                });

                status.setAbort(jqXHR);
            }

            var rowCount = 0;
            function createStatusbar(obj) {
                rowCount++;
                var row = "odd";
                if (rowCount % 2 == 0)
                    row = "even";
                this.statusbar = $("<div class='statusbar " + row + "'></div>");
                this.filename = $("<div class='filename'></div>").appendTo(this.statusbar);
                this.size = $("<div class='filesize'></div>").appendTo(this.statusbar);
                this.progressBar = $("<div class='progressBar'><div></div></div>").appendTo(this.statusbar);
                this.abort = $("<div class='abort'>Cancelar</div>").appendTo(this.statusbar);
                this.nombre=name;
                obj.after(this.statusbar);
               // obj.after(botonCerrar);
                
                this.setFileNameSize = function (name, size) {
                    var sizeStr = "";
                    var sizeKB = size / 1024;
                    if (parseInt(sizeKB) > 1024) {
                        var sizeMB = sizeKB / 1024;
                        sizeStr = sizeMB.toFixed(2) + " MB";
                    } else {
                        sizeStr = sizeKB.toFixed(2) + " KB";
                    }
                    this.filename.html(name);
                    this.size.html(sizeStr);
                }
                this.setProgress = function (progress) {
                    var progressBarWidth = progress * this.progressBar.width() / 100;
                    this.progressBar.find('div').delay(1000).animate({width: progressBarWidth}, 500).html(progress + "% ");
                  //       $(".thing").delay(2000).animate({top:'39px'},{duration:500});
                    
                    if (parseInt(progress) >= 100) {
                        
    /////////////////////esto es cuenaodpok klse ter ianark dke lgrambar luan ficherosodiei ikdkdk                    
      //      alert("fin");
            this.statusbar.remove();
            
            if($('.statusbar').length>0){                
                  $("#dragandrophandler").css('background-color', 'green');
                  $("#dragandrophandler").text(""+$('.statusbar').length);
            }else{
                  $("#dragandrophandler").css('background-color', 'white');
                   $("#dragandrophandler").text(""+$('.statusbar').length);
            }
            
                        this.ultimoFicheroSubido="unficheroultimo";
                        this.abort.hide();
                        obj.css('border', '20px dotted transparent');
                        // this.ultimoFicheroSubido=this.filename;
                    }
                }
                this.setAbort = function (jqxhr) {
                    var sb = this.statusbar;
                    this.abort.click(function () {
                        jqxhr.abort();
                        sb.hide();
                         this.statusbar.remove();
                        $("#dragandrophandler").text(""+$('.statusbar').length);
                    });
                }
            }
            function handleFileUpload(files, obj) {
                for (var i = 0; i < files.length; i++) {
                    var fd = new FormData();
                    fd.append('file', files[i]);
  const count = listaFicheros.push(files[i]);
                    var status = new createStatusbar(obj); //Using this we can set progress.
                    status.setFileNameSize(files[i].name, files[i].size);
                    sendFileToServer(fd, status);

                }
            }
            $(document).ready(function () {
                var obj = $("#dragandrophandler");
                obj.on('dragenter', function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    $(this).css('border', '10px solid #0B85A1');
                    $(this).css('background-color', '#0B85A1');

                });
                obj.on('dragover', function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                });
                obj.on('drop', function (e) {

                    $(this).css('border', '10px dotted #0B85A1');
                    $(this).css('background-color', '#0B85A1');
                    e.preventDefault();
                    var files = e.originalEvent.dataTransfer.files;

                    //We need to send dropped files to Server
                    handleFileUpload(files, obj);
                });
                $(document).on('dragenter', function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                });
                $(document).on('blur', function (e) {

                    obj.css('background-color', 'transparent');
                    obj.css('background-color', 'red');
                });
                $(document).on('dragover', function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    obj.css('border', '20px dotted #0B85A1');
                    obj.css('background-color', '#0B85A1');
                });
                $(document).on('drop', function (e) {
                    obj.css('border', '20px dotted red');
                    e.stopPropagation();
                    e.preventDefault();
                });
                $("#botonCerrar").click(function () {
                    window.location.replace("/ProyectoPANDORA/listadoStageArea.jsp");
                });
            });
            
            
        </script>
    </body>
</html>