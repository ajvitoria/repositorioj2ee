<%-- 
    Document   : generarImportacion
    Created on : 12-may-2020, 13:52:54
    Author     : MAJIVIAL
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <STYLE>

        #cargando,#carga{
            text-align: center;
        }

        #cargando{
            display:none;
        }

</STYLE>


    </head>
    <body>
        

<div id='loading' style="opacity: .8; display: table-cell; vertical-align: middle; text-align: center; ">
  <img src="/ProyectoPANDORA/images/loading.gif" alt="no se ve" ><h3>Cargando página ...</h3>
</div>
<div id=respuesta></div>


<script type="text/javascript">
    

$(document).ready(function(){
  $('#loading').hide();
  $('form').submit(function(){
    $('#loading').show();
  });
});


</script>
        <h1>Generación de Proceso de Cruces</h1>
        <div class="" style="width: 480px; margin: 0 auto; margin-top: 20px;" id="form_wrapper">
            <img src="images/loading.gif" id="gif" style="display: block; margin: 0 auto; width: 100px; visibility: hidden;">
       
       
       
       
        <fieldset>
            <div class="fade" id="form">
            <form action="<%=request.getContextPath() %>/GenerarCruces">

                <legend>Ejercicio a importar</legend>
                <label for="ejercicio">EJERCICIO</label>
                <br/><input name="ejercicio" type="text" placeholder="2018"/>
                <br/>
                 <label for="descripcion">Descripcion</label>
                <br/><input name="descripcion" type="text" placeholder="2018 15/05/2018"/>
                <br/><label for="borrado">Borrado previo</label>
                <input type="checkbox" name="borrado"/>

                <br/>
                <input class="load_button" type="submit" value="Generar Cruces de Información"/>

            </form>
        </fieldset>
</div>
            <div class="fifty"></div>
            
  
    </body>
</html>
