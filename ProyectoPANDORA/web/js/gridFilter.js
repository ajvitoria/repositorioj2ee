/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


      $(document).ready(function() {

  var jobCount = $('#listaFicheros .in').length;
  $('.list-count').text(jobCount + ' items');
  
  $("#search-text").keyup(function () {
  
    var searchTerm = $("#search-text").val();
    var listItem = $('#listaFicheros').children('li');
   
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
    
    $.extend($.expr[':'], {
      'containsi': function(elem, i, match, array) {

        return (elem.textContent || elem.innerText || '').toLowerCase()
        .indexOf((match[3] || "").toLowerCase()) >= 0;
      }
  });   
    
  $("#listaFicheros li").not(":containsi('" + searchSplit + "')").each(function(e)   {
    $(this).addClass('hiding out').removeClass('in');
    setTimeout(function() {
      $('.out').addClass('hidden');
    }, 300);
  });
    
  $("#listaFicheros li:containsi('" + searchSplit + "')").each(function(e) {
    $(this).removeClass('hidden out').addClass('in');
    setTimeout(function() {
      $('.in').removeClass('hiding');
    }, 1);
  });  
  
  var jobCount = $('#listaFicheros .in').length;
    $('.list-count').text(jobCount + ' items');
    
    if(jobCount == '0') {
      $('#listaFicheros').addClass('empty');
    }
    else {
      $('#listaFicheros').removeClass('empty');
    }
    
  });
     
  function searchList() {                

    var listArray = [];
  
    $("#listaFicheros li").each(function() {
      var listText = $(this).text().trim();
        listArray.push(listText);
    });
    /*
    $('#search-text').autocomplete({
      source: listArray
    });  
    */
  }
                                   
  searchList();
                  
});
