/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


      $(document).ready(function() {

  var jobCount = $('#list .in').length;
  $('.list-count').text(jobCount + ' items');
  
  $("#search-text").keyup(function () {
  
    var searchTerm = $("#search-text").val();
    var listItem = $('#list').children('li');
   
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
    
    $.extend($.expr[':'], {
      'containsi': function(elem, i, match, array) {

        return (elem.textContent || elem.innerText || '').toLowerCase()
        .indexOf((match[3] || "").toLowerCase()) >= 0;
      }
  });   
    
  $("#list li").not(":containsi('" + searchSplit + "')").each(function(e)   {
    $(this).addClass('hiding out').removeClass('in');
    setTimeout(function() {
      $('.out').addClass('hidden');
    }, 300);
  });
    
  $("#list li:containsi('" + searchSplit + "')").each(function(e) {
    $(this).removeClass('hidden out').addClass('in');
    setTimeout(function() {
      $('.in').removeClass('hiding');
    }, 1);
  });  
  
  var jobCount = $('#list .in').length;
    $('.list-count').text(jobCount + ' items');
    
    if(jobCount == '0') {
      $('#list').addClass('empty');
    }
    else {
      $('#list').removeClass('empty');
    }
    
  });
     
  function searchList() {                

    var listArray = [];
  
    $("#list li").each(function() {
      var listText = $(this).text().trim();
        listArray.push(listText);
    });
    /*
    $('#search-text').autocomplete({
      source: listArray
    });  
    */
  }
                                   
  searchList();
                  
});