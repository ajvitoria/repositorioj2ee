<%--
    Document   : newjsp
    Created on : 11-mar-2020, 10:36:09
    Author     : MAJIVIAL
--%>


<%@page import="es.icac.meh.pandora.vista.Componentes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.io.File"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Stage Área</title>
        <!--script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <!--script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script-->
        <!--script src="https://code.jquery.com/jquery-1.11.1.min.js"></script-->


                <script
			  src="https://code.jquery.com/jquery-3.4.1.js"
			  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
			  crossorigin="anonymous"></script>
        <script
			  src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
			  integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
			  crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="css/estiloCheck.css">
        <link rel="stylesheet" type="text/css" href="css/listado.css">

        <style>

            .ui-menu .ui-menu-item a{
    color: #96f226;
    border-radius: 0px;
    border: 1px solid #454545;
}
.ui-menu-item .ui-menu-item-wrapper.ui-state-active {
    background: #6693bc;
    font-weight: bold;
    color: #ffffff;
}
            .botonCerrar{
                background-color:#A8352F;
                -moz-border-radius:4px;
                -webkit-border-radius:4px;
                border-radius:4px;display:inline-block;
                color:#fff;
                font-family:arial;font-size:13px;font-weight:normal;
                padding:4px 15px;
                cursor:pointer;
                vertical-align:top;
                text-align: center;
                width: 130px;
            }
            .botonTraspasar{
                background-color:#00A800;
                -moz-border-radius:4px;
                -webkit-border-radius:4px;
                border-radius:4px;display:inline-block;
                color:#fff;
                font-family:arial;font-size:13px;font-weight:normal;
                padding:4px 15px;
                cursor:pointer;
                vertical-align:top;
                text-align: center;
                width: 200px;
            }

                <%= Componentes.estiloBotonSubmit %>
                <%= Componentes.estiloBotonReset %>

        </style>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/addContext.jspf" %>


        <h1>Ficheros en Área de Almacenamiento.</h1>
        
        

        <!--
        <input type="text" id="search-text" class="search-box">
        <span class="list-count">sss</span>
        -->
        <form  action="<%=request.getContextPath()+"/TraspasarFicheros"%>"  method="POST">
        <ul id="listaFicheros">
            <%
                String RUTA_SUBIDAS = contextConfig.getStageDirectory();
                File directorio = new File(RUTA_SUBIDAS);
                File[] listaFicheros = directorio.listFiles();
                if (directorio == null || !directorio.exists()) {
                    response.sendError(403, "No hay acceso al directorio de archivos");
                } else {
                    out.write("<h2>"+directorio+"</h2>");
                }
            %>
            
                    <fieldset>
            
            <legend>Ficheros subidos</legend>
            <%
                for (int i = 0; i < listaFicheros.length; i++) {
            %>
            <li>
                <label class="toggle">
                    <input id="caja" name="respuesta[]" class="toggle__input" type="checkbox" checked value="<%=listaFicheros[i].getAbsolutePath()%>">
                    <span class="toggle__label">
                        <span class="toggle__text">
                            <%=listaFicheros[i].getAbsolutePath()%>
                        </span>
                    </span>
                </label>
            </li>
            <%
                }
            %>



            <span class="empty-item">no results</span>
<!--
            <div id="botonCerrar" class="botonCerrar">Cerrar</div>
            <div id="botonTraspasar" class="botonTraspasar">Traspasar</div>
            <!--script src="./js/gridFilter.js"></script-->


            <script>
                $(document).ready(function () {
                    ////////////////////////////////////////////////////////////
                     var jobCount = $('#listaFicheros .in').length;
                    $('.list-count').text(jobCount + ' ficheros');
                    $("#search-text").keyup(function () {
                        var searchTerm = $("#search-text").val();
                        var listItem = $('#listaFicheros').children('li');
                        var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
                        $.extend($.expr[':'], {
                            'containsi': function(elem, i, match, array) {
                                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                            }
                        });
                        $("#listaFicheros li").not(":containsi('" + searchSplit + "')").each(function(e){
                            $(this).addClass('hiding out').removeClass('in');
                            setTimeout(function() {
                                $('#listaFicheros .out').addClass('hidden');
                            }, 300);
                        });
                        $("#listaFicheros li:containsi('" + searchSplit + "')").each(function(e) {
                            $(this).removeClass('hidden out').addClass('in');
                            setTimeout(function() {
                                $('#listaFicheros .in').removeClass('hiding');
                            }, 1);
                        });
                        var jobCount = $('#listaFicheros .in').length;
                        $('.list-count').text(jobCount + ' ficheros');

                        if(jobCount == '0') {

                            $('#listaFicheros').addClass('empty');
                        }
                        else {
                            $('#listaFicheros').removeClass('empty');
                        }
                    });

                    function searchList() {
                        // $("#search-text").css('background-color', 'yellow');
                        var listArray = [];
                        $("#listaFicheros li").each(function() {
                            var listText = $(this).text().trim();
                            listArray.push(listText);
                        });

                        $('#search-text').autocomplete({
                            source: listArray
                        });

                    }
                   // searchList();



////////////// CREO QUE NO SE USA PARA NADA  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     $("#botonTraspasar").click(function (){
//                            alert("a ver ");
//                            var listData=[];
//                            $("input[type=checkbox]").each(function () {
//                                                      console.log("JSON.stringify(new Date(2006, 0, 2, 15, 4, 5))");
//                                                      if (this.checked) {
//                                                          var objeto = {ruta: this.value};
//                                                          console.log(this);
//                                                          listData.push(this.value);
//                                                      }
//                            });
//                            console.log("a ver que manda");
//
//                            $.post('/ProyectoPANDORA/TraspasarFicherosServlet', {
//				frutas : listData
//			}, function(responseText) {
//                            $('#frutas').html(responseText);
//				alert("hola");
//			});
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                         /*
                            $.ajax({
                                type: 'post',
                                url: 'http://localhost:8080/ProyectoPANDORA/TraspasarFicheros',
                                data: { frutas:listData },
                                dataType:"json",
                                success: function(datos){
                                    alert("datos enviados"+datos);
                                    console.log(datos);
                                },
                                error: function(ex){
                                    alert(ex);
                                }
                            });
                            */
                            console.log(listData);
                            console.log("se fue a");
                        });

                    $("#botonCancelar").click(function () {
                        window.location.replace("/ProyectoPANDORA/subidaFicheros.jsp");
                    });
                });

                         function serialize(arr){
                        var res = 'a:'+arr.length+':{';
                        for(i=0; i<arr.length; i++){
                            res += 'i:'+i+';s:'+arr[i].length+':"'+arr[i]+'";';
                        }
                        res += '}';

                        this.document.forms[0].respuesta.value = res;
                    }


            </script>
        </ul>
            
        </fieldset>
        
               &nbsp;&nbsp;
    <input class="botonTraspasar" type="submit" value="Incorporar datos de cobros" />
    
    <input class="botonCerrar" type="reset" value="Cancelar" onclick="location.href='/ProyectoPANDORA/index.html';" />
  &nbsp;&nbsp;
</form>



    </body>
</html>
