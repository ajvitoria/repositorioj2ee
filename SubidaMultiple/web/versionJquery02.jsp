<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Upload page</title>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript" language="javascript"></script>

<style>
    body
{
	margin:0px;
	font-size:14px;
	font-family:Helvetica,arial,sans-serif;
}

#progressBar
{
	width:0px;
	height:5px;
	background-color:#F44336;
	position:fixed;
	top:0px;
	left:0px;
	display:none;
}

#progressBar.active
{
	display:block;
	transition: 3s linear width;
	-webkit-transition: 3s linear width;
	-moz-transition: 3s linear width;
	-o-transition: 3s linear width;
	-ms-transition: 3s linear width;
}

form
{
	width:300px;
	padding:20px;
	margin:100px auto 0 auto;
	border:1px solid #cf2c20;
}

#label
{
	font-size:20px;
	font-weight:bold;
	margin-bottom:20px;
}

input[type="submit"]
{
	display:block;
	width:100%;
	margin-top:20px;
	border-width:0px;
	padding:10px;
	color:#fff;
	background-color:#cf2c20;
	outline:none;
	cursor:pointer;
}
</style>
</head>
<body>
    
            <div id="upload-media-modal" class="modal" tabindex="-1" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col p-3">
                                <h3 class="mt-2 mb-3">Upload Files</h3>

                                <form id="upload-media-form" name="upload-media-form" enctype="multipart/form-data">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="upload-media-file" name="bluditInputFiles[]" />
                                        <label class="custom-file-label" for="upload-media-file">Upload</label>
                                    </div>
                                </form>

                                <div class="progress mt-2">
                                    <div id="upload-media-progressbar" class="progress-bar bg-primary" role="progressbar" style="width:0%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    
	<form action="UploadServlet" method="post" enctype="multipart/form-data" name="form1" id="form1">
		<center>

                    
                    		<div id="progressBar"></div>
		
			<div id="label">Select a file to upload</div>
		
                        <input name="file" type="file" id="file" multiple>
			<input type="submit" value="Upload file">
		
                                <script>
 // The Button, which shows the Media Modal
if(d.querySelector("[data-handle='media']")){
    d.querySelector("[data-handle='media']").addEventListener("click", function(event){
        event.preventDefault();
        jQuery("#upload-media-modal").attr("data-target", "#" + this.id);
        jQuery("#upload-media-modal").modal("show");
    });
}

// Handle AJAX Request made by the Media Modal
if(d.querySelector("#upload-media-modal")){
    d.querySelector("#upload-media-modal").addEventListener("change", function(){
        d.querySelector("#upload-media-progressbar").style.width = "1%";

        // AJAX Data
        var formData = new FormData(d.querySelector("#upload-media-form"));

        // The respective page nonce
        formData.append("tokenCSRF", d.querySelector("#nonce").value); 

        // AJAX Request
        jQuery.ajax({
            url: HTML_PATH_ADMIN_ROOT + "ajax/upload-images",
            type: "POST",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            xhr: function(){

                // Animated ProgressBar 
                
                var xhr = jQuery.ajaxSettings.xhr();
                if(!xhr.upload){
                    return xhr;
                }
                xhr.upload.addEventListener("progress", function(event){
                    if(event.lengthComputable){
                        var percent = (event.loaded / event.total) * 100;
                        d.querySelector("#upload-media-progressbar").style.width = percent + "%";
                    }
                }, false);
                return xhr;
            }
        }).done(function(data, status){
            var file = DOMAIN_UPLOADS + data.filename;

            // Handle File Stuff using the filepath
        });
    });
}



                                </script>
                
                
		</center>
	</form>
</body>
</html>