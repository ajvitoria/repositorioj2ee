<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Upload page</title>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript" language="javascript"></script>

<style>
    body
{
	margin:0px;
	font-size:14px;
	font-family:Helvetica,arial,sans-serif;
}

#progressBar
{
	width:0px;
	height:5px;
	background-color:#F44336;
	position:fixed;
	top:0px;
	left:0px;
	display:none;
}

#progressBar.active
{
	display:block;
	transition: 3s linear width;
	-webkit-transition: 3s linear width;
	-moz-transition: 3s linear width;
	-o-transition: 3s linear width;
	-ms-transition: 3s linear width;
}

form
{
	width:300px;
	padding:20px;
	margin:100px auto 0 auto;
	border:1px solid #cf2c20;
}

#label
{
	font-size:20px;
	font-weight:bold;
	margin-bottom:20px;
}

input[type="submit"]
{
	display:block;
	width:100%;
	margin-top:20px;
	border-width:0px;
	padding:10px;
	color:#fff;
	background-color:#cf2c20;
	outline:none;
	cursor:pointer;
}
</style>
</head>
<body>
	<form action="UploadServlet" method="post" enctype="multipart/form-data" name="form1" id="form1">
		<center>

                    
                    		<div id="progressBar"></div>
		
			<div id="label">Select a file to upload</div>
		
                        <input name="file" type="file" id="file" multiple>
			<input type="submit" value="Upload file">
		
                                <script>
                    
 $(function()
{	
	var pbar = $('#progressBar'), currentProgress = 0;
	function trackUploadProgress(e)
	{
		if(e.lengthComputable)
		{
			currentProgress = (e.loaded / e.total) * 100; // Amount uploaded in percent
			$(pbar).width(currentProgress+'%');

			if( currentProgress == 100 )
			console.log('Progress : 100%');
		}
	}

	function uploadFile()
	{
		var formdata = new FormData($('form')[0]);
		$.ajax(
		{
			url:'UploadServlet',
			type:'post',
			data:formdata,
			xhr: function()
			{
				// Custom XMLHttpRequest
				var appXhr = $.ajaxSettings.xhr();

				// Check if upload property exists, if "yes" then upload progress can be tracked otherwise "not"
				if(appXhr.upload)
				{
					// Attach a function to handle the progress of the upload
					appXhr.upload.addEventListener('progress',trackUploadProgress, false);
				}
				return appXhr;
			},
			success:function(){ console.log('File uploaded !'); },
			error:function(){ console.log('Error ssssssss !'+error); },

			// Tell jQuery "Hey! don't worry about content-type and don't process the data"
			// These two settings are essential for the application
			contentType:false,
			processData: false 
		})
	}

	$('form').submit(function(e)
	{
		e.preventDefault();
		$(pbar).width(0).addClass('active');
		uploadFile();
	});
})                   
                                </script>
                
                
		</center>
	</form>
</body>
</html>