package testsqlserver;


import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
      
      

/**
 *
 * @author Alejandro Jiménez Vitoria
 */
public class TestSqlServer {

 
    public static void main(String[] args) {
 
        Connection conn = null;
 	
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException ex) {
            System.err.println("NO TIENE REGISTRADO EL DRIVER");
            ex.printStackTrace();
        }


        try {
 //thspcisv51wvm51
           // String dbURL = "jdbc:sqlserver://172.24.204.40\\DIBAL";
             String dbURL = "jdbc:sqlserver://localhost\\SQLEXPRESS";
            String user = "admin";//icac_mad";
            String pass = "Junio01+";//Ic@c2013";
            conn = DriverManager.getConnection(dbURL, user, pass);
            if (conn != null) {
                DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
                System.out.println("Driver : " + dm.getDriverName());
                System.out.println("Driver version: " + dm.getDriverVersion());
                System.out.println("Product nombre: " + dm.getDatabaseProductName());
                System.out.println("Product version: " + dm.getDatabaseProductVersion());
            }
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (conn != null && !conn.isClosed()) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
