package testsqlserver;


import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
      
      

/**
 *
Para habilitar el protocolo de red TCP/IP:
Inicie el Administrador de configuración de SQL Server. Haga clic en Inicio, seleccione Todos los programas y haga clic en Microsoft SQL Server. Haga clic en Herramientas de configuración y, a continuación, en Administrador de configuración de SQL Server.
En el Administrador de configuración de SQL Server, en el panel de la consola, expanda Configuración de red de SQL Server.
En el panel de la consola, haga clic en Protocolos de<instance_name>.
En el panel de detalles, haga clic con el botón secundario en TCP/IP y, a continuación, haga clic en Habilitar.
En el panel de la consola, haga clic en Servicios de SQL Server.
En el panel de detalles, haga clic con el botón secundario en SQL Server (<instance_name>) y, a continuación, haga clic en Reiniciar para detener y reiniciar el servicio SQL Server.

 */
public class TestSqlServerLocal {

 
    public static void main(String[] args) {
 
        Connection conn = null;
 	
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException ex) {
            System.err.println("NO TIENE REGISTRADO EL DRIVER");
            ex.printStackTrace();
        }


        try {
 //thspcisv51wvm51
 
 //jdbc:sqlserver://localhost\SQLEXPRESS:1433;databaseName=db_nuevoROAC
           // String dbURL = "jdbc:sqlserver://172.24.204.40\\DIBAL";
        //   String dbURL = "jdbc:sqlserver://localhost:1433;databaseName=db_nuevoROAC";
        
        //jdbc:sqlserver://localhost\\NOMBRE_INSTANCIA;databaseName=BASE_DE_DATOS.
        
        String dbURL="jdbc:sqlserver://localhost\\SQLEXPRESS;databaseName=db_nuevoROAC";
         //    String dbURL = "jdbc:sqlserver://localhost\\db_nuevoROAC";
            String user = "admin"; //icac_mad";
            String pass = "Junio01+";//Ic@c2013";
            conn = DriverManager.getConnection(dbURL, user, pass);
            if (conn != null) {
                DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
                System.out.println("Driver : " + dm.getDriverName());
                System.out.println("Driver version: " + dm.getDriverVersion());
                System.out.println("Product nombre: " + dm.getDatabaseProductName());
                System.out.println("Product version: " + dm.getDatabaseProductVersion());
            }
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (conn != null && !conn.isClosed()) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
