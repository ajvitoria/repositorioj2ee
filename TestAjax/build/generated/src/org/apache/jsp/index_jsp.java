package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("\r\n");
      out.write("<title>AJAX JSP Servelts</title>\r\n");
      out.write("<script src=\"http://code.jquery.com/jquery-latest.js\">\r\n");
      out.write("\r\n");
      out.write("</script>\r\n");
      out.write("<script>\r\n");
      out.write("\t$(document).ready(function() {\r\n");
      out.write("\t\t$('#submit').click(function(event) {\r\n");
      out.write("\t\t\tvar nombreVar = $('#nombre').val();\r\n");
      out.write("\t\t\tvar apellidoVar = $('#apellido').val();\r\n");
      out.write("\t\t\tvar edadVar = $('#edad').val();\r\n");
      out.write("\t\t\t// Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get\r\n");
      out.write("\t\t\t$.post('ActionServlet', {\r\n");
      out.write("\t\t\t\tnombre : nombreVar,\r\n");
      out.write("\t\t\t\tapellido: apellidoVar,\r\n");
      out.write("\t\t\t\tedad: edadVar\r\n");
      out.write("\t\t\t}, function(responseText) {\r\n");
      out.write("\t\t\t\t$('#tabla').html(responseText);\r\n");
      out.write("\t\t\t});\r\n");
      out.write("\t\t});\r\n");
      out.write("\t});\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\t<h2>Ejemplo de AJAX con JSP y Servelts</h2>\r\n");
      out.write("\t<form id=\"form1\">\r\n");
      out.write("\t\tNombre:<input type=\"text\" id=\"nombre\" /> <br>\r\n");
      out.write("\t\tApellido: <input type=\"text\" id=\"apellido\" /> <br>\r\n");
      out.write("\t\tEdad: <input type=\"text\" id=\"edad\" /> <br>\r\n");
      out.write("\t\t<input type=\"button\" id=\"submit\" value=\"Añadir\" /> \r\n");
      out.write("\t</form>\r\n");
      out.write("\t<br>\r\n");
      out.write("\t<!-- \tEn este div metemos el contenido de la tabla con AJAX -->\r\n");
      out.write("\t<div id=\"tabla\"></div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
